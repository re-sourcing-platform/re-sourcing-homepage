module.exports = {
  purge: ["./src/**/*.js", "./public/**/*.html"],
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  theme: {
    fontFamily: {
      body: ["Calibri", "sans-serif"],
      display: ["Calibri", "sans-serif"],
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    fontSize: {
      xs: "0.75rem",
      sm: "0.875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
    },
    colors: {
      primary1: "#1C5469",
      primary2: "#397992",
      primary3: "#7cb0bd",
      primary4: "#c6dde3",
      primary5: "#eef4f5",
      accent1: "#972e63",
      accent2: "#a7b32a",
      accent3: "#f0a846",
      buttonDark: "#3A85CA",
      buttonLight: "#37C8DC",
      heroGradientLight: "#7CB0BD",
      heroGradientDark: "#247492",
      white: "#fff",
      body: "#FDFDFD",
      twitter: "#5DABD8",
      linkedin: "#3A66AF",
      youtube: "#E62F2F",
    },
    extend: {
      spacing: {
        "100px": "100px",
        80: "20rem",
        128: "32rem",
        "1/2": "50%",
        "1/7": "14.2857143%",
        "2/7": "28.5714286%",
        "3/7": "42.8571429%",
        "4/7": "57.1428571%",
        "5/7": "71.4285714%",
        "6/7": "85.7142857%",
      },
    },
  },
  variants: {},
  plugins: [],
}
