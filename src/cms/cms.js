import CMS from "netlify-cms-app"
import sectorPages from "./pages/sectors"
import generalPages from "./pages/general-pages"
import knowledgeHub from "./pages/knowledge-hub"
import news from "./pages/news"
import events from "./pages/events"
import consortium from "./pages/consortium"
import projectOutputs from "./pages/project-outputs"
import eventHub from "./pages/event-hub"

window.CMS_MANUAL_INIT = true

const cms_branch = window.location.hostname.includes("test") ? "test" : "master"

const config = {
  load_config_file: false,
  local_backend: true,
  backend: {
    name: "git-gateway",
    branch: cms_branch,
    repo: "re-sourcing-platform/re-sourcing-homepage",
    accept_roles: ["admin", "editor"],
    commit_messages: {
      create: "Create {{collection}} “{{slug}}”",
      update: "Update {{collection}} “{{slug}}”",
      delete: "Delete {{collection}} “{{slug}}”",
      uploadMedia: "[skip ci] Upload “{{path}}”",
      deleteMedia: "[skip ci] Delete “{{path}}”",
    },
  },
  media_folder: "static/files",
  public_folder: "/files",
  publish_mode: "editorial_workflow",
  logo_url: "https://re-sourcing.eu/files/re-sourcing_transparent.png",
  slug: { encoding: "ascii" },
  collections: [
    sectorPages,
    generalPages,
    knowledgeHub,
    news,
    events,
    consortium,
    projectOutputs,
    eventHub,
  ],
}

CMS.init({ config })
