const agendaItem = {
  label: "Agenda Item",
  name: "agendaItem",
  widget: "list",
  required: false,
  minimize_collapsed: true,
  summary: "{{fields.title}}",
  fields: [
    { label: "Title", name: "title", widget: "string" },
    {
      label: "Description",
      name: "description",
      widget: "string",
      required: false,
    },
    { label: "Hour", name: "hour", widget: "number", min: 0, max: 23 },
    {
      label: "Minute",
      name: "minute",
      widget: "number",
      min: 0,
      max: 59,
    },
    {
      label: "End Time",
      name: "endTime",
      widget: "string",
      required: false,
      hint: "Use 00:00 format",
    },
    {
      label: "About",
      name: "body",
      widget: "markdown",
      required: false,
    },
  ],
}

export default agendaItem
