const dropdowns = {
  label: "Dropdowns",
  label_singular: "dropdown",
  name: "dropdowns",
  widget: "list",
  minimize_collapsed: true,
  summary: "{{fields.title}}",
  fields: [
    { label: "Title", name: "title", widget: "string" },
    { label: "Text", name: "text", widget: "markdown" },
  ],
}

export default dropdowns
