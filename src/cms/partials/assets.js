import tags from "./tags"

const assets = {
  label: "Assets",
  name: "assets",
  widget: "list",
  required: false,
  minimize_collapsed: true,
  summary: "{{fields.title}}",
  fields: [
    { label: "Title", name: "title", widget: "string" },
    {
      label: "Subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "Image", name: "logoImage", widget: "image" },
    { label: "Outside Link", name: "outsideLink", widget: "string" },
    {
      label: "Excerpt",
      name: "excerpt",
      widget: "text",
      required: false,
    },
    tags,
  ],
}

export default assets
