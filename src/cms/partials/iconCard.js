const iconCard = {
  label: "Cards",
  name: "cards",
  label_singular: "card",
  widget: "list",
  minimize_collapsed: true,
  summary: "{{fields.title}}",
  fields: [
    { label: "Icon", name: "icon", widget: "string" },
    { label: "Title", name: "title", widget: "string" },
    { label: "Body", name: "body", widget: "text" },
  ],
}

export default iconCard
