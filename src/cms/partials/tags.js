const tags = {
  label: "Tags",
  name: "tags",
  widget: "list",
  required: false,
  minimize_collapsed: true,
  summary: "{{fields.title}}",
  fields: [{ label: "Title", name: "title", widget: "string" }],
}

export default tags
