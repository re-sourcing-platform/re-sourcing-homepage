const fileAsset = {
  name: "assets",
  label: "File assets",
  label_singular: "file asset",
  widget: "list",
  minimize_collapsed: true,
  summary: "{{fields.name}},",
  fields: [
    { label: "Name", name: "name", widget: "string" },
    {
      label: "File",
      name: "file",
      widget: "file",
      allow_multiple: false,
    },
  ],
}

export default fileAsset
