import tags from "../../partials/tags"

const news = {
  name: "news",
  label: "News",
  folder: "src/pages/news",
  create: true,
  slug: "{{year}}-{{month}}-{{day}}-{{slug}}",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "news-post",
    },
    { label: "Type", name: "type", widget: "hidden", default: "news" },
    { label: "Title", name: "title", widget: "string" },
    {
      label: "Subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "Publish date", name: "date", widget: "datetime" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    {
      label: "Outside link",
      name: "outsideLink",
      required: false,
      widget: "string",
    },
    { label: "Author", name: "author", widget: "string", required: false },
    { label: "Excerpt", name: "excerpt", widget: "text", required: false },
    { label: "Body", name: "body", widget: "markdown" },
    tags,
  ],
}

export default news
