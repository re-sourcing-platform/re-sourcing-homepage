const page = {
  name: "sectors",
  label: "Sectors",
  label_singular: "Page",
  identifier_field: "subtitle",
  folder: "src/pages/sectors",
  create: true,
  // adding a nested object will show the collection folder structure
  nested: { depth: 100, summary: "{{title}}" },
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "sector-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "select",
      options: ["Renewable energy", "Mobility", "EEE"],
    },
    { label: "Subtitle", name: "subtitle", widget: "string" },
    {
      label: "Sector",
      name: "sector",
      widget: "select",
      options: [
        "/sectors/renewable-energy/",
        "/sectors/mobility/",
        "/sectors/electronics-and-electronic-equipment/",
      ],
    },
    {
      label: "Sort order",
      name: "sort_order",
      widget: "number",
      value_type: "int",
      min: 1,
    },
    { label: "Body", name: "body", widget: "markdown" },
    // # adding a meta object with a path property allows editing the path of entries
    // # moving an existing entry will move the entire sub tree of the entry to the new location
  ],
  meta: { path: { widget: "string", label: "Path", index_file: "index" } },
}

export default page
