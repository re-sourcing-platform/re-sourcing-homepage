import fileAssets from "../../partials/fileAssets"

const events = {
  name: "events",
  label: "Events",
  folder: "src/pages/events",
  media_folder: "",
  public_folder: "",
  create: true,
  add_to_top: true,
  slug: "{{slug}},",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "event-post",
    },
    { label: "Title", name: "title", widget: "string" },
    {
      label: "Subtitle1",
      name: "subtitle1",
      widget: "string",
      required: false,
    },
    {
      label: "Subtitle2",
      name: "subtitle2",
      widget: "string",
      required: false,
    },
    {
      label: "Featured Image",
      name: "featuredImage",
      widget: "image",
      required: false,
    },
    {
      label: "Type",
      name: "type",
      widget: "select",
      options: [
        "Conference",
        "Flagship Lab",
        "Virtual Conference",
        "Virtual Event",
        "Roadmap Workshop",
      ],
    },
    {
      label: "Virtual Event",
      name: "virtualEvent",
      widget: "boolean",
      default: false,
    },
    {
      label: "Location",
      name: "location",
      widget: "string",
      required: false,
    },
    { label: "Starting Time", name: "date_start", widget: "datetime" },
    {
      label: "Ending Time",
      name: "date_end",
      widget: "datetime",
      required: false,
      default: null,
    },
    {
      label: "Show month only",
      name: "month_only",
      widget: "boolean",
      default: false,
    },
    {
      label: "Show countdown timer",
      name: "countdown",
      widget: "boolean",
      default: false,
    },
    {
      label: "Register link",
      name: "registerLink",
      widget: "string",
      required: false,
    },
    fileAssets,
    { label: "Body", name: "body", widget: "markdown" },
    {
      label: "Password",
      name: "password",
      widget: "string",
      required: false,
      hint: "The event page will have a password check if both the Password and Protected Content are filled in.",
    },
    {
      label: "Protected Content",
      name: "protectedContent",
      widget: "markdown",
      required: false,
      hint: "The event page will have a password check if both the Password and Protected Content are filled in.",
    },
  ],
}

export default events
