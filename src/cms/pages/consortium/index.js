const page = {
  name: "consortium",
  label: "Consortium",
  folder: "src/pages/consortium",
  media_folder: "",
  public_folder: "",
  create: true,
  slug: "{{slug}}",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "consortium-post",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      hint: "Shorter version, to be used as the website url e.g. re-sourcing.eu/consortium/MineHutte",
    },
    {
      label: "Legal name",
      name: "legalName",
      widget: "string",
      required: false,
    },
    {
      label: "Sub-institution",
      name: "subInstitution",
      widget: "string",
      required: false,
    },
    { label: "Country", name: "country", widget: "string" },
    { label: "Logo image", name: "logoImage", widget: "image" },
    { label: "Email", name: "email", widget: "string", required: false },
    { label: "Homepage", name: "homepage", widget: "string" },
    { label: "Body", name: "body", widget: "markdown" },
    {
      name: "members",
      label: "Members",
      widget: "list",
      minimize_collapsed: true,
      summary: "{{fields.name}}",
      fields: [{ label: "Name", name: "name", widget: "string" }],
    },
  ],
}

export default page
