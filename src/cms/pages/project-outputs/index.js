import tags from "../../partials/tags"

let assetTypeCommonFields = [
  { label: "Title", name: "title", widget: "string" },
  {
    label: "Subtitle",
    name: "subtitle",
    widget: "string",
    required: false,
  },
  { label: "Excerpt", name: "excerpt", widget: "text", required: false },
  {
    label: "Image",
    name: "image",
    widget: "image",
    required: false,
  },
  { label: "Date", name: "date", widget: "date" },
  { label: "Active", name: "active", widget: "boolean", default: true },
  tags,
]

let assetTypeFileFields = [
  { label: "File", name: "file", widget: "file", allow_multiple: false },
  {
    label: "Type",
    name: "type",
    widget: "select",
    options: ["Project Report", "Briefing Document"],
  },
]

let assetTypeYoutubeFields = [
  {
    label: "Youtube Link",
    name: "youtube_link",
    widget: "string",
    pattern: [
      "((?:https?:)?//)?((?:www|m).)?((?:youtube.com|youtu.be))(/(?:[w-]+?v=|embed/|v/)?)([w-]{10}).\b",
      "Must be youtube link",
    ],
  },
  { label: "Type", name: "type", widget: "hidden", default: "Video" },
]

let assetTypeFile = {
  label: "File",
  name: "file",
  widget: "object",
  summary: "{{fields.title}}",
  fields: [...assetTypeCommonFields, ...assetTypeFileFields],
}

let assetTypeYoutube = {
  label: "Youtube Link",
  name: "youtube_link",
  widget: "object",
  summary: "{{fields.title}}",
  fields: [...assetTypeCommonFields, ...assetTypeYoutubeFields],
}

const page = {
  name: "projectOutputs",
  label: "Project Outputs",
  folder: "src/pages/project-outputs",
  create: false,
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "project-outputs-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "Project outputs",
    },
    {
      label: "Subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    {
      label: "assets",
      name: "assets",
      widget: "list",
      typeKey: "content_type",
      minimize_collapsed: true,
      add_to_top: true,
      summary: "{{types.label}}",
      types: [assetTypeFile, assetTypeYoutube],
    },
  ],
}

export default page
