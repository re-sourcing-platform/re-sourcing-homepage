import dropdowns from "../../partials/dropdowns"

const page = {
  file: "src/pages/consultations.md",
  label: "Consultations",
  name: "consultations",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "content-with-faq",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "Consultations",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    dropdowns,
  ],
}

export default page
