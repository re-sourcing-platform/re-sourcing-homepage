import dropdowns from "../../partials/dropdowns"

const page = {
  file: "src/pages/moving-forward.md",
  label: "Moving Forward",
  name: "moving-forward",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "content-with-faq",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "Moving Forward",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    dropdowns,
  ],
}

export default page
