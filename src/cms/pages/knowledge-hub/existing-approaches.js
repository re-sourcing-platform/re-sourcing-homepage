import assets from "../../partials/assets"

const page = {
  file: "src/pages/existing-approaches.md",
  label: "Existing Approaches",
  name: "existing-approaches",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "assets-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "Existing Approaches",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    assets,
  ],
}

export default page
