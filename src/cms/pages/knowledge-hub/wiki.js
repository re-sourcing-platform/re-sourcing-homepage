import dropdowns from "../../partials/dropdowns"

dropdowns.fields = [
  ...dropdowns.fields,
  {
    label: "Type",
    name: "type",
    widget: "select",
    options: ["Glossary", "Mining", "Manufacturing", "Recycling"],
  },
]

const page = {
  file: "src/pages/wiki.md",
  label: "WIKI",
  name: "rswiki",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "wiki-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "Wiki on RS",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    dropdowns,
  ],
}

export default page
