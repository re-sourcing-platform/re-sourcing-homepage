import assets from "../../partials/assets"

assets.label = "External Links"
assets.label_singular = "External link"

const page = {
  file: "src/pages/external-links.md",
  label: "External Links",
  name: "external-links",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "assets-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
      default: "External Links",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    assets,
  ],
}

export default page
