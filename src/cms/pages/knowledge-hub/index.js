import wiki from "./wiki"
import consultations from "./consultations"
import movingForward from "./moving-forward"
import existingApproaches from "./existing-approaches"
import externalLinks from "./external-links"

const knowledgeHub = {
  name: "knowledgeHub",
  label: "Knowledge Hub",
  files: [
    wiki,
    consultations,
    movingForward,
    existingApproaches,
    externalLinks,
  ],
}

export default knowledgeHub
