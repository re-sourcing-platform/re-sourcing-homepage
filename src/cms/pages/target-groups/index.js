import dropdowns from "../../partials/dropdowns"

const page = {
  name: "targetGroups",
  label: "Target Groups",
  folder: "src/pages/target-groups",
  create: false,
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "target-groups",
    },
    {
      label: "Title",
      name: "title",
      widget: "string",
    },
    dropdowns,
    { label: "Featured image", name: "featuredImage", widget: "image" },
    { label: "Body", name: "body", widget: "markdown" },
  ],
}

export default page
