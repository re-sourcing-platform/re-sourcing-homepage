const page = {
  file: "src/pages/consortium/members/index.md",
  label: "Members",
  name: "members",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "members-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "hidden",
      default: "Members",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    {
      name: "member",
      label: "Partners",
      widget: "list",
      minimize_collapsed: true,
      add_to_top: true,
      summary: "{{fields.name}}",
      fields: [
        { label: "Name", name: "name", widget: "string" },
        { label: "Image", name: "headshotImage", widget: "image" },
        { label: "Institution", name: "institution", widget: "string" },
        {
          label: "Sub-institution",
          name: "subInstitution",
          required: false,
          widget: "string",
        },
        { label: "Position", name: "position", widget: "string" },
        {
          label: "LinkedIn page",
          name: "linkedIn",
          widget: "string",
          required: false,
        },
      ],
    },
    {
      name: "partner",
      label: "Board and Committee Members",
      widget: "list",
      add_to_top: true,
      minimize_collapsed: true,
      summary: "{{fields.name}}",
      fields: [
        { label: "Name", name: "name", widget: "string" },
        { label: "Image", name: "headshotImage", widget: "image" },
        { label: "Institution", name: "institution", widget: "string" },
        {
          label: "Sub-institution",
          name: "subInstitution",
          required: false,
          widget: "string",
        },
        { label: "Position", name: "position", widget: "string" },
        {
          label: "Type",
          name: "type",
          widget: "select",
          options: ["PLATFORM STEERING COMMITTEE", "ADVISORY BOARD"],
          default: "PLATFORM STEERING COMMITTEE",
        },
        {
          label: "LinkedIn page",
          name: "linkedIn",
          widget: "string",
          required: false,
          pattern: [
            "[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)",
            "Please provide a URL link.",
          ],
        },
      ],
    },
  ],
}

export default page
