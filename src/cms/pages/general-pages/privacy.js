const page = {
  file: "src/pages/privacy.md",
  label: "Privacy Policy",
  name: "privacy",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "privacy-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "hidden",
      default: "Privacy Policy",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    { label: "About", name: "body", widget: "markdown" },
  ],
}

export default page
