import home from "./home"
import about from "./about"
import contact from "./contact"
import privacy from "./privacy"
import members from "./members"

const generalPages = {
  name: "generalPages",
  label: "General Pages",
  files: [home, about, contact, privacy, members],
}

export default generalPages
