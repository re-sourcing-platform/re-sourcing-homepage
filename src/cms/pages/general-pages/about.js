import iconCard from "../../partials/iconCard"

const page = {
  file: "src/pages/about.md",
  label: "About",
  name: "about",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "about-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "hidden",
      default: "About",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      required: false,
    },
    {
      label: "Sections",
      name: "sections",
      widget: "list",
      minimize_collapsed: true,
      summary: "{{fields.title}}",
      fields: [{ label: "Title", name: "title", widget: "string" }, iconCard],
    },
  ],
}

export default page
