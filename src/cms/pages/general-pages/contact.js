const page = {
  file: "src/pages/contact.md",
  label: "Contact",
  name: "contact",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "contact-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "hidden",
      default: "Contact",
    },
    {
      label: "Page subtitle",
      name: "subtitle",
      widget: "string",
      default: "Get in touch with us!",
      required: false,
    },
    {
      label: "Address Heading",
      name: "addressHeading",
      widget: "string",
    },
    { label: "Email", name: "email", widget: "string" },
    { label: "Phone", name: "phone", widget: "string" },
    {
      label: "Google Maps latitude",
      name: "positionLat",
      widget: "number",
      default: 48.2134275,
      value_type: "float",
    },
    {
      label: "Google Maps longitude",
      name: "positionLng",
      widget: "number",
      default: 16.4062779,
      value_type: "float",
    },
    {
      label: "Map label",
      name: "mapLabel",
      widget: "string",
      default: "VIENNA UNIVERSITY OF ECONOMICS AND BUSINESS",
    },
    { label: "About", name: "body", widget: "markdown" },
  ],
}

export default page
