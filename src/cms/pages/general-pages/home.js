const page = {
  file: "src/pages/index.md",
  label: "Home",
  name: "home",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "index-page",
    },
    {
      label: "Title",
      name: "title",
      widget: "hidden",
      default: "Home",
    },
    {
      label: "Sections",
      label_singular: "section",
      name: "sections",
      widget: "list",
      create: false,
      minimize_collapsed: true,
      summary: "{{fields.type}}",
      fields: [
        { label: "Title", name: "title", widget: "string" },
        { label: "Text", name: "text", widget: "text" },
        {
          label: "Type",
          name: "type",
          widget: "select",
          options: [
            "hero",
            "video",
            "goals",
            "members",
            "subscribe",
            "roadmap",
          ],
        },
      ],
    },
    {
      label: "Benefit cards",
      label_singular: "benefit card",
      name: "cards",
      widget: "list",
      minimize_collapsed: true,
      summary: "{{fields.title}}",
      fields: [
        { label: "Title", name: "title", widget: "string" },
        { label: "Icon", name: "icon", widget: "string" },
        { label: "Body", name: "body", widget: "text" },
      ],
    },
    {
      label: "Newsletter subscribe link",
      name: "subscribeLink",
      widget: "string",
    },
  ],
}

export default page
