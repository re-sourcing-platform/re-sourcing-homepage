import agendaItem from "../../partials/agendaItem"

const page = {
  name: "eventHub",
  label: "Event Hub",
  label_singular: "Event",
  folder: "src/pages/hub",
  create: true,
  preview_path: "hub/{{slug}}",
  fields: [
    {
      label: "Template",
      name: "template",
      widget: "hidden",
      default: "hub-event",
    },
    {
      label: "Slogan",
      name: "slogan",
      widget: "string",
      hint: "Welcome message on top on title",
    },
    { label: "Title", name: "title", widget: "string" },
    { label: "SEO Title", name: "seoTitle", widget: "string" },
    { label: "Event is closed", name: "eventIsClosed", widget: "boolean" },
    { label: "Show Event Page", name: "showEventPage", widget: "boolean" },
    { label: "Show Resources Page", name: "showResourcesPage", widget: "boolean" },
    { label: "Resource Password", name: "resourcePassword", widget: "string" },
    { label: "Dates", name: "dateString", widget: "string" },
    {
      label: "Countdown end / Event start",
      name: "dateStart",
      widget: "datetime",
      hint: "Set past date to not display countdown",
    },
    {
      label: "Register Link",
      name: "registerLink",
      widget: "string",
      required: false,
    },
    {
      label: "Days",
      label_singular: "Day",
      name: "days",
      widget: "list",
      minimize_collapsed: true,
      required: false,
      summary: "{{fields.title}}",
      fields: [
        { label: "Title", name: "title", widget: "string" },
        {
          label: "Thematic focus",
          name: "theme",
          widget: "string",
          required: false,
        },
        {
          label: "Theme description",
          name: "description",
          widget: "string",
          required: false,
        },
        {
          label: "Zoom URL",
          name: "zoomUrl",
          widget: "string",
          required: false
        },
        agendaItem,
        {
          label: "Resource",
          name: "resourceFile",
          widget: "list",
          required: false,
          minimize_collapsed: true,
          summary: "{{fields.title}}",
          fields: [
            { label: "Title", name: "title", widget: "string" },
            {
              label: "Subtitle",
              name: "subtitle",
              widget: "string",
              required: false,
            },
            {
              label: "Icon",
              name: "icon",
              widget: "select",
              options: ["document", "link", "video"],
              default: "document",
            },
            {
              label: "Excerpt",
              name: "excerpt",
              widget: "text",
              required: false,
            },
            {
              label: "File",
              name: "file",
              widget: "file",
              allow_multiple: false,
              required: false,
            },
            {
              label: "Link",
              name: "link",
              widget: "text",
              required: false,
            },
          ],
        },
        { label: "About", name: "body", widget: "markdown", required: false },
      ],
    },
    { label: "About", name: "body", widget: "markdown" },
    {
      label: "Agenda Message",
      name: "agendaMessage",
      widget: "markdown",
      required: false,
      hint: "Info on the bottom of agenda. If empty, will display 'More information will follow soon.'",
    },
    {
      label: "Youtube Video (Stream) Id",
      name: "ytVideoId",
      widget: "string",
      required: false,
      hint: "Combination of letters and numbers from the end of the youtube link. In case of https://www.youtube.com/watch?v=5ohfGOenwj8, insert 5ohfGOenwj8.",
    },
    { label: "Featured image", name: "featuredImage", widget: "image" },
    {
      label: "Password Check Intro text 1",
      name: "passwordCheckIntroText1",
      widget: "markdown",
      required: false
    },
    {
      label: "Password Check Intro text 2",
      name: "passwordCheckIntroText2",
      widget: "markdown",
      required: false
    }
  ],
}

export default page
