import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"
import DetailsCard from "../components/layout/cards/DetailsCard"
//import useSiteMetadata from "../components/SiteMetadata"
import ContentBox from "../components/layout/ContentBox"
import {
  getAssetTagList,
  filterAssets,
} from "../components/utility/tagFilterMethods"
import TagBar from "../components/layout/TagBar"
//import { useWithoutTrailingSlash } from "../components/utility/hooks/useTrailingSlash"

const ProjectOutputsPage = ({ data, location }) => {
  const { title, subtitle, featuredImage, assets } =
    data.markdownRemark.frontmatter
  /*let { siteUrl } = useSiteMetadata()
  siteUrl = useWithoutTrailingSlash(siteUrl)*/

  // TODO: set from querystring
  const [filteredAssets, setFilteredAssets] = useState(assets) // TODO: set from querystring

  const tagList = getAssetTagList(assets)

  return (
    <Layout
      pageName={title}
      location={location}
      metaDescription={data.markdownRemark.html.replace(/<[^>]+>/gm, "")}
    >
      <PageHeader title={title} subtitle={subtitle} />
      <Container className="px-4">
        <ContentBox
          html={data.markdownRemark.html}
          imageData={featuredImage.childImageSharp.gatsbyImageData}
        />
        <TagBar
          navOptions={tagList}
          className="mt-12 mb-4"
          onChange={e => setFilteredAssets(filterAssets(assets, tagList, e))}
        />
        <div className="grid grid-cols-1 gap-4 mt-8 lg:grid-cols-2 md:gap-8">
          {filteredAssets.map((asset, i) => {
            const {
              title,
              subtitle,
              file,
              excerpt,
              date,
              active,
              type,
              content_type,
              youtube_link,
              name,
            } = asset

            if (active) {
              return (
                <div key={i} className="col-span-1">
                  <DetailsCard
                    name={name}
                    title={title}
                    subtitle={subtitle}
                    type={type}
                    excerpt={excerpt}
                    date={date}
                    socialShareLink={
                      content_type === "youtube_link"
                        ? youtube_link
                        : "https://re-sourcing.eu/reports/" +
                          string_to_slug(file?.name)
                    }
                    outsideLink={
                      content_type === "youtube_link"
                        ? youtube_link
                        : "https://re-sourcing.eu/reports/" +
                          string_to_slug(file?.name)
                    }
                    className="shadow-component"
                    trackEventName={
                      process.env.NODE_ENV === "development"
                        ? false
                        : "Resource download"
                    }
                  />
                </div>
              )
            } else {
              return ""
            }
          })}
        </div>
      </Container>
    </Layout>
  )
}

export default ProjectOutputsPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
        featuredImage {
          childImageSharp {
            gatsbyImageData(width: 800, placeholder: BLURRED)
          }
        }
        assets {
          date(formatString: "MMMM YYYY")
          active
          content_type
          type
          file {
            name
            publicURL
          }
          youtube_link
          title
          subtitle
          excerpt
          logoImage {
            childImageSharp {
              gatsbyImageData(width: 215)
            }
          }
          tags {
            title
          }
        }
      }
    }
    defaultImage: file(relativePath: { eq: "re-sourcing_transparent.png" }) {
      childImageSharp {
        gatsbyImageData(width: 215, placeholder: BLURRED)
      }
    }
  }
`

function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, "") // trim
  str = str.toLowerCase()

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
  var to = "aaaaeeeeiiiioooouuuunc------"
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i))
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes

  return str
}
