import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import HtmlProvider from "../components/utility/HtmlProvider"
import PageHeader from "../components/layout/headers/PageHeader"
import ContentBox from "../components/layout/ContentBox"
import Accordion from "../components/layout/Accordion"
import Container from "../components/layout/Container"

const ContentFAQPage = ({ data, location }) => {
  const { title, subtitle, dropdowns, featuredImage } =
    data.markdownRemark.frontmatter

  return (
    <Layout pageName={title} location={location}>
      <PageHeader title={title} subtitle={subtitle} />
      {/* Section BG ... */}
      <Container className="px-4">
        {/* CONTENT BOX*/}
        <ContentBox
          className={`${dropdowns?.length > 0 && "lg:mb-16"}`}
          html={data.markdownRemark.html}
          imageData={featuredImage.childImageSharp.gatsbyImageData}
          containImg
        />
        {dropdowns?.length > 0 && (
          <div
            className="
          flex
          flex-col
          gap-4
          mt-8
          lg:mt-0
          lg:gap-8
          "
          >
            {dropdowns.map((dropdown, i) => {
              return (
                <Accordion key={i} text={dropdown.title}>
                  <HtmlProvider
                    className="text-lg body-content"
                    markdown={dropdown.text}
                  />
                </Accordion>
              )
            })}
          </div>
        )}
      </Container>
    </Layout>
  )
}

export default ContentFAQPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
        featuredImage {
          childImageSharp {
            gatsbyImageData(
              width: 800
              layout: CONSTRAINED
              placeholder: BLURRED
            )
          }
        }
        dropdowns {
          text
          title
        }
      }
    }
  }
`
