import React, { useState } from "react"
import { graphql } from "gatsby"
import AssetCard from "../components/layout/cards/AssetCard"
import Container from "../components/layout/Container"
import TagBar from "../components/layout/TagBar"
import {
  getAssetTagList,
  filterAssets,
} from "../components/utility/tagFilterMethods"

const AssetsPage = ({ data }) => {
  const { assets } =
    data.markdownRemark.frontmatter
  // TODO: set from querystring
  const [filteredAssets, setFilteredAssets] = useState(assets) // TODO: set from querystring
  const tagList = getAssetTagList(assets)
  return (

      <Container>
        <div className="px-4 md:px-0 mt-4 mb-8">

          <TagBar
            navOptions={tagList}
            className="mt-12"
            onChange={e => setFilteredAssets(filterAssets(assets, tagList, e))}
          />
          <div className="grid grid-cols-1 gap-4 mt-8 lg:grid-cols-2 lg:gap-8 md:mt-12">
            {filteredAssets?.map((asset, i) => {
              const { title, outsideLink, excerpt, subtitle, logoImage } = asset

              return (
                <div key={i} className="col-span-1">
                  <AssetCard
                    title={title}
                    imageData={logoImage?.childImageSharp?.gatsbyImageData}
                    subtitle={subtitle}
                    excerpt={excerpt}
                    outsideLink={outsideLink}
                    className={`shadow-component`}
                  />
                </div>
              )
            })}
          </div>
        </div>
      </Container>
  )
}

export default AssetsPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        assets {
          title
          subtitle
          excerpt
          outsideLink
          tags {
            title
          }
          logoImage {
            childImageSharp {
              gatsbyImageData(width: 215, placeholder: BLURRED)
            }
          }
        }
      }
    }
  }
`
