import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeader from "../components/layout/headers/PageHeader"
import AssetCard from "../components/layout/cards/AssetCard"
import ContentBox from "../components/layout/ContentBox"
import Container from "../components/layout/Container"
import TagBar from "../components/layout/TagBar"
import {
  getAssetTagList,
  filterAssets,
} from "../components/utility/tagFilterMethods"

const AssetsPage = ({ data, location }) => {
  const { title, subtitle, featuredImage, assets } =
    data.markdownRemark.frontmatter
  // TODO: set from querystring
  const [filteredAssets, setFilteredAssets] = useState(assets) // TODO: set from querystring
  const tagList = getAssetTagList(assets)
  return (
    <Layout
      pageName={title}
      location={location}
      metaDescription={data.markdownRemark.html.replace(/<[^>]+>/gm, "")}
    >
      <PageHeader title={title} subtitle={subtitle} />
      <Container>
        <div className="px-4 md:px-0">
          <ContentBox
            html={data.markdownRemark.html}
            imageData={featuredImage?.childImageSharp?.gatsbyImageData}
          />
          <TagBar
            navOptions={tagList}
            className="mt-12"
            onChange={e => setFilteredAssets(filterAssets(assets, tagList, e))}
          />
          <div className="grid grid-cols-1 gap-4 mt-8 lg:grid-cols-2 lg:gap-8 md:mt-12">
            {filteredAssets?.map((asset, i) => {
              const { title, outsideLink, excerpt, subtitle, logoImage } = asset

              return (
                <div key={i} className="col-span-1">
                  <AssetCard
                    title={title}
                    imageData={logoImage?.childImageSharp?.gatsbyImageData}
                    subtitle={subtitle}
                    excerpt={excerpt}
                    outsideLink={outsideLink}
                    className={`shadow-component`}
                  />
                </div>
              )
            })}
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default AssetsPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
        featuredImage {
          childImageSharp {
            gatsbyImageData(width: 800, placeholder: BLURRED)
          }
        }
        assets {
          title
          subtitle
          excerpt
          outsideLink
          tags {
            title
          }
          logoImage {
            childImageSharp {
              gatsbyImageData(width: 215, placeholder: BLURRED)
            }
          }
        }
      }
    }
  }
`
