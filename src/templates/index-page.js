import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import GradientButton from "../components/utility/GradientButton"
import videoMp4 from "../files/re-sourcing-slogan-video-640x274.mp4"
import videoWebm from "../files/re-sourcing-slogan-video-640x274.webm"
import Card from "../components/layout/cards/Card"
import IconCard from "../components/layout/cards/IconCard"
import SideCardItem from "../components/layout/cards/SideCardItem"
import SideCardSocial from "../components/layout/cards/SideCardSocial"
import PersonCard from "../components/layout/cards/PersonCard"
import Roadmap from "../components/layout/roadmap/Roadmap"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Banner from "../components/layout/Banner";

export default function Home({ data, location }) {
  const sectionInfo = data.sectionData.frontmatter.sections

  const heroSection = sectionInfo.filter(section => section.type === "hero")[0]

  const videoSection = sectionInfo.filter(
    section => section.type === "video"
  )[0]

  const benefitsSection = sectionInfo.filter(
    section => section.type === "benefits"
  )[0]
  const roadmapSection = sectionInfo.filter(
    section => section.type === "roadmap"
  )[0]

  const membersSection = sectionInfo.filter(
    section => section.type === "members"
  )[0]

  const boxSection = sectionInfo.filter(
    section => section.type === "subscribe"
  )[0]

  const benefits = data.sectionData.frontmatter.cards
  const persons = data.members.edges[0].node.frontmatter.member

  const settings = {
    dots: true,
    infinite: false,
    arrows: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 540,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          className: "-ml-16",
          dots: false,
          centerMode: true,
          arrows: true,
          centerPadding: "120px",
        },
      },
      {
        breakpoint: 440,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          className: "-ml-16",
          dots: false,
          centerMode: true,
          centerPadding: "90px",
          arrows: true,
        },
      },
    ],
  }

  return (
    <Layout pageName="Home" location={location}>
      {/*<Banner*/}
      {/*  className={"-mb-32 "}*/}
      {/*  textTop={"The RE-SOURCING Virtual Conference 2022 -<br>is now open!"}*/}
      {/*  primaryButtonText={"Join us!"}*/}
      {/*  primaryButtonLink={"/hub/3rd-virtual-conference/about"}*/}
      {/*  bgColor={"bg-accent1"}*/}
      {/*/>*/}
      <div className="container mx-auto grid grid-cols-1 gap-8 px-4 pt-6 lg:grid-cols-2 mt-32 lg:pt-12">
        <div>
          <h1 className="index-title text-primary1">{heroSection.title}</h1>
          <h3 className="mb-6 text-primary3 index-text">
            {heroSection.text.split("-- ").map((p, i) => (
              <p key={i} className={i !== 0 ? "mt-4" : ""}>
                {p}
              </p>
            ))}
          </h3>
          <div className="hidden lg:flex gap-4">
            <GradientButton className="rounded-md shadow-md" to="/about/">
              Read more
            </GradientButton>
            <a
              className="rounded-md shadow-md group py-2 px-6 border-2 border-accent2 text-accent2 hover:bg-accent2 hover:text-white uppercase"
              href={data?.sectionData?.frontmatter?.subscribeLink}
              target="_blank"
              rel="noreferrer noopener"
            >
              Subscribe to our network
            </a>
          </div>
        </div>
        <div className="w-full lg:flex relative items-center bg-body">
          {/* eslint-disable-next-line*/}
          <video
            className="w-full h-auto shadow-lg"
            preload="auto"
            autoPlay
            muted
          >
            <source src={videoMp4} type="video/mp4" />
            <source src={videoWebm} type="video/webm" />
          </video>
          <div className="mt-8 w-full flex flex-col gap-4 justify-center items-center lg:hidden">
            <GradientButton
              className="rounded-md w-full md:w-3/5 shadow-md"
              to="/about/"
              lg
            >
              Read more
            </GradientButton>
            <a
              className="rounded-md shadow-md group text-lg py-4 text-center border-2 border-accent2 text-accent2 hover:bg-accent2 hover:text-white uppercase w-full md:w-3/5"
              href={data?.sectionData?.frontmatter?.subscribeLink}
              target="_blank"
              rel="noreferrer noopener"
            >
              Subscribe to our network
            </a>
          </div>
        </div>
      </div>
      {/* Hero section END*/}

      {/* Situation section */}
      <div className="relative">
        {/* Section BG */}
        <div className="mt-24 lg:mt-40 absolute w-full h-128 bg-primary5 z-0"></div>
        {/* Section content */}
        <div
          className="container mx-auto pt-40 z-10 relative grid grid-cols-1 px-4 gap-4 
        lg:pt-64 md:gap-16 lg:grid-cols-3"
        >
          <div className="lg:col-span-2 flex flex-col justify-between">
            <div className="">
              <h1 className="index-title pb-4 text-primary1">
                {videoSection.title}
              </h1>
              <h3 className="mb-6 text-primary2 index-text">
                {videoSection.text.split("-- ").map((p, i) => (
                  <p key={i} className={i !== 0 ? "mt-4" : ""}>
                    {p}
                  </p>
                ))}
              </h3>
            </div>
            <div
              className="relative block h-0 p-0 overflow-hidden shadow-component"
              style={{ paddingTop: "56.25%" }}
            >
              <iframe
                title="image-video"
                className="absolute top-0 left-0 bottom-0 w-full h-full"
                src="https://www.youtube-nocookie.com/embed/_Vyx1wkvTgE"
                srcDoc={`
                <style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style>
                <a href=https://www.youtube-nocookie.com/embed/_Vyx1wkvTgE?autoplay=1><img src=https://i3.ytimg.com/vi/_Vyx1wkvTgE/hqdefault.jpg alt='Video RE-SOURCING Imagevideo'><span>▶</span></a>
                `}
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                loading="lazy"
              ></iframe>
            </div>
          </div>
          <div className="col-span-1 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-1 gap-4 mx-auto">
            <div className="col-span-1" style={{ maxWidth: "440px" }}>
              <Card footer title="Latest News" link="/news" className="gap-3">
                {data.latestNews.nodes.map((item, i) => (
                  <SideCardItem
                    key={i}
                    imageData={
                      item.frontmatter.newsImageData?.childImageSharp
                        ?.gatsbyImageData
                    }
                    date={item.frontmatter.date}
                    author={item.frontmatter.type}
                    title={item.frontmatter.title}
                    outsideLink={item.frontmatter.outsideLink}
                    slug={item.fields.slug}
                  />
                ))}
              </Card>
            </div>

            <div className="col-span-1" style={{ maxWidth: "440px" }}>
              <Card footer title="Upcoming Events" link="/events">
                {data.upcomingEvents.nodes.map((item, i) => {
                  let type = "Physical Event"
                  if (item.frontmatter.virtualEvent) type = "Virtual Event"
                  return (
                    <SideCardItem
                      key={i}
                      imageData={
                        item.frontmatter.eventImageData?.childImageSharp
                          ?.gatsbyImageData
                      }
                      type={type}
                      date={
                        !item?.frontmatter?.month_only
                          ? item.frontmatter.date_start
                          : item.frontmatter.monthStart
                      }
                      title={item.frontmatter.title}
                      slug={item.fields.slug}
                      author={item.frontmatter.type}
                    />
                  )
                })}
              </Card>
            </div>
            <div className="col-span-1" style={{ maxWidth: "440px" }}>
              <Card className="" title="Social">
                <SideCardSocial social="twitter" />
                <SideCardSocial social="linkedin" />
                <SideCardSocial social="youtube" round />
              </Card>
            </div>
          </div>
        </div>
      </div>
      {/* Situation section END */}

      {/* Roadmap section START */}
      {roadmapSection && (
        <div className="container mx-auto mt-32">
          <div className="text-center w-3/4 xl:w-2/3 mx-auto pt-8">
            <h1 className="index-title pb-4 text-primary1">
              {roadmapSection?.title}
            </h1>
            {roadmapSection?.text && (
              <h3 className="md:mb-6 text-primary2 index-text">
                {roadmapSection?.text}
              </h3>
            )}
          </div>
          <Roadmap size={2000} {...{ location }} />
        </div>
      )}
      {/* Roadmap section END */}

      {/* Benefits section START*/}
      {benefitsSection !== null && (
        <div className="relative">
          {/* Section BG ... */}
          <div className="mt-24 absolute w-full h-80 bg-primary5 z-0"></div>
          {/* Section content ... */}
          <div className="container mx-auto pt-24 lg:pt-32 z-10 relative">
            <div className="text-center w-1/2 mx-auto pb-4 pt-8">
              <h1 className="index-title pb-4 text-primary1">
                {benefitsSection.title}
              </h1>
              {benefitsSection.text && (
                <h3 className="md:mb-6 text-primary2 index-text">
                  {benefitsSection.text}
                </h3>
              )}
            </div>

            <div className="flex gap-4 flex-col sm:flex-row">
              {benefits.map((benefit, i) => {
                return (
                  <IconCard
                    className="flex-1 sm:my-2 px-2 w-full sm:max-w-1/3"
                    icon={benefit.icon}
                    key={i}
                    title={benefit.title}
                  >
                    <div className="flex flex-col gap-4">
                      {benefit.body.split("-- ").map((bullet, i) => {
                        if (i === 0) return ""
                        return (
                          <div
                            key={i}
                            className="leading-4 relative text-left pl-2 ml-3"
                          >
                            {bullet}

                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-4 w-4  lg:h-5 md:w-5 absolute top-0 -left-4"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"
                              />
                            </svg>
                          </div>
                        )
                      })}
                    </div>
                  </IconCard>
                )
              })}
            </div>
          </div>
        </div>
      )}
      {/* Benefits section END */}

      {/* Team section */}
      <div className="relative">
        {/* Section content ... */}
        <div className="container mx-auto pt-24 z-10 relative">
          <div className="text-center w-3/4 md:w-1/2 mx-auto pt-8">
            <h1 className="index-title pb-4 text-primary1">
              {membersSection.title}
            </h1>
          </div>
          <div className="text-center w-5/6 mx-auto pb-12 pt-6">
            <h3 className="text-primary3 index-text">{membersSection.text}</h3>
          </div>
          {/* Gallery */}
          <div className="pb-5 overflow-hidden">
            <Slider {...settings}>
              {persons.map((person, i) => (
                <PersonCard
                  src={person.personImageData.relativePath}
                  imageData={
                    person.personImageData.childImageSharp.gatsbyImageData
                  }
                  name={person.name}
                  linkedIn={person.linkedIn}
                  className="w-56 mx-auto flex-shrink-0 mb-12"
                  key={i}
                >
                  <div
                    className="text-primary3 text-center text-sm"
                    style={{ height: "180px" }}
                  >
                    <p>{person.institution}</p>
                    {person.subInstitution && (
                      <>
                        <p>•</p>
                        <p>{person.subInstitution}</p>
                      </>
                    )}
                    {person.position && (
                      <>
                        <p>•</p>
                        <p>{person.position}</p>
                      </>
                    )}
                  </div>
                </PersonCard>
              ))}
            </Slider>
          </div>
        </div>
      </div>
      {/* Team section END */}

      {/* Subscription section START*/}
      <div className="relative">
        <div
          className="-mt-128 sm:-mt-64 md:-mt-56 absolute w-full bg-primary5 z-0"
          style={{ height: "1000px" }}
        ></div>
      </div>
      <div className="relative z-20 ">
        {/* Section BG ... */}

        {/* Section content ... */}
        <div className="md:container mx-auto pt-16 sm:pt-32 lg:pt-40 z-10 relative px-4 md:px-0">
          <div className="card-bg text-center mx-auto p-4 md:p-12 md:w-5/6 shadow-component">
            <h1 className="index-title pb-8 pt-2 text-primary1">
              {boxSection.title}
            </h1>
            <h3 className="mb-4 md:mb-16 text-primary2 index-text">
              {boxSection.text}
            </h3>
            <GradientButton
              className="text-xl px-16 py-3 rounded-md"
              href={data?.sectionData?.frontmatter?.subscribeLink}
            >
              Subscribe
            </GradientButton>
          </div>
        </div>
      </div>
      {/* Subscription section END */}
    </Layout>
  )
}

export const indexQuery = graphql`
  query indexPage($currentDate: Date!) {
    sectionData: markdownRemark(frontmatter: { title: { eq: "Home" } }) {
      frontmatter {
        sections {
          title
          text
          type
        }
        cards {
          body
          icon
          title
        }
        subscribeLink
      }
    }

    members: allMarkdownRemark(
      filter: { frontmatter: { title: { eq: "Members" } } }
    ) {
      edges {
        node {
          frontmatter {
            member {
              institution
              name
              position
              subInstitution
              linkedIn
              personImageData: headshotImage {
                relativePath
                childImageSharp {
                  gatsbyImageData(
                    layout: CONSTRAINED
                    width: 228
                    placeholder: BLURRED
                  )
                }
              }
            }
          }
        }
      }
    }

    latestNews: allMarkdownRemark(
      filter: { frontmatter: { template: { eq: "news-post" } } }
      sort: { fields: frontmatter___date, order: DESC }
      limit: 2
    ) {
      nodes {
        frontmatter {
          title
          author
          type
          date(formatString: "DD MMM YYYY")
          outsideLink
          newsImageData: featuredImage {
            childImageSharp {
              gatsbyImageData(
                layout: CONSTRAINED
                width: 250
                placeholder: BLURRED
              )
            }
          }
        }
        fields {
          slug
        }
      }
    }

    upcomingEvents: allMarkdownRemark(
      filter: {
        frontmatter: {
          template: { eq: "event-post" }
          date_start: { gte: $currentDate }
        }
      }
      sort: { fields: frontmatter___date_start, order: ASC }
      limit: 2
    ) {
      nodes {
        frontmatter {
          title
          virtualEvent
          type
          date_start(formatString: "DD MMM YYYY")
          date_end(formatString: "DD MMM YYYY")
          monthStart: date_start(formatString: "MMM YYYY")
          month_only
          eventImageData: featuredImage {
            childImageSharp {
              gatsbyImageData(
                layout: CONSTRAINED
                width: 250
                placeholder: BLURRED
              )
            }
          }
        }
        fields {
          slug
        }
      }
    }
  }
`
