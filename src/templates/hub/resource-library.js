import React, { useEffect, useState } from "react"
import useSiteMetadata from "../../components/SiteMetadata"
import Head from "../../components/utility/Head"
import PageHeaderHub from "../../components/layout/headers/PageHeaderHub"
import EventHubFooter from "../../components/layout/EventHubFooter"
import Accordion from "../../components/layout/Accordion"
import LinkProvider from "../../components/utility/LinkProvider"
import ScrollToTop from "../../components/layout/ScrollToTop"
import PasswordCheck from "../../components/layout/forms/PasswordCheck"
import {graphql, Link, navigate} from "gatsby"
import EventHubNavigation from "../../components/layout/EventHubNavigation";

const EventResourcePage = ({ location, data }) => {
  const [hidden, setHidden] = useState(true)
  const storageKey = `isPassed-${data.markdownRemark.id}`
  useEffect(() => {
    if (sessionStorage.getItem(storageKey)) {
      setHidden(false)
    }
  }, [ storageKey ])

  const handlePlausible = () => {
    typeof window !== "undefined" && window.plausible(`Password Sign In`)
  }

  const handleChange = () => {
      process.env.NODE_ENV !== "development" && handlePlausible()
      return setHidden(false)
  }

  const { title, description, siteUrl } = useSiteMetadata()
  const link = `${siteUrl}${location.pathname}`

  let { frontmatter, fields } = data.markdownRemark
  const { days, dateStart, seoTitle } = frontmatter
  const dateObj = dateStart !== null ? new Date(dateStart) : false;

    if (frontmatter.eventIsClosed) {
        navigate(`${fields.slug}about/`);
        return <><p>Redirecting...</p></>
    }

  const PasswordCheckScreen = () => {
    return (
      <div className="bg-white shadow-xl rounded-md overflow-hidden mx-2 md:mx-0 text-xs sm:text-sm w-full max-w-prose md:max-w-3xl p-4 body-content text-center">
        <h4 className="py-3">
          Welcome to the resource library of the RE-SOURCING Virtual Conference 2022!
        </h4>

        <p>
          The library offers you a concise selection of cutting-edge materials for each of the four main conference topics.
          It comprises documents, links and videos to explore and will also include the recordings of the live sessions after each day.
        </p>

        <PasswordCheck password={ frontmatter.resourcePassword || "RE-S_VC21" } storageKey={storageKey} onChange={handleChange}/>

        <p className="mt-4">
          <i>
            *Registered participants receive their password in a separate email together with the Zoom links in the week
            before the conference and again on the first day of the conference. Please also check your spam folder in case
            you have not received any messages or contact <a href="mailto:noe.barriere@wu.ac.at">noe.barriere@wu.ac.at</a>
            and <a href="mailto:andreas.endl@wu.ac.at">andreas.endl@wu.ac.at</a> in case you are facing any issues.
          </i>
        </p>

        <h4 className="my-4">
          Please{" "}
          <a
            href={ frontmatter.registerLink || '#' }
            target="_blank"
            rel="noreferrer"
          >
            register
          </a>{" "}
          to receive your password.
        </h4>

        <h4>
          In additon, visit our <Link to="/wiki">knowledge hub</Link> and{" "}
          <Link to="/project-outputs">project outputs</Link> to learn more about
          Responsible Sourcing!
        </h4>
      </div>
    )
  }

  const LibraryScreen = ({ days }) => {
    const zoomDays = days.filter(( day ) => day.zoomUrl?.trim().length > 0)
    let zoomLinks = '';

    if (zoomDays.length) {
      zoomLinks = (
          <div className="bg-white shadow-xl rounded-md overflow-hidden mb-4 text-xs sm:text-base w-full max-w-prose md:max-w-3xl body-content px-4">
            <p>
              Your Zoom links to the <b>Informal exchange meetings</b> at the end
              of each day (see agenda):
            </p>
            <ul className="ml-4">
              { days.map(( day, i ) => {
                if (day.zoomUrl?.trim().length <= 0) {
                  return <></>
                }

                return (
                    <li>
                      <b>
                        <a
                            href={ day.zoomUrl }
                            target="blank"
                            rel="noopener noreferrer"
                        >
                          Day {i} ({ day.title })
                        </a>
                      </b>
                    </li>
                )
              }) }
            </ul>
            <p>
              Please note that the meeting rooms will open shortly before the
              start of the informal exchange.
            </p>
          </div>
      );
    }

    return (
      <>
        { zoomLinks }

        <div className="bg-white shadow-xl rounded-md overflow-hidden text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
          {days
            ?.filter(day => day.resourceFile !== null)
            ?.map((day, i) => {
              const colors = setColorsbyIndex(i)
              return (
                <div key={i}>
                  <Accordion
                    text={day.title}
                    titleColor="text-white"
                    titleBg={colors.bgColor}
                    clean
                  >
                    <div className="grid grid-cols-1 gap-2 p-4">
                      {day?.resourceFile?.map((asset, i) => {
                        const { title, subtitle, excerpt, icon, link, file } =
                          asset
                        let href = file?.publicURL
                          ? `${location.origin}${file.publicURL}`
                          : link?.trim().length > 0
                          ? link
                          : null
                        return (
                          <FileCard
                            {...{
                              title,
                              subtitle,
                              href,
                              excerpt,
                              icon,
                            }}
                            key={i}
                          />
                        )
                      })}
                      <div className="body-content">
                        {i === 0 || i === 2 ? (
                          <h3 className="text-xl">
                            Please see our{" "}
                            <a
                              href="https://re-sourcing.eu/wiki"
                              target="_blank"
                              rel="noreferrer"
                            >
                              knowledge hub
                            </a>{" "}
                            and{" "}
                            <a
                              href="https://re-sourcing.eu/project-outputs"
                              target="_blank"
                              rel="noreferrer"
                            >
                              project outputs
                            </a>{" "}
                            for more materials!
                          </h3>
                        ) : (
                          <h3 className="text-xl">
                            Please see our website on the{" "}
                            <a
                              href="https://re-sourcing.eu/sectors/renewable-energy/overview/"
                              target="_blank"
                              rel="noreferrer"
                            >
                              Renewable Energy sector
                            </a>{" "}
                            for more materials!
                          </h3>
                        )}
                      </div>
                    </div>
                  </Accordion>
                </div>
              )
            })}
        </div>
      </>
    )
  }

  return (
    <>
      <Head
        pageName={`${seoTitle} | Resource Library`}
        location={location}
        title={title}
        description={description}
        siteUrl={siteUrl}
        link={link}
      />
      <ScrollToTop showBelow={500} />
      <div className="bg-body flex flex-col justify-between min-h-screen w-full overflow-x-hidden">
        <div className="top">
          <PageHeaderHub
            title={frontmatter.slogan}
            subtitle={frontmatter.title}
            date={frontmatter.dateString}
            eventStart={frontmatter.dateStart}
            link={link}
            pathname={location.pathname}
            imageData={
              frontmatter.featuredImage.childImageSharp.gatsbyImageData
            }
            registerLink={
              frontmatter.registerLink ||
              "https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339"
            }
          />
          <section>
            <div className="pb-6 md:container mx-auto relative flex flex-col lg:px-24 z-20 items-center gap-4 px-2 md:px-0">
              {/* NAV */}
              <EventHubNavigation
                  linkPrefix={fields.slug}
                  eventIsClosed={frontmatter.eventIsClosed}
                  showEventPage={frontmatter.showEventPage}
                  showResourcesPage={frontmatter.showResourcesPage}
                  location={location}
              />
              {/* CONTENT */}
              <div className="bg-white rounded-md text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
                {hidden ? (
                  <PasswordCheckScreen />
                ) : (
                  <LibraryScreen days={days} />
                )}
              </div>
            </div>
          </section>
        </div>
        <EventHubFooter />
      </div>
    </>
  )
}

export default EventResourcePage

const FileCard = ({ title, subtitle, href, icon = "document", excerpt }) => {
  let path

  if (icon === "document")
    path =
      "M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"
  if (icon === "link")
    path =
      "M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"
  if (icon === "video")
    path =
      "M7 4v16M17 4v16M3 8h4m10 0h4M3 12h18M3 16h4m10 0h4M4 20h16a1 1 0 001-1V5a1 1 0 00-1-1H4a1 1 0 00-1 1v14a1 1 0 001 1z"
  return (
    <div
      className={`relative w-full border border-primary5 ${
        href && "hover:border-primary3"
      } rounded-md overflow-hidden`}
    >
      <LinkProvider
        href={href}
        rel="noreferrer noopener"
        target="blank"
        className="w-full h-full"
      >
        <div className="h-full flex flex-col md:flex-row p-4 md:p-2 items-center text-left">
          {/* IMAGE */}
          <div className="hidden md:block w-full md:w-1/3 md:h-full pb-2 md:pb-0">
            <div className="w-full h-full flex items-center justify-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-primary1 "
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1.5}
                  d={path}
                />
              </svg>
            </div>
          </div>

          {/* RIGHT */}
          <div className="min-h-full w-full md:w-2/3">
            <h1 className="text-base md:text-xl font-bold text-primary1">
              {title}
            </h1>
            {subtitle && (
              <h2 className="font-bold text-primary3 pt-2">{subtitle}</h2>
            )}
            {excerpt?.trim().length > 0 && (
              <p className="mt-2 md:mt-3 md:pb-2">{excerpt}</p>
            )}
          </div>
        </div>
      </LinkProvider>
    </div>
  )
}

const setColorsbyIndex = i => {
  let bgColor
  let textColor
  let linkColor
  if (i === 0 || i % 3 === 0) {
    bgColor = "bg-accent1"
    textColor = "text-accent1"
    linkColor = "highlight-accent1"
  }
  if (i === 1 || i % 3 === 1) {
    bgColor = "bg-accent2"
    textColor = "text-accent2"
    linkColor = "highlight-accent2"
  }
  if (i === 2 || i % 3 === 2) {
    bgColor = "bg-accent3"
    textColor = "text-accent3"
    linkColor = "highlight-accent3"
  }

  return { bgColor: bgColor, textColor: textColor, linkColor: linkColor }
}

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      id
      frontmatter {
        title
        slogan
        seoTitle
        eventIsClosed
        showEventPage
        showResourcesPage
        resourcePassword
        passwordCheckIntroText1
        passwordCheckIntroText2
        dateString
        dateStart
        registerLink
        featuredImage {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
        }
        days {
          title
          theme
          description
          zoomUrl
          resourceFile {
            title
            subtitle
            icon
            excerpt
            link
            file {
              publicURL
            }
          }
        }
      }
    }
  }
`
