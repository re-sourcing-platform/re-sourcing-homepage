import React from "react"
import useSiteMetadata from "../../components/SiteMetadata"
import Head from "../../components/utility/Head"
import PageHeaderHub from "../../components/layout/headers/PageHeaderHub"
import EventHubFooter from "../../components/layout/EventHubFooter"
import Accordion from "../../components/layout/Accordion"

import HtmlProvider from "../../components/utility/HtmlProvider"
import "../../styles/agenda.css"
import ScrollToTop from "../../components/layout/ScrollToTop"

import {graphql, navigate} from "gatsby"
import EventHubNavigation from "../../components/layout/EventHubNavigation";

const EventAgendaPage = ({ location, data }) => {
  const { title, description, siteUrl } = useSiteMetadata()
  const link = `${siteUrl}${location.pathname}`
  let { frontmatter, fields } = data.markdownRemark
  let { days = [], seoTitle } = frontmatter

    if (frontmatter.eventIsClosed) {
        navigate(`${fields.slug}about/`);
        return <><p>Redirecting...</p></>
    }

  return (
    <>
      <Head
        pageName={`${seoTitle} | Agenda & Sessions`}
        location={location}
        title={title}
        description={description}
        siteUrl={siteUrl}
        link={link}
      />
      <ScrollToTop showBelow={500} />
      <div className="bg-body flex flex-col justify-between min-h-screen w-full overflow-x-hidden">
        <div className="top">
          <PageHeaderHub
            title={frontmatter.slogan}
            subtitle={frontmatter.title}
            eventStart={frontmatter.dateStart}
            date={frontmatter.dateString}
            link={link}
            pathname={location.pathname}
            registerLink={
              frontmatter.registerLink ||
              "https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339"
            }
            imageData={
              frontmatter.featuredImage.childImageSharp.gatsbyImageData
            }
          />

          <section className="bg-body">
            <div className="pb-6 md:container mx-auto relative flex flex-col lg:px-24 z-20 items-center gap-4 px-2">
              {/* NAV */}
              <EventHubNavigation
                  linkPrefix={fields.slug}
                  eventIsClosed={frontmatter.eventIsClosed}
                  showEventPage={frontmatter.showEventPage}
                  showResourcesPage={frontmatter.showResourcesPage}
                  location={location}
              />
              {/* CONTENT */}
              <div className=" bg-white shadow-xl rounded-md overflow-hidden text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
                {days.map((day, i) => {
                  const colors = setColorsbyIndex(i)
                  return (
                    <div key={i}>
                      <Accordion
                        text={day.title}
                        titleColor="text-white"
                        titleBg={colors.bgColor}
                      >
                        <div className="px-4 py-2 w-full body-content">
                          <p
                            className={`text-primary2 sm:text-lg font-bold ${colors.textColor}`}
                          >
                            {day.theme}
                          </p>
                          <span className="text-black text-xs sm:text-sm flex">
                            {day.description ? (
                              <>
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-3 w-3 self-center mr-3"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="M17 8l4 4m0 0l-4 4m4-4H3"
                                  />
                                </svg>
                                <p>{day.description}</p>
                              </>
                            ) : (
                              ""
                            )}
                          </span>
                          {day.body && (
                            <HtmlProvider
                              markdown={day.body}
                              className={`-mt-2 pb-2 ${colors.linkColor}`}
                            />
                          )}
                        </div>
                        <div className="-mt-2 mb-2"></div>
                        {day.agendaItem?.length > 0 &&
                          day.agendaItem.map((item, i) => (
                            <div
                              className={`pr-4 pb-2 flex items-start pt-2`}
                              key={i}
                            >
                              {item.hour !== null && item.minute !== null ? (
                                <div
                                  className={`w-1/3 text-center text-primary1 bg-primary4 bg-opacity-75 text-sm lg:text-base font-bold mx-4`}
                                >
                                  <span>
                                    {" "}
                                    {item.hour < 10
                                      ? `0${item.hour}`
                                      : item.hour}
                                    :
                                    {item.minute < 10
                                      ? `0${item.minute}`
                                      : item.minute}
                                  </span>
                                  {item.endTime?.trim().length > 0 ? (
                                    <span>{` - ${item.endTime}`}</span>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              ) : (
                                ""
                              )}
                              <div
                                className={`${
                                  item.hour !== null && item.minute !== null
                                    ? "w-2/3"
                                    : "w-full"
                                } flex flex-col justify-start`}
                              >
                                <h5
                                  className={`text-primary1 font-bold pb-1 text-sm lg:text-base`}
                                >
                                  {item.title}
                                </h5>
                                {item.description?.length > 0 ? (
                                  <h5 className="text-primary3 pb-1 font-bold">
                                    {item.description}
                                  </h5>
                                ) : (
                                  ""
                                )}
                                {item.body && (
                                  <HtmlProvider
                                    markdown={item.body}
                                    className="body-content agenda-content"
                                  />
                                )}
                              </div>
                            </div>
                          ))}
                      </Accordion>
                    </div>
                  )
                })}
              </div>
              <div className="body-content bg-white shadow-xl rounded-md overflow-hidden px-2 text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
                <div>
                  {frontmatter.agendaMessage?.trim().length > 0 ? (
                    <HtmlProvider
                      className="mb-3 md:mt-3 md:mb-6"
                      markdown={frontmatter.agendaMessage}
                    ></HtmlProvider>
                  ) : (
                    <h4 className="mt-3 mb-6">
                      More information will follow soon.
                    </h4>
                  )}
                </div>
              </div>
            </div>
          </section>
        </div>
        <EventHubFooter />
      </div>
    </>
  )
}

export default EventAgendaPage

const setColorsbyIndex = i => {
  let bgColor
  let textColor
  let linkColor
  if (i === 0 || i % 3 === 0) {
    bgColor = "bg-accent1"
    textColor = "text-accent1"
    linkColor = "highlight-accent1"
  }
  if (i === 1 || i % 3 === 1) {
    bgColor = "bg-accent2"
    textColor = "text-accent2"
    linkColor = "highlight-accent2"
  }
  if (i === 2 || i % 3 === 2) {
    bgColor = "bg-accent3"
    textColor = "text-accent3"
    linkColor = "highlight-accent3"
  }

  return { bgColor: bgColor, textColor: textColor, linkColor: linkColor }
}

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        title
        slogan
        seoTitle
        eventIsClosed
        showEventPage
        showResourcesPage
        dateString
        dateStart
        registerLink
        agendaMessage
        featuredImage {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
          name
        }
        days {
          title
          theme
          description
          body
          agendaItem {
            title
            description
            hour
            minute
            endTime
            body
          }
        }
      }
    }
  }
`
