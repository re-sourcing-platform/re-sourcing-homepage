import React from "react"
import useSiteMetadata from "../../components/SiteMetadata"
import Head from "../../components/utility/Head"
import PageHeaderHub from "../../components/layout/headers/PageHeaderHub"
import EventHubFooter from "../../components/layout/EventHubFooter"
import { graphql } from "gatsby"
import EventHubNavigation from "../../components/layout/EventHubNavigation";

const EventHubAboutPage = ({ location, data }) => {
  const { title, description, siteUrl } = useSiteMetadata()
  const link = `${siteUrl}${location.pathname}`
  let {
    frontmatter,
    html,
    fields
  } = data.markdownRemark

  const seoTitle = frontmatter.seoTitle || "2nd Virtual Conference";

  return (
    <>
      <Head
        pageName={`${seoTitle} | About`}
        location={location}
        title={title}
        description={description}
        siteUrl={siteUrl}
        link={link}
      />
      <div className="flex flex-col justify-between min-h-screen w-full overflow-x-hidden">
        <div className="top">
          <PageHeaderHub
            title={frontmatter.slogan}
            subtitle={frontmatter.title}
            date={frontmatter.dateString}
            eventStart={frontmatter.dateStart}
            link={link}
            pathname={location.pathname}
            registerLink={
              frontmatter.registerLink ||
              "https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339"
            }
            imageData={
              frontmatter.featuredImage.childImageSharp.gatsbyImageData
            }
          />
          <section className="bg-body">
            <div className="pb-6 md:container mx-auto relative flex flex-col lg:px-24 z-20 items-center gap-4">
              {/* NAV */}
              <EventHubNavigation
                  linkPrefix={fields.slug}
                eventIsClosed={frontmatter.eventIsClosed}
                showEventPage={frontmatter.showEventPage}
                showResourcesPage={frontmatter.showResourcesPage}
                  location={location}
              />
              <div className="bg-white shadow-md rounded-md mx-2 md:mx-0 p-4 text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
                <div
                  className="body-content"
                  dangerouslySetInnerHTML={{ __html: html }}
                ></div>
              </div>
            </div>
          </section>
        </div>
        <EventHubFooter />
      </div>
    </>
  )
}

export default EventHubAboutPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        seoTitle
        eventIsClosed
        showEventPage
        showResourcesPage
        slogan
        dateStart
        dateString
        registerLink
        featuredImage {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
        }
      }
    }
  }
`
