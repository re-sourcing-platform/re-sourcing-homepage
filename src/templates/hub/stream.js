import React from "react"
import useSiteMetadata from "../../components/SiteMetadata"
import Head from "../../components/utility/Head"
import PageHeaderHub from "../../components/layout/headers/PageHeaderHub"
import EventHubFooter from "../../components/layout/EventHubFooter"
import {graphql, navigate} from "gatsby"
import EventHubNavigation from "../../components/layout/EventHubNavigation";

const EventHubStreamPage = ({ location, data }) => {
  const { title, description, siteUrl } = useSiteMetadata()
  const link = `${siteUrl}${location.pathname}`
  let { frontmatter, fields } = data.markdownRemark
  const { seoTitle } = frontmatter;

    if (frontmatter.eventIsClosed) {
        navigate(`${fields.slug}about/`);
        return <><p>Redirecting...</p></>
    }

  return (
    <>
      <Head
        pageName={seoTitle}
        location={location}
        title={title}
        description={description}
        siteUrl={siteUrl}
        link={link}
      />
      <div className="flex flex-col justify-between min-h-screen w-full overflow-x-hidden">
        <div className="top">
          <PageHeaderHub
            title={frontmatter.slogan}
            subtitle={frontmatter.title}
            date={frontmatter.dateString}
            eventStart={frontmatter.dateStart}
            link={link}
            pathname={location.pathname}
            registerLink={
              frontmatter.registerLink ||
              "https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339"
            }
            imageData={
              frontmatter.featuredImage.childImageSharp.gatsbyImageData
            }
          />
          <section className="bg-body">
            <div className="pb-6 mx-auto relative flex flex-col lg:px-24 z-20 items-center gap-4 xl:">
              {/* NAV */}
              <EventHubNavigation
                  linkPrefix={fields.slug}
                  eventIsClosed={frontmatter.eventIsClosed}
                  showEventPage={frontmatter.showEventPage}
                  showResourcesPage={frontmatter.showResourcesPage}
                  location={location}
              />
              {/* CONTENT */}

              <div
                className="w-full xl:flex xl:gap-8 "
                style={{ maxWidth: "1400px" }}
              >
                {/* STREAM VIDEO */}
                <div className="bg-white shadow-md rounded-md mx-auto p-4 text-xs sm:text-sm xl:w-5/7 xl:self-start max-w-prose md:max-w-3xl xl:max-w-none mb-8 xl:mb-0">
                  <div
                    className="relative block h-0 p-0 overflow-hidden"
                    style={{ paddingTop: "56.25%" }}
                  >
                    <iframe
                      title="image-video"
                      className="absolute top-0 left-0 bottom-0 w-full h-full"
                      src="https://www.youtube.com/embed/zer2WuMOJG8"
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media;"
                      allowFullScreen
                    ></iframe>
                  </div>
                </div>
                {/* SLIDO */}
                <div className="bg-white shadow-md rounded-md mx-auto p-4 text-xs sm:text-sm xl:w-2/7 scroll max-w-prose md:max-w-3xl xl:max-w-none ">
                  <div className="relative block h-full scroll pb-6/7 xl:pb-0">
                    <iframe
                      title="image-video"
                      className="w-full h-full absolute xl:relative"
                      src="https://app.sli.do/event/zw8ttfya/questions"
                      frameBorder="0"
                      scrolling="yes"
                      allowFullScreen
                      height="100%"
                      width="100%"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <EventHubFooter />
      </div>
    </>
  )
}

export default EventHubStreamPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        slogan
        seoTitle
        eventIsClosed
        showEventPage
        showResourcesPage
        dateStart
        dateString
        registerLink
        featuredImage {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
        }
        days {
          title
          theme
          description
          body
          resourceFile {
            title
            subtitle
            file {
              publicURL
            }
          }
          agendaItem {
            title
            description
            hour
            minute
            endTime
            body
          }
        }
      }
    }
  }
`
