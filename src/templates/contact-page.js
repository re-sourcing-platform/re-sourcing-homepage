import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeader from "../components/layout/headers/PageHeader"
import ContactDetailsBox from "../components/layout/ContactDetailsBox"

const Contact = ({ data, location }) => {
  const {
    title,
    subtitle = {},
    addressHeading,
    email,
    phone,
    positionLat,
    positionLng,
    mapLabel,
  } = data.markdownRemark.frontmatter

  let letterIconPath = (
    <>
      <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
      <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
    </>
  )

  const phoneIconPath = (
    <>
      <path
        fillRule="evenodd"
        d="M7 2a2 2 0 00-2 2v12a2 2 0 002 2h6a2 2 0 002-2V4a2 2 0 00-2-2H7zm3 14a1 1 0 100-2 1 1 0 000 2z"
        clipRule="evenodd"
      ></path>
    </>
  )

  return (
    <Layout pageName={title} location={location}>
      <PageHeader title={title} subtitle={subtitle} />
      {/* CONTENT */}
      <div className="content-container mx-auto relative z-20 -mt-8 px-4">
        <div className="pt-6 pb-6 card-bg shadow-component text-center px-6 md:px-32">
          <h1 className="uppercase text-2xl md:text-4xl font-bold text-primary2">
            {addressHeading}
          </h1>
          <br />
          <h4
            className="text-primary3 md:text-xl md:pb-4"
            dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
          ></h4>
        </div>
        <div className="flex gap-4 md:gap-16 flex-col mt-4 md:mt-16 md:flex-row">
          <ContactDetailsBox type="email" title="email" info={`${email}`}>
            {letterIconPath}
          </ContactDetailsBox>
          <ContactDetailsBox type="phone" title="phone" info={`${phone}`}>
            {phoneIconPath}
          </ContactDetailsBox>
        </div>
        <div
          className="relative card-bg shadow-component mt-4 md:mt-16 w-full"
          style={{ height: "350px" }}
        >
          <iframe
            src={`https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8942.964692458849!2d${positionLng}!3d${positionLat}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d07cb1a63aebf%3A0x54ed2a67e5d67eaa!2sVienna%20University%20of%20Economics%20and%20Business!5e0!3m2!1sen!2see!4v1625813844877!5m2!1sen!2see`}
            width="800"
            height="350"
            className="absolute top-0 left-0 w-full h-full p-4"
            style={{ border: "0" }}
            allowFullScreen=""
            loading="lazy"
            title={mapLabel}
          />
        </div>
      </div>
    </Layout>
  )
}

export default Contact

export const query = graphql`
  query ContactPage {
    markdownRemark(frontmatter: { title: { eq: "Contact" } }) {
      frontmatter {
        title
        subtitle
        addressHeading
        email
        phone
        positionLat
        positionLng
        mapLabel
      }
      html
    }
  }
`
