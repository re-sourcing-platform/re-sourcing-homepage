import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeader from "../components/layout/headers/PageHeader"
import ScrollToTop from "../components/layout/ScrollToTop"
import SectorCard from "../components/layout/cards/SectorCard"

const SectorPost = ({ data, location }) => {
  const post = data.markdownRemark
  const { subtitle, title } = post.frontmatter

  const sectorSubpages = data.sectorpages.nodes.filter(
    node =>
      node.fields.slug !== node.frontmatter.sector &&
      node.frontmatter.sector === post.frontmatter.sector
  )
  const sectorCardsInfo = [
    {
      icon: "wind",
      title: "RENEWABLE ENERGY",
      textColor: "text-accent1",
      activeBgColor: "bg-accent1",
      sector: "/sectors/renewable-energy/",
    },
    {
      icon: "car",
      title: "MOBILITY",
      textColor: "text-accent2",
      activeBgColor: "bg-accent2",
      sector: "/sectors/mobility/",
    },
    {
      icon: "mobile-alt",
      title: "ELECTRONICS",
      textColor: "text-accent3",
      activeBgColor: "bg-accent3",
      sector: "/sectors/electronics-and-electronic-equipment/",
    },
  ]

  return (
    <Layout
      pageName={`${title} - ${subtitle}`}
      location={location}
      metaDescription={`RE-SOURCING will develop visions & roadmaps for responsible sourcing of minerals in the three sectors of Renewable Energy, Mobility and Electric & Electronic Equipment. It will support a sustainable transition based on environmentally friendly, socially equitable and economically profitable sourcing in global mineral value chains.`}
    >
      <ScrollToTop showBelow={500} />
      <span className="fixed top-0" id="nav" />

      <PageHeader title={`${title} Sector`} subtitle={subtitle} />
      <div className="container mx-auto relative z-20  -mt-4 sm:-mt-8 lg:px-32">
        <section className="grid-cols-3 sm:gap-4 md:gap-8 justify-items-center fixed bottom-0 sm:static grid w-full z-40 border-primary5 border-t sm:border-t-0">
          {sectorCardsInfo.map((sectorInfo, i) => {
            const { icon, title, textColor, activeBgColor, sector } = sectorInfo
            return (
              <SectorCard
                key={i}
                {...{ icon, title, textColor, activeBgColor }}
                active={post?.frontmatter?.sector === sector}
                to={`${sector}overview/#nav`}
                className="shadow-component sm:rounded-lg"
                sm
              />
            )
          })}
        </section>

        <section className="sm:mt-4 md:mt-8 p-4 shadow-component bg-white rounded-lg mx-4 sm:mx-auto">
          <div className="xl:flex xl:justify-center relative divide-y divide-primary2 xl:divide-y-0">
            <div className="flex flex-col md:mr-8 xl:w-1/3 mb-4 xl:mb-0 ">
              {sectorSubpages.map((subpage, i) => {
                return (
                  <Link
                    to={`${subpage.fields.slug}#nav`}
                    className={`block text-center text-xs py-1 font-bold lg:text-base lg:leading-6 text-primary2 hover:text-primary3 ${
                      location.pathname === subpage.fields.slug
                        ? "underline text-primary3"
                        : "no-underline"
                    }`}
                    key={i}
                  >
                    {subpage.frontmatter.subtitle}
                  </Link>
                )
              })}
            </div>

            <div
              dangerouslySetInnerHTML={{ __html: post.html }}
              className="body-content text-sm lg:text-lg xl:w-2/3 pt-4 xl:pt-0"
            />
          </div>
        </section>
      </div>
    </Layout>
  )
}
export default SectorPost

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        subtitle
        title
        sector
      }
    }
    sectorpages: allMarkdownRemark(
      filter: { frontmatter: { template: { eq: "sector-page" } } }
      sort: { fields: frontmatter___sort_order, order: ASC }
    ) {
      nodes {
        frontmatter {
          subtitle
          sector
          sort_order
        }
        fields {
          slug
        }
      }
    }
  }
`
