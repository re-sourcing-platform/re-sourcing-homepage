import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeader from "../components/layout/headers/PageHeader"
import ContactDetailsBox from "../components/layout/ContactDetailsBox"
import ContentBox from "../components/layout/ContentBox"
import PersonCard from "../components/layout/cards/PersonCard"
import Container from "../components/layout/Container"

const Contact = ({ data, location }) => {
  let allMembers = data.members.frontmatter.member
  const {
    title,
    legalName = {},
    subInstitution,
    email,
    homepage,
    members,
    logoImage,
  } = data.organization.frontmatter

  let letterIconPath = (
    <>
      <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
      <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
    </>
  )

  const homepageIconPath = (
    <>
      <path
        fillRule="evenodd"
        d="M4.083 9h1.946c.089-1.546.383-2.97.837-4.118A6.004 6.004 0 004.083 9zM10 2a8 8 0 100 16 8 8 0 000-16zm0 2c-.076 0-.232.032-.465.262-.238.234-.497.623-.737 1.182-.389.907-.673 2.142-.766 3.556h3.936c-.093-1.414-.377-2.649-.766-3.556-.24-.56-.5-.948-.737-1.182C10.232 4.032 10.076 4 10 4zm3.971 5c-.089-1.546-.383-2.97-.837-4.118A6.004 6.004 0 0115.917 9h-1.946zm-2.003 2H8.032c.093 1.414.377 2.649.766 3.556.24.56.5.948.737 1.182.233.23.389.262.465.262.076 0 .232-.032.465-.262.238-.234.498-.623.737-1.182.389-.907.673-2.142.766-3.556zm1.166 4.118c.454-1.147.748-2.572.837-4.118h1.946a6.004 6.004 0 01-2.783 4.118zm-6.268 0C6.412 13.97 6.118 12.546 6.03 11H4.083a6.004 6.004 0 002.783 4.118z"
        clipRule="evenodd"
      ></path>
    </>
  )

  // Connect consortium members with member data
  let membersArray = []
  members.forEach(member => membersArray.push(member.name))
  function isConsortiumMember(value) {
    return membersArray.includes(value)
  }
  let consortiumMembers = allMembers.filter(member =>
    isConsortiumMember(member.name)
  )

  const PersonsSection = () => {
    return (
      <>
        {/* Section heading  */}
        <div className="text-center w-1/2 mx-auto pt-24">
          <h1 className="text-4xl pb-4 font-semibold text-primary1">Persons</h1>
          <h3 className="mb-6 text-primary2 text-lg font-medium">
            People working with Re-sourcing from {title}:
          </h3>
        </div>
        {/* PERSONS SECTION*/}
        <div className="flex justify-center pb-16 mt-16">
          <div className="flex justify-center flex-wrap gap-4">
            {consortiumMembers.map((person, i) => (
              <PersonCard
                src={person?.headshotImage?.relativePath}
                imageData={
                  person?.headshotImage?.childImageSharp?.gatsbyImageData
                }
                name={person.name}
                linkedIn={person.linkedIn}
                className="w-56 mx-4 flex-shrink-0"
                key={i}
              >
                <div className="text-primary3 text-center text-sm">
                  <p>{person.institution}</p>
                  {person.subInstitution && (
                    <>
                      <p>•</p>
                      <p>{person.subInstitution}</p>
                    </>
                  )}
                  {person.position && (
                    <>
                      <p>•</p>
                      <p>{person.position}</p>
                    </>
                  )}
                </div>
              </PersonCard>
            ))}
          </div>
        </div>
      </>
    )
  }

  return (
    <Layout
      pageName={title}
      location={location}
      metaDescription={data.organization.html.replace(/<[^>]+>/gm, "")}
    >
      <PageHeader
        title={legalName}
        subtitle={subInstitution ? subInstitution : ""}
      />
      {/* Section BG ... */}
      <div
        className="absolute w-full bg-primary5 z-0"
        style={{ height: "750px" }}
      ></div>
      <Container className="px-4">
        {/* CONTENT BOX*/}
        <ContentBox
          className="lg:mb-16"
          html={data.organization.html}
          imageData={logoImage.childImageSharp.gatsbyImageData}
          logo
        />
        {consortiumMembers ? <PersonsSection /> : ""}
        {/* Section heading */}
        <div className="text-center w-1/2 mx-auto pt-16">
          <h1 className="text-4xl pb-4 font-semibold text-primary1">Contact</h1>
          <h3 className="mb-6 text-primary2 text-lg font-medium">
            {`How to reach ${legalName}`}
          </h3>
        </div>
        {/* CONTACT SECTION */}
        <div className="flex gap-4 justify-center items-center md:gap-16 flex-col mt-4 md:mt-16 md:flex-row">
          {email ? (
            <div className="max-w-lg">
              <ContactDetailsBox type="email" title="email" info={`${email}`}>
                {letterIconPath}
              </ContactDetailsBox>
            </div>
          ) : (
            ""
          )}
          <div className="max-w-lg">
            <ContactDetailsBox
              type="homepage"
              title="homepage"
              info={`${homepage}`}
            >
              {homepageIconPath}
            </ContactDetailsBox>
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default Contact

export const query = graphql`
  query ($slug: String!) {
    organization: markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        logoImage {
          childImageSharp {
            gatsbyImageData(
              layout: CONSTRAINED
              height: 150
              placeholder: BLURRED
            )
          }
        }
        title
        email
        legalName
        homepage
        members {
          name
        }
      }
    }

    members: markdownRemark(frontmatter: { title: { eq: "Members" } }) {
      frontmatter {
        member {
          institution
          name
          position
          subInstitution
          linkedIn
          headshotImage {
            relativePath
            childImageSharp {
              gatsbyImageData(
                layout: CONSTRAINED
                width: 228
                placeholder: BLURRED
              )
            }
          }
        }
      }
    }
  }
`
