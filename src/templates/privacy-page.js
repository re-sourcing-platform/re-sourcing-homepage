import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"

const PrivacyPage = ({ data, location }) => {
  const { title, subtitle } = data.markdownRemark.frontmatter
  return (
    <Layout pageName={title} location={location}>
      <PageHeader title={title} subtitle={subtitle} />
      <Container className="px-4">
        <div
          className="pt-6 pb-6 card-bg shadow-component px-6 lg:px-16 xl:px-32 body-content"
          dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
        ></div>
      </Container>
    </Layout>
  )
}

export default PrivacyPage

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
      }
    }
  }
`
