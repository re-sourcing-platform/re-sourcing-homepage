import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeaderNews from "../components/layout/headers/PageHeaderNews"
import GradientButton from "../components/utility/GradientButton"
import useSiteMetadata from "../components/SiteMetadata"
import { Helmet } from "react-helmet"
import { useWithoutTrailingSlash } from "../components/utility/hooks/useTrailingSlash"

export default function NewsPost({ data, location }) {
  const { siteUrl } = useSiteMetadata()
  const link = `${useWithoutTrailingSlash(siteUrl)}${location.pathname}`
  const post = data.markdownRemark
  const { author, date, excerpt, subtitle, title, featuredImage } =
    post.frontmatter

  let ogImageSrc = `${useWithoutTrailingSlash(siteUrl)}${
    featuredImage?.childImageSharp?.original?.src
  }`
  return (
    <Layout pageName={title} location={location} metaDescription={excerpt}>
      {featuredImage?.childImageSharp?.original?.src && (
        <Helmet>
          <meta property="og:image" content={ogImageSrc} />
          <meta property="og:type" content="article" />
        </Helmet>
      )}
      <PageHeaderNews
        title={title}
        subtitle={subtitle}
        author={author}
        date={date}
        imageData={featuredImage.childImageSharp.gatsbyImageData}
        link={link}
        buttonLink="/news"
      />
      <div className="container mx-auto relative z-20 sm:mt-3 lg:mt-8 lg:px-32">
        <div className="body-content p-8 mx-auto shadow-component bg-white rounded-t-lg">
          <div>
            <div className="font-bold mb-4 lg:text-xl ">{excerpt}</div>
            <div
              dangerouslySetInnerHTML={{ __html: post.html }}
              className="text-sm lg:text-lg"
            />
          </div>
        </div>
        <GradientButton full className="rounded-b-lg" to="/news">
          BACK TO NEWS
        </GradientButton>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        author
        date(formatString: "DD MMM YYYY")
        excerpt
        featuredImage {
          childImageSharp {
            original {
              src
            }
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
        }
        subtitle
        title
      }
    }
  }
`
