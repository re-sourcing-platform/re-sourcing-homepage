import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"
import ContentBox from "../components/layout/ContentBox"
import SectorCard from "../components/layout/cards/SectorCard"

const TargetGroupPage = ({ data, location }) => {
  const { title, subtitle, featuredImage } = data.markdownRemark.frontmatter

  return (
    <Layout
      pageName={`${title} - ${subtitle}`}
      location={location}
      metaDescription={`RE-SOURCING will develop visions & roadmaps for responsible sourcing of minerals in the three sectors of Renewable Energy, Mobility and Electric & Electronic Equipment. It will support a sustainable transition based on environmentally friendly, socially equitable and economically profitable sourcing in global mineral value chains.`}
    >
      <PageHeader title={title} subtitle={subtitle} />
      {/* Section BG ... */}
      <Container className="px-4">
        {/* CONTENT BOX*/}
        <ContentBox
          className="mb-16"
          html={data.markdownRemark.html}
          imageData={featuredImage.childImageSharp.gatsbyImageData}
        />

        <div className="grid grid-cols-3 gap-2 md:gap-8 justify-items-center sm:justify-items-stretch">
          <SectorCard
            icon="wind"
            title="RENEWABLE ENERGY"
            textColor="text-accent1"
            activeBgColor="bg-accent1"
            active={true}
            to="/sectors/renewable-energy/overview/"
            className="shadow-component rounded-lg"
          />
          <SectorCard
            icon="car"
            title="MOBILITY"
            textColor="text-accent2"
            activeBgColor="bg-accent2"
            active={true}
            to="/sectors/mobility/overview/"
            className="shadow-component rounded-lg"
          />
          <SectorCard
            icon="mobile-alt"
            title="ELECTRONICS"
            textColor="text-accent3"
            activeBgColor="bg-accent3"
            active={true}
            to="/sectors/electronics-and-electronic-equipment/overview/"
            className="shadow-component rounded-lg"
          />
        </div>
      </Container>
    </Layout>
  )
}

export default TargetGroupPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        subtitle

        featuredImage {
          childImageSharp {
            gatsbyImageData(width: 800, placeholder: BLURRED)
          }
        }
      }
    }
  }
`
