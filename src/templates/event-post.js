import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeaderEvent from "../components/layout/headers/PageHeaderEvent"
import GradientButton from "../components/utility/GradientButton"
import useSiteMetadata from "../components/SiteMetadata"
import HtmlProvider from "../components/utility/HtmlProvider"
import {
  isFutureEvent,
  getTimeRemaining,
} from "../components/utility/timeMethods"
import moment from "moment-timezone"
import { Helmet } from "react-helmet"
import { useWithoutTrailingSlash } from "../components/utility/hooks/useTrailingSlash"
import useSessionStorage from "../components/utility/hooks/useSessionStorage"
import PasswordCheck from "./../components/layout/forms/PasswordCheck"

export default function EventPost({ data, location }) {
  let storageKey = `isPassed-${data.markdownRemark.id}`
  const [isPassed, setIsPassed] = useSessionStorage(storageKey, false)

  const {
    date_start,
    date_end,
    month_only,
    event_start,
    event_end,
    countdownTimer,
    subtitle1,
    subtitle2,
    title,
    registerLink,
    virtualEvent,
    assets,
    featuredImage,
    password,
    protectedContent,
    eventLocation,
  } = data.markdownRemark.frontmatter

  const { siteUrl } = useSiteMetadata()
  const link = `${useWithoutTrailingSlash(siteUrl)}${location.pathname}`
  let ogImageSrc = `${useWithoutTrailingSlash(siteUrl)}${
    featuredImage?.childImageSharp?.original?.src
  }`
  let time_start = moment(event_start).tz("Europe/Vienna").format("h:mm A")
  let time_end = moment(event_end).tz("Europe/Vienna").format("h:mm A")
  let eventStart = new Date(event_start)
  const showCountdown = () => {
    return countdownTimer && isFutureEvent(eventStart)
  }

  const [timeRemaining, setTimeRemaining] = useState(
    getTimeRemaining(eventStart)
  )

  useEffect(() => {
    if (!showCountdown()) return
    const timerId = setInterval(
      () => setTimeRemaining(getTimeRemaining(eventStart)),
      60000
    )
    return () => clearInterval(timerId)
  })

  return (
    <Layout
      pageName={title}
      location={location}
      metaDescription={`${title}${
        subtitle1?.length > 0 ? ` > ${subtitle1}` : ""
      }${
        subtitle2?.length > 0 ? ` > ${subtitle2}` : ""
      } is a RE-sourcing project ${virtualEvent ? "virtual" : ""} event `}
    >
      {featuredImage?.childImageSharp?.original?.src && (
        <Helmet>
          <meta property="og:image" content={ogImageSrc} />
        </Helmet>
      )}
      <PageHeaderEvent
        title={title}
        date_start={date_start}
        date_end={date_end}
        imageData={featuredImage?.childImageSharp?.gatsbyImageData}
        link={link}
        month_only={month_only}
        buttonLink={isFutureEvent(eventStart) && registerLink}
        type="event"
        time_start={time_start}
        time_end={time_end}
        location={virtualEvent ? "Virtual event" : eventLocation}
      />
      {/* Countdown timer */}
      {showCountdown() && (
        <div className="container mx-auto relative z-30 sm:mt-3 lg:mt-8 lg:px-32">
          <div className="mx-auto flex justify-center gap-16 card-bg px-32 py-4 shadow-component text-primary2">
            <div className="text-center w-full">
              <h2 className="text-2xl md:text-4xl">{timeRemaining.days}</h2>
              <h3 className="md:text-xl">Days</h3>
            </div>
            <div className="text-center w-full">
              <h2 className="text-2xl md:text-4xl">{timeRemaining.hours}</h2>
              <h3 className="md:text-xl">Hours</h3>
            </div>
            <div className="text-center w-full">
              <h2 className="text-2xl md:text-4xl">{timeRemaining.mins}</h2>
              <h3 className="md:text-xl">Minutes</h3>
            </div>
          </div>
        </div>
      )}

      <main className="container mx-auto relative z-20 md:mt-16 lg:px-32">
        {!isPassed ? (
          <div className="p-8 mx-auto shadow-component bg-white rounded-t-lg">
            {password && protectedContent && (
              <PasswordCheck
                password={password}
                storageKey={storageKey}
                title="Please enter password if you have already registered for the event"
                onChange={e => setIsPassed(e)}
              />
            )}
            <div className="font-bold text-center text-primary1 text-xl lg:text-3xl pb-8 xl:px-16">
              <h1>{title}</h1>
              <h2>{subtitle1}</h2>
              <h2>{subtitle2}</h2>
            </div>
            <div className="xl:flex xl:justify-center">
              {assets && assets.length > 0 ? (
                <div className="xl:w-1/3 text-center text-primary2 pb-4 xl:mr-8">
                  {assets.map((asset, i) => (
                    <div
                      className="lg:py-2 py-1 font-bold text-lg lg:text-xl lg:leading-6"
                      key={i}
                    >
                      <a
                        href={asset.file.publicURL}
                        target="blank"
                        rel="noopener noreferrer"
                      >
                        {asset.name}
                      </a>
                    </div>
                  ))}
                </div>
              ) : (
                ""
              )}

              <div
                dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
                className="body-content event-body text-sm lg:text-lg xl:w-2/3"
              />
            </div>
          </div>
        ) : (
          <div className="p-8 mx-auto shadow-component bg-white rounded-t-lg">
            <HtmlProvider
              className="body-content mx-auto event-body text-sm lg:text-lg xl:w-2/3"
              markdown={protectedContent}
            />
          </div>
        )}

        <GradientButton full className="rounded-b-lg" to="/events">
          BACK TO EVENTS
        </GradientButton>
      </main>
    </Layout>
  )
}

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      id
      frontmatter {
        title
        subtitle1
        subtitle2
        type
        date_start(formatString: "DD MMM YYYY")
        date_end(formatString: "DD MMM YYYY")
        month_only
        event_start: date_start
        event_end: date_end
        countdownTimer: countdown
        registerLink
        virtualEvent
        eventLocation: location
        assets {
          file {
            publicURL
          }
          name
        }
        featuredImage {
          childImageSharp {
            original {
              src
            }
            gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
          }
        }
        password
        protectedContent
      }
    }
  }
`
