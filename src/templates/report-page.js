import React from "react"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import SocialShareCard from "../components/layout/cards/SocialShareCard"

import { social, responsive, iframe } from "../styles/report.module.css"

const ReportPage = props => {
  console.log(props)
  return (
    <Layout
      // pageName={props.pageContext.title}
      location={props.pageContext.location}
      //metaDescription={props.pageContext.html.replace(/<[^>]+>/gm, "")}
    >
      <Container className="px-4">
        <div className="shadow-component">
          <div className={social}>
            <SocialShareCard
              title="tiitel"
              socialShareLink={props.location.href}
            />
          </div>
          <div className={responsive}>
            <div>
              <iframe
                src={props.pageContext.location}
                allowFullScreen={true}
                frameBorder="0"
                allowtransparency="true"
                className={iframe}
                title="unique"
              ></iframe>
            </div>
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default ReportPage
