import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"
import PersonCard from "../components/layout/cards/PersonCard"

export default function MembersPage({ location }) {
  const data = useStaticQuery(graphql`
    query MembersData {
      markdownRemark(frontmatter: { title: { eq: "Members" } }) {
        frontmatter {
          member {
            institution
            name
            position
            subInstitution
            headshotImage {
              relativePath
              childImageSharp {
                gatsbyImageData(width: 250, placeholder: BLURRED)
              }
            }
          }
        }
      }
    }
  `)
  const persons = data.markdownRemark.frontmatter.member

  return (
    <Layout pageName="Members" location={location}>
      {/* Section BG ... */}
      <PageHeader title="Members" />
      <Container>
        <div className="flex flex-wrap gap-6 justify-center">
          {persons.map((person, i) => (
            <PersonCard
              src={person.headshotImage?.relativePath}
              imageData={person.headshotImage?.childImageSharp?.gatsbyImageData}
              name={person.name}
              className="w-56"
              key={i}
            >
              <div className="text-primary3 text-center text-sm">
                <p>{person.institution}</p>
                {person.subInstitution && (
                  <>
                    <p>•</p>
                    <p>{person.subInstitution}</p>
                  </>
                )}
                {person.position && person.position.trim().length > 0 && (
                  <>
                    <p>•</p>
                    <p>{person.position}</p>
                  </>
                )}
              </div>
            </PersonCard>
          ))}
        </div>
      </Container>
    </Layout>
  )
}
