import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import PageHeader from "../components/layout/headers/PageHeader"
import OptionsBar from "../components/layout/OptionsBar"
import ContentBox from "../components/layout/ContentBox"
import Accordion from "../components/layout/Accordion"
import Container from "../components/layout/Container"
import HtmlProvider from "../components/utility/HtmlProvider"

const WikiPage = ({ data, location }) => {
  const { title, subtitle, featuredImage, dropdowns } =
    data.markdownRemark.frontmatter

  const [navOptionActive, setNavOptionActive] = useState(0)
  const [refresh, doRefresh] = useState(0)

  let navOptions = []

  dropdowns.map(({ type }) => {
    if (!navOptions.includes(type)) navOptions.push(type)
    return navOptions
  })

  let activeDropdowns = []

  navOptions.forEach(option => {
    activeDropdowns.push(dropdowns.filter(dropdown => dropdown.type === option))
  })

  const handleChange = e => {
    setNavOptionActive(e)
    doRefresh(prev => prev + 1)
  }

  return (
    <Layout pageName={title} location={location}>
      <PageHeader title={title} subtitle={subtitle} />
      {/* Section BG ... */}
      <Container>
        {/* CONTENT BOX*/}
        <div className="px-4 order-2 md:order-1 md:px-0">
          <ContentBox
            html={data.markdownRemark.html}
            imageData={featuredImage.childImageSharp.gatsbyImageData}
          />
        </div>

        <OptionsBar
          className="order-1 md:mt-8 md:order-2"
          navOptions={navOptions}
          onChange={e => handleChange(e)}
        />

        <div className="flex flex-col gap-4 px-4 order-3 mt-8 md:mt-0 md:px-0 md:gap-8">
          {activeDropdowns[navOptionActive].map((dropdown, i) => {
            return (
              <Accordion key={i} text={dropdown.title} refresh={refresh}>
                <HtmlProvider markdown={dropdown.text} />
              </Accordion>
            )
          })}
        </div>
      </Container>
    </Layout>
  )
}

export default WikiPage

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
        featuredImage {
          childImageSharp {
            gatsbyImageData(
              height: 800
              layout: CONSTRAINED
              placeholder: BLURRED
            )
          }
        }
        dropdowns {
          title
          text
          type
        }
      }
    }
  }
`
