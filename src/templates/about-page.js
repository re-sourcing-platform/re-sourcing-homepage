import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Infographic from "../components/layout/infographic/Infographic"
import IconCard from "../components/layout/cards/IconCard"

const AboutPage = ({ data, location }) => {
  const { title, sections } = data.markdownRemark.frontmatter

  return (
    <Layout
      pageName={`${title}`}
      location={location}
      metaDescription={`Responsible Sourcing is becoming a reality for more and more businesses, NGOs, and policymakers. 
        Everyone is striving to keep ahead of rapidly evolving ecological and social needs, company practices, business models, government regulations, and initiatives spearheaded by civil society, etc.
        In response to the growing challenge of responsible sourcing, the Vienna University of Economics and Business, together with 11 international partners, started the RE-SOURCING Global Stakeholder Platform in 2020.`}
    >
      <Infographic />

      <section id="about">
        <div className="container mx-auto z-20 relative">
          <div className="mt-8 sm:mt-4 xl:mt-16">
            <div className="text-center w-5/6 xl:w-1/2 mx-auto pt-4 pb-6 md:pt-8 md:pb-12">
              <h1 className="text-2xl md:text-4xl pb-4 font-semibold text-primary1">
                {sections[0].title}
              </h1>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 md:gap-6 px-4 sm:px-16 md:px-0">
              {sections[0].cards.map((card, i) => {
                return (
                  <IconCard
                    className="w-full items-stretch col-span-1"
                    icon={card.icon}
                    key={i}
                    title={card.title}
                    body={card.body}
                  ></IconCard>
                )
              })}
            </div>
          </div>

          <div className="mt-16">
            <div className="text-center w-5/6 xl:w-1/2 mx-auto pt-4 pb-6 md:pt-8 md:pb-12">
              <h1 className="text-2xl md:text-4xl pb-4 font-semibold text-primary1">
                {sections[1].title}
              </h1>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 md:gap-6 px-4 sm:px-16 md:px-0">
              {sections[1].cards.map((card, i) => {
                return (
                  <IconCard
                    className="w-full items-stretch col-span-1"
                    icon={card.icon}
                    key={i}
                    title={card.title}
                    body={card.body}
                  ></IconCard>
                )
              })}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default AboutPage

export const query = graphql`
  query aboutPage {
    markdownRemark(frontmatter: { title: { eq: "About" } }) {
      frontmatter {
        sections {
          title
          cards {
            body
            icon
            title
          }
        }
        title
      }
    }
  }
`
