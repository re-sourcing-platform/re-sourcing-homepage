---
title: Home
template: index-page
sections:
  - title: Welcome to RE-SOURCING!
    text: >-
      RE-SOURCING is an EU-funded Multi-Stakeholder Platform that aims to
      advance Responsible Sourcing of raw materials along and across global
      mineral value chains. It strives to promote both strategic agenda setting
      and coherent application of practices for Responsible Sourcing.

      -- Explore the website to learn more about the project, visit the knowledge hub to answer your questions and join our network to support the future of Responsible Sourcing!
    type: hero
  - title: Challenging Times Require Responsible Solutions
    text: >-
      The European Green Deal is the European Union's bold ambition to tackle
      the growing threat of climate change and environmental degradation. The
      targets set under the Deal require innovation and deployment of new
      technologies in renewable energy, mobility and electric & electronic
      goods. The value chains that provide the resources and products for
      these  sectors are spanning across the globe.

      -- How to ensure that the extensive sourcing of raw materials required for the green transition does not cause social and environmental harm?
    type: video
  - title: "RE-SOURCING is your opportunity to address the challenge of Responsible
      Sourcing in a multi-stakeholder setting:"
    text: "  "
    type: roadmap
  - title: As a…
    type: benefits
    text: "  "
  - title: A team with diverse backgrounds to match the challenge
    text: Our team composition facilitates the setup of the RE-SOURCING Global
      Stakeholder Platform as a neutral and mediating space that is open to the
      many perspectives of involved stakeholders. Our skills, comprehensive
      background and expertise in the area of industry, academia and civil
      society will create the way forward that works for all - the planet, the
      people and businesses alike.
    type: members
  - title: Subscribe to Our Network
    text: We invite you to be part of the RE-SOURCING Platform by subscribing to our
      growing network. By joining our network, you will receive our newsletters,
      will be invited to the various RE-SOURCING platform events (e.g.
      Conferences, Workshops, Webinars and Peer-Learning Labs) and will be
      informed about our latest achievements and publications.
    type: subscribe
cards:
  - icon: briefcase
    title: ...business
    body: >
      -- better understand the challenges of responsible sourcing and your role
      in it

      -- learn about best practices, tools, and regulations in your industry

      -- network with like-minded businesses
  - icon: landmark
    title: ...policymaker
    body: >+
      -- increase your capabilities to implement effective policies and
      strategies

      -- co-develop policy recommendations, roadmaps and best practices

      -- learn how to best stimulate the private sector 

  - title: ...civil society representative
    body: |
      -- foster the adoption of sustainable development 
      -- raise awareness about responsible sourcing
      -- support a critical and constructive discourse that leaves no one behind
    icon: users
subscribeLink: https://re-sourcing.us9.list-manage.com/subscribe?u=4131de53447fbeab61124f6d0&id=2a1f831795
---
