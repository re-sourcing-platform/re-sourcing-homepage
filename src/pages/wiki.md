---
template: wiki-page
title: WIKI
subtitle: " "
featuredImage: /files/responsible-sourcing.jpg
dropdowns:
  - title: The RE-SOURCING Project Scope
    text: >-
      The RE-SOURCING project is an EU-funded Multi-Stakeholder Platform that
      aims to advance Responsible Sourcing of raw materials along and across
      global mineral value chains. It strives to promote both strategic agenda
      setting and coherent application of practices for Responsible Sourcing. 12
      partners aim at developing criteria for a responsible sourcing definition.
      The project focuses on three sectors: Renewable Energy, Mobility and
      Electronics. For more details please see the
      [Sectors](https://re-sourcing.eu/sectors/) page.


      ![re-sourcing-roadmap](/files/roadmap_timeline.jpg "Re-sourcing project sectors roadmap.")
    type: Glossary
  - title: Global Value Chain
    text: >-
      A supply chain is a process for mapping the movement of goods and
      services, it is **devoid of the power relations** that exist between the
      firms in a chain. A **value chain** on the other hand notes where ‘value’
      is created along the supply chain – which firms have the power of design,
      governance, standard setting, procurement guidelines, auditing control,
      financial control etc. This project considers three stages of the value
      chain:


      **Upstream**: Refers to the extraction process and includes exploration, mining and processing, intermediary and export of minerals. Smelters and refineries are included in this segment of the chain. 


      **Downstream:** Refers to (re)import, semi-fabrication, material conversion and manufacturing.


      **Use/Re-Use Phase**: Wholesale and retail, recycling/smelting are included as a third segment, for this project. This allows for an evaluation of RS practices specific to the recycling node of the chain. 


      ![value-chain](/files/value-chain.png "Value Chain")


      For more details on the project approach please see [The RE-SOURCING Common Approach](https://re-sourcing.eu/reports/d12-in-rs-template-final).
    type: Glossary
  - title: "Project Focus: Renewable Energy Sector"
    text: >-
      The renewable energy sector includes a host of sources, with 91% of the
      energy generation coming from biomass, hydropower and wind (2017). Solar
      energy remains an important source of renewable energy for the future. The
      EU target, under the revised Renewable Energy Directive, is set at 32%
      energy from renewable energy sources by 2035. The RE-SOURCING project
      focuses on the mining & processing stage and the manufacturing stages of
      the value chain in the renewable energy sector.


      For detailed information, please see [RE Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/) or [the State-of-play Report on Renewable Energy](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).


      ![Global Value Chain for Renewable Energy ](/files/global-value-chain-for-renewable-energy-.png "Global Value Chain for Renewable Energy - Source:RE-SOURCING Project ")
    type: Glossary
  - title: "Project Focus: Mobility Sector"
    text: >-
      The mobility sector is one of the largest manufacturing sectors in the EU.
      In 2017, the transport sector also accounted for 25% of the EU28 GHG
      emissions. Therefore, the main elements of the European Strategy for
      Low-emission Mobility include speeding up the deployment of low-emission
      alternative energy for transport and moving towards zero-emission
      vehicles. A key strategy for this shift to be successful is the transition
      to Hybrid and Electric Vehicles; in 2050, 80% of all newly registered
      passenger cars globally could be powered by alternative drivetrains.  The
      key to e-mobility is the production of strong, efficient and affordable
      batteries. In the value chain of electric cars, the battery is the most
      valuable component with a share of 40% of value. The mobility sector will
      focus on the more mature technology of the electric battery, which will be
      the key component of the sustainability transition agenda in the EU. 


      The focus of the research in the RE-SOURCING project is on three nodes within this value chain: the mining of selected minerals; cell manufacturing and recycling.  


      Focus within the relevant battery materials is on lithium, cobalt, nickel and graphite. 


      For further information on the mobility sector please see [Mobility Sector](https://re-sourcing.eu/sectors/mobility/overview/#nav) or the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).


      ![Lithium-ion battery value chain – circle around the value chain steps in the focus of the project ](/files/lithium-ion-battery-value-chain.png "Source:RE-SOURCING Project")
    type: Glossary
  - title: "Project Focus: Electric & Electronic Equipment Sector"
    text: >-
      The Electronics sector is one of the largest industries in the world, with
      approximately 18 million workers, producing 20% of global imports and $1.7
      trillion in trade in electronics products. It is a major contributor to
      creating economic growth and opportunities across the world and is key for
      many industries (automotive, healthcare, aeronautics, space,
      communications).  


      The EEE Sector covers many products from electronic components and boards; computers and peripheral equipment; consumer electronics, electronic and electric wires and cables.


      The range of materials used in EEE products is relatively extensive. The research focus in the project will be on:  


      **3TG (Tin, Tungsten, Tantalum, Gold):** They have high relevance due to legislation (OECD, EU, Dodd-Frank) that govern responsible sourcing practices and have high industry consumption and function criticality (tin, tantalum) for EEE products.  


      **Mica**: Has high relevance within the EEE and the mobility sector, with severe sustainability risks associated with its extraction.  


      For further details please see [EEE Sector](https://re-sourcing.eu/sectors/electronics-and-electronic-equipment/overview/#nav) or [The RE-SOURCING Common Approach](https://re-sourcing.eu/reports/d12-in-rs-template-final) - Chapter 4.3. 


      ![Value chain in the EEE sector ](/files/value-chain-in-the-eee-sector-.jpg "Value chain in the EEE sector. Source: EC: The Regulation Explained")
    type: Glossary
  - title: "Project Relevance: Narrative Analysis"
    text: >-
      The RE-SOURCING project conducted a narrative analysis in autumn
      2020 on the overall issue of responsible sourcing and on three sectors in
      the focus of the project: renewable energy, mobility and electronic and
      electric equipment. The results of the narrative analysis of the online
      discourse addressing responsible sourcing in general and specifically for
      the renewable energy sector are explained and visualized in the [State of
      Play Report on the Renewable Energy
      Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res) -
      Chapter 4. The results of the narrative analysis on the mobility sector
      are available in the [State of Play Report on the Mobility
      Sector](https://re-sourcing.eu/reports/sop-mobility-sector) -
      Chapter 5.


      ![Results of narrative analysis for term ‘Responsible Sourcing’](/files/results-of-narrative-analysis-for-term-responsible-sourcing-.jpg "Results of narrative analysis for term ‘Responsible Sourcing’")
    type: Glossary
  - title: Copper Mining
    text: >-
      Copper (Cu) is not classified as a critical mineral by the EU –it is a
      relevant material for various low-carbon energy technologies. The
      apparent European consumption of copper is very high with 2.57 Mt per
      year, as it is an essential raw material for many industrial sectors, such
      as tubes and wiring, digital appliances, etc. Copper is mined in the EU,
      accounting for 4% of total global extraction. The EU’s refined copper
      production represents 12% of global production. Moreover, downstream
      industries producing rods, bars, wires, tubes, etc. are active in the EU,
      with approximately 80 companies accounting for 35,000 jobs.


      Further information on copper is provided under the [Renewable Energy Sectors](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including primary copper production, challenges in the extraction phase, major copper mining companies globally and in Europe, copper mining plans in Europe, etc. More detailed information on copper can be found in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Copper) also provides information for copper.
    type: Mining
  - title: Cobalt Mining
    text: >-
      Cobalt (Co) is listed on the EU’s critical raw materials list. Cobalt
      is part of NMC (lithium nickel manganese cobalt oxide), which is the main
      cathode material for all types of electric vehicles batteries. Cobalt in
      most cases is extracted as a by-product of nickel or copper mining,
      although in different concentrations, linking its extraction to other
      metals. The majority of cobalt was produced in the Democratic Republic of
      the Congo (DRC), accounting for more than 70% of global supply in recent
      years. Further information on cobalt is provided under the Mobility Sector
      , including primary cobalt production, challenges in the extraction phase,
      major international cobalt mining companies, and cobalt mining plans in
      Europe, etc. More detailed information on cobalt can be found in
      the [State of Play Report on the Mobility
      Sector](https://re-sourcing.eu/reports/sop-mobility-sector).


      The [Raw Materials Information System](<https://rmis.jrc.ec.europa.eu/?page=rm-profiles#/Cobalt)>) also provides information for cobalt.
    type: Mining
  - title: Graphite Mining
    text: >-
      Lithium-ion batteries (LIB) mainly use carbon-based anode materials, in
      particular graphite, because these materials are associated with both a
      long cycle life and outstanding electrochemical properties such as high
      energy density and efficiency Additionally, producing carbon anodes is
      rather cheap per energy content compared to other alternatives like
      lithium titanium oxide (LTO).


      Natural graphite is classified as a critical mineral by the EU. It can either be mined in open pits or underground. Its counterpart, synthetic graphite, can be produced by heating coke or other carbon-based precursors. Both graphite forms must then be processed further before they can be used as anode materials. The different origins of natural graphite and synthetic graphite contribute to differences in their post-processing purity and their electrochemical properties.


      Further information on graphite is provided under the Mobility Sector, including graphite production, challenges in the extraction phase and synthetic graphite production, major graphite mining and producing companies globally and in Europe, graphite mining plans in Europe, etc. More detailed information on graphite can be found in the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/?page=rm-profiles#/Natural%20graphite) from the European Commission also provides information for graphite.
    type: Mining
  - title: Lithium Mining
    text: >-
      Lithium (Li) is classified as a critical mineral for the EU. Lithium is
      essential for lithium-ion batteries (LIB), although it only represents a
      small percentage of a battery cell’s weight (≈ 2 % per cell). According
      to [Benchmark Minerals
      Intelligence](https://www.benchmarkminerals.com/) (2020), LIBs are already
      responsible for 57% of lithium demand (ahead of glass and ceramics,
      lubricant/grease, metallurgy etc.) 


      Lithium is mined from hard rock (e.g. spodumene) or continental brines. Chile has the Earth’s largest lithium reserves (about 50% of worldwide estimates) in its salt flats within the arid ‘Lithium triangle’, an area covering parts of Chile, Bolivia and Argentina. Most hard-rock extraction of lithium occurs in Australia; [USGS](https://pubs.usgs.gov/periodicals/mcs2021/mcs2021.pdf)(2021) estimates this to be over 20% of worldwide reserves


      In general, the global lithium value chain has not yet been fully established. However, demand and extraction have been rising strongly over the last few years, shifting away from South America as the major producing region to Australia.


      Lithium is considered a relevant raw material in the mobility sector. Please see further information on lithium on the mobility sectors page, including primary lithium production, challenges in the extraction phase, major lithium mining companies globally and in Europe, lithium mining plans in Europe. More detailed information on nickel can be found in the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Lithium) also provides information for Lithium.
    type: Mining
  - title: Nickel Mining
    text: >-
      Nickel (Ni) is currently not included in the EU’s critical raw materials
      list. It the most common cathode material used for traction batteries, as
      part of lithium nickel cobalt manganese oxide (NMC). This cathode material
      comes with different ratios of transition metals (nickel, cobalt and
      manganese), although many producers tend to prefer high nickel ratios, as
      they increase the battery’s energy density. This also goes along with
      lower cobalt and manganese ratios. 


      The global production of nickel is much more distributed around the world than other battery materials. Since most nickel is used in steel production, lithium-ion batteries only use a fraction of global nickel production. Other nickel applications relate to defense systems, prompting countries around the world to consider securing their own supply. In general, nickel demand for batteries can be easily covered by the available supply. However, lithium-ion batteries need nickel with a very high degree of purification, which is only performed in certain countries.  


      Further information on nickel provided under the [Mobility Sector](https://re-sourcing.eu/sectors/mobility/overview/#nav), including primary nickel production, challenges in the extraction phase, major nickel mining companies globally and in Europe, nickel mining plans in Europe, etc.


      More detailed information on nickel can be found in the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Nickel) also provides information for nickel.
    type: Mining
  - title: Rare Earth Elements Mining
    text: >-
      Rare earth elements (REE) are classified as critical under the EU critical
      raw materials list. About 20% of total REE production is used to produce
      magnets (mainly neodymium, samarium, praseodymium, and dysprosium). These
      magnets are relevant for motors (electric vehicles) and generators (wind
      turbines), hard disc drives, speakers, etc. Depending on the future
      deployment of the permanent magnet motors and generators, a strong
      increase in demand for REE for renewable energy applications can be
      expected. 


      Concentrates of REE are mainly produced in China (in 2018 approximately 73.2%), which rose to fame in 2011, when they restricted the export of REE concentrates. Contrary to their name, the occurrences of REEs are not rare, but the extraction process is associated with major negative environmental impacts. 


      Further information on REEs is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including primary REE production, challenges in the extraction phase, major REE mining companies globally and in Europe, copper mining plans in Europe, etc. More detailed information on REE can be found in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/REEs) also provides information for REE.
    type: Mining
  - title: Silicon Mining
    text: >-
      Silicon is classified as critical mineral under the EU’s list of critical
      raw materials. The EU accounts for approximately 18% of global
      consumption and approximately6% of global supply of silicon metal. Pure
      silicon or silicon metal is mainly obtained from quartz (SiO2).
      The reserves and resources are abundant, the largest producer of silicon
      is China with more than 60% of global supply. Quartz can be extracted from
      vein type deposits by drilling and blasting or from fluvial deposits by
      excavation methods.


      Worldwide, the solar industry accounts for 10% of total consumption of silicon. There are numerous quartz mines and resources reported throughout the EU, however, it is unclear whether these resources have the high purity required for silicon metal production.


      Further information on silicon is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including primary production, challenges in the extraction phase, major mining companies globally and in Europe, etc. More detailed information on silicon can be found in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).


      The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Silicon%20metal) also provides information for silicon.
    type: Mining
  - title: Battery Cell Production
    text: >-
      The production of lithium-ion battery cells has increased exponentially
      over the last few years due to rising demand, especially to power electric
      vehicles. This trend is continuing, as many new cell production plants
      start production, with more announced to be built over the next years. As
      battery cells are the key part of an electric vehicle, there is a strong
      connection between the original equipment manufacturers (OEMs) and the
      cell producers, since a battery is usually directly manufactured for a
      specific battery pack and car. Some OEMs produce battery cells themselves
      (e.g. Tesla or BYD) and others are planning to do so (e.g. Volkswagen).  


      Further information on battery cell production is provided under the [Mobility Sector](https://re-sourcing.eu/sectors/mobility/overview/#nav) , including producing companies globally and in Europe, challenges in the manufacturing phase, etc. The detailed information on battery cell manufacturing including a technical overview is provided in the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).
    type: Manufacturing
  - title: Solar Panel Production
    text: >-
      Solar power production utilizes the radiation of the sun and transforms it
      into electricity or heat. Two different types of technologies can be
      differentiated, solar cells or photovoltaics (PV) that convert sunlight
      into electricity. These can be used on a small-scale, for example
      installed on roofs for personal use, or on large-commercial scale.
      Concentrated solar power (CSP) uses mirrors to concentrate the solar rays.
      The concentrated rays then heat fluids to create steam and power a
      turbine. This in turn generates electricity in large-scale power
      plants *([IRENA](https://www.irena.org/solar), 2020).* The RE-SOURCING
      project focuses on the first type – the solar PV panels. 


      China is the largest producer of the raw material required for the production of the most widely used solar cells, as well as the largest manufacturer of solar cells.  


      Further information on solar panel production is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including European and international manufacturing companies, challenges in the manufacturing phase, etc. Detailed information, including a technical overview, is provided in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).
    type: Manufacturing
  - title: Wind Turbine Production
    text: >-
      Wind turbines are used to produce electricity from a renewable resource –
      wind or moving air. Wind creates kinetic energy causing the blades of the
      turbine to rotate, thus creating rotational energy. This rotational energy
      is in turn transformed into electrical energy using generators. There are
      different types of wind turbines. The two main categories are onshore and
      offshore technologies, that can be further differentiated according to the
      type of generator system used. The generator and the magnet used also
      determine the materials required for the construction of the turbine. For
      example, electromagnets found in different types of generators consist of
      iron cores surrounded by wound copper wire. Other turbines might use
      permanent magnet generators that require rare earth magnets (neodymium,
      iron, and boron Nd-Fe-B magnets). The trend for wind turbine technology is
      moving towards larger wind turbines with higher capacities. Currently, the
      largest available turbines have a capacity of 8 MW and a rotor diameter of
      164
      meters *([IRENA](https://www.irena.org/wind,%20updated%20on%208/24/2020),
      2020).*


      Further information on battery cell production is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including European and international manufacturing companies, challenges in the manufacturing phase, etc. Detailed information, including a technical overview, is provided in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).
    type: Manufacturing
  - title: Lithium-ion Battery Recycling
    text: >-
      Lithium-ion battery (LIB) disposal is in part regulated under various
      existing and upcoming regulations (e.g. the Basel Convention, the EU’s
      updated Battery Directive, etc.). Proper battery disposal is generally
      motivated by initiatives to protect people and the environment and to keep
      resources within countries by supporting a circular economy. Consequently,
      battery recycling is the preferred method to manage end-of-life (EoL)
      batteries. Apart from the fact that recycling is the only viable waste
      treatment option for LIBs due to their intrinsic danger, the strategic
      importance of LIB recycling taking place in the EU can be summarized in
      the following four points: 


      * provides the EU with key materials for electromobility through environmentally friendly, socially acceptable and energy and cost-efficient processes; 

      * reduces EU dependence on non-European sources for materials for battery cells and the mobility sector in general;  

      * establishes and develops a new high-tech branch within the EU circular economy, with significant EU actors providing innovation and investment, whereby creating jobs and turnover by extending the value chain beyond the use phase; and  

      * transfers knowledge and recycling plant technology for LIBs into the EU.  


      Further information on battery cell production is provided under the [Mobility Sector](https://re-sourcing.eu/sectors/mobility/overview/#nav), including international and European recycling companies, challenges in the recycling phase, etc. Detailed information on lithium-ion battery recycling, including technological status, is provided in the [State of Play Report on the Mobility Sector](https://re-sourcing.eu/reports/sop-mobility-sector).
    type: Recycling
  - title: Solar Panel Recycling
    text: >-
      As the solar panel technology is a comparably young technology with a life
      span of 20-30 years, the return of used solar panels for the recycling
      process is quite small. For this reason, the number of treatment
      facilities is limited and the recycling per say is not economically
      attractive yet. With available technologies, a little over 90% of solar
      panels can be recycled. This is mainly done in existing glass and
      aluminium recycling plants. However, effective treatment of the entire
      solar panel is key to extract critical metals used, such as silver,
      tellurium, or indium.


      Further information on solar panel recycling is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including recycling companies, challenges in the recycling phase, etc.


      Detailed information on Solar Panel recycling is provided in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).
    type: Recycling
  - title: Wind Turbine Recycling
    text: >-
      Wind turbines generally have a high recyclability of approximately 80% to
      90%. The major components – such as steel from the tower, concrete from
      the foundation, etc. can be reused. However, so far this has not been
      possible for wind turbine blades, due to the complex structure of
      composite materials. Over the next couple of years 15,000 wind turbine
      blades will reach their end-of-life in Europe. This large volume poses
      great challenges for collection and waste disposal or processing. New
      technologies need to be developed to sustainably treat and reuse the
      materials to make the wind sector sustainable from cradle to grave.
      Currently there are three main options of handling the end-of-life blade
      material: (i) disposal, including landfill or incineration without heat
      recovery, (ii) energy recovery or recycling, i.e. incineration with energy
      recovery, thermal, chemical, or mechanical recycling, and (iii)
      repurposing, e.g. co-processing in a cement kiln. Apart from the issues
      with treatment and recycling itself, the dismantling of wind turbines also
      poses health and safety risks for the workers.  


      Further information on wind turbine recycling is provided under the [Renewable Energy Sector](https://re-sourcing.eu/sectors/renewable-energy/overview/#nav), including recycling companies, challenges in the recycling phase, etc. Detailed information on wind blade recycling is provided in the [State of Play Report on the Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).
    type: Recycling
  - title: Gold Mining
    text: >-
      Gold (Au) is not listed on the EU’s critical raw material list. Gold is a
      key mineral for the production of electronics due to its excellent
      conductivity, malleability and resistance to corrosion. Production of
      electronics account to the largest industrial demand of gold (representing
      6% to 6.5% of annual global gold demand). Gold is mined on all continents
      of the world except Antarctica; around 25% comes from Africa, while only
      1% is mined in Europe. Gold is mainly mined in large scale mining, but
      also around 15% of global supply comes from artisanal and small-scale
      mining (supporting the livelihoods of millions of people). Both can result
      in adverse human rights and environmental impacts.\

      Further information on gold is provided under the EEE (electrical and electronic equipment) sectors including primary production, challenges in the extraction phase, major mining companies. More detailed information on gold can be found in the [State of Play Report on the EEE sector](https://re-sourcing.eu/reports/final-sop-eees). The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Gold) also provides information on gold.
    type: Mining
  - title: Tantalum Mining
    text: Tantalum (Ta) is listed on the EU’s critical raw materials list. Tantalum
      is a rare metal that is very resistant to corrosion. The main application
      is to produce capacitors, where the electronics and telecommunications
      industries are the main consumer. With the shift to electric cars, the
      mobility sector is also increasing its tantalum consumption. Of all
      metals, tantalum has the highest share of artisanal and small-scale
      production with around 60% in the last 10 years. Major tantalum mining
      area is the Great Lakes region in Africa. Other important mining locations
      are in Brazil and Nigeria. Major mining risks are land conflicts, erosion
      and deforestation as well as poor working conditions and health and safety
      issues in artisanal mining (which is often done informally). Further
      information on tantalum is provided under the EEE sectors including
      primary production, challenges in the extraction phase, major mining
      companies. More detailed information on tantalum can be found in the
      [State of Play Report on the EEE
      sector](https://re-sourcing.eu/reports/final-sop-eees).
      The [Raw Materials Information
      System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Tantalum) also provides
      information on tantalum.
    type: Mining
  - title: Tin Mining
    text: >-
      Tin (Sn) is not listed on the EU’s critical raw material list and is one
      of the oldest known metals of humans. Tin’s low melting point makes it
      ideal for solders, which is the main application (mostly for electronics).
      In the electronics industry solders are used for instance for soldering
      electrical and electronics circuits. Main producing countries are China,
      Indonesia, and Myanmar accounting for more than 50% of global tin
      production. Tin production takes place onshore as well as offshore,
      representing different environmental challenges. Tin is mined in large
      scale as well as artisanal and small-scale mining. Both present serious
      social and environmental impacts.\

      Further information on tin is provided under the EEE sectors including primary production, challenges in the extraction phase, major mining companies. More detailed information on tin can be found in the [State of Play Report on the EEE sector](https://re-sourcing.eu/reports/final-sop-eees). The [Raw Materials Information System ](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Tin)also provides information on tin.
    type: Mining
  - title: Tungsten Mining
    text: >-
      Tungsten (W) is listed on the EU’s critical raw material list. It has the
      highest melting point of all metals and is used as an alloy to strengthen
      other metals. The largest end-use of tungsten is as tungsten carbide,
      which is utilised for the manufacturing of electronics, for instance in
      the vibration motors of mobile phones. China is by far the largest
      producer of tungsten with more that 80% of global production. While
      tungsten itself is non-toxic to humans, occupational exposure to tungsten
      can result in serious health impacts. Also, mine waste from tungsten poses
      serious threats to the environment.\

      Further information on tungsten is provided under the EEE sectors including primary production, challenges in the extraction phase, major mining companies. More detailed information on tungsten can be found in the [State of Play Report on the EEE sector](https://re-sourcing.eu/reports/final-sop-eees). The [Raw Materials Information System](https://rmis.jrc.ec.europa.eu/apps/rmp2/#/Tungsten) also provides information on tungsten.
    type: Mining
  - title: Mica Mining
    text: Mica is not listed in the EUs material lists. The extraordinary qualities
      of mica explain the wide use across many sectors. It is a perfect
      insulator in several ways due to its low thermal and electrical
      conductivity, high dielectric strength, and chemical inertness. This means
      it is resistant against extremely high temperatures and very high voltages
      and does not react to chemicals and is also very light. Sheet mica and
      splittings are used for electrical appliances and electronics like
      capacitors, resistors, insulators, encoders and DRAM. In the automotive
      sector mica is used for paints, coatings, brake and clutch pads,
      batteries, components in the motor, cylinders, compressors, plastics, LED
      lamps, pumps and various electronic parts. The biggest mine production of
      mica is in China, followed by India, Canada, Madagascar, and France.
      Mining of mica often takes place as informal artisanal and small-scale
      mining, which is associated with extremely low wages, poor and unsafe
      working conditions, child labour. Further information on mica is provided
      under the EEE sectors including primary production, challenges in the
      extraction phase, major mining companies. More detailed information on
      mica can be found in the [State of Play Report on the EEE
      sector](https://re-sourcing.eu/reports/final-sop-eees).
    type: Mining
  - title: Electronics Production
    text: Consumers mainly know the brands of electronics like Apple, Samsung, Dell,
      HP. But the production of the electronics itself is mainly done by often
      unknown and labor intensive contract or component manufacturers like Han
      Hai (Foxconn), Jabil, Flex or Taiwan Semiconductor. China continues to be
      the world’s largest electronics manufacturing hub. In 2018, China
      dominated the global exports of mobile phones (57%), computers and tablets
      (49%), and household electrical goods (43%). Other important manufacturing
      hubs include Vietnam, Indonesia, the Philippines, Malaysia, India, Mexico,
      and Brazil. Civil society, trade unions, and academics have documented
      serious violations of human rights in electronics contract manufacturing.
      Further information on electronics production is provided under the EEE
      Sector and in the [State of Play Report on the Electronics
      Sector](https://re-sourcing.eu/reports/final-sop-eees),
      including main producing companies globally and in Europe, and social and
      environmental challenges associated with (contract) manufacturers’
      activities and business relationships.
    type: Manufacturing
---

Responsible sourcing practices are embedded in the wider sustainability agenda; they are a means to an end. Numerous approaches have been developed by industry and civil society to address responsible sourcing challenges, across global value chains, ranging from employing safe and fair labour practices, protecting human rights, safeguarding financial integrity, mitigating impacts on the environment linked to operations, the social impact on communities and inter-related business practices, to name a few. For a greater discussion on the business case for responsible sourcing, please [see ‘Responsible Sourcing: The Case for Business Competitiveness’](https://re-sourcing.eu/reports/re-sourcing-briefing-document-1) briefing document from the project team.

This Wiki page outlines the RE-SOURCING Project’s area of focus, the minerals it is taking into consideration and the specific mining, manufacturing and recycling targets it looks at. A detailed description of the project’s approach can be found in the report: [The RE-SOURCING Common Approach](https://re-sourcing.eu/reports/d12-in-rs-template-final).
