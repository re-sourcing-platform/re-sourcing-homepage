---
title: About
template: about-page
tabs:
  - title: Business Stakeholders
    cards:
      - icon: warehouse
        title: Increased capacity
        body: >
          Increased capacity for implementing responsible business conduct and
          compliance with current and upcoming regulations
      - icon: brain
        title: Better understanding
        body: >
          Better understanding of Responsible Sourcing and your role in it in
          three key sectors of the EU’s Green Deal: automotive, electric and
          electronic equipment, and renewable energy
      - icon: recycle
        title: Stable conditions
        body: Lasting and stable sectoral framework conditions of RS for competitive and
          future-oriented EU markets
  - title: EU Policymakers
    cards:
      - icon: warehouse
        title: Increased capacity
        body: |
          Increased capacity for RS policy design and implementation
      - icon: award
        title: Innovative ideas
        body: >
          Innovative ideas on policy recommendations for stimulating RS in the
          private sector
      - icon: brain
        title: Better understanding
        body: >
          Better understanding and awareness of RS in three sectors: automotive,
          electric and electronic equipment, and renewable energy
  - title: Civil Society
    cards:
      - icon: compress-arrows-alt
        title: Integration
        body: >
          Integration of sustainable development and environmental concerns into
          the RS discourse
      - icon: equals
        title: Level playing field
        body: >
          an established global level playing field of RS in international
          political fora and business agendas
      - icon: brain
        title: Better understanding
        body: "Better understanding and awareness of RS in three sectors: automotive,
          electric and electronic equipment, and renewable energy"
sections:
  - title: "Major RE-SOURCING outcomes are tailored to achieve the following impacts:"
    cards:
      - icon: map-signs
        title: Established roadmaps
        body: >
          Established three EU sector roadmaps for wider adoption to create
          effective RS framework conditions that will carry future
          competitiveness and commitment of key European players
      - icon: people-carry
        title: Vibrant community
        body: >
          Built a vibrant and active community, consisting of ambitious and
          future-oriented businesses, policymakers and international experts
          with the commitment to implement RE-SOURCING best practices and
          inspire future RS initiatives
      - icon: book-reader
        title: Trained leaders
        body: >
          Trained leaders in business, politics and civil society on what is
          required from them in order to achieve Responsible Sourcing
      - icon: equals
        title: Level playing field
        body: Created a level playing field of RS on the global level by promoting RS
          principles in global agendas and  international initiatives across 3
          world regions (SSA, Asia, LA)
  - title: "Purpose and objectives at Project end:"
    cards:
      - icon: language
        title: Widely used
        body: >
          The RE-SOURCING Platform is a well-known and widely used hub for
          information provision, networking and knowledge exchange among key
          stakeholders
      - icon: book-open
        title: Accelerated uptake
        body: >
          Substantially expanded the knowledge base on effective RS practices
          (RE-SOURCING FS Cases), which will accelerate uptake and support the
          mainstreaming of RS in business practices
      - icon: chart-line
        title: Improved perception
        body: Contributed to an improved perception and shift of attitudes among key
          business players, consultants and industry initiatives, as well as
          public policy that RS provides a competitive advantage and has
          acquired a deeper understanding of its practical implementation
---
