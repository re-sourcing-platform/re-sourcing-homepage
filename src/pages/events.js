import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"
import OptionsBar from "../components/layout/OptionsBar"
import EventsCard from "../components/layout/cards/EventsCard"
import moment from "moment-timezone"

const EventsPage = ({ data, location }) => {
  const events = data.events.nodes
  const navOptions = ["upcoming", "past events"]
  const [navOptionActive, setNavOptionActive] = useState(0)

  let upcomingEvents = []
  let archivedEvents = []

  upcomingEvents = events.filter(event => {
    let now = Date.now()
    let startTime = new Date(event.frontmatter.utc)
    return startTime > now
  })

  archivedEvents = events.filter(event => {
    let now = Date.now()
    let startTime = new Date(event.frontmatter.utc)
    return startTime < now
  })

  let choice
  if (navOptionActive === 0) choice = upcomingEvents
  else if (navOptionActive === 1) choice = archivedEvents.reverse()
  return (
    <Layout
      pageName="Events"
      location={location}
      metaDescription="Find info about the RE-SOURCING project events. In four years the Project will host three Flagship Labs, two Conferences, three Roadmap Workshops, three Global Advocacy Fora, several online webinars and consultations for the RE-SOURCING platform."
    >
      <PageHeader title="EVENTS" />
      {/* CONTENT */}
      <Container>
        <OptionsBar
          navOptions={navOptions}
          onChange={e => setNavOptionActive(e)}
        />

        <div className="grid grid-cols-1 gap-4 lg:grid-cols-2 lg:gap-8 px-4 md:px-0">
          {choice.map((event, i) => {
            const {
              registerLink,
              date_start,
              monthStart,
              date_end,
              month_only,
              featuredImage,
              title,
              subtitle1,
              subtitle2,
              virtualEvent,
              eventLocation,
            } = event.frontmatter

            let event_start = moment(date_start)
              .tz("Europe/Vienna")
              .format("MMM D YYYY, h:mm A")
            let event_end = moment(date_end)
              .tz("Europe/Vienna")
              .format("MMM D YYYY, h:mm A")
            return (
              <div key={i} className="col-span-1">
                <EventsCard
                  key={i}
                  imageData={
                    featuredImage?.childImageSharp?.gatsbyImageData
                      ? featuredImage.childImageSharp.gatsbyImageData
                      : data.defaultImage?.childImageSharp?.gatsbyImageData
                  }
                  title={title}
                  subtitle1={subtitle1}
                  subtitle2={subtitle2}
                  slug={event.fields.slug}
                  registerLink={navOptionActive === 0 && registerLink}
                  date_start={month_only ? monthStart : `${event_start} CET`}
                  date_end={month_only ? null : `${event_end} CET`}
                  location={virtualEvent ? "Virtual Event" : eventLocation}
                />
              </div>
            )
          })}
        </div>
      </Container>
    </Layout>
  )
}

export default EventsPage

export const query = graphql`
  query eventsData {
    events: allMarkdownRemark(
      filter: { frontmatter: { template: { eq: "event-post" } } }
      sort: { fields: frontmatter___date_start, order: ASC }
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          title
          subtitle1
          subtitle2
          registerLink
          date_start
          monthStart: date_start(formatString: "MMMM YYYY")
          date_end
          month_only
          utc: date_start
          virtualEvent
          eventLocation: location
          featuredImage {
            childImageSharp {
              gatsbyImageData(width: 250, placeholder: BLURRED)
            }
          }
        }
      }
    }

    defaultImage: file(relativePath: { eq: "re-sourcing_transparent.png" }) {
      childImageSharp {
        gatsbyImageData(
          width: 215
          placeholder: BLURRED
          transformOptions: { fit: CONTAIN }
        )
      }
    }
  }
`
