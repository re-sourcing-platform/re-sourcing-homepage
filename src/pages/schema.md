---
title: WIKI
subtitle: " "
featuredImage: /files/responsible-sourcing.jpg
assets:
  - content_type: file
    youtube_link: https://www.youtube.com/watch?v=X0ilZMyLZG4
dropdowns:
  - title: The RE-SOURCING Project Scope
    text: "The RE-SOURCING project is an EU-funded Multi-Stakeholder Platform that
    aims to advance Responsible Sourcing of raw materials along and across
    global mineral value chains. It strives to promote both strategic agenda
    setting and coherent application of practices for Responsible Sourcing. 12
    partners aim at developing criteria for a responsible sourcing definition.
    The project focuses on three sectors: Renewable Energy, Mobility and
    Electronics. For more details please see the Sectors page."
    type: Glossary
sector: "/sectors/mobility/"
days:
  - title: MONDAY, NOVEMBER 8
    theme:
      Supply Chain Due Diligence - How to move towards meaningful and holistic
      impact?
    description: ""
    agendaItem:
      - title: Conference opening
        body:
          "**Setting the Stage: Are sustainability and responsibility strategic
          success factors for corporations?** by Prof. [André
          Martinuzzi](https://www.linkedin.com/in/andre-martinuzzi-92741a49/)
          (Vienna University of Economics and Business, Institute for Managing
          Sustainability)"
        hour: 13
        minute: 45
        endTime: 14:00  CET
        description: Welcome and opening note
    resourceFile:
      - title: "Assessing impacts of responsible sourcing initiatives for cobalt: Insights from a case study"
        subtitle:
          "Lucia Mancini, Nicolas A. Eslava, Marzia Traverso, Fabrice Mathieux in
          'Resources Policy' 71 (2021)"
        author: ""
        date: ""
        icon: "document"
        excerpt: "Max one sentence Participant Information Sheet Participant Information Sheet"
        file: /files/1.fairmined_trans.png
        link: https://www.gatsbyjs.com/plugins/gatsby-plugin-modal-routing/
password: ABC123
protectedContent: Lorem Ipsum
---

Schema is for all optional fields in the CMS so that Gatsby won't break, when there is no actual content - the GraphlQL schema is derived from filled content.
