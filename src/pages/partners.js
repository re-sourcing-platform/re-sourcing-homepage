import React, { useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import PageHeader from "../components/layout/headers/PageHeader"
import PersonCard from "../components/layout/cards/PersonCard"
import DetailsCardMin from "../components/layout/cards/DetailsCardMin"
import OptionsBar from "../components/layout/OptionsBar"

const PartnersPage = ({ data, location }) => {
  const advisoryBoardMembers = data.partnerPersons.frontmatter.partner.filter(
    person => person.type === "ADVISORY BOARD"
  )
  const committeeMembers = data.partnerPersons.frontmatter.partner.filter(
    person => person.type === "PLATFORM STEERING COMMITTEE"
  )

  const partners = data.partners.nodes
  const [navOptionActive, setNavOptionActive] = useState(0)

  const navOptions = ["PARTNERS", "STEERING COMMITTEE", "ADVISORY BOARD"]

  const partnersSection = (
    <div className="w-full grid grid-cols-1 lg:grid-cols-2 gap-4 lg:gap-6">
      {partners.map((partner, i) => {
        const { legalName, homepage, country, logoImage } = partner.frontmatter
        return (
          <DetailsCardMin
            key={i}
            title={legalName}
            subtitle2={country}
            bodyText={homepage}
            slug={partner.fields.slug}
            imageData={logoImage.childImageSharp.gatsbyImageData}
            className="col-span-1"
          />
        )
      })}
    </div>
  )

  const advisoryBoard = (
    <div className="flex justify-center gap-6 flex-wrap">
      {advisoryBoardMembers.map((person, i) => (
        <PersonCard
          src={person.headshotImage?.relativePath}
          imageData={person.headshotImage?.childImageSharp?.gatsbyImageData}
          name={person.name}
          className="w-56 mx-4"
          key={i}
        >
          <div className="text-primary3 text-center text-sm">
            <p>{person.institution}</p>
            {person.subInstitution && (
              <>
                <p>•</p>
                <p>{person.subInstitution}</p>
              </>
            )}
            {person.position && person.position.trim().length > 0 && (
              <>
                <p>•</p>
                <p>{person.position}</p>
              </>
            )}
          </div>
        </PersonCard>
      ))}
    </div>
  )

  const committee = (
    <div className="flex justify-center gap-6 flex-wrap">
      {committeeMembers.map((person, i) => (
        <PersonCard
          src={person.headshotImage?.relativePath}
          imageData={person.headshotImage?.childImageSharp?.gatsbyImageData}
          name={person.name}
          className="w-56 mx-4"
          key={i}
        >
          <div className="text-primary3 text-center text-sm">
            <p>{person.institution}</p>
            {person.subInstitution && (
              <>
                <p>•</p>
                <p>{person.subInstitution}</p>
              </>
            )}
            {person.position && (
              <>
                <p>•</p>
                <p>{person.position}</p>
              </>
            )}
          </div>
        </PersonCard>
      ))}
    </div>
  )

  let currentContent
  let sectionTitle
  let sectionText

  if (navOptionActive === 0) {
    currentContent = partnersSection
    sectionTitle = "Partners"
    sectionText = `The RE-SOURCING project is implemented by a consortium of 12
    partners from 9 countries:`
  } else if (navOptionActive === 1) {
    currentContent = committee
    sectionTitle = "Steering Committee"
    sectionText = `The RE-SOURCING Platform Steering Committee (PSC) is a board of high-level valued experts from organisations with a strong role in agenda setting & advocacy for Responsible Sourcing across Europe and globally.`
  } else if (navOptionActive === 2) {
    currentContent = advisoryBoard
    sectionTitle = "Advisory Board"
    sectionText = `The RE-SOURCING Advisory Board consists of high-level valued experts who will meet regularly with the RE-SOURCING project. The Advisory Board provides high-level advice to the project, ensuring that all relevant aspects and perspectives are considered in the RE-SOURCING project.`
  }

  return (
    <Layout
      pageName="Consortium"
      location={location}
      metaDescription="The RE-SOURCING project is implemented by a consortium of 12 partners from 9 countries. The project is also aided by the high-level valued experts of the Platform Steering Committee and th eAdvisory Board"
    >
      <PageHeader title="consortium" />
      <Container>
        {/* Nav BOX*/}
        <OptionsBar
          navOptions={navOptions}
          onChange={value => setNavOptionActive(value)}
        />
      </Container>
      <div className="container mx-auto relative z-20 px-4 md:px-24">
        {/* Text area */}
        <div className="mb-8">
          <div className="text-center md:w-1/2 mx-auto">
            <h1 className="text-2xl md:text-4xl pb-4 font-semibold text-primary1">
              {sectionTitle}
            </h1>
          </div>
          <div className="text-center md:w-5/6 mx-auto">
            <h3 className="text-primary2 text-md md:text-lg font-medium">
              {sectionText}
            </h3>
          </div>
        </div>
        {/* Content cards */}
        {currentContent}
      </div>
    </Layout>
  )
}

export default PartnersPage

export const query = graphql`
  query {
    partners: allMarkdownRemark(
      filter: { frontmatter: { template: { eq: "consortium-post" } } }
    ) {
      nodes {
        frontmatter {
          title
          type
          legalName
          country
          homepage
          logoImage {
            childImageSharp {
              gatsbyImageData(
                height: 190
                layout: CONSTRAINED
                placeholder: TRACED_SVG
              )
            }
          }
        }
        fields {
          slug
        }
      }
    }
    partnerPersons: markdownRemark(frontmatter: { title: { eq: "Members" } }) {
      frontmatter {
        partner {
          institution
          subInstitution
          name
          position
          type
          headshotImage {
            relativePath
            childImageSharp {
              gatsbyImageData(
                width: 250
                layout: CONSTRAINED
                placeholder: BLURRED
              )
            }
          }
        }
      }
    }
  }
`
