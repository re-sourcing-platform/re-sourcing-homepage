---
title: Privacy Policy
subtitle: null
template: privacy-page
agendaMessage: ""
---

## Introduction

Thank you for visiting the website of the RE-SOURCING project which aims to establish a global stakeholder platform for responsible sourcing.

RE-SOURCING is funded by the European Commission's Horizon 2020 programme and runs from 1 November 2019 - 31 October 2023. The project is implemented by an international consortium coordinated by the Institute for Managing Sustainability at the Vienna University of Economics and Business. The RE-SOURCING consortium holds joint control over the RE-SOURCING website within the meaning of Art. 26 GDPR.

## Scope

This Policy explains which personal data are collected during visits to the RE-SOURCING website or when content provided under the “re-sourcing.eu” and “www.re-sourcing.eu” domains is accessed and how the RE-SOURCING project deals with these data. The Policy also provides information on the rights of data subjects pursuant to data protection law.

## Object and Purpose

The RE-SOURCING consortium seeks to ensure the privacy of its website users.
This Policy explains which personal data are collected during visits to the RE-SOURCING website or when content provided under the “re-sourcing.eu” and “www.re-sourcing.eu” domains is accessed and how the RE-SOURCING project deals with these data.
The Policy also provides information on the rights of data subjects pursuant to data protection law. This Policy refers to both the private and public sections of the RE-SOURCING virtual community platform. For competitions (such as our video competition), available on our website, additional terms and conditions and privacy policies apply.
The RE-SOURCING project consortium partners may run individual websites. The respective consortium partners are responsible for the content provided there. The data collected on these websites may include additional types of personal data that are not listed in this Policy.
By using this website, you consent to the personal information practices described in this Privacy Policy. The RE-SOURCING consortium reserves the right to update or change this Privacy Policy at any time.

## Data collected when visiting the website

In line with common practice, the RE-SOURCING project collects those data that are transmitted automatically during visits of its publicly accessible website. These data are recorded in server log files. This applies to the following types of data:

The IP address from which the website is accessed

- Access date and time
- Access method (e.g. GET)
- URL visited
- Access protocol (e.g. HTTP/1.1)
- HTTP response code
- File size
- Previously visited URL (referrer URL)
- Operating system and browser type (user agent)

Log files are saved for a period of 3 months.

The RE-SOURCING website uses this information to generate anonymized usage statistics, to ensure correct operation, and to monitor website performance. The RE-SOURCING consortium’s legitimate interests are the legal basis for processing this data.

In addition to the data collected when visiting the website, you can subscribe to our RE-SOURCING stakeholder network to receive newsletters, event invitations, and project results, and to participate in project activities. The subscription is hosted by the RE-SOURCING project on Mailchimp and requires the collection of your name, email and organisation. The collection and use of personal data on MailChimp is subject to the [Privacy Policy](https://mailchimp.com/legal/privacy) and [Data Processing Addendum](https://mailchimp.com/legal/data-processing-addendum/) including the EU Standard Contractual Clauses for data transfers from the EU to third countries. For further information, please also refer to the GDPR Information Sheet during the subscription or contact us via [info@re-sourding.eu](mailto:info@re-sourding.eu).

The website also directly links to video content on YouTube via plug-ins. The use of YouTube is subject to [YouTube’s Privacy Policy](https://policies.google.com/privacy) and confirms to rely on the EU model contract clauses for data transfers from the EU to third countries.

In addition, the collected informationmay be securely stored on the project-internal Microsoft SharePoint system hosted by WU, subject to WU privacy policies and the Data Processing Agreement of Microsoft Online Services, including the EU Standard Contractual Clauses for data transfers from the EU to third countries. A copy of the Data Processing Agreement is being kept on file by the RE-SOURCING project.Access to the SharePoint system is only granted to consortium partners. For further information regarding the system’s data protection procedures, please contactinfo@re-sourcing.eu.

The website also provides links to event registrations using service providers such as Eventbrite or MS Forms. For the use of these services, the respective data privacy policies and Data Processing Agreements apply. No registration data is being collected or stored on the RE-SOURCING website. The joint control of the RE-SOURCING website does not mean joint control over event registrations. Any personal data collected during event registrations remain under the exclusive control of the Party that organizes the event.

## Cookies

The RE-SOURCING website does not use any cookies.

## Transmission of Collected Data

The RE-SOURCING consortium does not transmit the personal data collected on the publicly accessible RE-SOURCING website to third parties, unless otherwise specified in statutory provisions and in this Privacy Policy.
The personal data you provide when subscribing to our stakeholder network will only be accessible to the RE-SOURCING consortium that is developing and operating the Global Stakeholder Platform (not to other organisations or persons outside of the RE-SOURCING consortium) via MailChimp and the project-internal MS SharePoint system. For more information, please refer to the GDPR Information Sheet during subscription or contact us via [info@re-sourcing.eu](mailto:info@re-sourcing.eu).

Likewise, any data collected during event registrations will only be accessible to the RE-SOURCING consortium. For further information, please refer to the Participant Information Sheet that is provided during the event registration.

## Data Subject Rights

Pursuant to the applicable data protection laws, in particular Articles 15 through 21 of the General Data Protection Regulation (GDPR), data subjects at all times have the following rights:

- The right to obtain information about any of their personal data that are being processed
- The right to rectification of inaccurate information and the completion of incomplete data
- The right to erasure of personal data, which may however be limited in certain cases specified by law, especially due to statutory data retention periods the RE-SOURCING project is subject to, the right of freedom of expression, archiving purposes in the public interest, or scientific research purposes
- The right to restriction of processing
- The right to receive the personal data they have provided in a structured, commonly used, and machine-readable format (right to data portability)
- The right to object, on grounds relating to the data subject’s particular situation, to the processing of personal data. This applies if the data are processed for the performance of a task carried out in the public interest, in the exercise of official authority vested in RE-SOURCING project, or based on a comparative assessment of interests. If a data subject lodges such an objection, the RE-SOURCING project will no longer process his or her personal data, unless the RE-SOURCING project demonstrates compelling legitimate grounds for the processing which override the interests, rights, and freedoms of the data subject or unless the data processing is required for the establishment, exercise, or defense of legal claims (GDPR Art. 21 (1).
- The right to object to the processing of personal data concerning the data subject for purposes of direct marketing. If a data subject lodges such an objection, the RE-SOURCING project will no longer process his or her personal data for these purposes.
- Where personal data are processed for scientific or historical research purposes or statistical purposes pursuant to Article 89(1), the data subject, on grounds relating to his or her particular situation, shall have the right to object to processing of personal data concerning him or her, unless the processing is necessary for the performance of a task carried out for reasons of public interest.

Requests to exercise any of these rights can be directed to the office indicated in the “Joint Contact Point” section below.

Some of the services offered on the RE-SOURCING website that require a login allow the user to change or amend their personal data themselves.

In addition, data subjects have the right to file a complaint with the competent supervisory authority (e.g. that of your host country or of the data controller), if they feel that their rights may have been violated or that the RE-SOURCING project may have violated its obligations under data protection law.

## Data Security

The RE-SOURCING consortium takes appropriate technical and organizational measures to protect the data subjects’ personal data against accidental or unlawful destruction, loss, and unauthorized access.

## Joint Contact Point

For questions or requests related to data protection, data subjects can contact the RE-SOURCING project’s Joint Contact Point:

**Joint Contact Point**

Vienna University of Economics and Business\
Institute for Managing Sustainability
Welthandelsplatz 1, Building D1
A-1020 Vienna
Austria

Email address:
[info@re-sourcing.eu](mailto:info@re-sourcing.eu)

**Responsible DPO:**

Vienna University of Economics and Business
Data protection officer
Welthandelsplatz 1, Gebäude AD
1020 Vienna, Austria
datenschutzbeauftragter@wu.ac.at
