import React from "react"
import { Link } from "gatsby"

const Page404 = () => {
  return (
    <div className="text-center pt-32">
      <h1 className="text-primary2 text-6xl">Page not found</h1>
      <p className="text-primary3 text-2xl">
        Oops! The page you are looking for has been removed or relocated.
      </p>
      <Link to="/">Go to Re-sourcing Homepage</Link>
    </div>
  )
}

export default Page404
