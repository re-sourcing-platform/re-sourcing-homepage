---
template: sector-page
title: EEE
subtitle: Way Forward
sector: /sectors/electronics-and-electronic-equipment/
sort_order: 6
---
**Vision, Roadmap & Guidance documents**

The objective of developing a state of play review of the electronics sector for the RE-SOURCING Project is to identify what has been achieved and what needs to be done for the future to achieve a level playing field, in implementing RS practices, in the electronics sector.

With that in mind, the project team has developed a Vision, which visualizes the ideal practices in the future electronics sector, to be achieved by 2050. The Vision has been prepared in consultation with experts from the Project’s Platform Steering Committee (PSC) and the Advisory Board (AB).

The Vision for the EEES is focused on three main pillars. First, businesses and States achieve full respect for and protection of human rights across all entire value chain operations including effective mechanisms for accountability and access to remedy for affected rights holders. Second is the imperative of protecting the environment, including remaining within planetary boundaries, preventing global warming of more than 1.5°C above pre-industrial levels, and preventing further biodiversity loss. Third is the global eradication of poverty and a significant reduction of inequality that includes a minimum social foundation and a fair share of costs and benefits among the value chain actors.

Over the course of the project, the team will be developing a roadmap, in consultation with important stakeholders and experts from industry, policy and civil society, to provide a step-by-step approach towards achieving the vision.

![](/files/vision_eee_sector.jpg)