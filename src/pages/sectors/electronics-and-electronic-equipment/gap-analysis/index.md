---
template: sector-page
title: EEE
subtitle: Gap Analysis
sector: /sectors/electronics-and-electronic-equipment/
sort_order: 4
---
The gap analysis undertaken by the RE-SOURCING project indicates areas where further engagement and actions are required to support sustainable and responsible practices in the electronics sector.

**Too narrow focus and lack of transparency of the EU Conflict Minerals Regulation**

The EU Conflict Minerals Regulation’s focus on 3TG minerals is too limited, because there are also other minerals that tend to support conflict financing, including cobalt, graphite, lithium, and nickel. Also, the Regulation does not apply to the import of manufactured goods, such as electronics and cars, into the EU, despite high-risk sourcing practices in these supply chains.

Other shortcomings of the Regulation relate to: **the set** **threshold that allows loopholes; insufficient sanctions; the different implementation per Member State; and lack of transparency**. The **lack of transparency is the biggest obstacle** that negatively affects the effectiveness of the Regulation. The fact that the names of EU importers that fall under the Regulation will not be published hampers monitoring of implementation. This also applies to the criteria and assessment process used for making the *“white list” of smelters and refiners*[](#_ftn1) (to help companies, the European Commission will create a so-called “white list” of global smelters and refiners that source responsibly) and the assessment process for recognising existing voluntary supply chain due diligence schemes as implementation schemes

 With regards to industry due diligence schemes the following gaps have been identified by different studies.

**Lack of transparency regarding implementation of voluntary scheme by country**

A study conducted by the OECD in 2018 assessing five industry schemes found that most researched schemes were close to full alignment on paper with the OECD DDG but implementation remained as the biggest gap. Also, that there is a lack of transparency regarding the degree of implementation by scheme members. Both the OECD DDG and the supplement to the EU Regulation emphasise that simply the participation in a recognised voluntary DD scheme is not sufficient to comply; companies retain individual responsibility to comply with their due diligence obligations. The reasoning behind this is also that participation does not guarantee implementation in the company.

**Scope of due diligence too limited**

Due diligence is often limited to the first tier of suppliers of smelters and refiners, and there is a lack of on-the-ground due diligence. There are insufficient supporting programmes to strengthen supplier capacity to implement due diligence. Smelters and refiners tended to disengage if risks were identified. Main incentive for industries to undertake due diligence is the reputational risk.

The scope of due diligence as defined by the OECD DDG is too limited with the segmented approach. In practice, the OECD DDG suggests to downstream companies such as electronics brands that auditing the smelters and refiners upstream is enough – whereas many human rights violations are not at the smelters/refiners’ level but at the mining level. Furthermore, companies do not currently undertake exclusive environmental and climate-related due diligence.

**Credibility of audits**

Many audits are over focused on documentation checks, and auditors lack critical analysis competencies. The reliance on audits as enforcement mechanism is too much. Instead, ongoing monitoring and beyond-audit due diligence activities are needed. Furthermore, there is an issue of potential bias of the auditors – conflict of interest – due to financial dependencies, because auditors are paid by the companies that have an interest in achieving what they would consider “successful” outcomes from the audit. Often, there is also a lack of involvement of stakeholders in the audit process, and the results are kept secret from rights holders.

**Protection of human rights**

International mandatory due diligence regulation is crucial to protect human rights in the mineral supply chain, but this is still lacking. The US conflict minerals legislation adopted in 2010 had a great effect and has catalysed numerous standards and initiatives that indicate greater awareness among companies and across sectors. However, the schemes do not ensure implementation of HRDD, and authorities cannot transfer their responsibility to regulate companies to voluntary schemes.

Planned EU legislation for mandatory human rights due diligence is urgently needed, and it is important that it goes beyond current national mandatory HRDD legislation, which is too vague and not comprehensive enough. Next to the EU legislation, the UN binding Treaty on Business and Human Rights needs to become reality.

**Protection of the environment**

The externalisation of costs – when the price of a product does not take negative externalities such as pollution or greenhouse gas emissions into account – can lead to high private returns and at the same time costs for society as a whole. However, this hampers a long-term perspective on environmental sustainability and the fundamental changes needed. These necessary fundamental systemic changes are currently not sufficiently on the agenda of businesses and policymakers as they require new business models. Reduction in resource consumption is key to protect the environment, including preventing global warming of more than 1.5°C above pre-industrial levels and preventing further biodiversity loss. The design of new electronics products should be focused on longer use, reuse, and recyclability. Electronics brand companies should make it a priority to develop products with minimal use of newly mined minerals. For instance, The EU can set take-back and recycling targets or create consumer demand for circular electronics products.

**The fight against poverty and inequality**

There is a gap between current due diligence efforts and the rights holders who are still insufficiently protected, and the lack of impact on the ground, for instance at artisanal mining sites.

Important reasons why improvements on the ground are limited is that too few resources are invested in supporting initiatives on the ground, for instance those that aim to formalise the ASM sectors and certify artisanal mining sites. Another gap concerns the lack of attention paid to local actors. Also, local actors are not sufficiently represented in international supply chain initiatives.