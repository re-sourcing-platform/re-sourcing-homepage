---
template: sector-page
title: EEE
subtitle: Key Players
sector: /sectors/electronics-and-electronic-equipment/
sort_order: 3
---
Key players are identified as countries and companies across the supply chain for the electronics sector.

## Mining

**Tin** is mainly produced in China (30%) and Indonesia (24%), followed by Myanmar (12%), Peru (7%), the DRC (6%), Brazil (5%), and Bolivia (5%).

Accounting for 82% of global supply, China is the largest producer of **Tungsten.** Other countries as Vietnam (5%), Russia (2,6%), Mongolia (2.2%), Bolivia (1.6%), and Rwanda (1.2%) play a minor role.

**Tantalum** is mainly produced in the Great Lakes region in Africa, followed by Brazil, other African producers and Australia.

**Gold** is mined on all continents of the world except Antarctica; around 25% comes from Africa, while only 1% is mined in Europe.

**Mica** is mainly produced in China, followed by India, Canada, Madagascar, and France. Sheet mica is the grade especially used for electrical appliances and electronics, with the main exporting country Madagascar, followed by India, China, and Brazil.

## Manufacturing

Electronics brands design, brand, and sell the finished products (for instance mobile phones, tablets, or computers) to the public. Popular brands include Apple, Samsung, LG, Dell, and HP, etc.

While a few electronics brands remain vertically integrated to a certain degree (especially South Korean and Japanese companies such as Samsung, LG, and Panasonic), most outsource manufacturing to contract manufacturers. According to some estimates, around two-thirds of production is outsourced.

China is the largest manufacturer of electronics, dominating in 2018 the global exports of mobile phones (57%), computers and tablets (49%), and household electrical goods (43%). Other important manufacturing hubs include Vietnam, Indonesia, the Philippines, Malaysia, India, Mexico, and Brazil. Some contract manufacturers also have significant production facilities based in Europe, including in Hungary, Poland, Romania, Slovakia, and the Czech Republic.

Two-thirds of the largest contract manufacturers are Taiwanese with global operations, concentrating around 80% of the total market. The largest is Hon Hai, better known as Foxconn.

The semiconductor production is dominated by a few well-established companies, major companies include the Taiwan Semiconductor Manufacturing Company, Intel Corporation, and Texas Instruments.