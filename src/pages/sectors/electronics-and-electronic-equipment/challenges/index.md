---
template: sector-page
title: EEE
subtitle: Challenges
sector: /sectors/electronics-and-electronic-equipment/
sort_order: 2
---
Several different sustainability and responsible sourcing challenges in the mining and manufacturing step have been identified in the EEE sector.

## Mining

Human rights violations can occur in the mining and processing stage of all raw materials discussed. This includes conflicts with local communities and indigenous people, disrespect for land-rights, forced labour, violation of children’s rights, and conflicts between artisanal and large-scale mining activity. Artisanal mining and small-scale mining in particular are associated with accidents. These challenges can be aggravated by corruption and organised crime.

Currently many mining operations are cause for large environmental impacts. These are: land use, tailings, waste rock, acid mine draining (AMD), energy consumption, air emissions & wastewater, pollution of air, water, food, and soil. For example, mercury pollution in gold mining is a challenge

Health and safety of both, workers and local communities are important operational aspects.

These challenges are outlined in detail in Chapter 2 of the [State of Play & Roadmap Concepts: Electronics Sector Report](https://re-sourcing.eu/reports/final-sop-eees). 

## Manufacturing

For many years, civil society organisations, trade unions, and academics have documented cases and allegations of electronics brands causing, contributing to, or being directly linked to serious social and environmental impacts along the entire electronics supply chain.

In 2020, the Corporate Human Rights Benchmark assessed 44 of the largest ICT companies against core indicators based on the UN Guiding Principles, including the companies’ governance and policy commitments, human rights due diligence, remedies, and grievance mechanisms. The average score of the ICT companies was a low 7.9 out of 26. Other important findings were that only 14% of the companies had commitments to the ILO core labour standards; only 23% committed to providing remedy for adverse impacts; and no company demonstrated commitments to work with suppliers and partners and not to obstruct access to remedy. Sixteen companies scored 0 with regard to disclosure of human rights due diligence. And 21 of the assessed companies were subject to at least one serious allegation of human rights abuse.

The following table includes a non-exhaustive list of recurrent issues in the electronics supply chain: exposure to hazardous substances, low wages, violation of freedom of association and collective bargaining, forced labour, child labour, excessive overtime, poor health and safety conditions, harassment, and gender discrimination.

![](/files/figure_social_environmental_risks_electronics.jpg "Social and environmental risks in the electronics supply chain")

These challenges and a description of the manufacturers are outlined in detail in Chapter 2 of the [State of Play & Roadmap Concepts: Electronics Sector Report](https://re-sourcing.eu/reports/final-sop-eees). 