---
template: sector-page
title: EEE
subtitle: Overview
sector: /sectors/electronics-and-electronic-equipment/
sort_order: 1
---

The Electronics sector is one of the largest industries in the world, with approximately 18 million workers who produce 20% of global imports in electronics products. In Europe, the revenue from the consumer electronics segment is expected to be around US$ 78mn (€ 71mn) in 2020; the sales of European semiconductors alone are expected to reach US$ 3.25bn (€ 2.97 bn) this year.

## **What challenges are we considering?**

Sub-standard working conditions and human rights violations are pervasive along the production value chain in the EEE sector, including low wages, exposure to toxic chemicals, negative environmental impacts, health & safety risks, child labour and forced labour, indigenous people’s rights, intimidation, to name a few.

The risks that are specific to the supply chain of 3T from conflict and high-risk areas and include lack of implementation of Artisanal mining zones; Child labour; Unfair compensation and Corruption. Mica in particular is associated with dire working conditions in mines.

Given the prevailing risks that arise in this sector, related to the nature of the industrial work undertaken, how can responsible sourcing practices be implemented and verified across the supply chain?

## **What segments of the value chain are we engaging?**

Given the width and depth of actors in the EEE sector, the research team will be considering: End-users; Smelters/refineries; Component & Product Manufacturers and Mining companies within the EEE value chain.

## **What questions are we asking?**

The research team will have a strong focus on solutions favouring reducing the material and energy throughput that goes into consumption and production, including material efficiency and recycling rather than focusing on mining virgin raw materials.

- How can virgin raw material extraction meet responsible sourcing requirements through verified third-party certification schemes and (mandatory) due diligence of the supply chains?
- How can we build a cross sectoral responsible sourcing approach between the mining sector, electronics and automotive industries, given that they share a number of mineral supply chains and sources?

## **What minerals are we studying?**

The research team will be focusing on:

- 3TG (Tin, Tungsten, Tantalum, Gold)
- Mica

## **How are we going to do this?**

_Road maps_: Proposed consultations starting in summer 2022.

A draft roadmap/discussion paper will be developed in cooperation with project partners (Platform Steering Committee), including expert interviews, to develop a first draft for the roadmap. The roadmap will be further elaborated during the roadmap workshops with input from all relevant stakeholders.

_Workshops_: they are expected to be held in the Summer of 2022.

_Sector state of play reports/guidelines_: The project foresees three publications for the EEE sector starting in Spring 2023:

- “State-of-Play and roadmap concepts”: A report on the current situation of the sector including the proposed process for developing the roadmap and a first draft roadmap
- “Guidelines for good practice learning and impact in the renewable energy sector”: A report on so-called flagship cases (good practice cases incl. guidance for peer-learning)
- “Renewable energy sector roadmap for responsible sourcing of raw materials”: the final roadmap including milestones of how to reach responsible sourcing until 2050 and good practice information.

## **Who should get involved?**

The research team would be interested in the views all interested stakeholders, both from the EU and the international community, particularly if you are an Electronic Equipment manufacturing (including brands) company, work with or for the Responsible Minerals Initiative/ Responsible Business Alliance (industry), part of the GoodElectronics Network (civil society, trade unions, academics), work with or for the Responsible Mica Initiative, have experience with IRMA and work with/for Mining companies involved in the extraction of raw materials under

### More information provided in 2022
