---
template: sector-page
title: Mobility
subtitle: Overview
sector: /sectors/mobility/
sort_order: 1
---
Meeting the Paris Agreement’s goals requires transformation in the mobility sector. Battery electric vehicle technology today offers a promising technology to achieve the necessary changes and transform the sector. This transformation goes hand in hand with a significant increase in the raw material demand for lithium-ion batteries. This expansion of electric vehicle production will pose increasing pressure on the primary mineral production as well as an overarching responsible recycling system and secondary raw material production. Primary production of the relevant raw materials for lithium-ion battery production as well as battery cell production and recycling are associated with potential negative impacts on environment, social and economic aspects.

The RE-SOURCING project focuses on lithium-ion batteries and the respective supply chain. The project assesses three stages of the supply chain:

1. mining, with focus on lithium, cobalt, nickel, and graphite 
2. manufacturing of cells for lithium-ion batteries
3. collection, transportation, and treatment of end-of-life lithium-ion batteries.

## **What challenges are we considering?**

In the mobility sector, a rapid transformation is needed to achieve the climate targets set out for the EU. This will create a number of challenges including:  

* A rapid increase in demand for the materials used in lithium-ion batteries, in response to the rapid market upturn in electric mobility, will require new mining projects to be brought on line as well expanding production at existing facilities. Such a rapid increase risks non-compliance with environmental and social standards as well as governance issues emerging, in response to raw material demands.   
* Technical and economic measures are needed to increase recycling yields. A significant amount of spent batteries will need to be collected and recycled in the future, requiring adequate infrastructure to facilitate recycling. Additionally, the recovery of raw materials used in lithium-ion batteries via recycling is currently not addressing all relevant raw materials, which will need to be addressed.   

![lithium ion battery value chain](/files/mobility_segments.jpg "Lithium ion battery value chain")

The research in the mobility sector will focus on the lithium-ion batteries for electric vehicles. Within the electric car value chain, the battery is the most valuable component, account for 40% of the value addition.   

Within the battery value chain, the project team is considering looking at three steps: Mining, Cell Manufacturing, and Recycling

## **What questions are we asking?**

* Does the current certification landscape around responsible and sustainable mining meet the requirements to achieve sustainable material production? 
* What constitutes sustainable cell production in Europe? And how can sustainable cell production be better established across Europe? 
* What actions and policies need to be undertaken to increase material specific recovery rates in recycling of electric batteries? 

## **What minerals are we studying?** 

The research team will focus on battery materials: lithium, cobalt, nickel, and graphite.

## **Standards & Sustainability**

There are a plethora of RS standards, guidelines and process that are being used to address identified RS challenges for the mobility sector. These existing standards and sustainability schemes, particularly for the mining sector are very comprehensive, but do not contain measures for all the problems that exist in the sector. However, it is acknowledged that difficult for one standard to cover all challenges. This page provides a summary of the more commonly found standards.

![standards and initiatives](/files/standards.png "Standards and initiatives")

For a description of these standards and initiatives, see [Existing Approaches](https://re-sourcing.eu/existing-approaches). For a more detailed overview on the different standards and initiatives, see Chapter 4 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

## **Mining**

The mining sector has been the focus of many initiatives and voluntary standards to improve production practices and impacts caused by mining operations, including respect for human rights, prevention of conflicts, stakeholder management, or environmental protection.

Sustainability schemes and standards considered by RE-SOURCING Project, specifically for the mining stage include IRMA, ICMM, EITI, IFC EHS for Mining, ARM, Natural Resource Charter, World Bank Climate-smart mining, OECD Meaningful Stakeholder Engagement, GRI, EBRD and TSM.

For more details on these standards, see Chapter 4 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

[Introduction Mining session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/2021-10-28-presentation-introduction-work-jb-op) 



## **Manufacturing**

The manufacturing of lithium-ion batteries currently lack in international frameworks for EHS. Available standards and initiatives consider the quality and technical specifications of the equipment, performance measurements and test procedures, monitoring and controlling systems, as well as design requirements.

The current proposal of the European Battery Regulation is to be highlighted. If passed, the proposal would introduce mandatory due diligence for cobalt, natural graphite, lithium, nickel, and other chemical compounds. Also, the carbon footprint for the whole value chain for all batteries would be needed; a benchmark for a maximum carbon footprint of a battery would be set. Furthermore, a minimum recycled content is to be met in new batteries for nickel, lithium, and cobalt.

For more details on these standards, see Chapter 4 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)



[Introduction Cell production session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/2021-10-28-workshop-mobility-sector-cell-production-op) 



## **Recycling**

The most recent research uncovered no international standards for recycling lithium-ion batteries. However, the EU has several regulations in place that mention recyclers. The Battery Directive, which is essential for the battery recycling, is currently under review, and a new proposal for revisions has recently been submitted by the European Commission. Two other EU directives are also relevant for waste management of lithium-ion batteries from passenger cars: the Waste Electrical and Electronic Equipment (WEEE) Directive and the End-of-Life Vehicles (ELV) Directive

For more details on these standards, see Chapter 4 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

[Introduction Recycling session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/re-sourcing-roadmap-workshop-mobility-session-recycling-v2) 



## **Research Material**

[The State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

[Summary of work in the mobility sector to date (status October 2021)](https://re-sourcing.eu/reports/2021-10-28-presentation-introduction-work-jb-op) 

[Introduction Recycling session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/re-sourcing-roadmap-workshop-mobility-session-recycling-v2) 

[Introduction Mining session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/2021-10-28-presentation-introduction-work-jb-op) 

[Introduction Cell production session in Roadmap Workshop (28 October 2021)](https://re-sourcing.eu/reports/2021-10-28-workshop-mobility-sector-cell-production-op)