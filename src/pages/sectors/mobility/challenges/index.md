---
template: sector-page
title: Mobility
subtitle: Challenges
sector: /sectors/mobility/
sort_order: 2
---

A number of sustainability and responsible sourcing challenges, across the value chain, have been identified for this sector.

## **Mining**

**Human rights** violations can occur in the mining and processing stage of all raw materials discussed. This includes conflicts with local communities and indigenous people, disrespect for land-rights, violation of women’s and children’s rights, and conflicts between artisanal and large-scale mining activity. Risks related with artisanal and small-scale mining are especially connected with cobalt mining in the DRC.

Currently many mining operations are cause for large **environmental impacts**. These are: land use, tailings, waste rock, acid mine draining (AMD), energy consumption, air emissions & wastewater. For example the high energy consumption is a challenge in the synthetic graphite production.

**Health and safety** of both, workers and local communities are important operational aspects (For example,working conditions in ASM cobalt mining).

To avoid **financial crime,** such as corruption and tax evasion, the transparency of operations and money transfers needs to be improved (for example bribery to obtain mining licences).

These challenges are outlined in detail in Chapter 3 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

## **Manufacturing**

**Environmental Impact**: The production of cells is very energy intensive, which, depending on the source of energy used, is related to major emissions of GHG. Furthermore, the high susceptibility to errors leading to high scrap rates, especially at the beginning of production (2 % - 40 %). As battery cell production is very material intensive, it can lead to large waste streams that must be managed

**Occupational Health & Safety**: The lithium-ion battery contains many toxic substances that must be managed to reduce impacts on the health of workers and the broader community. Faulty cells or cells damaged during production could pose a safety hazard, especially due to the possibility of thermal runaway.

These challenges are outlined in detail in Chapter 3 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

## **Recycling**

A proper recycling of used lithium-ion batteries is essential: The greatest challenges in the various battery cell recycling and transport processes is the control of thermal runaway (TR) and the high fire load of a lithium-ion battery.

**Occupational Health & Safety**: Waste batteries can pose a safety risk. Therefore, it is crucial to treat the waste stream with the necessary safety standards. Recycling lithium-ion batteries as the only option for a circular economy is not yet common practice in all parts of the world, as it is difficult and often costs more than the resulting recycled material brings in when sold. However, recycling of batteries is becoming more common.

**Environmental Impact**: Recycling lithium-ion batteries can be energy intensive and, if not done appropriately, results in significant emissions of pollutants to air and water. Depending on the energy source, greenhouse gases are also emitted, but the savings from the recovered materials are usually greater.

These challenges are outlined in detail in Chapter 3 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)
