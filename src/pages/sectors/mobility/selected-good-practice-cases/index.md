---
template: sector-page
title: Mobility
subtitle: Selected good practice cases
sector: /sectors/mobility/
sort_order: 5
---
**Responsible procurement of minerals by using a strong standard**\
Rebecca Burton (IRMA) and Claudia Becker (BMW) share insights into why IRMA is a strong standard and how purchasing companies can join and change the impacts by adjusting supplier contracts

* Case presentation [video](https://youtu.be/ATouXpKorAg)
* Case presentation [slides](https://re-sourcing.eu/reports/irma-bmw-flagshipcase-presentation)

\
**Overarching regulation for a circular economy that covers the entire product value chain and focuses on sustainability**\
Cesar Santos (European Commission) presents how the Regulation was developed, sharing  the benefits and insights into the strategic choices, challenges and success factors.

* Case presentation [video](https://youtu.be/jIPq2bhcJ1c)
* Case presentation [slides](https://re-sourcing.eu/reports/battery-regulation-flagship-case-presentation)

\
**How to implement a circular economy for batteries**\
Olivier Groux (Kyburz) shares his experiences and insights in the entire take-back scheme, reuse and recycling process with strategic choices as well as challenges and success factors.

* Case presentation [video](https://youtu.be/sifPYh-XxMA) 
* Case presentation [slides](https://re-sourcing.eu/reports/battery-regulation-kyburz-case-presentation)

\
**Chinese standards: What can they achieve and where do they fail?**\
Masuma Farooki (MineHutte) discusses what Chinese standards for mining and recycling look like and how they align with other guidelines from around the world

* Case presentation [video](https://youtu.be/Q8nMgIuRPTM) 
* Case presentation [slides](https://re-sourcing.eu/reports/fs-lab-chinese-standards)