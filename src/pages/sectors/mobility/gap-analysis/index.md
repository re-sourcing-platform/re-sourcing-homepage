---
template: sector-page
title: Mobility
subtitle: Gap Analysis
sector: /sectors/mobility/
sort_order: 4
---
![Issues of RS](/files/rs-issues.png "Issues of RS")

The gap analysis undertaken by the RE-SOURCING project indicates areas where further engagements and actions are required to support sustainable and responsible practices across all steps along the supply chain of the mobility sector. Some of the more urgent issues that need to be addressed are discussed in this section.

### Weak Proof of Origin Schemes

Also referred to as traceability, this issue has revolved around conflict minerals and production sourced from Artisanal and Small-scale Mining (ASM). While blockchain technology, [ARM](https://www.responsiblemines.org/en/) or the [Fairmined](https://fairmined.org/) Standards provide traceability options, on-the-ground implementation remains challenging.

### Lack of Harmonization of Sustainability Requirements

There is a ‘jungle’ of sustainability schemes for mining and manufacturing, with little signs of a joint framework that can harmonise these requirements. Unclear RS targets and measurements, for both downstream and upstream actors, have been set out by a plethora of external reporting schemes and standards.

Currently various standards are under development, with several non-sector specific international standards addressing some aspects of the RE manufacturing process, such as the ISO-standards for environmental ([14001](https://www.iso.org/iso-14001-environmental-management.html)), occupational health & safety ([45001](https://www.iso.org/iso-45001-occupational-health-and-safety.html)), and energy ([50001](https://www.iso.org/iso-50001-energy-management.html)).

### Strengthen Recycling

The broad aim of circular economy is to reduce the negative impacts of primary raw material sourcing and the dependency of raw material imports. Recycling capacities for end-of-life Li-ion batteries need to be increased, as it is already becoming apparent that the recycling capacities will no longer be sufficient to meet market demands. As recycling of lithium-ion batteries from the mobility sector is a comparatively young technology, improvements of recycling processes are to be expected, must be significantly scaled up and must be further supported financially with research funds e.g. from the European Union or within the Member States. This includes, on the one hand, a more efficient recycling process itself, which requires further technological development and research. But on the other hand, the entire process of collecting end-of-life batteries, as well as transport and storage, must also be considered.

Also, standards and regulations for recycling need be developed and considered – currently, there are no international standards for recycling Li-ion batteries. However, the EU has several regulations in place that mention recyclers. The Battery Directive, which is essential for battery recycling, is currently under review, with a new proposal containing detailed and ambitious targets of revisions recently submitted by the European Commission.

An international approach to recycling Li-ion batteries is essential for several reasons: the high risk of fire and explosion of used or damaged Li-ion batteries; the valuable resources contained in batteries; sustainability advantages of secondary materials; and the fact that cars are often exported to other countries and continents, allowing these materials to leave the EU market

### Resource Efficiency not Being Addressed

Considering the enormous expansion of electric vehiclesexpected over the next couple of decades, resource and impact decoupling is essential for making sustainable mobility truly sustainable. These considerations need to be included in all stages of the supply chain. For example, the manufacturing of for battery cells needs to address its high production losses, efficient collection and treatment systems need to be employed to enable the reuse of materials. Actions on the consumer side are required, including new business models such as car sharing, public transportation, etc. This is a consideration many of the identified standards and initiatives, including the UN SDGs, are failing to address.

### Weak Tracking of Procurement across Value Chain

Procurement practices link the various nodes of a value chain and impact the inter-operability of RS practices. Some companies have developed internal procurement standards (such as [Umicore](https://www.umicore.com/storage/main/umicore-sustainable-procurement-charter-2017.pdf)) as have some governments ([Sustainable Procurement Guidelines of the Australian government](https://www.environment.gov.au/system/files/resources/856a1de0-4856-4408-a863-6ad5f6942887/files/sustainable-procurement-guide.pdf)). There are other non-sector specific standards, such as those provided by [ISO 20400](https://www.iso.org/standard/63026.html) or [UNEP Sustainable Procurement Guidelines](https://un-page.org/files/public/3.1_spp_guidelines_.pdf), but guidance specifically addressing batteries need to be developed.

Battery cell specific procurement standards can potentially affect sustainability schemes on the sector level, as they determine requirements downstream actors need to assess for their suppliers. This would have the added value of harmonising procurement guidelines with sector specific standards. Otherwise, they add to the multitude of already available standards and partially overlapping guidelines.

### Level Playing Field is missing

A level playing field is the basis for a globally sustainable market. In this target state, producers who do not act sustainably are excluded from the market. A level playing field requires globally binding, unified framework conditions. For example, recycling can be promoted by setting obligatory rates of recycled content in new products, as proposed for the revised EU regulation on batteries. Also, harmonization and mutual recognition of standards and certifications are essential for this approach.

Many of today’s issues in the value chain arise from asymmetries in regulation in different countries. Countries with high environmental & social standards are more expensive when it comes to production. While one country might for instance allow offshore tailings disposal (e.g. discharge into rivers or submarine disposal), creating a cost advantage, others forbid this practice leading to higher costs for mining waste management or even to not mining certain deposits at all.

The long-term aim should be that globally negotiated and agreed upon standards are applied. Cost advantages at the expense of social and environmental criteria should not be present. Having no options to cut costs on sustainability issues could even encourage cost savings through productivity increase to gain a competitive advantage

### Global Due Diligence needed

A global due diligence approach across all sectors and value chain steps would highlight the challenges across all the pillars of sustainability as well as in all value chain steps. “Due diligence is the process enterprises should carry out to identify, prevent, mitigate and account for how they address \[…] actual and potential adverse impacts in their own operations, their supply chain and other business relationships \[…] (OECD 2020).

With a global approach following the OECD Due Diligence Guidance, the negative impacts along the entire value chain can be identified and addressed at an early stage. Mandatory due diligence is already introduced in in the EU regulation on conflict minerals directly referring to the OECD guidelines. Moreover, the newly proposed EU Battery Regulation also introduced this concept for raw materials in industrial and EV batteries.

For a detailed discussion of these gaps, see Chapter 7 of [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)