---
template: sector-page
title: Mobility
subtitle: Way Forward
sector: /sectors/mobility/
sort_order: 7
---
The [](https://re-sourcing.eu/static/97b7f3dbddcd7fc58acb1c3012acecbb/re-sourcing-mobility-sector-roadmap.pdf)**[Roadmap 2050](https://re-sourcing.eu/reports/re-sourcing-mobility-sector-roadmap)** and the **[Good Practice Guidance](https://re-sourcing.eu/reports/d53-guidelines-for-mobility-sector-final-20220629-final-style-guide)** for the Mobility Sector are finalised and accessible for all interested actors!

Together, the Roadmap 2050 and the Good Practice Guidance for the Mobility Sector, provide concrete recommendations and practices for **policy makers**, **industry** and **civil society** to implement and achieve Responsible Sourcing.

![](/files/resourcing-graphic_roadmap_mobility_220621.png "Mobility Roadmap")

The **Roadmap** is based on the [State of Play report for Mobility](https://re-sourcing.eu/reports/sop-mobility-sector), which emphasises various impacts and challenges in the resource extraction of **lithium, cobalt, nickel** and **graphite**, as well as in the value chain of **lithium-ion batteries** and their recycling.

The following three main areas identifies and addressed in the **Roadmap** in Mobility are:

* **Circular economy & decreased resource consumption**
* **Responsible procurement**
* **Level playing field**

The **Roadmap** and **Guidance** document on Mobility Sector address issues such as significant environmental impacts, human rights violations, lack of commitment to fair wages and workers’ rights, as well as conflicts with local populations and give a clear idea which steps need to be taken to mitigate their effects.

Both reports are developed through a co-creative and consultative multi-stakeholder process. The results have been co-designed at the [Mobility Roadmap Workshop](https://re-sourcing.eu/events/the-roadmap-workshop-on-the-mobility-sector/) for a brainstorming with external actors and the [Flagship Lab](https://re-sourcing.eu/events/the-flagship-lab-for-the-mobility-sector/) respectively and discussed with the project team, project’s Platform Steering Committee and Advisory Board.

In addition, the Roadmap development was based on consultations with webinars and interviews with experts from different stakeholder groups and regions. This led to the identification of necessary points of actions and the establishment of recommendations.

In the working process from [State of Play](https://re-sourcing.eu/reports/sop-mobility-sector) to the [Roadmap](https://re-sourcing.eu/reports/re-sourcing-mobility-sector-roadmap), the [Flagship Lab](https://re-sourcing.eu/events/the-flagship-lab-for-the-mobility-sector/) and the [Good Practice Guidance Document](https://re-sourcing.eu/reports/d53-guidelines-for-mobility-sector-final-20220629-final-style-guide) provided input for good practices in policy making and industry.