---
template: sector-page
title: Mobility
subtitle: Key Players
sector: /sectors/mobility/
sort_order: 3
---

Key players are identified as countries and companies across the supply chain for lithium-ion batteries.

## **Mining**

**Lithium** mining activities in the EU is not significant. A mine in Portugal only produces a small amount of lithium for ceramics (<1% of global production). However, several projects for lithium production in the EU are planned. While Chile led the primary lithium production market up through 2016, thereafter Australia has become the largest lithium producer in the world. Today, Australia has a market share of 62% followed by Chile (18%), China and Argentina (7% each) and Canada (3%).

**Cobalt** is mainly produced in the DRC, accounting for around 65% of global supply, followed by New Caledonia (6%), China (5%), Australia (3%) and Philippines (3%) Australia and the United States. In Europe, only Finland reports cobalt production (<1%).

**Nickel** mining is much more distributed around the world than the other battery materials. Indonesia led the primary nickel production (23%) followed by Philippines (15%), Russia (10%), New Caledonia (10%) and Canada (8%). Producers in the EU are Finland, Greece and Poland (together 3% of global primary production).

**Natural graphite** mining activities in the EU only account for 2% of global production. China is the main supplier with 62%, followed by Mozambique (11(), Brazil (4%), Korea (4%), and India (4%).

Detailed information on the respective mining companies are provided in [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

## **Manufacturing**

Current battery cell production is dominated by Asian countries. The companies with the highest share (in US Dollars) in 2018 were: LG Chem (with 10%) from South Korea followed by Samsung SDI (9%) also from South Korea, Tesla (8%), Panasonic Sanyo (6%), CATL (6%) and a couple a further number of companies with lower market share.

Although the battery market is currently dominated by Asian countries, the battery production landscape in Europe is growing, too. The figure below indicates the production sites already built and producing battery cells (dark blue arrows with bold letters) and the announcements for new production sites or plants which have not started production yet (light blue arrows). Most companies already producing battery cells (not only packs, where different cells are put together) have their plants in Eastern Europe (LG Chem, Magna Energy Storage, Samsung SDI and SK Innovation). Additionally, there are Envision AESC producing cells in Great Britain, Blue Solutions in France and Leclanché in Germany. There are many more plants to come, especially in Germany. It is difficult to keep up with the announcements of new battery cell plants, e.g. from Volkswagen , some of which are not yet on the map.

![Eu map and overview of battery cell plants announced in Europe](/files/overview-of-battery-cell-plants-announced-in-europe.jpg "Overview of battery cell plants announced in Europe")

\- Overview of battery cell plants announced in Europe (T&E 2021; Zenn 2021)

Detailed information on the producing companies are provided in [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)

## **Recycling**

A proper battery recycling of end-of-life lithium-ion batteries isthe only viable waste treatment option for LIBs due to their intrinsic danger. The EU27 currently has several battery recycling companies that are working on improving their battery recycling processes. Some of the recycling routes already implemented on an industrial scale are capable of recovering steel, copper, nickel and cobalt compounds from LIB modules with a yield of at least 90 %. Currently, research and development activities for recycling LIBs mainly target optimising yields, recovering lithium compounds, and separating and using graphite (Buchert et al. 2020).

Detailed information on the recycling companies in and outside of Europe are provided in [the State of Play & Roadmap Concepts: Mobility Sector Report.](https://re-sourcing.eu/reports/sop-mobility-sector)
