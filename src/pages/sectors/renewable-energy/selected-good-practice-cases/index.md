---
template: sector-page
title: Renewable energy
subtitle: Selected good practice cases
sector: /sectors/renewable-energy/
sort_order: 5
---
## Consultative process for national mining policy  

Juan Carlos Jobet Eluchans, Bi-Minister of Energy & Mining, Chile, shares his country’s experiences and approach with the Re-SOURCING Project in designing a consultation process for Chile’s National Mining Policy 2050. 

* Case presentation [video](https://www.youtube.com/watch?v=oUmFvl1F2lk&t=33s) 
* Case summary [slides](https://re-sourcing.eu/reports/consultations-for-a-national-mining-policy)  

## Effective collection & treatment for EOI PV modules 

Andreas Wade, the Global Sustainability Director at First Solar, discusses the Circularity Pre-Requisites for Terawatt Scale Photovoltaics at a peer learning event held by the Re-SOURCING Project. 

* Case presentation [video](https://www.youtube.com/watch?v=LXEpF84wLyI) 
* Case summary [slides](https://re-sourcing.eu/reports/first-solar-recycling-system)  

## Supplier Auditing for Responsible Sourcing 

Together for Sustainability Initiative’s Jakob Smets shares insights in designing a supplier sustainability audit for the chemical industry, with lessons for other industrial clusters in this presentation, hosted by the Re-SOURCING Project. 

* Case presentation [video](https://www.youtube.com/watch?v=qZrMJC6soAU) 
* Case summary [slides](https://re-sourcing.eu/reports/auditing-for-rs-tfs-case-study)