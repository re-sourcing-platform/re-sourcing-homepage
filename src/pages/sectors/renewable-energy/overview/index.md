---
template: sector-page
title: Renewable energy
subtitle: Overview
sector: /sectors/renewable-energy/
sort_order: 1
---
Energy production from renewable sources has been identified as a key factor, to stay within the limits of 1.5°C global temperature rise as defined by the UN Paris Agreement 2015.

The expansion of renewable energy has consequences for the demand of raw materials required for the construction of equipment, plants, and infrastructure, posing increasing pressure on the world’s primary and secondary mineral production. Raw materials extraction is often associated with negative impacts on environmental, social, and economic aspects.

Additionally, in the manufacturing and recycling of equipment for energy production, sustainability issues can occur, such as the release of toxic waste from solar panel production, or the limited recyclability of wind turbine blades. These problems require decisive actions on the part of governments and industry alike, to ensure a clean and affordable energy supply.

The RE-SOURCING project focuses on wind and solar PV energy and their respective supply chains, because of their importance for future energy supply. The project assesses three stages of the supply chain:

* mining, with focus on copper for renewable energy technologies in general, rare earth elements for wind turbines, and silicon for photovoltaics
* manufacturing of equipment, mainly wind turbines and solar PV panels
* their collection and treatment.

## What challenges are we considering?

With the predicted capacity expansion of renewable energies, the demand for certain raw materials is expected to considerably increase. This will lead to several challenges, including:

* How will the rapid increase in demand for raw materials be met without disregarding sustainable and responsible mining practices?
* Recycling can make significant contributions to metal supply, and yet Indium, Lithium, Neodymium, and Vanadium, essential for successful energy transition, show end-of-life recycling rates of less than 1%. How can recycling rates be improved?
* With only small steps having been taken towards the implementation OECD Due Diligence Guidance on Responsible Business Conduct in the renewable energy sector, what actions are required to reinforce efforts to take up responsible sourcing along the supply chain?

## What segments of the value chain are we engaging?  

![mining and manufacturing segments](/files/target-group-re-sector.jpg "mining and manufacturing segments")

The research team will be focusing on the mining and manufacturing segments of the renewable energy value chain.

## What questions are we asking? 

* What indicators and approaches should be included for responsible sourcing and a responsible supply chain for the renewable energy sector?
* What is the current state of responsible sourcing initiatives and are they fit-for-purpose?
* What actions and strategies have been adopted by the industry to meet responsible sourcing objectives?
* Are the current responsible sourcing approaches and standards adequate and fit-for-purpose, or does the industry need additional guidelines and assistance to ensure responsible sourcing?

## What minerals are we studying?   

The research team will focus on materials required for the energy transition. This includes traditional metals (mainly copper), as well as green tech minerals (e.g. rare earth elements).

## Standards & Sustainability

There are a plethora of RS standards, guidelines and process that are being used to address identified RS challenges for the renewable energy sector. These existing standards and sustainability schemes, particularly for the mining sector are very comprehensive, but do not contain measures for all the problems that exist in the sector. However, it is acknowledged that difficult for one standard to cover all challenges. This page provides a summary of the more commonly found standards.

![Standards & Sustainability ](/files/processing.jpg "Standards & Sustainability ")

For a description of these standards and initiatives, see [Existing Approaches](https://re-sourcing.eu/existing-approaches). For a more detailed overview on the different standards and initiatives, see Chapter 3 of the [State of Play & Roadmap Concepts: Renewable Energy Sector Report](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).

## Mining 

The mining sector has been the focus of many initiatives and voluntary standards to improve production practices and impacts caused by mining operations, including respect for human rights, prevention of conflicts, stakeholder management, or environmental protection.

Sustainability schemes and standards considered by RE-SOURCING Project, specifically for the mining stage include IRMA, ICMM, EITI, IFC EHS for Mining, The Copper Mark, ARM, Natural Resource Charter, World Bank Climate-smart mining, OECD Meaningful Stakeholder Engagement, GRI, EBRD and TSM.

For more details on these standards, see **Chapter 3** of the S[tate of Play & Roadmap Concepts: Renewable Energy](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res) Report.

## Manufacturing 

The manufacturing of wind turbines and solar panels currently lack in international frameworks for EHS. Available standards consider the quality and technical specifications of the equipment, performance measurements and test procedures, monitoring and controlling systems, as well as design requirements.

Some countries have national guidelines, e.g., NSF/ANSI 457 “Sustainability Leadership Standard for Photovoltaic Modules and Photovoltaic Inverters” in the US. However, there are numerous guidelines currently under development. Two examples are:

• The American Wind Energy Association (AWEA) is developing technical, workforce, and environmental, health and safety standards

• EU Eco-design, Energy Label, Ecolabel, and Green Public Procurement for solar PV modules Some general standards and guidelines are also of relevance for this sector, e.g., ILO or IFC EHS guidelines

For more details on these standards, see **Chapter 3** of the [State of Play & Roadmap Concepts: Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res) Report.

## Recycling 

For the collection and treatment of wind turbines general standards can be applied. For instance, the IEC EHS Guidelines for Wind Energy contain issues and recommendations related to the decommissioning of wind power plants. The German Institute for Standardization (DIN) developed the first industry standard for dismantling and recycling of wind turbines.

In some European countries there is a legislation in place forbidding composite materials from being landfilled. Others are considering introducing mandatory recycling rates for wind turbines.

Wind Europe set up a cross-sector platform to advance wind turbine recycling with specific focus on turbine blades. In addition, they are currently working on international guidelines for wind turbine dismantling and decommissioning.

For solar equipment, the best-known standard is the European directive on waste of electrical and electronic equipment (WEEE directive). This directive includes a section on photovoltaic panels and provides a framework for collection, transport, and treatment of photovoltaic panels on the notion of extended producer responsibility

For more details on these standards, see **Chapter 3** of the [State of Play & Roadmap Concepts: Renewable Energy](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res) Report. 

## Research Material  

* Report: [State of Play & Roadmap Concepts: Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res)
* Briefing Document: [Identifying Challenges & Required Actions for Responsible Sourcing the Renewable Energy Sector](https://re-sourcing.eu/reports/re-sourcing-briefing-document-4)
* Flagship Lab: [Good Practice cases for Renewable Energy Sector](https://re-sourcing.eu/events/flagship-lab-for-the-renewable-energy-sector-1/)
* Roadmap Workshop – [Workshop Minutes](https://re-sourcing.eu/reports/minutes-roadmap-workshop)
* Roadmap Workshop – [Presentations](https://www.re-sourcing.eu/files/RE_Virtual_Roadmap_Workshop_Presentations.zip) (download)
* Roadmap: [Roadmap for Responsible Sourcing of Raw Materials until 2050](https://re-sourcing.eu/reports/final-res-roadmap-2021)
* Guidelines: [Good Practice Guidelines for the Renewable Energy Sector](https://re-sourcing.eu/reports/d52-res-guidance-document-final)