---
template: sector-page
title: Renewable energy
subtitle: Challenges
sector: /sectors/renewable-energy/
sort_order: 2
---

## Mining 

**Human rights** violations can occur in the mining and processing stage of all raw materials discussed. This includes conflicts with local communities and indigenous people, disrespect for land-rights, violation of women’s and children’s rights, and conflicts between artisanal and large-scale mining activity.

Currently many mining operations are cause for large **environmental impacts**. These are: land use, tailings, waste rock, acid mine draining (AMD), energy consumption, air emissions & wastewater (For example pollution caused by REE mining and processing at Bayan-Obo mine, China)

**Health and safety** of both, workers and local communities are important operational aspects (For example, cancer rate in Chilean copper mining regions).

To avoid **financial crime**, such as corruption and tax evasion, the transparency of operations and money transfers needs to be improved (for example bribery to obtain mining licences).

These challenges are outlined in detail in Chapter 2 of the [State of Play & Roadmap Concepts: Renewable Energy Sector Report](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).

## Manufacturing 

**Human Rights:** Renewable Energy technologies manufacturers have been found to lack in commitment to basic human rights principles, land rights, indigenous people rights, and gender equality.

**Environmental Impact:** The production of materials for wind turbines shows a high energy consumption and depending on the energy source high CO2 emissions, even more so if permanent magnet generator systems are used. The production of silicon wafers for PV panels requires large amounts of energy, water, and chemicals, all of which can have a significant impact if not monitored and treated appropriately. Resource efficiency regarding losses during production needs to be considered.

**Occupational Health & Safety:** The production of wind turbines poses many risks for the health and safety of workers, for example, health implications of working with epoxy resins or fibreglass.

These challenges are outlined in detail in Chapter 2 of the [State of Play & Roadmap Concepts: Renewable Energy Sector Report](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).

## Recycling 

The main problem for the recycling or reuse of wind turbine materials is the collection and treatment of **turbine blades**. New technologies for treatment, ideas for reuse, etc. are necessary to avoid land filling. The dismantling of wind turbines can pose great health risks to workers, including exposure to harmful substances. The dismantling process also needs to take the restoration of land used for wind farms into consideration.

**Solar PV** Panels are generally well recyclable; the issue is the appropriate collection and treatment of the panels. For example, in China a lot of photovoltaic equipment is burned causing severe environmental pollution and potentially has adverse impacts on biodiversity and human health. Appropriate collection and treatment need to become economically attractive for companies and policy guidelines need to be established for increasing waste streams in the future.

These challenges are outlined in detail in Chapter 2 of the [State of Play & Roadmap Concepts: Renewable Energy Sector Report](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res).
