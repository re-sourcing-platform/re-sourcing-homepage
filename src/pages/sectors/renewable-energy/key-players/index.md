---
template: sector-page
title: Renewable Energy
subtitle: Key Players
sector: /sectors/renewable-energy/
sort_order: 3
---

Key players are identified as countries and companies across the supply chain for wind and solar PV energy.

## Mining  

**Copper** mining activities in the EU account for 4% of total global extraction. The supply comes from Poland, Spain, Bulgaria, and Sweden. Globally, Chile is the largest producer accounting for approximately 28.5% of total global supply, followed by Peru, and China. Poland accounts for 27% of EU supply and is next to Germany one of the main producers of refined Copper in the EU. The EU’s refined copper production represents 12% of global production.

**Rare earth concentrates** (REO-content) are mainly produced in China, accounting for 73.23% of global supply in 2018, followed by Australia and the United States. In 2018 there was no EU production. However, there are several projects currently in the development phase: Norra Kärr, Tasjo, and Olserum in Sweden; Matamulas in Spain; and one project each in Germany and Finland.

**High purity quartz** is the main source for the production of silicon metal. Globally, China is the largest producer of silicon accounting for 64% of global supply in 2019. It is classified as a critical material for the EU, with the EU accounting for approximately 18% of global consumption, and approximately 6% of global supply of silicon metal. Producers in the EU are France, Spain, and Germany. The main source of EU supply is Norway, followed by France, and China.

## Manufacturing 

China is the largest manufacturer of **solar PV** equipment. The top four global manufacturers of solar panels, with the largest market share in 2019, are based in China. Only three manufacturers in the top 10 ranking are not Chinese. Outside the top 10, there are various European companies producing solar panels. Some of the larger manufacturers include the REC Group (Norway), Hanover Solar (Germany), the ATERSA Group (Spain), or Kioto Solar (Austria).

The two largest producers for **wind turbines** are the European companies Vestas (Denmark) and Siemens Gamesa (Germany/Spain). Considering only offshore installations, Siemens Gamesa is in the lead. Within the top 10 there are five Chinese companies, as well as one US manufacturer. Wind turbines and associated equipment for the European market are manufactured in Europe, apart from minor volumes of electrical equipment being produced in China, India, and North Africa. While China, India, and Brazil also have large wind turbine industries, they meet their domestic needs.

## Recycling 

**Wind turbines** have a high recyclability of 80% to 90%. A major part of the components – such as steel from the tower, concrete from the foundation, etc. can be reused. Currently there are three main options employed for the treatment of wind turbine blades:

- disposal, including landfill or incineration without heat recovery
- energy recovery or recycling, i.e., incineration with energy recovery, thermal, chemical, or mechanical recycling
- repurposing, for example, co-processing in a cement kiln.

With available technologies a little over 90% of solar panels can be recycled. This is mainly done in existing glass and aluminium recycling plants. There are two individual solutions for adequate recycling currently in use:

- independent recycling providers, such as the European PV Cycle or the Australian Reclaim PV Recycling
- collection and treatment by solar PV manufacturers.
