---
template: sector-page
title: Renewable energy
subtitle: Gap Analysis
sector: /sectors/renewable-energy/
sort_order: 4
---
## RS issues not adequately covered by existing initiatives

* women rights
* conflict with agriculture
* money laundering
* conflict with indigenous people
* responsible person for the standard
* alluvial mining
* conflict with LSM
* extortion
* mergers and acquisitions
* pricing & price premiums

The gap analysis undertaken by the RE-SOURCING project indicates areas where further engagements and actions are required to support sustainable and responsible practices across all steps along the supply chain of the renewable energy sector. Some of the more urgent issues that need to be addressed are discussed in this section.

## Weak Proof of Origin Schemes 

Also referred to as traceability, this issue has revolved around conflict minerals and production sourced from Artisanal and Small-scale Mining (ASM). While blockchain technology, [ARM](https://www.responsiblemines.org/en/) or the [Fairmined](https://fairmined.org/) Standards provide traceability options, on-the-ground implementation remains challenging.

## Lack of Harmonization of Sustainability Requirements 

There is a ‘jungle’ of sustainability schemes for mining and manufacturing, with little signs of a joint framework that can harmonise these requirements. Unclear RS targets and measurements, for both downstream and upstream actors, have been set out by a plethora of external reporting schemes and standards.

Currently various standards are under development, with several non-sector specific international standards addressing some aspects of the RE manufacturing process, such as the ISO-standards for environmental ([14001](https://www.iso.org/iso-14001-environmental-management.html)), occupational health & safety ([45001](https://www.iso.org/iso-45001-occupational-health-and-safety.html)), and energy ([50001](https://www.iso.org/iso-50001-energy-management.html)).

## Specific Recycling Standards not Available  

There is a lack of standards specific to the recycling of wind energy and solar PV equipment. While there are some more general guidelines or directives, e.g., the IEC EHS Guidelines for Wind Energy or the EU’s [WEEE Directive](https://ec.europa.eu/environment/waste/weee/index_en.htm), that also cover issues related to the decommissioning of wind power plants and the collection and treatment of PV panels respectively, international guidelines that address the specific requirements of wind turbines and solar PV equipment are not available. Given the recycling of these products will be required across several jurisdictions, an international framework(s) needs to be established to prevent their disposal in landfills or environmental pollution by battery toxins.

## Resource Efficiency not Being Addressed 

Considering the enormous expansion of the renewable energy sector expected over the next couple of decades, resource and impact decoupling is essential for making renewable energy technologies truly sustainable. These considerations need to be included in all stages of the supply chain. For example, the manufacturing of silicon wafers for solar PV panels needs to address its high production losses, efficient collection and treatment systems need to be employed to enable the reuse of materials. Actions on the consumer side are required, including new business models such as car sharing, etc. This is a consideration many of the identified standards and initiatives, including the UN SDGs, are failing to address

## Weak Tracking of Procurement across Value Chain 

Procurement practices link the various nodes of a value chain and impact the inter-operability of RS practices. Some companies have developed internal procurement standards (such as [Umicore](https://www.umicore.com/storage/main/umicore-sustainable-procurement-charter-2017.pdf)) as have some governments ([Sustainable Procurement Guidelines of the Australian government](https://www.environment.gov.au/system/files/resources/856a1de0-4856-4408-a863-6ad5f6942887/files/sustainable-procurement-guide.pdf)). There are other non-sector specific standards, such as those provided by [ISO 20400](https://www.iso.org/standard/63026.html) or [UNEP Sustainable Procurement Guidelines](https://un-page.org/files/public/3.1_spp_guidelines_.pdf), but guidance specifically addressing RE need to be developed.

RE specific procurement standards can potentially affect sustainability schemes on the sector level, as they determine requirements downstream actors need to assess for their suppliers. This would have the added value of harmonising procurement guidelines with sector specific standards. Otherwise, they add to the multitude of already available standards and partially overlapping guidelines.

## Lack of Engagement on Select Issues 

The RE-SOURCING project also conducted a narrative analysis to investigate the online discourse on topics related to responsible sourcing and renewable energies. The narrative analysis indicated that multistakeholder engagement is prominent for investors, companies, and the public for issues such as “ESG mining” or “sustainable mining,” other issues have not received the same attention. Examples include “human rights procurement” or “responsible sourcing raw materials/minerals.” The RE-SOURCING project consider both narratives highly relevant in the context of RS for the renewable energy sector. These topics require more commitment, as they relate to unaddressed challenges in the renewable energy supply chain and not just particular to one node. Please see the chapter on narrative analysis in the ‘[Responsible Sourcing in the Renewable Energy Supply Chain](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res)’ Report for more details.

## Link to Wider Human Development Missing 

Any advancements in the Renewable Energy sector need to consider not only the value chain nodes (mining/manufacturing/treatment) but also the larger international aspects and planetary boundaries. The UN SDGs encompass a wide range of goals to reach a more sustainable and equitable growth path for all citizens. Yet, these goals may not be enough. RE-SOURCING Project agrees with Jain and Jain (2020) who compare the enhancement of human well-being by using the Human Development Index (HDI) with the carrying capacity of the planet by using the Ecological Footprint (EF). The HDI ranges between 0 and 1, where scores below 0.55 signify low development and above 0.7 as high human development. The ecologically productive area per person is 1.7 Gha (global hectares). This means a country with an HDI above 0.70 and an EF above 1.7 Gha can be categorised as sustainable. Jain and Jain’s assessment however shows that countries with a high HDI usually also have an EF exceeding the 1.7 Gha. In contrast, countries with a low HDI also have a significantly smaller EF. This leads them to argue that the preference of economic and social development remains a national issue, often ignoring the larger global environmental aspects and the respect for planetary boundaries.

For a detailed discussion of these gaps, see Chapter 6 of the [State of Play & Roadmap Concepts: Renewable Energy Sector](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res) Report.