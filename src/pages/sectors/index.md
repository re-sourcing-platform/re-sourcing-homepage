---
template: target-groups
title: Sectors
subtitle: Overview
featuredImage: industrial.jpg
dropdowns: null
---

The European Green Deal marks Europe’s commitment to become the world’s first climate-neutral continent by 2050.

RE-SOURCING will develop visions & roadmaps for responsible sourcing of minerals in the three sectors of Renewable Energy, Mobility and Electric & Electronic Equipment. It will support a sustainable transition based on environmentally friendly, socially equitable and economically profitable sourcing in global mineral value chains.

These three sectors represent essential priorities in the European long-term vision: the Mobility sector for clean mobility and Renewable energy for the fully de-carbonised en ergy supply. The EEE-sector reflects the relevant questions on the conflict mineral regulation. Their contribution is essential to achieving the targets that have been set by the EU Green Deal. To ensure that the ‘output’ from these sectors contributes to the sustainability agenda, responsible sourcing practices within these sectors need to be strengthened.
