import React, { useState } from "react"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import { Helmet } from "react-helmet"
import { navigate } from "@reach/router"
import PageHeader from "../components/layout/headers/PageHeader"
import useHasMounted from "../components/utility/hooks/useHasMounted"

const SearchPage = ({ location }) => {
  return (
    <Layout pageName="search" location={location}>
      <Helmet>
        <script
          async
          src="https://cse.google.com/cse.js?cx=8fda83fed67f1b519"
        ></script>
      </Helmet>
      <PageHeader title="Search" />
      <Container>
        <SearchForm />
        {location.search && (
          <div className="shadow-component rounded-md overflow-hidden mt-8 p-4 bg-white">
            <SearchResults />
          </div>
        )}
      </Container>
    </Layout>
  )
}

export default SearchPage

const SearchResults = () => {
  const hasMounted = useHasMounted()
  if (!hasMounted) return null
  return <div className="gcse-searchresults-only"></div>
}

const SearchForm = () => {
  const [formData, setFormData] = useState("")
  const handleInput = event => {
    setFormData(event.target.value)
  }

  const handleSubmit = e => {
    e.preventDefault()
    if (formData.length > 0) {
      navigate(`?q=${formData}`)
      window.location.reload()
    } else return
  }

  return (
    <form
      className="w-full flex items-center justify-center shadow-component rounded-md overflow-hidden h-16 mx-auto"
      style={{ maxWidth: "40rem" }}
    >
      <input
        type="text"
        name="google_search_site"
        className="appearance-none h-full w-full pl-4 text-xl rounded-l-md border-2 border-white focus:outline-none focus:shadow-outline focus:border-buttonDark"
        placeholder="Search..."
        onChange={e => handleInput(e)}
      />

      <button
        className="flex items-center justify-center flex-grow h-full px-6 text-white bg-button-gradient transition duration-300"
        type="submit"
        onClick={e => handleSubmit(e)}
      >
        <svg
          className="w-6 h-6 text-white"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
        >
          <path d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
        </svg>
      </button>
    </form>
  )
}
