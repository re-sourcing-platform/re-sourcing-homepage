---
template: assets-page
title: External Links
subtitle: Major studies & knowledge repositeries on responsible sourcing
featuredImage: /files/existing-approaches.jpg
assets:
  - title: Approaches to responsible sourcing in mineral supply chains
    subtitle: "Academia "
    outsideLink: https://www.sciencedirect.com/science/article/pii/S092134491930103X
    excerpt: Article - Open access (2019)
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/screenshot-2021-12-03-at-12.14.32.png
  - title: "Assessing impacts of responsible sourcing initiatives for cobalt:
      Insights from a case study"
    subtitle: "Academia "
    outsideLink: https://doi.org/10.1016/j.resourpol.2021.102015
    excerpt: Article (2021) - Open Access
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/article-1.jpg
  - title: Automotive industry guiding principles to enhance sustainability
      performance in the supply chain
    subtitle: Industry & Business
    excerpt: |-
      Report / Document
      (2017)
    outsideLink: https://www.drivesustainability.org/wp-content/uploads/2020/07/Guiding-Principles.pdf
    logoImage: /files/csr-europe.png
    tags:
      - title: Social & Human Rights in LSM
      - title: Environmental issues in LSM
  - title: "British Geological Survey: World mineral statistics"
    subtitle: Other
    outsideLink: https://www2.bgs.ac.uk/mineralsuk/statistics/wms.cfc?method=searchWMS
    logoImage: /files/bgs.png
    excerpt: Mining Statistics
  - title: "Building mining's economic linkages: A critical review of local content
      policy theory"
    subtitle: "Academia "
    outsideLink: https://www.sciencedirect.com/science/article/pii/S0301420721003226#!
    logoImage: /files/article-4.jpg
    excerpt: Article 2021 - Journal access
    tags:
      - title: Transparency, Traceability & Due Diligence
  - title: CEWASTE Project
    subtitle: A Horizon 2020 research & innovation program
    outsideLink: https://cewaste.eu
    excerpt: Developed a voluntary certification scheme for waste treatment.
      Specifically, the project created, validated and launched the scheme for
      collection, transport and treatment facilities of key types of waste
      containing significant amounts of valuable and critical raw materials such
      as waste electrical and electronic equipment (WEEE) and batteries.
    logoImage: /files/cewaste_logo.png
    tags:
      - title: Project
  - title: Changing Wealth of Nations
    subtitle: "International Organisation "
    logoImage: /files/world-bank.jpeg
    outsideLink: https://www.worldbank.org/en/publication/changing-wealth-of-nations
    tags:
      - title: All
    excerpt: Report / Document (2021)
  - title: "Cobalt from the DRC: Potential, risks & significance for the global
      cobalt market"
    subtitle: Government/Policy Making
    excerpt: |-
      Report / Document
      (2017)
    tags:
      - title: Environmental and Social isses in ASM
    outsideLink: https://www.researchgate.net/publication/326060301_COBALT_FROM_THE_DR_CONGO_-_POTENTIAL_RISKS_AND_SIGNIFICANCE_FOR_THE_GLOBAL_COBALT_MARKET_1_Commodity_Top_News_53
    logoImage: /files/cobalt-study.png
  - title: Commodity summaries (Annual)
    subtitle: Government/Policy Making
    excerpt: |
      Mineral statistics
    outsideLink: https://www.usgs.gov/centers/nmic/mineral-commodity-summaries
    logoImage: /files/usgs.png
  - title: "Conflict minerals and battery materials supply chains: A mapping review
      of responsible sourcing initiatives"
    subtitle: "Academia "
    excerpt: Article (2021) - Journal access
    outsideLink: https://www.sciencedirect.com/science/article/pii/S2214790X21000976
    logoImage: /files/article-6.jpg
    tags:
      - title: Transparency, Traceability & Due Diligence
  - title: Costs and Value of Due Diligence in Mineral Supply Chains
    subtitle: International Organisation
    outsideLink: http://mneguidelines.oecd.org/costs-and-value-of-due-diligence-in-mineral-supply-chains.pdf
    excerpt: Report / Document (2021)
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/oecd-cost-vc.png
  - subtitle: "Academia "
    excerpt: Article (2021) - Open access
    tags:
      - title: Recycling
    outsideLink: https://onlinelibrary.wiley.com/doi/epdf/10.1111/jiec.13187
    logoImage: /files/article-7.jpg
    title: Critiques of Circular Economy
  - title: Distributional impact of corporate extraction & (un)authorised
      clandestine mining at & around large-scale copper & cobalt-mining sites in
      DRC
    subtitle: "Academia "
    logoImage: /files/katz-levig-article.png
    outsideLink: https://www.sciencedirect.com/science/article/abs/pii/S0301420719309833
    excerpt: |-
      Report / Document
      (2020)
  - title: Due Diligence Ready! Tool
    subtitle: Government/Policy Making
    outsideLink: https://ec.europa.eu/growth/sectors/raw-materials/due-diligence-ready_en
    excerpt: Reporting Template
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/dd-tool.jpg
  - title: "Environmental Justice Atlas "
    subtitle: Civil society
    outsideLink: https://ejatlas.org
    excerpt: Website
    logoImage: /files/env-just-atals.jpg
  - title: EU Critical Raw Materials factsheets
    subtitle: Government/Policy Making
    logoImage: /files/3.Eueopean_Commission_Lucia_Mancini.jpg
    outsideLink: https://ec.europa.eu/growth/sectors/raw-materials/specific-interest/critical_en
    excerpt: Website
  - title: European Partnership for Responsible Mineral (EPRM)
    outsideLink: https://europeanpartnership-responsibleminerals.eu
    tags:
      - title: Social & Human Rights in LSM
    logoImage: /files/eprm.png
    subtitle: International Organisation
    excerpt: Reporting/Auditing
  - title: European Raw Materials Alliance (ERMA)
    outsideLink: https://erma.eu/
    logoImage: /files/erma.png
    subtitle: International Organisation
    excerpt: Network
  - title: "EU-Latin America Partnership on Raw Materials "
    subtitle: "An EU Funded Project "
    excerpt: EU-Latin America Partnership on Raw Materials project partners are the
      EU Member states and Argentina, Brazil, Chile, Colombia, Mexico, Peru and
      Uruguay. The project aims to move one step further towards integrating
      strategic industrial value chains for both regions, exploring new business
      models and delivering value for society, while keeping high environmental
      and social standards at the core of this partnership. This project brings
      together multiple stakeholders along the raw materials value chain, from
      the EU and Latin America, to deliver on a green and climate-neutral
      economy.
    outsideLink: https://www.mineralplatform.eu/about/the-partnership
    logoImage: /files/logo.png
    tags:
      - title: "Project "
  - title: Extractive Commodity Trading
    outsideLink: https://www.responsibleminingfoundation.org/extractivecommoditytrading/
    excerpt: Report / Document (2021)
    subtitle: Consultancy/Think tank
    logoImage: /files/rmf_ret_cover_small.jpeg
    tags:
      - title: Transparency, Traceability & Due Diligence
      - title: Environmental issues in LSM
  - title: "Future of wind: Deployment, investment, technology, grid integration &
      socio-economic aspects"
    subtitle: Government/Policy Making
    outsideLink: https://www.irena.org/publications/2019/Oct/Future-of-wind
    excerpt: |-
      Report / Document
      (2019)
    logoImage: /files/irena-report-2019.png
  - title: "Global renewables outlook: Energy transformation 2050"
    subtitle: Government/Policy Making
    logoImage: /files/irena-report-2020.png
    outsideLink: https://www.irena.org/publications/2020/Apr/Global-Renewables-Outlook-2020
    excerpt: |-
      Report / Document
      (2020)
  - title: Global tailings review
    outsideLink: https://globaltailingsreview.org/about/
    tags:
      - title: Transparency, Traceability & Due Diligence
      - title: Environmental issues in LSM
    logoImage: /files/global-tailings.png
    subtitle: International Organisation
    excerpt: Standard setting/adherence
  - title: Harmful Impacts of Mining
    subtitle: Consultancy/Think tank
    outsideLink: https://www.responsibleminingfoundation.org/es/harmful-impacts-of-mining/
    logoImage: /files/rmf_harmful_impacts_report_cover-scaled.jpeg
    excerpt: Report / Document (2021)
    tags:
      - title: Social & Human Rights in LSM
      - title: Environmental issues in LSM
  - title: Human rights in wind turbine supply chains
    subtitle: Civil society
    logoImage: /files/action-aid-report-2019.png
    outsideLink: https://www.somo.nl/wp-content/uploads/2019/11/Human-Rights-in-Wind-Turbine-Supply-Chians-2019.pdf
    excerpt: Report / Document (2019)
  - title: "IGF Guidance for Governments: Environmental management and mining
      governance"
    subtitle: Consultancy/Think tank
    outsideLink: https://www.iisd.org/system/files/2021-05/igf-guidance-governments-environmental-management-mining-en.pdf
    excerpt: Report / Document (2020)
    logoImage: /files/article-3.jpg
    tags:
      - title: Transparency, Traceability & Due Diligence
      - title: Environmental issues in LSM
  - title: Information gap analysis for decision makers to move EU towards a
      Circular Economy for the lithium-ion battery value chain.
    subtitle: Government/Policy Making
    excerpt: |-
      Report / Document
      (2020)
    logoImage: /files/jrc-report.png
    outsideLink: https://op.europa.eu/de/publication-detail/-/publication/afea64bf-084d-11eb-a511-01aa75ed71a1/language-en
    tags:
      - title: Transparency, Traceability & Due Diligence
      - title: Recycling
  - title: Initiative for Responsible Mining Assurance (IRMA)
    outsideLink: https://responsiblemining.net
    tags:
      - title: Environmental issues in LSM
      - title: Social & Human Rights in LSM
    logoImage: /files/screenshot-2021-06-18-at-17.02.14.png
    subtitle: International Organisation
    excerpt: Sourcing strategy
  - title: "International mineral trade on the background of due diligence
      regulation: A case study of tantalum and tin supply chains from East and
      Central Africa"
    subtitle: "Academia "
    outsideLink: https://www.sciencedirect.com/science/article/pii/S0301420718304720
    logoImage: /files/article-5.jpg
    excerpt: Article (2019) - Journal access
    tags:
      - title: Transparency, Traceability & Due Diligence
  - title: "Interconnected supply chains: A comprehensive look at due diligence
      challenges & opportunities sourcing cobalt & copper from the DRC"
    subtitle: Consultancy/Think tank
    logoImage: /files/oecd-intercon-supply-chinas.png
    outsideLink: https://mneguidelines.oecd.org/Interconnected-supply-chains-a-comprehensive-look-at-due-diligence-challenges-and-opportunities-sourcing-cobalt-and-copper-from-the-DRC.pdf
    excerpt: |-
      Report / Document
      (2019)
  - title: "Mapping the renewable energy sector to the SDGs: An atlas"
    subtitle: Consultancy/Think tank
    outsideLink: http://biblioteca.olade.org/opac-tmpl/Documentos/cg00717.pdf
    logoImage: /files/mapping-sdgs-to-mining.png
    excerpt: |-
      Report / Document
      (2018)
  - title: "Material Insights "
    subtitle: Website
    outsideLink: https://www.material-insights.org/material/dysprosium/
    tags:
      - title: Mineral Statistics
    excerpt: Material Profiles Dataset
    logoImage: /files/mi-logo-blue.svg
  - title: "Minerals for climate action: The mineral intensity of the clean energy
      transition"
    subtitle: International Organisation
    logoImage: /files/wb-report-minerals-for-climate-action.png
    outsideLink: https://www.worldbank.org/en/topic/extractiveindustries/brief/climate-smart-mining-minerals-for-climate-action
    excerpt: |-
      Report / Document
      (2020)
  - title: Mining & metals in a sustainable world
    subtitle: Consultancy/Think tank
    outsideLink: http://www3.weforum.org/docs/WEF_MM_Sustainable_World_2050_report_2015.pdf
    logoImage: /files/wef-report-2014.png
    excerpt: |-
      Report / Document
      (2014)
  - title: "New Exploration Technologies (NEXT) Project "
    outsideLink: https://new-exploration.tech
    excerpt: "The project develops new geomodels and novel sensitive exploration
      technologies which together are fast, cost-effective, environmentally safe
      and potentially socially accepted. The project is built on three pillars
      of technological advances: (1) Mineral systems modeling, (2) exploration
      methods and approaches as well as (3) data processing and data integration
      tools"
    tags:
      - title: Project
    logoImage: /files/next-logo-wc.svg
  - title: OECD due diligence guidance for responsible supply chains of minerals
      from conflict-affected & high-risk areas
    outsideLink: https://www.oecd.org/corporate/mne/mining.htm
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/screenshot-2021-06-18-at-16.44.10.png
    subtitle: International Organisation
    excerpt: Sourcing strategy
  - title: Renewable Energy & Human Rights Benchmark 2021
    subtitle: International Organisation
    outsideLink: https://media.business-humanrights.org/media/documents/2021_Renewable_Energy_Benchmark_v4.pdf
    excerpt: Report / Document (2021)
    tags:
      - title: All
    logoImage: /files/screenshot-2021-12-03-at-11.55.50.png
  - title: "Resource efficiency: Potential & economic implications"
    subtitle: International Organisation
    logoImage: /files/unep-report-2017.png
    outsideLink: https://www.resourcepanel.org/reports/resource-efficiency
    excerpt: |-
      Report / Document
      (2017)
  - title: Responsible Minerals Initiative (RMI)
    outsideLink: http://www.responsiblebusiness.org/
    tags:
      - title: Social & Human Rights in LSM
    logoImage: /files/rba.png
    subtitle: Civil society
    excerpt: Standard setting/adherence
  - title: Responsible Mining Foundation (RMF)
    logoImage: /files/rmf-logo.jpg
    outsideLink: https://www.responsibleminingfoundation.org
    tags:
      - title: Social & Human Rights in LSM
    subtitle: Consultancy/Think tank
    excerpt: Standard setting/adherence
  - title: Responsible mining in Latin America and the Caribbean?
    subtitle: Consultancy/Think tank
    outsideLink: https://www.responsibleminingfoundation.org/app/uploads/RMI-Report_Regional-Study-2020_LAC-EN.pdf
    excerpt: Report / Document (2020)
    tags:
      - title: Social & Human Rights in LSM
      - title: Environmental issues in LSM
    logoImage: /files/responsible-mining-lac-2020_cover.png
  - subtitle: "A EU Funded Project "
    title: Resource Impact Dashboard
    outsideLink: https://www.resource-impact.org/en/#/home
    excerpt: The Resource Impact Dashboard enables stakeholders to monitor the
      effects of resource extraction on local development at the mine site level
      – based on data from household surveys, public sources, and from mining
      companies
    tags:
      - title: "Project "
    logoImage: /files/rid_logo_neg.svg
  - title: Responsible & sustainable sourcing of battery raw materials
    subtitle: Government/Policy Making
    excerpt: |-
      Report / Document
      (2020)
    outsideLink: https://op.europa.eu/de/publication-detail/-/publication/1c2bfa6d-ba77-11ea-811c-01aa75ed71a1/language-en
    logoImage: /files/jrc-report-battery.png
    tags:
      - title: Mobility
  - title: Study on future demand & supply security of nickel for EV batteries
    excerpt: |-
      Report / Document
      (2021)
    subtitle: Industry & Business
    outsideLink: https://publications.jrc.ec.europa.eu/repository/handle/JRC123439
    logoImage: /files/nickel-roskil.png
    tags:
      - title: Mobility
  - title: Study on due diligence requirements through the supply chain
    subtitle: Government/Policy Making
    outsideLink: https://op.europa.eu/en/publication-detail/-/publication/8ba0a8fd-4c83-11ea-b8b7-01aa75ed71a1/language-en
    excerpt: Report / Document (2021)
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/eu-study-on-dd.jpeg
  - title: Sustainable procurement guide
    subtitle: Government/Policy Making
    excerpt: |-
      Report / Document
      (2020)
    outsideLink: http://www.environment.gov.au/system/files/resources/856a1de0-4856-4408-a863-6ad5f6942887/files/sustainable-procurement-guide.pdf
    tags:
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/australai-guide.png
  - title: "Sustainability schemes for mineral resources: A comparative overview"
    subtitle: Consultancy/Think tank
    outsideLink: Consultancy/Think tank
    logoImage: /files/bgr-report-compari-review.png
    excerpt: |-
      Report / Document
      (2017)
  - title: Supply & demand of natural graphite - DERA
    subtitle: Government/Policy Making
    excerpt: |-
      Report / Document
      (2020)
    outsideLink: https://www.deutsche-rohstoffagentur.de/DERA/DE/Downloads/Studie%20Graphite%20eng%202020.pdf?__blob=publicationFile&v=3
    logoImage: /files/grpahite.png
  - title: The Analytical Fingerprint (AFP)
    subtitle: Consultancy/Think tank
    logoImage: /files/bgr-report-analytical-fingerprints.png
    outsideLink: https://www.bgr.bund.de/EN/Themen/Min_rohstoffe/CTC/Downloads/AFP_Manual.pdf?__blob=publicationFile&v=7
    excerpt: Reporting/Auditing
  - title: The complementing role of sustainability standards in managing
      international and multi-tiered mineral supply chains
    subtitle: "Academia "
    logoImage: /files/screenshot-2021-12-03-at-12.17.11.png
    outsideLink: https://www.sciencedirect.com/science/article/pii/S0921344921003566
    excerpt: Article (2021) - Open access
    tags:
      - title: Transparency, Traceability & Due Diligence
  - title: UN framework classification for resources (UNFC)
    tags:
      - title: Environmental issues in LSM
      - title: Social & Human Rights in LSM
      - title: Transparency, Traceability & Due Diligence
    logoImage: /files/unfc.png
    outsideLink: https://unece.org/sustainable-energy/unfc-and-sustainable-resource-management
    subtitle: Government/Policy Making
    excerpt: Standard setting/adherence
  - title: World Economic Forum (Mining & Metals)
    outsideLink: http://www.weforum.org/communities/mining-and-metals
    logoImage: /files/wef.png
    tags:
      - title: Environmental issues in LSM
    excerpt: Reporting/Auditing
  - title: "Why environmental due diligence matters in minerals supply chain : the
      case of the planned Llurimagua copper mine, Ecuador"
    subtitle: Consultancy/Think tank
    outsideLink: https://germanwatch.org/sites/default/files/Fallstudie_Ecuador_EN_final.pdf
    excerpt: Report / Document (2020)
    tags:
      - title: Transparency, Traceability & Due Diligence
      - title: Environmental issues in LSM
    logoImage: /files/article-2.jpg
---

Given the width and depth of material and knowledge available on responsible sourcing practices in the mineral value chain, this section provides links to relevant studies and material for stakeholders. \
This is not a comprehensive list and more material may be available. The RE-SOURCING Project team does not endorse these studies and these are listed for gudiance only.
