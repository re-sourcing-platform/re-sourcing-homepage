---
template: assets-page
title: Existing Approaches
featuredImage: /files/existing-approaches.jpg
assets:
  - title: Alliance for Responsible Mining (ARM)
    outsideLink: https://www.responsiblemines.org/en/our-work/standards-and-certification/
    excerpt: The ARM initiative seeks to promote ‘inclusive and sustainable
      development’ to legitimise the artisanal and small-scale mining (ASM)
      sector. ARM has set up voluntary standards and certification schemes and
      promotes the legitimacy of responsible ASM on commodities markets. ARM
      supports gender equality, diversification, and socially and
      environmentally responsible production through implementing good-practice
      techniques and certain technological advances.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/arm-logo.png
  - title: Aluminium Stewardship Initiative (ASI)
    outsideLink: https://aluminium-stewardship.org/asi-standards/
    excerpt: The ASI works towards building stakeholder consensus in the aluminium
      sector around responsible practices and performance standards. The
      organisation aims to create a collective impact on environmental and
      social issues. The ASI Standards program is applicable to all stages of
      the aluminium production and transformation stages. Members are certified
      when they have met these standards.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/asi-logo.png
  - title: Basel Convention
    outsideLink: http://www.basel.int/TheConvention/Overview/tabid/1271/Default.aspx
    excerpt: The Basel Convention on the Control of Transboundary Movements of
      Hazardous Wastes and their Disposal (‘Basel Convention’) is an
      international environmental agreement that has established environmentally
      sound waste management and regulates the control of transboundary
      hazardous waste shipments. Lithium-ion batteries at their end of life also
      fall under the Basel Convention, which makes it difficult to ship them
      around the world, as the process involves many necessary authorisations.
    tags:
      - title: Regulation
      - title: Reporting Template
    logoImage: /files/basil-logo.jpg
  - title: Certified Trading Chains (BGR)
    outsideLink: https://www.bgr.bund.de/EN/Themen/Min_rohstoffe/CTC/Concept_MC/CTC-Standards-Principles/ctc_standards-principles_node_en.htmlt
    excerpt: "The Certified Trading Chains certification scheme was developed by the
      German agency Federal Institute for Geosciences and Natural Resources
      (BGR) in 2007 for Rwanda and, in an adapted form, for the Democratic
      Republic of the Congo. The scheme acknowledges the specific challenges
      pertaining to the artisanal and small-scale mining sector and is hence
      particularly concerned with feasibility and impact in an artisanal
      context. It emphasizes process rather than just demanding and certifying
      certain performance targets. "
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/bgr-logo.png
  - title: China Responsible Mineral Supply Chain Due Diligence Management Guide
    outsideLink: http://en.cccmc.org.cn
    logoImage: /files/cccmc_logo.jpg
    excerpt: "The China Responsible Mineral Supply Chain Due Diligence Management
      Guide was launched in 2015 by the China Chamber of Commerce for Metals,
      Minerals and Chemical Importers (CCCMC) in cooperation with the OECD. In
      following the guidelines, Chinese companies achieve international due
      diligence standards that can be mutually recognized around the world. "
    tags:
      - title: General
      - title: "Guidance & Initiative "
      - title: Reporting Template
  - title: The Copper Mark
    outsideLink: https://coppermark.org/
    excerpt: "The Copper Mark is a framework specifically for the copper industry to
      support responsible production practices. The goal is to provide
      independent certification for copper producers. To receive the Copper
      Mark, 32 criteria must be fulfilled. These criteria were developed by the
      Responsible Minerals Initiative and cover environmental, social, and
      governance areas. A company can also qualify for The Copper Mark by
      implementing equivalent sustainability systems and standards. "
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/copper-mark-logo.jpg
  - title: Drive Sustainability
    outsideLink: https://www.drivesustainability.org/guiding-principles/
    excerpt: "Facilitated by CSR Europe, the automobile sector has set up the Drive
      Sustainability partnership to improve the sustainability of the automotive
      supply chain. The partnership has drawn up the Automotive Industry Guiding
      Principles to Enhance Sustainability Performance in the Supply Chain that
      is accompanied by The Global Automotive Sustainability Practical Guidance.
      These Principles and Guidance documents define and provide examples for
      efforts toward ensuring ‘fundamental principles of social and
      environmental responsibility’ in the areas ‘Business ethics’,
      ‘Environment’ and ‘Human rights and working conditions’. "
    tags:
      - title: Mobility
      - title: "Guidance & Initiative "
    logoImage: /files/drive-sustainability-logo.jpg
  - title: Electronic Industry Citizenship Council (EICC)
    outsideLink: "http://www.responsiblebusiness.org/media/docs/EICCCodeofConduct5_\
      /English.pdf "
    excerpt: The EICC Code of Conduct provides standards for the working conditions
      of employees in the supply chain for the electronics industry.
      Participants are also required to ask their first-tier supplier to
      implement the code. The code addresses labour, health & safety,
      environment, and business ethics.
    tags:
      - title: EEE
      - title: "Guidance & Initiative "
    logoImage: /files/eicclogo.jpg
  - title: European Bank for Reconstruction & Development
    outsideLink: https://www.ebrd.com/policies/sector/draft-mining-strategy.pdf
    subtitle: "General, Guidance & Initiative "
    excerpt: >
      The EBRD published a new mining strategy for 2018-2022 in conjunction with
      their other policies on environmental and social governance, the green
      economy transition, public information, etc. While mainly providing
      guidelines for EBRD involvement in mining projects, the strategy also
      offers guidance for governments on policy development and supports
      companies when introducing new technologies and good corporate practices.
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/ebrd-logo.png
  - title: EU Collection, Logistics, Pre-treatment, Downstream treatment of WEEE
    outsideLink: https://standards.iteh.ai/catalog/standards/clc/2083367a-2542-4c75-9477-650a3a13431a/en-50625-1-2014
    excerpt: >-
      The purpose of this European Standard for Collection, Logistics,
      Pre-treatment, Downstream treatment of WEEE (EN 50625-1:2014) is to
      support institutions to:

      -	carry out effective and efficient treatment and disposal of waste electrical and electronic equipment (WEEE) in order to prevent pollution and reduce emissions;

      -	promote increased mechanical recycling;

      -	support high quality recycling processes;

      -	prevent improper disposal of WEEE and its fractions;

      -	ensure human health and safety and environmental protection; and

      -	prevent shipments of WEEE to suppliers whose operations do not comply with the requirements of this normative document or comparable specifications.

      This EU standard is focused on WEEE but also includes batteries generally.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: EU Conflict Mineral Regulations
    outsideLink: "https://ec.europa.eu/trade/policy/in-focus/conflict-minerals-regu\
      lation/regulation-explained/index_en.htm "
    excerpt: "This EU Directive requires EU importers of 3TG (tin, tungsten,
      tantalum and gold) to meet international responsible sourcing standards
      set by the OECD. The regulation is based on a due diligence approach to
      supply chains. "
    tags:
      - title: General
      - title: Regulation
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: EU Corporate Sustainability Reporting Directive
    outsideLink: https://ec.europa.eu/info/business-economy-euro/company-reporting-and-auditing/company-reporting/corporate-sustainability-reporting_en
    excerpt: The EU Directive 2014/95/EU requires companies with more than 500
      employees to disclose information on environmental matters, social matters
      & treatment of employees, respect for human rights, anti-corruption &
      bribery and diversity on company boards.
    tags:
      - title: General
      - title: Regulation
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: EU End of Life Vehicles Directive
    outsideLink: "https://ec.europa.eu/environment/topics/waste-and-recycling/end-l\
      ife-vehicles_de "
    excerpt: "Under the End-of-Life Vehicles Directive (2000/53/EC) of the European
      Union, which is currently under revision, vehicle manufacturers have
      extended responsibility for the vehicles they produced and for the
      vehicular components after use. This Directive thereby requires vehicle
      manufacturers to physically take back their products so that the items may
      be reused, recycled or remanufactured. Alternatively, manufacturers may
      delegate their responsibility to a third party.  "
    tags:
      - title: Mobility
      - title: Regulation
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: EU Environmental Health and Safety (EHS) Standards
    excerpt: The EU has broad legislation addressing environmental health and safety
      issues, divided into EU Directives, guidelines, standards and additional
      national legislation. These standards are managed by EU-OSHA, the European
      Union information agency for occupational safety and health. EU-level
      directives guide national legislation of Member States.
    outsideLink: https://osha.europa.eu/en/safety-and-health-legislation
    subtitle: ""
    tags:
      - title: General
      - title: Regulation
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: EU Extractive Waste Directive
    outsideLink: "https://ec.europa.eu/environment/topics/waste-and-recycling/minin\
      g-waste_de "
    excerpt: "The Extractive Waste Directive 2006/21/EC on the management of waste
      from extractive industries, ‘promotes preventing or significantly reducing
      adverse environmental effects...’ The Extractive Waste Directive
      particularly addresses environmental risks from wastes that impact water,
      air, soil, plants, animals, and landscapes, as well as risks to human
      health. The Directive applies to how wastes are managed that were produced
      through prospecting, extraction including quarrying, treatment, and
      storage of mineral resources. "
    logoImage: /files/1200px-Flag_of_Europe.svg.png
    tags:
      - title: General
      - title: Regulation
  - title: EU Proposal on Concerning Batteries & Waste Batteries
    outsideLink: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A52020PC0798
    excerpt: >-
      In December 2020, the European Commission published a proposal for a new
      battery regulation in the EU. It is still awaiting ratification by the
      European Parliament and the Council. Covering several key policies of the
      battery value chain, once it comes into force, it will shape the future
      production and recycling landscape of lithium-ion batteries. 

      If passed, the Proposal would introduce mandatory due diligence for cobalt, natural graphite, lithium, nickel and other chemical compounds based on one or more of these raw materials. The due diligence processes would be based on the OECD Due Diligence Guidance and would include assessment of the impacts on social and environmental risk categories.
    tags:
      - title: Mobility
      - title: Regulation
    logoImage: /files/1200px-Flag_of_Europe.svg.png
  - title: Extractive Industry Transparency Initiative (EITI)
    outsideLink: https://eiti.org/collections/eiti-standard
    excerpt: The EITI is a global standard that promotes transparency and
      accountability in bringing together the government, companies, and civil
      society within a country to ensure transparency along the oil, gas, and
      minerals value chain by disclosing information about contracts and
      licences, revenues, and economic benefits, among other aspects. Gaining
      and maintaining EITI certification has multi-stakeholder oversight as a
      prerequisite.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/eiti.0a77bc79.png
  - title: Fair Cobalt Alliance
    outsideLink: https://www.theimpactfacility.com/commodities/cobalt/fair-cobalt-alliance/
    excerpt: "The Fair Cobalt Alliance joins the key stakeholders Huayou Cobalt,
      Glencore, Tesla, The Impact Facility, The Responsible Cobalt Initiative
      (RCI) and Sono Motors in a pact to improve working conditions at artisanal
      and small-scale cobalt mining (ASM) sites in the Democratic Republic of
      the Congo. The group seeks to implement responsible mining practices by
      eliminating child labour and increasing household incomes. "
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/fair-cobalt-alliance-logo.jpg
  - title: Global Battery Alliance
    outsideLink: https://www.weforum.org/global-battery-alliance/publications-390458cd-5c86-4ed9-9b63-2ee31b672f1c
    excerpt: >-
      This project by the World Economic Forum is a global collaboration
      platform that aims to catalyse and accelerate action towards a socially
      responsible, environmentally sustainable and innovative battery value
      chain to power the Fourth Industrial Revolution. The Global Battery
      Alliance (GBA) has 10 Principles for a sustainable battery value chain,
      which as of 23 January 2020 had been adopted by 42 organisations.

      One of the GBA’s flagships is the ‘Battery Passport’, which they state should ‘offer a digital representation of a battery, conveying information about all applicable environmental, social, governance and lifecycle requirements based on a comprehensive definition of a “sustainable” battery’. It is planned to be fully operational at the end of 2022.
    tags:
      - title: Mobility
      - title: "Guidance & Initiative "
    logoImage: /files/3.GBA.jpg
  - title: Global Tailings Review
    outsideLink: https://globaltailingsreview.org/
    excerpt: "The Global Tailings Review was established by ICMM, the UNEP and PRI
      (Principles for Responsible Investment) in response to several tailing dam
      breaks over a short period of time. They created the Global Industry
      Standard on Tailings Management, a voluntary standard for safer tailings
      storage facilities, which is directed at the organization operating the
      tailings storage facility. "
    tags:
      - title: General
      - title: Standard
    logoImage: /files/global-tailings-standard.jpg
  - title: Global Reporting Initiative
    outsideLink: https://www.globalreporting.org/how-to-use-the-gri-standards/
    excerpt: The GRI provides a voluntary standard for companies from various
      sectors along the entire supply chain to achieve sustainable reporting and
      increase transparency. The standard aims to help corporations comprehend
      and communicate their influence on sustainability issues, including
      governance, human rights, social well-being and climate change. This
      initiative holds as a tenet that maintaining an informed public through
      transparency can influence corporate decisions and promote positive
      change.
    tags:
      - title: General
      - title: Reporting Template
    logoImage: /files/1.global-reporting-initiative.png
  - title: ICMM Mining Principles
    outsideLink: "https://www.icmm.com/en-gb/members/member-requirements/mining-principles "
    excerpt: "The ICMM Mining Principles provide guidance for achieving the UN
      Sustainable Development Goals (SDGs). These Principles provide guidance
      and a certification mechanism for mining corporations and associations.
      Achieving these Mining Principles can be certified by a third-party
      auditor every three years after completing a self-assessment
      questionnaire. "
    tags:
      - title: General
      - title: Standard
      - title: "Reporting Template "
    logoImage: /files/1.the-international-count-on-metals.png
  - title: IFC EHS Guidelines (World Bank)
    outsideLink: "https://www.ifc.org/wps/wcm/connect/topics_ext_content/ifc_extern\
      al_corporate_site/sustainability-at-ifc/policies-standards/ehs-guidelines
      "
    excerpt: The World Bank Group requires its clients to employ relevant measures
      that are defined under its Environmental, Health, and Safety Guidelines,
      or ‘EHS Guidelines’. This collection of technical reference documents
      developed by the International Finance Corporation (IFC) can be applied to
      all industry sectors in combination with the appropriate Industry Sector
      Guidelines, as defined by the IFC. The EHS Guidelines for Mining are
      applicable to all types of mining operations and mineral processing,
      except for the extraction of construction materials that are covered
      separately.
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/ifc-logo.png
  - title: IFC Performance Standards (World Bank)
    outsideLink: https://www.ifc.org/wps/wcm/connect/Topics_Ext_Content/IFC_External_Corporate_Site/Sustainability-At-IFC/Policies-Standards/Performance-Standards
    excerpt: >
      The 2012 edition of the IFC’s environmental and social standards, which
      define the responsibilities for managing the associated risks, are
      applicable to any project receiving funding from the IFC. An audit is
      required for those businesses that have received IFC funding.
    tags:
      - title: General
      - title: Standard
      - title: "Guidance & Initiative "
    logoImage: /files/ifc-logo.png
  - title: "ITSCI "
    outsideLink: "https://www.itsci.org "
    excerpt: "Focused on the countries Burundi, the Democratic Republic of the
      Congo, Rwanda and Uganda, the ITSCI monitors supply chains to ensure a
      common standard and audits upstream companies. Its members are required to
      conduct due diligence assessments and apply risk mitigation techniques,
      the outcomes of which are then made publicly available. The standard is in
      compliance with the OECD guidelines. "
    tags:
      - title: EEE
      - title: "Guidance & Initiative "
    logoImage: /files/itsci-logo.jpg
  - title: International Labour Organisation
    outsideLink: "https://www.ilo.org/global/lang--en/index.htm "
    excerpt: The ILO, an agency under the United Nations, broadly seeks to provide a
      system of international labour standards that support social justice. The
      standards, policies and programmes that it develops should promote safe
      and decent work. Labour standards are defined under legally binding
      ‘conventions’, eight of which are seen as fundamental, as well as certain
      associated ‘protocols’ and non-binding ‘recommendations’.
    tags:
      - title: General
      - title: Standard
    logoImage: /files/ilo-logo.png
  - title: International Labour Organisation (Mining)
    subtitle: ""
    excerpt: Apart from general labour standards, the ILO has also developed
      standards specifically for the mining sector, which cover issues from
      child labour to social development. These standards are also detailed to
      address regional differences.
    outsideLink: "https://www.ilo.org/inform/online-information-resources/research-\
      guides/economic-and-social-sectors/energy-mining/mining/lang--en/index.htm
      "
    tags:
      - title: General
      - title: Standard
    logoImage: /files/ilo-logo.png
  - title: International Standards Organisation
    outsideLink: https://www.iso.org/home.html
    excerpt: >
      Description 

      The ISO develops standards in diverse areas, including those addressing quality management, environmental management, health and safety or energy management. The most used standards covering these aspects include: 

      -	ISO 14000 on environmental management standards & guidelines

      -	ISO 45001 (based on OHSAS 18001 from the International Labour Organisation) on occupational health & safety management systems

      -	ISO 50001 on corporate energy management

      -	ISO 26000 on social responsibility to organisations

      -	ISO 20400 on sustainable procurement, addressing responsible sourcing

      -	ISO 9001 on Quality Management Systems
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/ilo-logo.png
  - title: Initiative for Responsible Mining Assurance (IRMA)
    outsideLink: https://responsiblemining.net/what-we-do/standard/#:~:text=IRMA%E2%80%99s%20Standard%20for%20Responsible%20Mining%20defines%20good%20practices,was%20developed%20and%20how%20you%20can%20give%20input
    excerpt: "The IRMA offers voluntary certification for large-scale mines of all
      commodity types according to its Standard for Responsible Mining. This set
      of criteria certifies individual mines, not mining companies, based on
      requirements for: 1) business integrity, 2) planning for positive
      legacies, 3) social responsibility and 4) environmental responsibility.
      The certification audit verifies performance of all types of mining
      operations, including surface, underground or solution mining, by an
      independent third party. "
    tags:
      - title: General
      - title: Standard
    logoImage: /files/3.IRMA_Lock up_Portrait_Blk_RGB.png
  - title: London Bullion Market Association
    outsideLink: https://www.lbma.org.uk/responsible-sourcing
    excerpt: "Based on OECD Guidance, these standards aim to strengthen
      risk-assessment business practices of refineries. The enhanced audit
      programme requires third party assessment as part of the assurance
      process. "
    tags:
      - title: Standard
    logoImage: /files/lbma-logo.jpeg
  - title: LME Responsible Sourcing Requirements
    outsideLink: https://www.lme.com/en-GB/About/Responsibility/Responsible-sourcing
    excerpt: The LME requires responsible sourcing for all of its listed brands. The
      LME is introducing responsible sourcing requirements for all metals (by
      brand) for delivery through the exchange. The first reporting period for
      firms is set for 2021 with audits (where required) to be carried out by
      the end of 2023. The LME Responsible Sourcing approach combines two
      principles – ethical responsibility and commercial interest – to employ a
      price discovery function that better reflects the value of responsibly
      sourced metal.
    tags:
      - title: General
      - title: Regulation
    logoImage: /files/1.lme-responsible-sourcing.png
  - title: Natural Resource Charter
    outsideLink: https://resourcegovernance.org/approach/natural-resource-charter
    excerpt: The Natural Resource Charter is a guidance document published by the
      Natural Resource Governance Institute and built around 12 core precepts.
      This document is meant for governments as a decision support system for
      the development of a country’s natural resources to obtain the highest
      possible benefit for countries and their citizens.
    tags:
      - title: General
      - title: Guidance & Initiative
    logoImage: /files/natural_resource_charter_.png
  - title: "OECD Guidelines for Multinational Enterprises "
    outsideLink: "http://mneguidelines.oecd.org/duediligence/ "
    excerpt: "The OECD maintains the framework Guidelines for Multinational
      Enterprises. As part of its definition of ‘responsible business conduct’,
      the Guideline indicates the importance of identifying and managing risks
      in a company’s supply chain and operations, contributing to economic,
      social, and environmental development in a host country, and contributing
      overall to the UN’s Sustainable Development Goals. "
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/3.OECD-logo.jpg
  - title: OECD Due Diligence Guidance for Responsible Mineral Supply Chains
    outsideLink: https://www.oecd.org/corporate/mne/mining.htm
    excerpt: This Guidance document is the underlying standard for a significant
      number of mineral certification schemes and audits as well as of several
      corporate policies addressing mineral sourcing from Conflict-Affected and
      High-Risk Areas (CAHRS). It provides structured and specific risk
      identification actions for a business to undertake as part of its
      responsible sourcing (RS) practices and is considered by many to be the de
      facto standard for most RS due diligence templates. EU directives,
      including the one on EU Conflict Minerals and EU Non-Financial Reporting
      Directive, also refer to this document.
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/1.oecd-due-dilligence.jpg
  - title: OECD Guidance for Meaningful Stakeholder Engagement
    outsideLink: https://www.oecd.org/publications/oecd-due-diligence-guidance-for-meaningful-stakeholder-engagement-in-the-extractive-sector-9789264252462-en.htm#:~:text=OECD%20Due%20Diligence%20Guidance%20for%20Meaningful%20Stakeholder%20Engagement,local%20communities%2C%20in%20their%20planning%20and%20decision%20making.
    excerpt: The OECD maintains its Due Diligence Guidance for Meaningful
      Stakeholder Engagement in the Extractive Sector to include various
      stakeholders – e.g. local communities, ASM, and governments – in corporate
      planning and decision-making for oil, gas, or mineral extraction
      operations.
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/3.OECD-logo.jpg
  - title: Responsible Cobalt Initiative (RCI)
    outsideLink: https://respect.international/responsible-cobalt-initiative-rci/
    excerpt: The China Chamber of Commerce for Metals, Minerals and Chemical
      Importers launched the RCI in 2016 with support from the OECD. In 2017,
      the RCI had 24 members, including Apple, BMW, CATL, Dell, HP, Huawei,
      Sony, Samsung SDI, LG Chem, Hunan Shanshan, L & F, Tianjin B & M and
      Huayou Cobalt. The initiative aims at addressing environmental and social
      risks along the cobalt supply chain, with the elimination of child labour
      as one of its primary goals. It is currently developing an auditing scheme
      and considering local development projects in the Democratic Republic of
      the Congo.
    tags:
      - title: General
      - title: "Guidance & Initiative "
      - title: Reporting Template
    logoImage: /files/rci-logo.jpeg
  - title: Responsible Minerals Initiative (RMI)
    outsideLink: http://www.responsiblemineralsinitiative.org/
    excerpt: "The RMI offers companies different tools to improve their due
      diligence schemes for their supply chains and responsible sourcing of
      minerals. Among the RMI tools, the Conflict Minerals Reporting Template
      and the Cobalt Reporting Template provide downstream companies with free,
      standardised reporting forms to share information about supply chains.
      These templates allow information about mineral origins or the refiners
      and smelters to be shared easily. This transparency also helps to identify
      smelters and refiners that have not yet been audited in the RMI
      Responsible Minerals Assurance Process. "
    tags:
      - title: General
      - title: Standard
      - title: "Reporting Template "
    logoImage: /files/rmi-logo.png
  - title: Responsible Steel Standard
    outsideLink: https://www.responsiblesteel.org/standard/
    excerpt: Developed by Responsible Steel, a not-for-profit organisation, the
      Responsible Steel Standard is an industry wide multi-stakeholder forum
      that has developed standards for its members to cover environmental,
      social, governance, management, system, stakeholder engagement, and
      closure principles.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/rsi-logo.png
  - title: Towards Sustainable Mining (TSM)
    outsideLink: "https://mining.ca/towards-sustainable-mining/ "
    excerpt: "TSM is a framework developed by the Mining Association of Canada (MAC)
      that is mandatory for all members in every country they operate. This
      system provides guidance for companies on how to manage their
      environmental and social responsibilities. Even though this is a standard
      developed with a focus on Canadian companies, it is gaining international
      recognition and has already been adopted by several other organisations,
      including the Finnish Mining Association (FinnMin), the Argentinean
      Chamber of Mining (Cámara Argentina de Empresarios Mineros), and the
      Botswana Chamber of Mines. "
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/1.towards-sustainable-mining.jpg
  - title: UN Human Rights Principles
    outsideLink: https://www.unglobalcompact.org/library/2
    excerpt: Forming the basis of many other standards, the human rights principles
      laid out by the United Nations (UN) in the UN Guiding Principles on
      Business & Human Rights provide guidelines on ‘the duties of countries and
      enterprises in respecting and protecting human rights’.
    tags:
      - title: General
      - title: Guidance & Initiative
    logoImage: /files/un-human-rights.jpg
  - title: UN Sustainable Development Goals
    outsideLink: "https://sdgs.un.org/goals "
    excerpt: As the United Nations defines its 17 goals, ‘the Sustainable
      Development Goals are the blueprint to achieve a better and more
      sustainable future for all.’ These 17 goals, which all UN member states
      agreed to achieve by year 2030, address a variety of issues, among them
      human rights, responsible consumption and production practices, poverty
      and global warming.
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/un-sdg.png
  - title: UN Principles for Responsible Investment
    outsideLink: https://www.unpri.org/sustainability-issues
    excerpt: Signatories to the UN Principles for Responsible Investment (PRI) must
      incorporate environmental and social considerations within their
      investment and ownership decisions. The PRI has 6 sustainability
      principles and offers advice and reporting guidance based on the type of
      investment tools that signatories use.
    tags:
      - title: General
      - title: Standard
      - title: Reporting Template
    logoImage: /files/pri.jpg
  - title: World Bank Climate-Smart Mining Initiative
    outsideLink: "https://www.worldbank.org/en/topic/extractiveindustries/brief/cli\
      mate-smart-mining-minerals-for-climate-action "
    excerpt: "The World Bank has set up the Climate-Smart Mining Initiative to
      address issues such as high energy consumption as well as social and
      environmental impacts coming from the mining sector. The Initiative
      focuses on four areas: 1) climate mitigation, 2) climate adaptation, 3)
      reducing material impacts, and 4) creating marketing opportunities. Part
      of the Climate-Smart Mining Initiative is also Forest-smart Mining, which
      aims to prevent ‘deforestation and supporting sustainable land-use
      practices; repurposing mine sites’ and includes ASM."
    tags:
      - title: General
      - title: "Guidance & Initiative "
    logoImage: /files/world-bank.jpg
  - title: "World Gold Council "
    outsideLink: "https://www.gold.org/about-gold/gold-supply/responsible-gold/resp\
      onsible-gold-mining-principles "
    excerpt: The Responsible Gold Mining Principles sets out a framework of
      principles addressing key environmental, social and governance issues
      specific to the gold sector. Companies are required to obtain third party
      assurance to indicate compliance with the principles.
    tags:
      - title: Standard
    logoImage: /files/world-gold-council.jpg
  - title: Cobalt Industry Responsible Assessment Framework (CIRAF)
    outsideLink: "https://www.cobaltinstitute.org/responsible-sourcing/industry-res\
      ponsible-assessment-framework-ciraf/ "
    excerpt: "The Cobalt Industry Responsible Assessment Framework (CIRAF) is a
      management tool designed for all companies, whether or not they are
      producing and/or sourcing from high-risk countries. "
    tags:
      - title: General
    logoImage: /files/cobalt_institute_ciraf.png
  - title: "RMI Downstream Assessment Programme (DAP) "
    outsideLink: "http://www.responsiblemineralsinitiative.org/responsible-minerals\
      -assurance-process/downstream-program/ "
    excerpt: "“In 2016 the RMI launched its Downstream Assessment Program to meet
      the growing demand for validation of sourcing practices of companies in
      the mineral and metal industries that are not eligible for the Responsible
      Minerals Assurance Process (RMAP) because they do not meet the definition
      of a smelter or refiner. The Downstream Assessment Program is
      minerals-agnostic and therefore inclusive of any metal/mineral within a
      company’s operations.” "
    tags:
      - title: Standard
      - title: General
    logoImage: /files/rmi.png
  - title: "RBA Validated Assessment Program (VAP) "
    outsideLink: http://www.responsiblebusiness.org/vap/about-vap/
    excerpt: "RBA has developed the Validated Assessment Program (VAP), which is a
      standard for onsite compliance verification and effective, shareable
      audits. The RBA itself does not conduct the audits but rather sets the
      standards and relies on approved audit firms. Since 2009, RBA members have
      completed more than 4,000 VAP onsite compliance audits conducted by
      independent third-party audit firms that have been approved by the RBA to
      execute the VAP protocol.  "
    tags:
      - title: Standard
      - title: General
    logoImage: /files/rmi.png
  - title: Mining local procurement reporting mechanism
    outsideLink: https://miningsharedvalue.org/mininglprm/
    excerpt: The Mining Local Procurement Reporting Mechanism (LPRM) is a set of
      disclosures on local procurement that are to be reported by organisations
      who report on mine sites. The LPRM addresses the gaps in current reporting
      frameworks and sustainability systems, and to help standardise the way the
      sector and host countries talk about these issues.
    tags:
      - title: General
      - title: Reporting Template
    logoImage: /files/lprm.png
---
*Existing approaches to Responsible Sourcing in mineral value chains include regulations, standards, guidance & initiatives and reporting templates. These are listed in alphabetical order here. Relevant approaches are listed, but does not claim to be complete.*

*For more information on Sector Roadmaps and Flagship Cases, please see* [RE-SOURCING Report](https://re-sourcing.eu/project-outputs) (1): [State-of-play: The International Responsible Sourcing Agenda.](https://re-sourcing.eu/reports/d11-in-rs-template-final)