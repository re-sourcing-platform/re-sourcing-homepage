---
template: project-outputs-page
title: Project Outputs
featuredImage: /files/project_outputs.png
assets:
  - content_type: file
    active: true
    date: 2022-08-16T09:41:03.139Z
    title: Good Practice Guidelines for the Mobility Sector
    excerpt: The good practice guidelines on responsible sourcing (RS) in the
      mobility sector outline outstanding key practices distilled from the
      RE-SOURCING Project’s research and consultations on the mobility sector.
      To promote peer learning and increase the uptake of RS practices, this
      document is of relevance to all actors involved in the mobility sector,
      especially for lithium-ion batteries, in the EU as well as
      internationally. Four good practice guidelines are elaborated in this
      document, with case studies to show how they have been implemented.
    tags:
      - title: Report
    type: Project Report
    file: /files/d5.3_guidelines_for_mobility_sector_final_20220629_final_style_guide.pdf
  - content_type: file
    excerpt: Following the State of Play and Roadmap Concepts for the Mobility
      Sector – a stock-taking report of the current sustainability challenges in
      the mobility sector – this report by the RE-SOURCING project focuses on
      the road towards achieving a sustainable mobility transition by 2050. This
      roadmap addresses four relevant raw materials used in lithium-ion
      batteries (lithium, cobalt, nickel and graphite) and three supply chain
      stages (mining, cell manufacturing and OEMs, and recycling).
      Recommendations were developed for EU policy makers, international
      industry (cell and battery producers, OEMs, recyclers, etc.) and Civil
      Society Organisations (CSOs).
    active: true
    date: 2022-08-16T09:32:41.656Z
    subtitle: Mobility Sector
    title: Roadmap for Responsible Sourcing of Raw Materials until 2050
    tags:
      - title: Report
    type: Project Report
    file: /files/re-sourcing-mobility-sector-roadmap.pdf
  - content_type: file
    excerpt: "The Roadmap for the Renewable Energy Sector by the RE-SOURCING project
      focuses on the road towards achieving a sustainable energy transition by
      2050. Five key targets are addressed in the roadmap: Circular Economy &
      Decreased Resource Consumption, Paris Agreement & Environmental
      Sustainability, Social Sustainability & Responsible Production,
      Responsible Procurement, and Level Playing Field. This report elaborates
      recommendations for civil society organisations, research and academia on
      how to achieve the five targets."
    active: true
    date: 2022-07-14T06:41:36.095Z
    subtitle: Civil Society, Research and Academia
    title: Renewable Energy Sector Roadmap for Responsible Sourcing of Raw Materials
      until 2050
    tags:
      - title: Report
    type: Project Report
    image: /files/og-image.png
    file: /files/csos_res_roadmap_20220621.pdf
  - content_type: file
    excerpt: "The Roadmap for the Renewable Energy Sector by the RE-SOURCING project
      focuses on the road towards achieving a sustainable energy transition by
      2050. Five key targets are addressed in the roadmap: Circular Economy &
      Decreased Resource Consumption, Paris Agreement & Environmental
      Sustainability, Social Sustainability & Responsible Production,
      Responsible Procurement, and Level Playing Field. This report elaborates
      recommendations for industry on how to achieve the five targets."
    active: true
    date: 2022-07-14T06:39:54.982Z
    subtitle: Industry
    title: Renewable Energy Sector Roadmap for Responsible Sourcing of Raw Materials
      until 2050
    tags:
      - title: Report
    type: Project Report
    image: /files/og-image.png
    file: /files/industry_res_roadmap_20220621.pdf
  - content_type: file
    excerpt: "The Roadmap for the Renewable Energy Sector by the RE-SOURCING project
      focuses on the road towards achieving a sustainable energy transition by
      2050. Five key targets are addressed in the roadmap: Circular Economy &
      Decreased Resource Consumption, Paris Agreement & Environmental
      Sustainability, Social Sustainability & Responsible Production,
      Responsible Procurement, and Level Playing Field. This report elaborates
      recommendations for policy makers on how to achieve the five targets."
    active: true
    date: 2022-07-14T06:33:17.451Z
    subtitle: Policy Makers
    title: Renewable Energy Sector Roadmap for Responsible Sourcing of Raw Materials
      until 2050
    tags:
      - title: Report
    type: Project Report
    image: /files/og-image.png
    file: /files/policy_res_roadmap_20220621.pdf
  - content_type: file
    excerpt: This briefing document summarises the State-of-play and roadmap concept
      of the mobility sector, including the identification of the main actors in
      and outside the EU and the challenges within the battery value chain for
      the mining, production and recycling stages. The findings indicate that
      the mining of the relevant materials is associated with negative social
      impacts like human rights violations, conflicts with local communities as
      well as environmental issues such as water & energy consumption, including
      the occurrence of toxic materials. Recycling is crucial for lithium-ion
      batteries at the end of their life cycle, among other things because the
      risk of thermal runaway is high.
    active: true
    date: 2022-06-21T06:11:50.812Z
    subtitle: Briefing Document 12
    title: Identifying Challenges & Required Actions for Responsible Sourcing in the
      e-Mobility Sector
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/bd-12-e-mobility_final.pdf
  - content_type: file
    excerpt: >-
      This briefing document examines the case for using environmental due
      diligence

      as an effective tool for risk assessment, along the entire mineral supply

      chain. It outlines the benefits of using a due diligence approach; the success

      of assessing human rights abuse risks through due diligence and the lessons

      learned that can be used to implement effective environmental risk assessments.
    active: true
    date: 2022-05-25T07:12:34.435Z
    subtitle: Briefing Document 11
    title: Environmental Due Diligence - A promising tool to manage supply chain risks
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/bd-11-environmental-due-diligence_final.pdf
  - content_type: file
    excerpt: This briefing document discusses the engineering perspective and role
      of engineers in the responsible sourcing of minerals and metals for
      manufacturing consumer goods and industrial products. As engineers largely
      develop the technical solution, generating the bill of materials and thus
      selecting materials and production processes, their impact on RS should
      not be underestimated. The decision of which raw materials are used,
      whether these raw material can be potentially sourced while fulfilling RS
      criteria, and how sustainable the supply chain for a product will be, is
      to a large extent already defined at the design and development stage of a
      new product.
    active: true
    date: 2022-03-09T13:07:13.866Z
    subtitle: Briefing Document 10
    title: Designing for Responsible Sourcing - An Engineering Perspective
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/bd-10-an-engineering-perspective.pdf
  - content_type: file
    excerpt: Este reporte describe la situación actual y el desarrollo de las
      cadenas de suministro de recursos minerales sustentables y los enfoques de
      abastecimiento responsable en el sector minero de América Latina, con
      especial atención a Chile. El documento describe la aplicación de diversas
      iniciativas internacionales, estrategias gubernamentales regionales y
      ejemplos de buenas prácticas de empresas del sector minero del cobre y el
      litio.
    active: true
    date: 2022-01-31T11:39:56.692Z
    subtitle: Briefing Document 9 (Spanish version)
    title: La búsqueda del abastecimiento responsable en el sector minero en Chile y
      América Latina
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/bd-9-latin-america-esp.pdf
  - content_type: file
    excerpt: "This briefing document outlines the current status and development of
      sustainable mineral supply chains and approaches to responsible sourcing
      in the mining sector in Latin America, with a particular focus on Chile.
      The paper outlines the implementation of various international
      initiatives, regional government strategies, and best practice examples of
      companies in the copper and lithium mining sector. "
    active: true
    date: 2022-01-31T11:33:06.332Z
    subtitle: Briefing Document 9
    title: The Pursuit of Responsible Sourcing in the Mining Sector in Chile & Latin
      America
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/bd-9-latin-america-eng.pdf
  - content_type: file
    excerpt: "Electronics is one of the largest and fastest-growing industries of
      the world. It is however associated to social and environmental risks on
      responsible sourcing. This State of play and roadmap concepts for the
      electronics sector focuses on three segments of the supply chain: mining
      companies, manufacturers (both contract and component manufacturers), and
      brands. The material focus of this State of Play report is on 3TG minerals
      (tin, tungsten, tantalum, and gold) and mica."
    active: true
    date: 2021-12-15T14:29:47.561Z
    subtitle: Project Report
    title: "State of play and roadmap concepts: Electronics Sector"
    tags:
      - title: Report
    type: Project Report
    file: /files/final_sop_eees.pdf
  - content_type: file
    excerpt: The Good Practice Guidelines on Responsible Sourcing (RS) in the
      Renewable Energy Sector outline key practices distilled from the
      RE-SOURCING Project’s research and consultations on the Renewable Energy
      (RE) Sector. As a means to promote peer learning and increase the uptake
      of RS practices, this document is of relevance to all actors involved in
      the RE Sector, in the EU as well as internationally. Four good practice
      guidelines are elaborated in this document, with case studies to show how
      they have been implemented.
    active: true
    date: 2021-10-15T07:10:18.636Z
    subtitle: Project Report
    title: Good Practice Guidelines for the Renewable Energy Sector
    tags:
      - title: Report
    type: Project Report
    file: /files/d5.2_res_guidance-document_final.pdf
  - content_type: file
    excerpt: "This Roadmap for the Renewable Energy Sector by the RE-SOURCING
      project focuses on the road towards achieving a sustainable energy
      transition by 2050. Five key targets are addressed in the roadmap:
      Circular Economy & Decreased Resource Consumption, Paris Agreement &
      Environmental Sustainability, Social Sustainability & Responsible
      Production, Responsible Procurement, and Level Playing Field. For all five
      targets recommendations to policy makers, industry and civil society are
      discussed. The RE-SOURCING projects ‘Vision 2050’ for the renewable energy
      sector based on the concepts of planetary boundaries and strong
      sustainability describes the ultimate goal to be achieved with the
      roadmap."
    active: true
    date: 2021-10-15T06:56:20.009Z
    subtitle: Renewable Energy Sector
    title: Roadmap for Responsible Sourcing of Raw Materials until 2050
    tags:
      - title: Report
    type: Project Report
    file: /files/final_res_roadmap_2021.pdf
  - content_type: file
    excerpt: "The Opening Conference ‘Drivers of Responsible Sourcing’ focused on
      four essential aspects for moving towards the responsible sourcing of raw
      minerals: Advocacy & Awareness; Industry Frontrunners & Business
      Alliances; Regulations & Standards; and Investment & Stock and Commodity
      Markets. This conference report summarises the main points that were
      brought forward during the six sessions of the conference, including
      keynote speeches and panel discussions. You can also find more information
      and detailed summaries of the individual topics in the RE-SOURCING
      Briefing Documents 5-8. "
    active: true
    date: 2021-01-20T08:37:35.950Z
    subtitle: Project Report
    title: Opening Conference Report
    tags:
      - title: Report
    type: Project Report
    file: /files/oc_conference_report_final.pdf
  - content_type: file
    excerpt: This Briefing Document details the discussions in the ‘Regulations &
      Standards’ session at the opening conference of the RE-SOURCING Project
      (18-19 January 2021). The discussion focuseson the key ingredients
      required for constructing an effective standard and its interplay with
      global benchmarks and regulations. A balance between the objectives of the
      multiple stakeholders needs to be maintained, as a well as between the
      needs for consultation and taking immediate actions. Good standards
      incorporate a vigorous standard setting process, measurable implementation
      strategies and transparency in trade-offs.
    active: true
    date: 2021-04-12T15:12:55.872Z
    subtitle: Briefing Document 8
    title: "Essentials for a Good Responsible Sourcing Standard: Purpose, Balance &
      Alignment"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-8.pdf
  - content_type: file
    active: true
    date: 2020-11-02T09:47:47.619Z
    tags:
      - title: Project Process Document
    title: "Global Exchange and Advocacy Strategy "
    file: /files/d3.1-global-exchange-and-advocacy-strategy.pdf
    type: Project Report
    excerpt: The general strategy of the RE-SOURCING project for identifying and
      engaging with international stakeholders is explained in this report. One
      of the key objectives of the project is to engage with key international
      stakeholders to foster the application of the responsible sourcing concept
      in global agenda setting.
  - content_type: file
    excerpt: "This Briefing Document details the discussions in the ‘Advocacy and
      Awareness Building’ session at the opening conference of the RE-SOURCING
      Project (18-19 January 2021). The discussion focuses on three important
      challenges of Responsible Sourcing in relation to its social aspect: How
      do we ensure that the voices of the most vulnerable groups are heard, find
      mechanisms that address their concerns, and ensure these voices are part
      of the decision-making process."
    active: true
    date: 2021-04-12T15:12:55.872Z
    subtitle: Briefing Document 7
    title: "Advocacy & Awareness Building: Connecting the Two Ends of a Mineral
      Value Chain"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/re-sourcing-briefing-document-7.pdf
  - content_type: file
    excerpt: "This Briefing Document details the discussions in the session
      ‘Industry Frontrunners & Business Alliances’ at the opening conference of
      the RE-SOURCING Project: ‘Drivers of Responsible Sourcing – Common Ground,
      Collective Action, Lasting Change’ (18-19 January 2021). The discussion
      focuses on the key factors for a successful alliance that can support
      Responsible Sourcing. Successful alliances need to have clear
      objectivesand balance the need for consultation with the need for action.
      Their governance structures need to be transparent, and they must clearly
      articulate the trade-offs their decisions entail. "
    active: true
    date: 2021-04-12T15:12:55.872Z
    subtitle: Briefing Document 6
    title: " Essentials of Successful Alliances to Support Responsible Sourcing"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-6.pdf
  - content_type: file
    excerpt: The state of play report (D4.2) for the mobility sector covers the
      value chain of the key component of the electric mobility, the lithium-ion
      battery. Focusing on mining of resources, production of battery cells and
      their recycling at the end, it looks into the responsible sourcing of the
      key materials lithium, cobalt, nickel and graphite, the key issues of the
      production of electrodes and their assembly to a battery cell and the
      various technologies of recycling of batteries and thereby shedding light
      on the involved stakeholders, initiatives and standards. The aim of the
      report is to determine topics the RE-SOURCING project needs to address in
      its roadmap for the mobility sector.
    active: true
    date: 2021-04-11T18:01:44.181Z
    subtitle: Project Report
    title: "State of Play and Roadmap Concept: Mobility Sector"
    tags:
      - title: Report
    type: Project Report
    file: /files/sop_mobility_sector.pdf
  - content_type: file
    excerpt: "This Briefing Document details the discussions in the ‘Investment,
      Stock & Commodity Markets’ session at the opening conference of the
      RE-SOURCING Project: ‘Drivers of Responsible Sourcing – Common Ground,
      Collective Action, Lasting Change’ (18-19 January 2021). Even though the
      financial sector is learning to leverage its financial power through
      direct engagement and greater sustainability reporting requirements from
      companies, challenges remain.  It is also important for policy
      stakeholders and civil society to understand the diversity within this
      sector and how to engage with these actors."
    active: true
    date: 2021-03-01T15:12:55.872Z
    subtitle: Briefing Document 5
    title: The Role of Financial Markets in Driving Responsible Sourcing
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-5.pdf
  - content_type: file
    excerpt: "This Briefing Document summarizes key points of the RE-SOURCING
      Project report ‘Responsible Sourcing in the Renewable Energy Supply Chain:
      A reality or still a long way to go? (Apr 2021). The document reviews the
      state-of-play within the renewable energy sector (PV and wind energy)
      identifying the main actors, standards and challenges across the value
      chain. It outlines major hurdles that will need to be addressed to support
      greater uptake of Responsible Sourcing practices by firms and includes the
      outline for a 2050 Vision for the sector."
    active: true
    date: 2021-03-01T15:12:55.872Z
    subtitle: Briefing Document 4
    title: Identifying Challenges & Required Actions for Responsible Sourcing in the
      Renewable Energy Sector
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-4.pdf
  - content_type: file
    active: true
    date: 2020-04-06T08:54:10.855Z
    title: Common approach for peer learning and good practice guidance
    tags:
      - title: Project Process Document
    type: Project Report
    file: /files/re-sourcing-d5.1-common-approach-for-peer-learning_08_07_2020_final.pdf
    excerpt: This report outlines the conceptual basis for the RE-SOURCING good
      practice learning1 and peer learning actions. Against this background, the
      report addresses questions of elaboration procedures, roles and
      responsibilities, and academic as well as practitioner background on these
      concepts. It will serve as a guide for project partners.
  - content_type: file
    excerpt: "This Briefing Document explains the narrative behind the opening
      conference of the RE-SOURCING Project: Drivers of Responsible Sourcing
      (18-19 January 2021). In this document, the project team shares its
      understanding of the drivers impacting the implementation of Responsible
      Sourcing  approaches and the role played by the main actors; to better
      understand the path RS approaches need to converge on and chart for the
      future in a bid to achieve a level playing field across mineral value
      chains."
    active: true
    date: 2021-03-08T09:00:05.550Z
    subtitle: Briefing Document 3
    title: "Drivers of Responsible Sourcing: Find Common Ground, Prompt Collective
      Action, Create Lasting Change"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-3.pdf
  - content_type: file
    excerpt: This Briefing Document summarizes the RE-SOURCING Project’s Virtual
      Event (October 2020) on the topic of ‘Disruptions to supply chain – RS &
      The Green Deal’. The three experts discuss the challenges in Responsible
      Sourcing and proposes policy interventions to support mining companies in
      meeting the expected increase in mineral demand for the manufacturing of
      green technologies.
    active: true
    date: 2021-03-01T15:12:55.872Z
    subtitle: Briefing Document 2
    title: "Responsible Sourcing in a Mineral Supply Restricted Environment: More
      Carrot, Less Stick?"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-2.pdf
  - content_type: file
    excerpt: "The state of play report (D4.1) for the renewable energy sector is a
      stock taking of the supply chain for the renewable energy technologies
      wind & solar power. It focuses on the following supply chain stages:
      mining and processing of copper, rare earth elements, and
      quartz;manufacturing of wind turbines and solar PV, and their collection
      and treatment. The aim of the report is to determine topics the
      RE-SOURCING project needs to address in its roadmap for the renewable
      energy sector."
    active: true
    date: 2021-02-08T09:58:25.696Z
    subtitle: Project Report
    title: "State of play and roadmap concepts: Renewable Energy Sector"
    tags:
      - title: Report
    type: Project Report
    file: /files/08.02.2021_re-sourcing_wp4_d4.1_v2_res.pdf
  - content_type: file
    excerpt: The report outlines technical information on the RE-SOURCING Platform &
      webpage, which are used to identify, manage and engage with stakeholders
      through a  digital exchange processes. It aims to combine interactive
      communication as well as knowledge provision tools on one integrated
      platform. The RE-SOURCING Platform provides opportunities to have the
      necessary information on responsible sourcing topics in one place.
    active: true
    date: 2020-04-20T07:44:01.044Z
    subtitle: ""
    title: RE-SOURCING Platform technical specification
    tags:
      - title: Project Process Document
    type: Project Report
    file: /files/d2.1-resourcing-platform-technical-specification.pdf
  - content_type: file
    excerpt: This Briefing Document summarises the increasing Responsible Souring
      (RS) requirements for businesses in mineral and metals supply chains. It
      considers the four stages of business engagement with RS practices and
      outlines reasons behind these practicesbeing a key factor for firms to
      maintain and expand their operations in the future.
    active: true
    date: 2020-09-01T15:12:55.872Z
    subtitle: Briefing Document 1
    title: "Responsible Sourcing: The Case for Business Competitiveness"
    tags:
      - title: Briefing Document
    type: Briefing Document
    file: /files/RE-SOURCING-Briefing-Document-1.pdf
  - content_type: file
    excerpt: This report provides an overview of the current state of responsible
      sourcing practices related to the extraction of minerals and their
      processing. It outlines the challenges that have been identified in the
      economic, social and environmental spheres and analysis how current RS
      approaches are attempting to address these.
    active: true
    date: 2020-04-01T15:06:33.354Z
    subtitle: Project Report
    title: "State-of-play: The International Responsible Sourcing Agenda"
    tags:
      - title: Report
    type: Project Report
    file: /files/d1.1_in-rs-template_final.pdf
  - content_type: file
    excerpt: This document defines all aspects related to the collaboration approach
      of the RE-SOURCING project and its Advisory Board and, thus, provides
      guidance on the Advisory Board’s purpose, setup, tasks, and
      responsibilities.
    active: true
    date: 2020-05-06T08:09:05.769Z
    subtitle: Deliverable 6.3
    title: Members of the Advisory Board and collaboration approach
    tags:
      - title: Project Process Document
    type: Project Report
    file: /files/d6.3-final-ab-and-collaboration-approach.pdf
  - content_type: file
    excerpt: The inception report “The RE-SOURCING Common Approach” describes the
      project approach to responsible sourcing, project priorities & activities,
      stakeholder engagement and outlines the project scope and topical areas.
    active: true
    date: 2020-04-02T15:18:00.955Z
    subtitle: Project Report
    title: The RE-SOURCING Common Approach
    tags:
      - title: Report
    type: Project Report
    file: /files/d1.2_in-rs-template_final.pdf
  - content_type: file
    active: true
    date: 2020-01-06T10:01:20.615Z
    title: "Stakeholder Management Strategy "
    tags:
      - title: Project Process Document
    file: /files/d6.1-stakeholder-management-strategy.pdf
    type: Project Report
    excerpt: This report aims to provide a centralised approach for managing the
      stakeholder’s data and engagement, and to define rules and
      respon-sibilities for project partners on how to communicate and engage
      with the external stakeholders. The main objective of the RE-SOURCING
      project is to set up an international platform on responsible sourcing
      that would provide physical and digital tools for connecting experts and
      stakeholders. Due to the key role of external stakeholders in implementing
      the project, a centralized approach to map and structure stakeholders, and
      manage their data, are defined and explained in this report.
  - content_type: file
    excerpt: The RE-SOURCING Platform Steering Committee constitutes of high-level
      experts and stakeholders to support advocacy and agenda setting for
      Responsible Sourcing (RS) in the Renewable Energy, Mobility and
      Electronics & Electrical Equipment sectors. The main tasks of the PSC will
      be co-developing sector specific roadmaps and fostering their uptake and
      dissemination to stakeholders along global mineral value chains.
    active: true
    date: 2020-05-06T07:51:42.181Z
    subtitle: Deliverable 2.2
    title: "Platform Steering Committee: Setup and Management"
    tags:
      - title: Project Process Document
    type: Project Report
    file: /files/d2.2_platform_steering-committee-setup_and_management.pdf
  - content_type: file
    excerpt: This document presents the RE-SOURCING Communication Plan. It serves as
      the guideline for communication and dissemination activities within the
      project, by defining key messages, target audiences and methods of
      communication.
    active: true
    date: 2020-03-13T08:53:46.971Z
    subtitle: Deliverable 6.2
    title: Communication plan
    tags:
      - title: Project Process Document
    type: Project Report
    file: /files/d6.2-re-sourcing-communication-plan.pdf
---

To facilitate the understanding and uptake of responsible sourcing practices in the mineral value chain, the project provides detailed reports for the three key sectors (Renewable Energy, Mobility and Electric & Electronic Equipment). Other important issues are addressed through a series of Briefing Documents.

This page also includes links to videos where industry and sector experts discuss issues around responsible sourcing and the role of various stakeholders and actors
