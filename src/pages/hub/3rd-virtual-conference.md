---
template: hub-event
slogan: Welcome to the RE-SOURCING Virtual Conference 2022
title: Reality Check on Responsible Sourcing - Trends, obstacles and opportunities
seoTitle: 3rd Virtual Conference
eventIsClosed: false
showEventPage: false
showResourcesPage: false
resourcePassword: re-s_vc22
dateString: November 7-13, 2022
dateStart: 2022-11-07T09:00:37.194Z
registerLink: https://www.eventbrite.at/e/re-sourcing-virtual-conference-2022-reality-check-on-responsible-sourcing-registration-401191914817
days:
  - agendaItem:
      - title: Conference opening
        description: "Conference moderator: Ursula Kopp (Vienna University of Economics
          and Business)"
        hour: 14
        minute: 0
        endTime: 14:30  CET
        body: Welcome and opening note
      - title: "Session 1: Stress test for Responsible Sourcing – In-between our 2050
          ambitions and economic resilience in view of rising geopolitical
          tensions"
        hour: 14
        minute: 30
        endTime: 16:00  CET
        body: >-
          The past weeks and months have been marked by rising geopolitical
          tensions and economic challenges. Businesses and the general public
          are struggling with the effects of rising inflation and uncertainties.
          In global supply chains, it seems that sustainability criteria are
          therefore taking a back seat and supply security and costs once again
          becoming the primary concern. With global challenges such as climate
          change, it is crucial that sustainability issues are not side-lined in
          global supply chains. Nevertheless, the question arises as to whether,
          in view of these changes in the framework conditions, adjustments to
          the EU climate targets and the upcoming supply chain due diligence are
          to be expected, or whether these targets can be achieved at all.


          The roadmaps of the RE-SOURCING project for the renewable energy and mobility sectors provide a tight schedule for the implementation of sustainability measures to mitigate climate change and achieve responsible supply chains.


          In this session, our experts will discuss whether sustainability is being forced into a secondary role in global supply chains, whether achieving the visions of the roadmaps seems realistic at all, and what further consequences can be expected in the future.
        description: "Session moderator: Patrick Nadoll (EIT Raw Materials)"
      - description: ""
        body: >-
          **Roadmap 2050 on Renewable Energy** 


          * [Marie-Theres Kügerl](https://www.linkedin.com/in/marie-theres-k%C3%BCgerl-59b000a2/) (Montanuniversität Leoben) 


          **Roadmap 2050 on Mobility**


          * [Johannes Betz](https://www.linkedin.com/in/johannes-betz-1a6959137/) (Oeko-Institut)
        title: "Input presentations: Perspectives from the RE-SOURCING Roadmaps 2050"
        hour: 14
        minute: 30
        endTime: 15:00  CET
      - title: "Panel discussion: Stress test for Responsible Sourcing"
        hour: 15
        minute: 0
        endTime: 16:00  CET
        body: >-
          **Panellists:**


          * [Jan Noterdaeme](https://www.linkedin.com/in/noterdaeme-jan-0b10aa63/) (CSR Europe)

          * [Cecilia Mattea](https://www.linkedin.com/in/cecilia-mattea/) (Transport & Environment)

          * [Richard Gloaguen](https://www.linkedin.com/in/richard-gloaguen-ab913b5/) (Helmholtz-Zentrum Dresden-Rossendorf)

          * [](https://www.linkedin.com/in/joanna-kuntonen-van-t-riet-0199b619/)[Ashlin Ramlochan](https://www.linkedin.com/in/ashlin-ramlochan-107342/) (Anglo American) \[tbc]


          moderated by [Patrick Nadoll](https://www.linkedin.com/in/patrick-nadoll-b7023538/) (EIT Raw Materials)
      - title: Break
        hour: 16
        minute: 0
        endTime: 16:30  CET
        body: Enjoy your break!
      - title: "Session 2: In Search of a Common Agenda - Aligning EU & Latin America
          Visions for Sustainability & Responsible Sourcing of Minerals"
        hour: 16
        minute: 30
        endTime: 18:00  CET
        body: >-
          In recent years, mineral raw materials have become a high priority
          issue in the trading relationships between European and Latin American
          countries. On one hand, the EU relies on Latin America as a major
          trading partner to source base & critical minerals for the growth of
          key industrial sectors, such as renewable energy, e-mobility and
          digital innovation. On the other hand, mining is a fundamental sector
          for many Latin American economies, with a significant weight in
          exports and in attracting direct foreign investments. While there are
          clear mutual advantages behind this trading relationship, persisting
          differences between the two regions on the role of and vision for the
          mining industry, represents a challenge in achieving sustainable,
          resilient and secure supply chains. 


          In light of recent geopolitical developments that only intensify the search for both economic resilience and socio-ecological sustainability, a stronger collaboration between the EU and Latin America is highly needed. This calls for a deeper dialogue about perspectives and needs on both sides and how the relationship needs to evolve to become a true partnership for sustainable co-development.
        description: "Session moderator: Emanuele DiFrancesco (World Resources Forum
          Association)"
      - title: "Input presentations: Perspectives from Latin America and the EU on
          Responsible Sourcing"
        hour: 16
        minute: 30
        endTime: 17:00  CET
        body: >-
          **Latin American perspective on responsible sourcing** – What’s
          responsible in countries with high raw materials extraction?


          * [Claudia Peña](https://www.linkedin.com/in/claudia-pe%C3%B1a-18a44313/) (International EPD® System)


          **European perspective on responsible sourcing** – What’s expected of trading partners?


          * Dániel Krámer (European Commission, DG TRADE)
      - title: "Panel discussion: In Search of a Common Agenda"
        hour: 17
        minute: 0
        endTime: 18:00  CET
        body: >-
          **Panellists:**


          * [Rafael Benke](https://www.linkedin.com/in/rafael-benke-9a5a171/) (Proactiva Results)

          * [Claudia Peña](https://www.linkedin.com/in/claudia-pe%C3%B1a-18a44313/) (International EPD® System)

          * Dániel Krámer (European Commission, DG TRADE)

          * [Elizabeth Ana Bastida](https://www.linkedin.com/in/anaebastida/) (University of Dundee)
      - title: Informal exchange - live day 1
        description: registered participants only
        hour: 18
        minute: 0
        endTime: 18:30  CET
        body: >-
          After a short closing remark on day 1, join this informal opportunity
          to exchange and network with conference participants and the
          RE-SOURCING project!


          The exchange will not be recorded or streamed.
    title: TUESDAY, NOVEMBER 8
    description: ""
    theme: "Zoom Webinar (registered participants) and live stream:
      https://www.youtube.com/watch?v=K0Ql2UHpt_Y"
    resourceFile:
      - icon: video
        title: (Session 1) Expert Insights - Responsible Sourcing of Coloured Gemstones
        excerpt: "Johanna H. Linus is a geologist with over 12 years experience in the
          mining industry, both in the private and public sector. In this video,
          she shares her insights about responsible sourcing in the coloured
          gemstones sector. "
        subtitle: Johanna H. Linus
        link: https://www.youtube.com/watch?v=lUt-S3zLnrQ
      - icon: document
        title: "(Session 1) Critical Raw Materials: Digging into a Sustainable Future? "
        subtitle: EU Committee of the Regions (2021)
        excerpt: "Raw materials are used to produce a wide range of goods and
          applications used in our everyday life and form a strong element of
          Europe's industrial basis. In particular, critical raw materials –
          metals, minerals and natural materials – are of economic and strategic
          importance for key and future-oriented industries. However, they have
          a high supply risk due to the import dependence from third countries,
          which are often located in unstable parts of the world. At the same
          time, the demand for (critical) raw materials is increasing worldwide
          and expected to double by 2050 in the EU, as outlined in the New
          Industrial Strategy for Europe.  "
        link: https://pes.cor.europa.eu/critical-raw-materials-digging-sustainable-future
      - icon: document
        title: "(Session 1) Raw Materials for Emerging Technologies "
        subtitle: DERA (2021)
        file: /files/dera-2021-rohstoffinformationen-50-en.pdf
        excerpt: The study analyses raw material demand from key and emerging
          technologies. The focus is on the question of which raw materials can
          be expected to experience possible increases in demand due to future
          technological developments over the next 20 years. Unexpected surges
          in demand due to technological changes on the market can have a
          significant impact on future commodity price and supply risks. The
          study is therefore updated every five years in close discussion with
          the German industry.
      - icon: document
        title: "(Session 1) Supply chains: To Build Resilience, Manage Proactively "
        subtitle: McKinsey (2022)
        excerpt: "Supply chain upheavals show little sign of abating. Companies can
          address them by reconsidering outdated, short-term strategies and
          beginning the hard work of building structural resilience. "
        link: https://www.mckinsey.com/capabilities/operations/our-insights/supply-chains-to-build-resilience-manage-proactively
      - icon: document
        title: (Session 1) Sustainable Supply Chains Helped Companies Endure the
          Pandemic
        subtitle: Sara Harrison (2021), Stanford Business
        excerpt: The COVID-19 pandemic put the fundamentals of economics front and
          center. As global supply chains were disrupted early last year,
          inventories ran short and prices rose. Yet even when things seemed to
          be falling apart, many businesses didn’t just fall back on the basics
          of supply and demand. Instead, they doubled down on making their
          supply chains more sustainable, a move that contributed to their
          survival, according to a survey of executives.
        link: https://www.gsb.stanford.edu/insights/sustainable-supply-chains-helped-companies-endure-pandemic
      - icon: document
        title: (Session 1) The Race for Raw Materials
        subtitle: Viktoria Reisch (2022), SWP
        link: https://www.swp-berlin.org/publications/products/journal_review/2022JR01_Race_RawMaterials.pdf
        excerpt: >-
          As a result of the energy transition and digitalisation, the demand
          for raw materials

          is increasing drastically. So-called critical raw materials play an important role in the economy of the European Union (EU) but are also subject to a high supply risk. This journal review examines questions about the extent to which the objectives of the EU’s policy on critical raw materials are compatible with its other aims. It also discusses how intergovernmental cooperation in the extractive sector is practiced and the role of EU member states in the process.
      - icon: document
        title: (Session 1) The Supply of Critical Raw Materials Endangered by Russia´s
          War on Ukraine
        subtitle: OECD (2022)
        excerpt: >
          Severe disruptions to global markets caused by Russia’s war on Ukraine
          have exposed vulnerabilities to the security of the supply of raw
          materials critical for industrial production and for the green
          transition.

          These supply chain vulnerabilities are the result of export restrictions, bilateral dependencies, a lack of transparency and persistent market asymmetries, including the concentration of production in just a few countries.
        link: https://www.oecd.org/ukraine-hub/policy-responses/the-supply-of-critical-raw-materials-endangered-by-russia-s-war-on-ukraine-e01ac7be/
      - icon: document
        title: "(Session 1) Trends in Global Mineral and Metal Criticality: The Need for
          Technological Foresight"
        subtitle: Patrice Christmann, Gaëtan Lefebvre (2021)
        excerpt: This study looks at future supply and demand considering the
          probability of significant technology shifts that, if confirmed, would
          deeply impact on future demand scenarios. Three technology shifts that
          appear as highly probable (Li-metal batteries (including solid-state
          Li batteries); low/no neodymium-, praseodymium-, dysprosium- or
          terbium-containing permanent magnets; and composite matrix ceramics
          used in aircraft jet engines and gas turbines used for electricity and
          heat production) are highlighting the need to better integrate
          technology foresight in criticality assessments.
        link: "https://doi.org/10.1007/s13563-022-00323-5 "
      - icon: video
        title: (Session 2) Expert Insights - Responsible ASM and how can the Industry
          Engage to make a Difference?
        subtitle: Natalia Uribe & Emanuele DiFrancesco
        excerpt: Discover in this video the presentation by Natalia Uribe from the NGO
          Alliance for Responsible Mining, followed by a Q&A, moderated by
          Emanuele Di Francesco from the World Resources Forum.
        link: https://www.youtube.com/watch?v=IkeOvhGbUhM
      - icon: document
        title: (Session 2) Economic Relations between the European Union and Latin
          America and the Caribbean
        subtitle: Luis Fierro (2022)
        excerpt: Despite the fact that a certain level of dissatisfaction is frequently
          expressed on both sides of the Atlantic, economic relations between
          the European Union (EU) and the countries of Latin America and the
          Caribbean (LAC) have grown stronger over the last decades. In recent
          however, the momentum has slowed because of the increasing trade
          between LAC and China.
        link: https://eulacfoundation.org/sites/default/files/2022-04/Policy-Brief-Relaciones-Econo%CC%81micas-EN.pdf
      - icon: document
        title: (Session 2) Latin America’s Policy Priorities on Mining and Sustainable
          Development, and Opportunities for EU Cooperation
        subtitle: "European Policy Brief: Strategic Dialogue on Sustainable Raw
          Materials for Europe (Bastida (2022), STRADE), 2018"
        link: https://ec.europa.eu/research/participants/documents/downloadPublic?documentIds=080166e5bbe3af42&appId=PPGMS
        excerpt: >-
          This Policy Brief will reviews the mining policies of governments of
          Latin American countries to provide insight into different visions of
          the sector in the region, as well as emerging trends and key
          sustainability challenges that they face.

          The ultimate objective is to inform the process of identifying the priorities of partner countries and explore suitable modes for international cooperation, in order to align both EU and partner countries’ agendas to create win-win relationships in cooperation engagements.
      - icon: document
        title: (Session 2) Trade in Raw Materials Between the EU and Latin America
        subtitle: Johanna Sydow (2015), Heinrich-Böll-Stiftung
        excerpt: In March 2014, parliamentarians from the Euro-Latin America
          Parliamentary Assembly (EuroLat) voted in favour of a resolution on
          “Trade in raw materials between the European Union and Latin America”.
          The resolution stresses that a transition towards a different model is
          needed and suggests several practical steps to improve the current
          situation.
        link: https://eu.boell.org/en/2015/01/21/trade-raw-materials-between-eu-and-latin-america
      - icon: document
        excerpt: Pía Marchegiani is Director of Environmental Policy at the Fundación
          Ambiente y Recursos Naturales (FARN), based in Buenos Aires,
          Argentina.
        title: "(Session 2) Responsible Sourcing in Argentina: Challenges, Discourses
          and Pathways for International Collaboration"
        subtitle: A conversation with Pía Marchegiani, RE-SOURCING project (2022)
        file: /files/interview-pia-marchegiani-final-version.pdf
      - icon: document
        title: (Session 2) The Search for Responsible Sourcing of Mineral Resources in
          Ecuador
        subtitle: A conversation with Elizabeth Peña Carpio and Ronald Koepke August,
          RE-SOURCING project (2022)
        excerpt: >-
          Elizabeth Peña Carpio is Professor at the Faculty of Engineering and
          Earth Sciences and Head of the Laboratory of Mineralogy and
          Mineralurgy at the Escuela Superior Politecnica del Litoral – ESPOL,
          in Ecuador.

          Ronald Koepke is an independent consultant, Germany.
        file: /files/interview-pena-carpio-koepke-english-re-sourcing.pdf
      - icon: document
        title: (Session 2) What will the Future of Colombia’s Mining Industry look like?
        subtitle: A conversation with Oscar Jaime Restrepo Baena, The Re-sourcing
          project, 2022
        excerpt: Oscar Jaime Restrepo Baena is Professor at the Department of Materials
          and Minerals of the School of Mines at Universidad Nacional de
          Colombia.
        file: /files/interview-baena-re-sourcing.pdf
      - icon: document
        title: "(Session 2) Indigenous Peoples’ Rights to Natural Tesources in
          Argentina: The Challenges of Impact Assessment, Consent and fair and
          Equitable Benefit-Sharing in Cases of Lithium Mining"
        subtitle: Pia Marchegiani, Elisa Morgera, Louisa Parks
        excerpt: >
          This article explores the role of ILO Convention 169 in two cases in
          the context of the development of lithium mining projects in
          Argentina. The cases serve to illustrate the implementation challenges
          arising from the Convention obligations on environmental impact
          assessment, free prior informed consent and benefit-sharing for the
          protection of indigenous and tribal peoples’ rights over natural
          resources pertaining to their lands.
        file: /files/indigenouspeoplesrightstonaturalresourcesinargentinathechallengesofimpactassessmentconsentandfairandequitablebenefitsharingin.pdf
      - icon: document
        title: (Session 2) Latin America’s Policy Priorities on Mining and Sustainable
          Development, and Opportunities for EU Cooperation
        subtitle: Ana Elizabeth Bastida CEPMLP (2018), University of Dundee
        excerpt: This policy brief explores themes and modes of cooperation on mining
          and minerals between the EU and Latin American countries, taking into
          consideration their policy priorities and approaches to advancing sus-
          tainable practices and the contribution of the sector to the
          Sustainable Development Goals (SDGs).
        link: https://www.stradeproject.eu/fileadmin/user_upload/pdf/STRADE_PB_LATAM_policy.pdf
  - title: WEDNESDAY, NOVEMBER 9
    agendaItem:
      - title: Opening day 2 of the live program
        hour: 9
        minute: 0
        endTime: 09:15  CET
        description: "Conference moderator: Ursula Kopp (Vienna University of Econocmics
          and Business)"
        body: ""
      - title: "Session 3: Digital Solutions for Supply Chain Due Diligence – A reality
          check for companies what to expect from blockchain & Co."
        hour: 9
        minute: 15
        endTime: 10:45  CET
        body: >-
          Companies are increasingly held accountable by regulators, workers and
          consoumers for their responsibility to minimize negative impact on
          people and the planet not only in their own operations but throughout
          their supply chains. They are pushed to collect and assess vast
          amounts of reliable information about raw material and product flows,
          supply chain actors, and their sustainability performance. 


          A variety of digital systems and platforms, especially based on concepts such as blockchain, material passports and digital twins, are emerging to offer solutions for automatizing and managing these information flows. They are aimed at scaling transparency in order to assess sustainability risks in the supply chain.


          Where do these technologies stand at the moment and what to expect from them as a company? 


          How to best get started and what is required for their implementation?  


          Are they the solution to all your problems of supply chain due diligence - what else is needed for their complementation?
        description: "Session moderator: Alexander Graf (Vienna University of Economics
          and Business)"
      - title: "Input presentations: Current State and Major Developments of Blockchain
          and Supply Chain Due Diligence"
        hour: 9
        minute: 15
        endTime: 09:45  CET
        body: >-
          **The Role of Emerging Technology to Support Sustainable Development
          in Supply Chains**


          * [](https://www.linkedin.com/in/alexander-graf-56149b160/)[Rashad Abelson](https://www.linkedin.com/in/rashadabelson/) (OECD)


          **Digital Solutions for Supply Chain Due Diligence - Some business perspectives**


          * [Audrey Daluz](https://www.linkedin.com/in/audrey-daluz/) (KPMG)
      - title: "Panel discussion: Digital Solutions for Supply Chain Due Diligence "
        hour: 9
        minute: 45
        endTime: 10:45  CET
        body: >-
          **Panellists:**


          * [Nathan Williams](https://www.linkedin.com/in/nathanwilliamsmba/) (MineSpider) \[tbc]

          * [Tanya Matveeva](https://www.linkedin.com/in/tanya-matveeva-763ba537/) (KamniChain)

          * [Anna Stancher](https://www.linkedin.com/in/anna-stancher-a32b7a32/) (Responsible Minerals Initiative)

          * [Niels Angel](https://www.linkedin.com/in/niels-angel-2a8985172/) (BMW, Catena-X)


          moderated by [Alexander Graf](https://www.linkedin.com/in/alexander-graf-56149b160/) (Vienna University of Economics and Business)
      - title: Break
        hour: 10
        minute: 45
        endTime: 11:15  CET
        body: Enjoy your break!
      - title: "Session 4: Chasing Unicorns? How Able is the Mining Sector to Extract
          Responsibly Sourced Minerals for the Green Economy?"
        hour: 11
        minute: 15
        endTime: 12:45  CET
        body: The Responsible Sourcing (RS) landscape has concentrated heavily on
          identifying RS problems and challenges in the extractive sector and
          outlined several standards and guidelines to overcome these obstacles.
          The flipside of this coin is the economic, technical and managerial
          capacity of the extractive sector to meet these ESG standards,
          particularly at a time where metal demand projections suggest that
          more raw materials well be required to pave the green roadmaps for
          renewable energy and e-mobility sectors. This panel brings together
          industry experts to discuss the mineral supply equation and industry
          capacity to supply responsibly extracted minerals. The panel addresses
          how realistic the chances are for the global mining sector to
          *responsibly* supply the minerals for a green economy.
        description: "Session moderator: Masuma Farooki (MineHutte)"
      - title: Input presentation
        hour: 11
        minute: 15
        endTime: 11:45  CET
        body: >-
          Efforts and Impact of Sustainability in the Mining Sector - Some Food
          for Thought 


          * [Masuma Farooki](https://www.linkedin.com/in/masumafarooki/) (MineHutte)
      - title: "Panel discussion: Chasing Unicorns?"
        hour: 11
        minute: 45
        endTime: 12:45  CET
        body: >-
          Panellists:


          * [](https://www.linkedin.com/in/andrew-van-zyl-0559505/)[Amanda van Dyke](https://www.linkedin.com/in/amanda-van-dyke-528b1b35/) (ARCH Emerging Markets Partner)

          * [Ângela Viana](https://www.linkedin.com/in/%C3%A2ngela-viana-60219317b/) (VdA Vieira de Almeida)

          * [Andrew van Zyl](https://www.linkedin.com/in/andrew-van-zyl-0559505/) (SRK Consulting South Africa)

          * [Mark Fellows](https://www.linkedin.com/in/mark-fellows-bab23312/) (Skarn Associates)


          moderated by [Masuma Farooki](https://www.linkedin.com/in/masumafarooki/) (MineHutte)
      - title: Closing of live program
        hour: 12
        minute: 45
        endTime: 13:00  CET
      - title: Informal exchange - live day 2
        hour: 13
        minute: 0
        endTime: 13:30  CET
        body: >-
          After a short closing remark of the live program, join this informal
          opportunity to exchange and network with conference participants and
          the RE-SOURCING project!


          The exchange will not be recorded or streamed.
    theme: Zoom Webinar (registered participants) and live stream (without
      registration)
    description: ""
    resourceFile:
      - icon: video
        title: "(Session 3) Expert Insights - Designing for Excellence: Responsible
          Sourcing & Product Development"
        excerpt: "In this video Masuma Farooki from MineHutte interviews Jan Rosenkranz,
          Luleå University of Technology to find out what design for excellence
          is, and its relationship to more responsible sourcing of raw
          materials. "
        subtitle: Jan Rosenkranz & Masuma Farooki, 2022
        link: https://www.youtube.com/watch?v=HP7Zt1J6ess
      - icon: video
        title: "(Session 3) Blockchain on the rocks "
        subtitle: Blockchain in Mining Webinars, 2022
        excerpt: In this series of webinars, speakers present concepts and examples of
          real-life applications of blockchain technology in mining and other
          industries.
        link: https://www.blockchaininmining.com/
      - icon: document
        title: (Session 3) Due diligence toolbox
        link: https://single-market-economy.ec.europa.eu/sectors/raw-materials/due-diligence-ready/due-diligence-toolbox_en
        excerpt: This online portal contains information, tools and training materials
          to guide your company in conducting due diligence on its minerals and
          metals supply chain.
      - icon: document
        title: (Session 3) Is there a role for blockchain in responsible supply chains?
        excerpt: This paper takes a critical look at how blockchain technology is
          currently being developed and used to advance due diligence in supply
          chains. It includes considerations for how responsible business
          conduct objectives can be integrated into emerging blockchain
          initiatives in a consistent and effective way.
        link: https://mneguidelines.oecd.org/Is-there-a-role-for-blockchain-in-responsible-supply-chains.pdf
        subtitle: OECD, 2019
      - icon: document
        title: (Session 3) RMI Blockchain Guidance (2nd edition)
        subtitle: Responsible Minerals Initiative, 2020
        excerpt: >-
          Blockchain technology is increasingly applied as a tool to enhance
          transparency in mineral supply chains with a view to determine the
          point of origin of minerals and metals as well as obtain and share
          data relative to responsible mining practices.

          The RMI has developed these draft Blockchain Guidelines (herein “Guidelines”) to promote the common adoption of definitions and concepts, consensus on the fundamental data attributes to be included, interoperability of blockchain-enabled solutions, and the understanding of governance and incentive models that support a direct or indirect 

          positive impact from the application of blockchain technology for supply chain actors and communities in mineral producing countries.
        file: /files/rmi-blockchain-guidelines-second-edition-march-2020.pdf
      - icon: document
        title: "(Session 3) Supply chain transparency - Creating stakeholder value on
          three levels "
        subtitle: KPMG Advisory, 2021
        link: https://assets.kpmg/content/dam/kpmg/nl/pdf/2021/services/supply-chain-transparency-web.pdf
        excerpt: >-
          Today’s business climate is becoming more and more interconnected and
          complex. Customer needs and expectations are increasing and
          businesses, while meeting these expectations, are required to be
          valuable, profitable, flexible, safe and cost-efficient.
          Simultaneously, business

          models are shifting towards partnerships and platform-based businesses. This requires companies to actively engage with different third-party organizations. Additionally, complexity is on the rise due to outsourcing, globally dispersed partners and constant pressure on costs. Such aspects lead to intertwined and multiplex supply chains that can be difficult to administer and are prone to risks.
      - icon: document
        title: (Session 3) The Potential of Machine Learning for a More Responsible
          Sourcing of Critical Raw Materials
        subtitle: Pedram Ghamisi; Kasra Rafiezadeh Shahi; Puhong Duan; Behnood Rasti; et
          al., 2021
        link: https://ieeexplore.ieee.org/abstract/document/9527092
        excerpt: The digitization and automation of the raw material sector is required
          to attain the targets set by the Paris Agreements and support the
          sustainable development goals defined by the United Nations. While
          many aspects of the industry will be affected, most of the
          technological innovations will require smart imaging sensors. In this
          review, we assess the relevant recent developments of machine learning
          for the processing of imaging sensor data. We first describe the main
          imagers and the acquired data types as well as the platforms on which
          they can be installed. We briefly describe radiometric and geometric
          corrections as these procedures have been already described
          extensively in previous works. We focus on the description of
          innovative processing workflows and illustrate the most prominent
          approaches with examples.
      - icon: document
        title: (Session 4) 5 Ways to Make Mining More Sustainable
        subtitle: " Megan R. Nichols, 2020"
        excerpt: Despite technological advancements that have made the industry more
          green, mining still uses significant amounts of resources — water,
          land, carbon and energy — and often causes severe harm to the
          environment. This damage, if not correctly handled, can last for
          decades after mining operations have shut down, make the land more
          vulnerable to natural processes like soil erosion and can worsen after
          the equipment is out.
        link: https://empoweringpumps.com/5-ways-to-make-mining-more-sustaina
      - icon: document
        title: (Session 4) Is the mining sector ready and able to deliver net zero?
        subtitle: " Cecilia Keating, 2022"
        excerpt: Interview  with Ro Dhawan, CEO of the International Council of Mining
          and Metals to discuss the battery gold rush and the potential for a
          'sustainable' mining boom in Europe. Ro Dhawan is confident Europe's
          drive to shore up mineral security and deliver on climate goals could
          result in a cleaner and greener future for the global mining industry.
        link: https://www.businessgreen.com/interview/4052991/mining-sector-ready-able-deliver-net-zero-commodities-hub
      - icon: document
        title: (Session 4) Minerals  for the Green Economy
        subtitle: Geological survey of Norway, 2017
        link: https://www.ngu.no/sites/default/files/Minerals%20for%20the%20green%20economy%202016_screen.pdf
        excerpt: The development of a green economy will, like all the major changes in
          history, require more use of mineral resources. The report examines
          different aspects of the minerals needed in a green economy,
          potentials and challenges, and also sketch ways in which
          private-public partnerships can contribute to achieving key goals.
      - icon: document
        title: (Session 4) Mining takes on the sustainability challenge
        link: https://www.kearney.com/social-impact-and-sustainability/article/-/insights/mining-takes-on-the-sustainability-challenge
        subtitle: Kearney, 2018
        excerpt: Mines have a lasting impact on a community’s air, water, and land.
          Despite the commodity crisis, more mining companies are making
          sustainability a core element in their business strategies.
      - icon: document
        title: (Session 4) Sustainability Reporting in the Mining Sector
        subtitle: UN Environmental program , 2020
        link: https://www.unep.org/resources/report/sustainability-reporting-mining-sector
        excerpt: "The report examines the context and state of sustainability reporting
          in the large-scale mining sector, with a specific focus on what
          governments have done to enhance the transparency of the sector. "
  - title: THURSDAY, NOVEMBER 10
    theme: Zoom Meeting (registered participants and reservation of consultation
      slots)
    agendaItem:
      - title: "Consult the RE-SOURCING Team #1"
        body: >-
          #### **Reservations closed, no more open slots available - Thank you
          for your interest!**


          **Please contact *[info@re-sourcing.eu](mailto:info@re-sourcing.eu)* for more information.**


          You have specific questions on Responsible Sourcing of mineral raw materials, about what is needed from you, or how to implement best practices in your organisation?


          No matter whether you are a business manager, policy maker, researcher or civil society representative, let us know your question and reserve your own informal 30-minute slot with experts from the RE-SOURCING team that we select based on your needs. Fully confidential and free of charge!
        hour: 10
        minute: 0
        endTime: 11:30  CET
      - body: >-
          #### **Reservations closed, no more open slots available - Thank you
          for your interest!**


          **Please contact *[info@re-sourcing.eu](mailto:info@re-sourcing.eu)* for more information.**


          You have specific questions on Responsible Sourcing of mineral raw materials, about what is needed from you, or how to implement best practices in your organisation?


          No matter whether you are a business manager, policy maker, researcher or civil society representative, let us know your question and reserve your own informal 30-minute slot with experts from the RE-SOURCING team that we select based on your needs. Fully confidential and free of charge!
        title: "Consult the RE-SOURCING Team #2"
        hour: 14
        minute: 0
        endTime: 15:00  CET
      - title: Project Networking Session
        hour: 15
        minute: 0
        endTime: 17:00  CET
        body: >-
          Join a selection of relevant projects in the sphere of Responsible
          Sourcing to network and learn from each other about the latest
          research, innovation and coordination activities in the EU's Horizon
          programme and beyond.


          Presenting projects and organisations:


          [ELLED](https://collaboration.worldbank.org/content/sites/collaboration-for-development/en/groups/extractives-for-local-development.html)


          [SUMEX](https://www.sumexproject.eu/)


          [KamniChain](https://kamni.co/)


          [Green Dealings](https://green-dealings.com/)


          [AfricaMaVal](https://africamaval.eu/)


          For registered participants only.
    description: ""
agendaMessage: "#### **See the conference agenda
  [here](https://www.eventbrite.at/e/re-sourcing-virtual-conference-2022-realit\
  y-check-on-responsible-sourcing-registration-401191914817)**"
ytVideoId: ""
featuredImage: /files/mari-lezhava-q65bne9fw-w-unsplash.jpg
passwordCheckIntroText1: >-
  **Welcome to the resource library of the RE-SOURCING Virtual
  Conference 2022!**


  The library offers you a concise selection of cutting-edge materials for each of the four main conference topics. It comprises documents, links and videos to explore and will also include the recordings of the live sessions after each day.
passwordCheckIntroText2: "*\\*Registered participants receive their password in
  a separate email together with the Zoom links in the week before the
  conference and again on the first day of the conference. Please also check
  your spam folder in case you have not received any messages or
  contact [noe.barriere@wu.ac.at](mailto:noe.barriere@wu.ac.at) and [andreas.en\
  dl@wu.ac.at](mailto:andreas.endl@wu.ac.at) in case you are facing any
  issues.*"
---
# **The RE-SOURCING Virtual Conference 2022 is closed!**

#### **Thank you for your interest and all the great feedback we received from speakers and the audience!**

#### **Follow the recordings in case you have missed our exciting live programme:**

**Day 1 (November 8):** <https://www.youtube.com/watch?v=K0Ql2UHpt_Y>

**Day 2 (November 9):**[](https://www.youtube.com/watch?v=K0Ql2UHpt_Y) [https://www.youtube.com/watch?v=3E5giz1r5m](https://www.youtube.com/watch?v=3E5giz1r5mE)

#### Stay tuned - Our conference report and the publication of speaker slides will follow soon on our website!

#### **See the online agenda [here](https://re-sourcing.eu/hub/3rd-virtual-conference/agenda-and-sessions/)**

*\*To learn more about the event's data privacy & protection, please read the [Participant Information Sheet](https://re-sourcing.eu/static/65293cb44ec19c2e64899d74c7e62152/virtual-conference-2022-participant-information-sheet_final.pdf).*