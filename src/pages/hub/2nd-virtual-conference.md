---
template: hub-event
slogan: Welcome to the RE-SOURCING Virtual Conference 2021
title: On the Road to Responsible Sourcing – How to achieve lasting impact
seoTitle: 2nd Virtual Conference
eventIsClosed: true
showEventPage: false
showResourcesPage: false
resourcePassword: RE-S_VC21
dateString: November 8-10, 2021
dateStart: 2021-11-08T12:30:00.000Z
registerLink: https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339
days:
  - title: MONDAY, NOVEMBER 8
    theme: Supply Chain Due Diligence - How to move towards meaningful and holistic
      impact?
    description: ""
    agendaItem:
      - title: Conference opening
        body: >-
          * **Welcome by the event moderater**

          * **Introduction to the RE-SOURCING project** by [Alexander Graf](https://www.linkedin.com/in/alexander-graf-56149b160/) (Vienna University of Economics and Business, Institute for Managing Sustainability)

          * **Setting the Stage: Are sustainability and responsibility strategic success factors for corporations?** by Prof. [André Martinuzzi](https://www.linkedin.com/in/andre-martinuzzi-92741a49/) (Vienna University of Economics and Business, Institute for Managing Sustainability)
        hour: 13
        minute: 30
        endTime: 14:00  CET
        description: Welcome and opening note
      - title: "Supply chain due diligence - How to move towards meaningful and holistic
          impact? "
        body: >-
          **Speaker:** 


          * [Fabiana Di Lorenzo](https://www.linkedin.com/in/fabianadilorenzo/) (Responsible Business Alliance)
        hour: 14
        minute: 0
        description: Keynote
        endTime: 14:30 CET
      - title: From due diligence and risk assessment to driving change on the ground in
          mineral value chains – Does DD work for businesses and local
          communities alike?
        description: Panel discussion
        hour: 14
        minute: 30
        body: >-
          **Panellists:**


          * [James Nicholson](https://www.linkedin.com/in/jamesajnicholson/) (Trafigura)

          * Guy Muswil (Kamoa Copper S.A.)

          * [Angela Jorns](https://www.linkedin.com/in/angela-jorns-513a0b49/) (Levin Sources)

          * [Telye Yurisch Toledo](https://www.linkedin.com/in/telye-heine-yurisch-toledo-8119b2167/) (TERRAM)


          moderated by [Tobias Kind-Rieper](https://www.linkedin.com/in/tobias-kind-rieper-765356ba/) (WWF Germany)
        endTime: 15:30 CET
      - title: Break
        hour: 15
        minute: 30
        body: Enjoy your coffee!
        endTime: 15:45 CET
      - description: Panel discussion
        title: Expanding due diligence to the environment – How to achieve holistic
          impact?
        hour: 15
        minute: 45
        body: >-
          **Panellists:**


          * [Jan Kosmol ](https://www.linkedin.com/in/jan-kosmol-21b15693/)(German Environment Agency)

          * [Jane Joughin](https://www.linkedin.com/in/jane-joughin-a45a0b106/) (SRK Consulting)

          * [Jorge Sanhueza](https://www.linkedin.com/in/jorge-sanhueza-urz%C3%BAa-8bab1787/) (CODELCO)

          * [Johanna Sydow ](https://www.linkedin.com/in/johanna-sydow-7b57a411b/)(Germanwatch)


          moderated by [Tobias Kind-Rieper](https://www.linkedin.com/in/tobias-kind-rieper-765356ba/) (WWF Germany)
        endTime: 16:45 CET
      - title: Project presentations
        hour: 16
        minute: 45
        body: >-
          **Projects:** 


          * [Circulor](https://www.circulor.com/) ([Ellen Carey](https://www.linkedin.com/in/ellenpcarey/))

          * [](https://www.mineralplatform.eu/)[EU-Latin America Partnership on Raw Materials](https://www.mineralplatform.eu/) ([Aissa Rechlin](https://www.linkedin.com/in/dr-aissa-rechlin-083b5b84/))

          * [GemFair](https://gemfair.com/) ([Adam Rolfe](https://www.linkedin.com/in/adrolfe/))
        endTime: 17:30 CET
      - title: "Informal exchange - day 1 "
        description: registered participants only
        minute: 30
        hour: 17
        endTime: 18:00 CET
        body: >-
          After a short closing remark on day 1, join this informal opportunity
          to exchange and network with project presenters and the RE-SOURCING
          project!


          The exchange will not be recorded or streamed.
    resourceFile:
      - icon: link
        title: RE-SOURCING Virtual Conference 2021 - Day 1
        subtitle: Session recording
        excerpt: >-
          Full recording of day 1 (Monday, November 8) of the RE-SOURCING
          Virtual Conference 2021!


          Please see the conference Agenda to see the speakers and topics.
        link: https://www.youtube.com/watch?v=jj3a0qQmJkc
      - icon: video
        link: https://www.youtube.com/watch?v=0aEORowxNDs
        title: "Assessing impacts of responsible sourcing initiatives for cobalt:
          Insights from a case study"
        subtitle: Paper presentation (15min) | Nicolas A. Eslava (2021)
        excerpt: 'This short video presentation summarizes the main questions and
          findings of the academic article "Assessing impacts of responsible
          sourcing initiatives for cobalt: Insights from a case study" by Lucia
          Mancini, Nicolas A. Eslava, Marzia Traverso, Fabrice Mathieux (2021;
          see below)'
      - title: "Assessing impacts of responsible sourcing initiatives for cobalt:
          Insights from a case study"
        subtitle: Academic article | Lucia Mancini, Nicolas A. Eslava, Marzia Traverso,
          Fabrice Mathieux (2021)
        icon: document
        file: /files/virtual-conference/assessing-impacts-of-rs-initiatives-for-cobalt-a-case-study-mancini-et-al-2021.pdf
        excerpt: >
          This article analyses two due diligence schemes for cobalt in the DRC,
          with a focus on ASM (artisanal and small-scale mining). It highlights
          key factors for their effectiveness while at the same time also
          addressing the most important shortcomings.
      - file: /files/virtual-conference/oecd-due-diligence-guidance-third-edition.pdf
        icon: document
        title: OECD Due Diligence Guidance for Responsible Supply Chains of Minerals
          from Conflict-Affected and High-Risk Areas Third ed.
        subtitle: Guidance document | OECD (2016)
        excerpt: This guidance document from the OECD serves as a foundational starting
          point for many current due diligence and responsible sourcing efforts.
          It provides detailed recommendations for companies to respect human
          rights and avoid contributing to conflict through their mineral
          purchasing decisions and practices.  The OECD Guidance is global in
          scope and applies to all mineral supply chains.
      - icon: link
        title: OECD Due Diligence Guidance for Responsible Mineral Supply Chains
        link: https://www.duediligenceguidance.org/
        subtitle: Website tool | OECD
        excerpt: "The website provides a practical and easy-to-use overview of the
          5-step process of the OECD Due Diligence Guidance in English, French
          and Spanish. "
      - title: OECD position paper - Costs and Value of Due Diligence in Mineral Supply
          Chains
        icon: document
        file: /files/virtual-conference/oecd-position-paper-costs-and-value-of-dd-in-mineral-supply-chains.pdf
        subtitle: Position paper | OECD (2021)
        excerpt: On-going multi-stakeholder discussions have raised important questions
          on the perceived imbalance of how due diligence costs and benefits are
          distributed along the supply chain. This position paper was drafted in
          response to stakeholder calls that the OECD examine this
          topic  with  the  objective  of  raising awareness, better informing
          discussions, identifying key research  questions, and guiding
          stakeholders towards viable solutions.
      - title: "Why Environmental Due Diligence Matters in Mineral Supply Chains: The
          Case of the Planned Llurimagua Copper Mine, Ecuador"
        icon: document
        file: /files/virtual-conference/why-environmental-dd-matters-in-mineral-supply-chains-ecuador-case-study.pdf
        subtitle: Case study | C. Zorilla & J. Sydow (2020)
        excerpt: This case study focuses on two copper mines to show the importance of
          environmental due diligence. It also highlights the linkages between
          environmental due diligence and improvements with regards to human
          rights.
      - title: IGF Guidance for Governments - Environmental Management and Mining
          Governance
        icon: document
        file: /files/virtual-conference/igf-guidance-for-governments-environmental-management-and-mining-governance.pdf
        subtitle: Guidance document | Intergovernmental Forum on Mining, Minerals,
          Metals and Sustainable Development (IGF) (2021)
        excerpt: This guidance summarises international practices and highlights
          universally applicable standards and practices in order to develop and
          implement legal frameworks, regulations, and policy.
      - title: The complementing role of sustainability standards in managing
          international and multi-tiered mineral supply chains
        subtitle: Academic article | P. C. Sauer (2021)
        icon: document
        file: /files/virtual-conference/the-complementing-role-of-sustainability-standards-in-mineral-supply-chains-sauer-2021.pdf
        excerpt: This paper shows how standards can complement companies' own supply
          chain due diligence efforts, and help them to tackle issues beyond
          their scope.
      - title: EU Study on due diligence requirements through the supply chain
        icon: document
        file: /files/virtual-conference/study-on-due-diligence-requirements-through-the-supply-chain-eu-2021.pdf
        subtitle: Study/report | L. Smit et al. (2020)
        excerpt: This background study lays the foundation and justification for the EU
          Directive on 'Mandatory Human Rights, Environmental and Good
          Governance Due Diligence', which is currently under development and
          expected to be a milestone in increasing responsible sourcing in the
          EU and beyond.
      - icon: document
        title: Approaches to Responsible Sourcing in Mineral Supply Chains
        subtitle: Academic article | S. van den Brink, R. Kleijn, A. Tukker, J. Huisman
          (2019)
        excerpt: This article is based on a literature review exploring existing
          definitions of responsible sourcing in order to specify the authors'
          own udnerstanding. It demonstrates a clear link between effective due
          diligence and sustainability schemes.
        file: /files/virtual-conference/approaches-to-responsible-sourcing-in-mineral-supply-chains-v.d.-brink-et-al..pdf
      - icon: document
        title: "International Mineral Trade on the Background of Due Diligence
          Regulation: A Case Study of Tantalum and Tin Supply Chains from East
          and Central Africa"
        subtitle: Academic article | P. Schütte (2019)
        excerpt: This article conducted an evaluation of international tantalum and tin
          trade data from the Great Lakes region following the implementation of
          due diligence regulations. It concluded that such regulations do not
          negatively impact trade volumes and improve the consistency of trade
          data.
        file: /files/virtual-conference/international-mineral-trade-on-the-background-of-due-diligence-regulation.pdf
      - icon: document
        title: Chinese Due Diligence Guidelines for Responsible Mineral Supply Chains
        subtitle: Guidance document | CCCMC (2017)
        excerpt: This guidance document has been developed by the China Chamber of
          Commerce of Metals, Minerals & Chemicals Importers and Exporters in
          collaboration with the OECD and aligned with the OECD Due Diligence
          Guidance.
        file: /files/virtual-conference/chinese-due-diligence-guidelines-for-responsible-mineral-supply-chains.pdf
      - icon: document
        title: "Responsible Sourcing in a Mineral Supply Restricted Environment: More
          Carrot, Less Stick?"
        subtitle: RE-SOURCING Briefing document | M. Farooki (2021)
        excerpt: This Briefing Document summarizes the RE-SOURCING Project’s Virtual
          Event (October 2020) on the topic of ‘Disruptions to supply chain – RS
          & The Green Deal’. Three experts discussed the challenges in
          Responsible Sourcing and propose policy interventions to support
          mining companies in meeting the expected increase in mineral demand
          for the manufacturing of green technologies.
        link: https://re-sourcing.eu/reports/re-sourcing-briefing-document-2
      - icon: link
        link: https://ec.europa.eu/growth/sectors/raw-materials/due-diligence-ready_en
        title: Due Diligence Ready!
        subtitle: Website tool | European Commission
        excerpt: "Due Diligence Ready! is the European Commission's online platform to
          provide concrete guidance for the implementation of the EU Conflict
          Mineral Regulation. It contains concrete information, tools and
          training materials for due diligence on minerals and metals supply
          chains, available in seven languages. "
    body: >-
      Supply Chain Due Diligence (DD) has gained traction over the last decade
      as a major means to increase transparency and accountability in companies’
      supply chains. Led by foundational efforts such as the [UN Guiding
      Principles on Business and Human
      Rights](https://www.ohchr.org/Documents/Publications/GuidingPrinciplesBusinessHR_EN.pdf) and
      the [OECD Due Diligence
      Guidance](https://www.oecd.org/investment/due-diligence-guidance-for-responsible-business-conduct.htm),
      multiple actors continue to develop DD strategies and practices in order
      to work towards global sustainability agendas. However, the increased
      adoption also leads to a heterogeneous understanding and implementation of
      DD with differing outcomes and impacts. In this session, a diverse group
      of experts from business, policy and civil society will tackle different
      issues surrounding a major question in this regard: How does DD lead to
      change that is both effective and holistic in terms of the society and the
      environment?


      **Event moderator:** Ursula Kopp (Institute for Managing Sustainability, Vienna University of Economics and Business)


      **Session moderator:** [Tobias Kind-Rieper](https://www.linkedin.com/in/tobias-kind-rieper-765356ba/) (WWF Germany)
    zoomUrl: https://wu-ac-at.zoom.us/j/95575444322?pwd=WUdiNkRJdkpFcWcwSFRQdmR1SVNFdz09
  - title: TUESDAY, NOVEMBER 9
    theme: Responsible Sourcing for the Green Transition – A Roadmap to 2050
    description: ""
    body: >-
      The global transition of the energy and mobility sectors that is necessary
      to combat climate change requires vast amounts of raw materials. To be
      truly sustainable, ’green’ energy and transportation need to ensure that
      the sourcing of raw materials is conducted in a socially and
      environmentally responsible way. In this session, international experts
      will take a deep dive into the concrete targets and measures needed to
      realize this ambition, informed by the RE-SOURCING Project’s Roadmap for
      the Renewable Energy Sector and a preview of results from our Mobility
      Sector Roadmap.


      **Event moderator:** Ursula Kopp (Institute for Managing Sustainability, Vienna University of Economics and Business)


      **Session moderator:** [Masuma Farooki](https://www.linkedin.com/in/masumafarooki/) (MineHutte)
    agendaItem:
      - title: Responsible Sourcing for the Green Transition - The Roadmap for the
          Renewable Energy Sector
        hour: 9
        minute: 0
        endTime: 09:55 CET
        description: Keynote with expert statements
        body: >-
          **Speaker:**


          * [Marie-Theres Kügerl](https://www.linkedin.com/in/marie-theres-k%C3%BCgerl-59b000a2/) (Montanuniversität Leoben)


          **Experts:**


          * Maria Nyberg (European Commission, DG Grow)

          * [Raffaele Rossi](https://www.linkedin.com/in/rossiraffaele/) (Solar Power Europe)

          * Isabelle Geuskens (Friends of Earth)

          * Benjamin Katz (OECD)

          * [Estelle Gervais](https://www.linkedin.com/in/estelle-gervais/?originalSubdomain=de) (Fraunhofer)
      - title: "Responsible sourcing for the Green Transition - Towards a roadmap for
          the Mobility Sector "
        hour: 9
        minute: 55
        endTime: 10:15 CET
        body: >-
          **Speaker:**


          * [Johannes Betz](https://www.linkedin.com/in/johannes-betz-1a6959137/) (Oeko-Institut)
        description: Keynote
      - body: Enjoy your coffee!
        minute: 15
        endTime: 10:30 CET
        hour: 10
        title: Break
      - title: "Procurement - Global perspectives on implementing a responsible supply
          chain "
        hour: 10
        minute: 30
        endTime: 11:45 CET
        body: >-
          **Panellists:**


          * [Jeff Geipel](https://www.linkedin.com/in/jeff-geipel-84a68718/) (Engineers without Borders Canada)

          * [Jonas Astrup](https://www.linkedin.com/in/jastrup/) (International Labor Organisation)

          * [Amir Shafaie](https://www.linkedin.com/in/amir-shafaie-5549782/) (Natural Resource Governance Institute)

          * [Marie-Theres Kügerl](https://www.linkedin.com/in/marie-theres-k%C3%BCgerl-59b000a2/) (Montanuniversität Leoben)


          moderated by [Masuma Farooki](https://www.linkedin.com/in/masumafarooki/) (MineHutte)
        description: Panel discussion
      - title: Project presentations
        description: ""
        hour: 11
        minute: 45
        endTime: 12:30 CET
        body: >-
          **Projects:**


          * [NEXT](http://www.new-exploration.tech/) (Dirk De Ketelaere)

          * [SUSMAGPRO](https://www.susmagpro.eu/) ([Carlo Burkhardt](https://www.linkedin.com/in/carlo-burkhardt-12363856/))

          * [Resource Impact Dashboard](https://www.resource-impact.org/#/home) ([Felicitas Fischer](https://www.linkedin.com/in/felicitas-fischer-3aa4675b/))
      - title: Informal exchange - day 2
        description: registered participants only
        hour: 12
        minute: 30
        endTime: 13:00 CET
        body: >-
          After a short closing remark on day 2, join this informal opportunity
          to exchange and network with project presenters and the RE-SOURCING
          project!


          The exchange will not be recorded or streamed.
    resourceFile:
      - icon: link
        title: RE-SOURCING Virtual Conference 2021 - Day 2
        subtitle: Session recording
        link: https://www.youtube.com/watch?v=dbIoeh1bRZk
        excerpt: >-
          Full recording of day 2 (Tuesday, November 9) of the RE-SOURCING
          Virtual Conference 2021!


          Please see the conference Agenda to see the speakers and topics.
      - icon: video
        title: "International mineral trade on the background of due diligence
          regulation: A case study of tantalum and tin supply chains from East
          and Central Africa"
        subtitle: Paper presentation (18min) | Philipp Schütte (2019)
        excerpt: >
          This short video presentation summarizes the main questions and
          findings of the academic article "International mineral trade on the
          background of due diligence regulation: A case study of tantalum and
          tin supply chains from East and Central Africa" by Philipp Schütte
          (2019). The article investigates various data about tin and tantalum
          with a focus on the effects that due diligence regulation may have on
          trade for producing countries.
        link: https://www.youtube.com/watch?v=MoHDtYqTZVM
      - icon: document
        title: Roadmap for Responsible Sourcing of Raw Materials in the Renewable Energy
          Sector until 2050
        subtitle: RE-SOURCING Deliverable 4.4 | M.-T. Kügerl, M. Tost (2021)
        excerpt: "This Roadmap for the Renewable Energy Sector by the RE-SOURCING
          project focuses on the road towards achieving a sustainable energy
          transition by 2050. Five key targets are addressed in the roadmap:
          Circular Economy & Decreased Resource Consumption, Paris Agreement &
          Environmental Sustainability, Social Sustainability & Responsible
          Production, Responsible Procurement, and Level Playing Field"
        file: /files/virtual-conference/re-sourcing-roadmap-for-the-renewable-energy-sector.pdf
      - icon: document
        title: "Presentation: Roadmap for Responsible Sourcing of Raw Materials in the
          Renewable Energy Sector until 2050"
        subtitle: Presentation slides | Marie-Theres Kügerl (2021)
        excerpt: "Presentation of the RE-SOURCING Roadmap for Responsible Sourcing in
          the Renewable Energy Sector during day 2 of the RE-SOURCING Virtual
          Conference 2021. "
        file: /files/res-roadmap_introduction.pdf
      - icon: document
        title: "Presentation: Responsible sourcing for the Green Transition - Towards a
          roadmap for the Mobility Sector"
        subtitle: Presentation slides | Johannes Betz (2021)
        excerpt: "Presentation of the current work on the RE-SOURCING Roadmap for
          Responsible Sourcing in the Mobility Sector during day 2 of the
          RE-SOURCING Virtual Conference 2021. "
        file: /files/2021_11_09_presentation_mobility_oeko_final.pdf
      - icon: video
        title: "Building mining's economic linkages: A critical review of local content
          policy theory"
        subtitle: Paper presentation (8min) | F. S. Weldegiorgis (2021)
        excerpt: "This short video presentation summarizes the main questions and
          findings of the academic article \"Building mining's economic
          linkages: A critical review of local content policy theory\" by F. S.
          Weldegiorgis, E. Dietsche, and D. M. Franks (2021). The article
          investigates the role of local content policies as a means for mining
          activities to have a positive impact on the host economy."
        link: https://www.youtube.com/watch?v=QgGRhI4aj_c
      - title: The EITI Standard 2019
        file: /files/virtual-conference/eiti-standard-2019.pdf
        icon: document
        subtitle: Standard document | Extractive Industries Transparency Initiative
          (2019)
        excerpt: The EITI is a global standard that promotes transparency and
          accountability in bringing together the government, companies, and
          civil society within a country to ensure transparency along the oil,
          gas, and minerals value chain by disclosing information about
          contracts and licences, revenues, and economic benefits, among other
          aspects. Gaining and maintaining EITI certification has
          multi-stakeholder oversight as a prerequisite.
      - title: IRMA Standard for Responsible Mining
        file: /files/virtual-conference/irma-standard-for-responsible-mining.pdf
        icon: document
        subtitle: Standard document | IRMA (Initiative for Responsible Mining Assurance)
          (2018)
        excerpt: This document outlines the IRMA standard including the principles and
          actions that stakeholders need to implement to adhere to the standard.
      - file: /files/virtual-conference/responsibly-sourcing-minerals-from-cahras-lessons-from-the-european-partnership-for-responsible-minerals.pdf
        title: "Responsibly Sourcing Minerals from CAHRAs: Lessons from the European
          Partnership for Responsible Minerals"
        icon: document
        subtitle: Briefing note | J. van Seters and N. Ashraf (2019)
        excerpt: >-
          This brief provides an overview of the EPRM. It further provides
          valuable insights for stakeholders interested in responsible mineral
          value chains and the role of multi-

          stakeholder initiatives.
      - title: "Conflict minerals and battery materials supply chains: A mapping review
          of responsible sourcing initiatives"
        subtitle: Academic article | R. Deberdt & P. Le Billon (2021)
        file: https://www.sciencedirect.com/science/article/abs/pii/S2214790X21000976
        icon: link
        excerpt: Based on a mapping review of 220 studies of responsible mineral supply
          chains, this study highlights the approaches that responsible minerals
          sourcing initiatives have taken, focusing on conflict minerals, as
          well as green technology minerals needed for renewable energy
          technologies in a transition to a low carbon economy.
        link: https://www.sciencedirect.com/science/article/abs/pii/S2214790X21000976
      - icon: document
        file: /files/virtual-conference/icmm-demonstrating-value-a-guide-to-responsible-sourcing.pdf
        title: Demonstrating Value - A guide to responsible sourcing
        subtitle: Guidance document | ICMM (2015)
        excerpt: "This guidance document provides stakeholders with practical advice on
          sustainable procurement and responsible supply practices along four
          main topics: - Mapping the value chain; - Developing effective
          programmes and standards; - Engagement with suppliers and value chain;
          - Data and information"
      - icon: document
        title: "State of Play and Roadmap Concept: Mobility Sector"
        subtitle: RE-SOURCING Deliverable 4.2 | J. Betz, S. Degreif, P. Dolega (2021)
        excerpt: The state of play report for the mobility sector covers the value chain
          of the key component of the electric mobility, the lithium-ion
          battery. Focusing on mining of resources, production of battery cells
          and their recycling at the end, it looks into the responsible sourcing
          of the key materials lithium, cobalt, nickel and graphite.
        file: /files/virtual-conference/re-sourcing-state-of-play-and-roadmap-concept-mobility-sector.pdf
      - icon: document
        title: The International Responsible Sourcing Agenda
        subtitle: RE-SOURCING Deliverable 1.1 | M. Farooki (2020)
        excerpt: This report provides an overview of the current state of responsible
          sourcing practices related to the extraction of minerals and their
          processing. It outlines the challenges that have been identified in
          the economic, social and environmental spheres and analysis how
          current RS approaches are attempting to address these.
        file: /files/virtual-conference/re-sourcing-the-international-responsible-sourcing-agenda.pdf
      - icon: document
        title: "Essentials for a Good Responsible Sourcing Standard: Purpose, Balance &
          Alignment"
        subtitle: "RE-SOURCING Brieifing document | M. Farooki (2021) "
        excerpt: This Briefing Document details the discussions in the ‘Regulations &
          Standards’ session at the opening conference of the RE-SOURCING
          Project (18-19 January 2021). The discussion focuses on the key
          ingredients required for constructing an effective standard and its
          interplay with global benchmarks and regulations.
        link: https://re-sourcing.eu/reports/re-sourcing-briefing-document-8
    zoomUrl: https://wu-ac-at.zoom.us/j/97526848538?pwd=cUxkUWp5V1M0Qy9IT2dWeGo3Y3FyZz09
  - title: WEDNESDAY, NOVEMBER 10
    theme: Closing the Loop of Responsible Sourcing - How to make global raw
      material flows more sustainable through circularity?
    description: ""
    body: >-
      The Circular Economy (CE) has become a major vehicle to deliver the
      ambitions enshrined in global and local sustainability agendas. Closing or
      at least narrowing the loop of product life cycles for decoupling economic
      activity from primary resource consumption is a promising proposition for
      business and sustainability alike. Naturally, CE has significant
      implications for the sourcing of raw materials both in primary and
      secondary raw material flows. However, a profound discourse on the precise
      interlinkages between Responsible Sourcing and Circular Economy is only
      emerging. This session will start by exploring Responsible Sourcing’s role
      for CE and vice versa, followed by a diverse selection of high-level
      experts that will zoom in on two concrete issues:  How to improve
      Responsible Sourcing in global secondary raw material streams and what is
      the responsibility of manufacturing companies to improve sustainable
      circularity in raw material flows?


      **Event moderator:** Ursula Kopp (Institute for Managing Sustainability, Vienna University of Economics and Business)


      **Session moderator:** [Mathias Schluep](https://www.linkedin.com/in/schluep/) (World Resources Forum)
    agendaItem:
      - title: "Closing the Loop of Responsible Sourcing: How to make global raw
          material flows more sustainable through circularity?"
        description: Keynote
        hour: 14
        minute: 0
        endTime: 14:35 CET
        body: >-
          **Speaker:** 


          * [Matthias Buchert](https://www.linkedin.com/in/matthias-buchert-9574a613/) (Oeko-Institut)
      - title: Responsible Sourcing in global secondary resource streams – Current
          challenges and how to address them
        hour: 14
        minute: 35
        endTime: 15:30 CET
        description: Panel discussion
        body: >-
          Speakers:


          * [Sonia Valdivia](https://www.linkedin.com/in/sonia-valdivia-2622aa8/) (World Resources Forum)

          * [Tatiana Terekhova](https://www.linkedin.com/in/tatiana-terekhova-9a699912/) (Secretariat to the Basel, Rotterdam and Stockholm Conventions, UNEP)

          * [Susanne Karcher](https://www.linkedin.com/in/susanne-yvonne-karcher-b10147b/) (African Circular Economy Network)

          * Luca Marmo (European Commission, DG Environment)


          moderated by [Mathias Schluep](https://www.linkedin.com/in/schluep/) (World Resources Forum)
      - title: Break
        description: ""
        body: Enjoy your coffee!
        hour: 15
        minute: 30
        endTime: 15:45 CET
      - title: From sourcing to recovery – What is the responsibility of producers to
          improve sustainable circularity of raw material flows?
        description: Panel discussion
        hour: 15
        minute: 45
        endTime: 16:30 CET
        body: >-
          **Speakers:**


          * [Pascal Leroy](https://www.linkedin.com/in/leroypascal/) (WEEE Forum)

          * [Martin Eriksson](https://www.linkedin.com/in/martin-eriksson-boliden/) (Boliden)

          * [Thea Kleinmagd](https://www.linkedin.com/in/thea-kleinmagd/) (Fairphone)

          * [Olivier Groux](https://www.linkedin.com/in/olivier-groux-1388b31b6/) (Kyburz)


          moderated by [Mathias Schluep](https://www.linkedin.com/in/schluep/) (World Resources Forum)
      - title: Project presentations
        hour: 16
        minute: 30
        endTime: 17:15 CET
        body: >-
          **Projects:**


          * [CEWASTE](https://cewaste.eu/) ([Shahrzad Manoochehri](https://www.linkedin.com/in/shahrzad-manoochehri-263991104/))

          * [BlackCycle](https://blackcycle-project.eu/) (Margarita Dorato)
      - title: Conference closing
        hour: 17
        minute: 15
        endTime: 17:25 CET
        description: ""
      - body: >-
          After a short closing remark on day 3, join this informal opportunity
          to exchange and network with project presenters and the RE-SOURCING
          project!


          The exchange will not be recorded or streamed.
        title: Informal exchange - day 3
        description: registered participants only
        hour: 17
        minute: 25
        endTime: 18:00 CET
    resourceFile:
      - icon: link
        excerpt: >-
          Full recording of day 3 (Wednesday, November 10) of the RE-SOURCING
          Virtual Conference 2021!


          Please see the conference Agenda to see the speakers and topics.
        subtitle: Session recording
        title: RE-SOURCING Virtual Conference 2021 - Day 3
        link: https://www.youtube.com/watch?v=zer2WuMOJG8
      - icon: video
        title: Critiques of the Circular Economy
        subtitle: Paper presentation (13min) | H. Corvellec, A. F. Stowell (2021)
        excerpt: This short video presentation summarizes the main questions and
          findings of the academic article "Critiques of the Circular Economy"
          by H. Corvellec, A. F. Stowell, and N. Johansson (2021; see below)
        link: |
          https://www.youtube.com/watch?v=5ohfGOenwj8
      - icon: document
        title: Critiques of the Circular Economy
        subtitle: Academic article | H. Corvellec, A. F. Stowell, N. Johansson (2021)
        file: /files/virtual-conference/critiques-of-the-circular-economy-2021.pdf
        excerpt: This article is a summary of critiques of the Circular Economy,
          including biophysical limits, social consequences and problems with
          the narrative of Circular Economy as synonym for sustainable economy.
      - icon: document
        title: "Circles, Spirals, Pyramids and Cubes: Why the Circular Economy cannot
          Work"
        subtitle: Academic article | K. R. Skene (2018)
        excerpt: "This article critically investigates the concept of Circular Economy,
          especially with regards to thermodynamics and energetic waste, by
          showing that material flows are complex and manifest manyfold. "
        file: /files/virtual-conference/circles-spirals-pyramids-and-cubes-why-the-circular-economy-cannot-work.pdf
      - icon: link
        title: ISO guidance Principles for the Sustainable Management of Secondary
          Metals | ISO (2017)
        subtitle: Guidance document
        excerpt: This document outlines the main principles, issues and definitions of
          terms in relation to the sustainable management of secondary metal raw
          materials.
        file: https://www.iso.org/obp/ui/#iso:std:iso:iwa:19:ed-1:v1:en
        link: https://www.iso.org/obp/ui/#iso:std:iso:iwa:19:ed-1:v1:en
      - icon: document
        title: "Basel Convention on the Control of Transboundary Movements of Hazardous
          Waste and their Disposal "
        subtitle: Policy paper | UNEP (2019)
        excerpt: "This policy document includes the protocol on liability and
          compensation for damage resulting from transboundary movements of
          hazardous wastes and their disposal. "
        file: /files/virtual-conference/basel-convention-on-the-control-of-transboundary-movements-of-hazardous-wastes.pdf
      - icon: document
        title: Re-thinking Producer Responsibility for a Sustainable Circular Economy
          from Extended Producer Responsibility to Pre-Market Producer
          Responsibility
        subtitle: Academic article | Eléonore Maitre-Ekern (2021)
        excerpt: This article explores the current tensions between EU waste law and the
          objectives of the Circular Economy, and calls for incorporating a
          pre-market producer responsibility (PPR) within a legal framework for
          products.
        file: /files/virtual-conference/re-thinking-producer-responsibility-for-a-sustainable-circular-economy.pdf
    zoomUrl: https://wu-ac-at.zoom.us/j/96153060671?pwd=c2pUZEptM2V6ZUFIbFF6Q1JRQXhDZz09
agendaMessage: ""
featuredImage: /files/photo-by-tom-fisk.jpg
passwordCheckIntroText1: The library offers you a concise selection of
  cutting-edge research, relevant reports and tools for each of the three main
  conference topics. It comprises video presentations, documents and links for
  you to explore and will also include the recordings of the live sessions after
  each day.
passwordCheckIntroText2: \*Registered participants received their password in a
  separate email via Eventbrite together with the technical information about
  the conference. If you registered on November 8 or later, you will find the
  password at the very bottom of your automated confirmation email via
  Eventbrite after registration. Please also check your spam folder for any of
  these messages or contact
  [noe.barriere@wu.ac.at](mailto:noe.barriere@wu.ac.at) and
  [andreas.endl@wu.ac.at](mailto:andreas.endl@wu.ac.at) in case you are facing
  any issues.
---

#### **The RE-SOURCING Virtual Conference 2021 is closed after 3 days of live program and a resource library with session recordings and additional materials!**

#### **We would like to thank more than 400 registered participants and over 30 speakers from around the world for your great support!**

During three half-days we dived deeper into three key issues of responsible sourcing:

1. **Supply Chain Due Diligence** - How to move towards meaningful and holistic impact?
2. **Responsible Sourcing for the Green Transition** - A Roadmap to 2050
3. **Closing the Loop of Responsible Sourcing** - How to make global raw material flows more sustainable through circularity?

#### **The session recordings and a conference report will be published in the upcoming weeks.**

#### **Please visit our main site and stay tuned for our next events!**

#### **In additon, have a look at our [knowledge hub](https://re-sourcing.eu/wiki) and [project outputs](https://re-sourcing.eu/project-outputs) to learn more about Responsible Sourcing!**
