---
template: content-with-faq
title: CONSULTATIONS
featuredImage: /files/consultation-page-01-01.jpg
dropdowns: []
---

RE-SOURCING is a network driven project and aims to share the experiences and knowledge from stakeholders facing the everyday challenges of responsible sourcing. Thus, the project focuses on interactions and dialogues with actors along the whole supply chain in the three key sectors.

To facilitate these learning opportunities, project consultations involve both physical and virtual events in addition to conferences.

_As these events take place over the project, this webpage will be updated to include agendas, presentations and summary findings of these events._

_For upcoming and past events, please see [Events](https://re-sourcing.eu/events)._
