---
template: consortium-post
title: SOMO
legalName: Stichting Onderzoek Multinationale Ondernemingen
country: Netherlands
logoImage: somo.jpg
homepage: www.somo.nl
members:
  - name: Alejandro Gonzàlez
  - name: Irene Schipper
---
The Centre for Research on Multinational Corporations (SOMO) is a critical, independent, not-for-profit knowledge centre on multinationals. Since 1973, SOMO has investigated multinational corporations and the impact of their activities on people and the environment. SOMO provides custom-made services (research, consulting and training) to non-profit organisations and the public sector. SOMO strengthens collaboration between civil society organisations through its worldwide network. As a centre of knowledge on key issues, SOMO drives analysis and pursues strategies aimed at social and sustainable change.