---
template: consortium-post
title: WWF DEUTSCHLAND
legalName: WWF DEUTSCHLAND
country: Germany
logoImage: wwf.jpg
homepage: www.wwf.de
members:
  - name: Tobias Kind-Rieper
---

WWF is the world's leading conservation organization. WWF works to help local communities conserve the natural resources they depend upon, transform markets and policies toward sustainability, protect and restore species and their habitats. WWF has expertise in the fields of sustainable supply chains, responsible business, mining and energy within certain landscapes. WWF can provide links to civil society and international organisations, which are essential for RE-SOURCING.
