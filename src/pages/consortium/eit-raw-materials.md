---
template: consortium-post
title: "EIT RawMaterials GmbH "
legalName: "EIT RawMaterials GmbH "
country: Germany
logoImage: eitrm_portrait_rgb.png
homepage: www.eitrawmaterials.eu
members:
  - name: Esther Laabs
  - name: Patrick Nadoll
  - name: Sven Kreigenfeld
---
EIT RawMaterials, initiated and funded by the EIT (European Institute of Innovation and Technology), a body of the European Union, is the largest consortium in the raw materials sector worldwide. Its vision is to develop raw materials into a major strength for Europe. Its mission is to enable sustainable competitiveness of the European minerals, metals and materials sector along the value chain by driving innovation, education and entrepreneurship. Partners of EIT RawMaterials are active across the entire raw materials value chain; from exploration, mining and mineral processing to substitution, recycling and circular economy. EIT RawMaterials will communicate the benefits of establishing a competitive advantage of responsible sourcing to their network and will help find alternative ways of funding for promising flagship cases and technologies for responsible sourcing.