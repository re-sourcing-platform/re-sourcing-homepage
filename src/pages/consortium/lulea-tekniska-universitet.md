---
template: consortium-post
title: Luleå University of Technology
legalName: Luleå University of Technology
country: Sweden
logoImage: ltu_eng.png
homepage: www.ltu.se
members:
  - name: Jan Rosenkranz
---
Luleå University of Technology (LTU) develops the attractive sustainable society through research and education. Many of our research subjects relate to different aspects of sustainability, like natural resources management, sustainable urban development, waste management and recycling. Sustainability is also a key issue in our work to develop our learning environment. Within the project, LTU contributes by providing expertise on the raw materials supply chains for minerals and metals from both primary and secondary sources, and through its connection with the Swedish and European mineral industry as well as international non-EU partner universities and organizations.