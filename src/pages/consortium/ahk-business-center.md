---
template: consortium-post
title: AHK Business Center
legalName: AHK Business Center
country: Chile
logoImage: ahk.jpg
homepage: www.camchal.cl
members:
  - name: Iris Wunderlich
  - name: Christoph Meyer
---

AHK Business Center, locally known as "CAMCHAL", will bring in expertise from the "Competence Centre for Mining and Mineral Resources" and the Chilean-German network Eco Mining Concepts. AHK Business Center will provide its network in the Latin-American mining industry to share and promote the project findings and discuss them with the interested regional actors. AHK Chile will be in charge of the organisation of the Global Advocacy Workshop (Latin America)
