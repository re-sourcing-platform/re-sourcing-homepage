---
template: consortium-post
title: WirtschaftuniversitatWien
legalName: " WIRTSCHAFTSUNIVERSITAT WIEN"
subInstitution: Institute for Managing Sustainability
country: Austria
logoImage: wu.jpg
homepage: www.wu.ac.at
members:
  - name: Noé Barriere
  - name: Alexander Graf
  - name: Andreas Endl
  - name: Constanze Fetting
---

The Vienna University of Economics and Business (WU) is a leading academic institution and one of Europe's most attractive universities in business and economics. The Institute for Managing Sustainability has long-standing experience in co-ordinating European research projects (in FP5-7; Horizon 2020), has built up extensive experience in organising stakeholder dialogues and peer learning events by applying various interactive conference and workshop formats in different contexts (e.g. in the FP7 and Horizon 2020 projects MIN-GUIDE, MINLAND, COBALT, DYNAMIX and coordinating the European Sustainable Development Network - ESDN). The Institute for Managing Sustainability has extensive experience with policy analysis (institutions, policy sectors, policies, strategies, etc.) at the EU and EU MS level.
