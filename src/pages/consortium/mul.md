---
template: consortium-post
title: MUL
legalName: " MONTANUNIVERSITÄT LEOBEN"
country: Austria
logoImage: mul_v2.jpg
homepage: www.unileoben.ac.at
members:
  - name: Michael Tost
  - name: Marie-Theres Kügerl
---
Montanuniversität Leoben (MUL) is one of Europe's leading technical universities in mineral exploration, extraction, and minerals policy expertise. MUL can draw on its global network in mineral resources production, as well as its extensive experience from managing and involvement with a number of EIT / KIC and FP6, FP7 and H2020 projects (e.g. MIN-GUIDE, MIREU, MINLAND). MUL is a core partner of EIT RawMaterials and is the Regional Center for East and South East Europe (ESEE Region). It is currently participating in more than 40 innovation projects and has extensive contacts with RE sector manufacturing companies.