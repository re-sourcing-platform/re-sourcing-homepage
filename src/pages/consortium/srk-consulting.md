---
template: consortium-post
title: " SRK Consulting"
legalName: " SRK Consulting (South Africa)"
country: South Africa
logoImage: srk.jpg
homepage: www.srk.com
members:
  - name: Roger Dixon
  - name: Lisl Pullinger
  - name: Bjanka Korb
  - name: Andrew van Zyl
---
SRK Consulting is an independent, international consulting practice providing focused advice and solutions to clients, mainly in the earth and water resource industries. SRK Consulting offers specialist services for the entire life cycle of a mining project from exploration to closure and across the full spectrum of mineral commodities. The industry background of many of their staff ensures that their advice is not only technically sound but also thoroughly practical. The SRK Group is an employee-owned organization and does not hold equity in any project. This permits SRK Consulting consultants to provide clients with conflict-free and objective support on crucial issues. SRK Consulting clients include many of the world’s major, medium-sized and junior metal and industrial mineral mining houses, exploration companies, financial institutions, construction firms, and government departments.