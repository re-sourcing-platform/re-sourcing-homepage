---
template: consortium-post
title: WRFA
legalName: WORLD RESOURCES FORUM ASSOCIATION
country: Switzerland
logoImage: world_resources_forum.jpg
homepage: www.wrforum.org
members:
  - name: Emanuele Di Francesco
  - name: Mathias Schluep
  - name: Shahrzad Manoochehri
---

The World Resources Forum Association (WRFA) is an independent non-profit international organization that serves as a platform connecting and fostering knowledge exchange on resources management amongst business leaders, policy-makers, NGOs, scientists and the public. The World Resources Forum (WRF) conference is the WRFA’s flagship event and is held annually. WRFA is also involved in multi-stakeholder dialogue projects such as the H2020 projects FORAM, CEWASTE and CICERONE, and various development cooperation programs, among others the Sustainable Recycling Industries (SRI) programme and several UN initiatives.
