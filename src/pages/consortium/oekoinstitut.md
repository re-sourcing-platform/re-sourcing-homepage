---
template: consortium-post
title: OekoInstitut
legalName: OEKO-INSTITUT E.V.
subInstitution: Institut Fuer Angewandte OEKOLOGIE
country: Germany
logoImage: öko_institut.jpg
homepage: www.oeko.de
members:
  - name: Peter Dolega
  - name: Stefanie Degreif
  - name: Johannes Betz
---

Oeko-Institut (OEKO) is an independent, non-profit research association. It can draw on its extensive background in the fields of responsible mining & sourcing, raw-material supply of the EU downstream sectors, resource efficiency and critical material issues. Besides an SME due diligence support system related to minerals and metals and a focus on 3TG, it also has deep insights and various projects dealing with the automotive supply chain, especially lithium-ion batteries. In the latter, OEKO has a focus on the mining of critical raw materials, cell production and especially recycling.
