---
template: consortium-post
title: TalTech
legalName: " Tallinn University Of Technology"
country: Estonia
logoImage: taltech.jpg
homepage: www.taltech.ee
members:
  - name: Karin Robam
  - name: Veiko Karu
---
Tallinn University of Technology (TalTech) is the flagship of Estonian engineering and technology education. The mission of TalTech is to support Estonia's sustainable development through R&D and science-based higher education in the field of engineering, technology, natural and social sciences. TalTech covers all raw materials sector value chain aspects from exploration, mining, manufacturing to a circular economy and social aspects. TalTech has extensive experience in applying innovative learning processes, e.g. using high-level e-learning systems and special video capture tools.