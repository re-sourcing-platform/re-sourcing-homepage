---
template: consortium-post
title: MineHutte
legalName: MH Intelligence (UK) LTD
subInstitution: null
country: United Kingdom
logoImage: /files/minehutte.jpg
homepage: www.minehutte.com
members:
  - name: Masuma Farooki
type: PARTNERS
---

MineHutte is a global resource for regulatory risk ratings and analysis for the mining sector with a team of international legal and policy experts and development economists. MineHutte's objective 17-factor benchmarking system analyses regulatory risk, focusing on mining codes and regulations, environmental legislation, reviews of international best practice, voluntary codes of conduct and responsible mining standards.\
\
MineHutte aims to provide rich, intelligent and objective information to guide investment decisions and public policy in the mining sector. MineHutte has worked with governments and mining companies in Europe, Africa, Canada and Latin America.
