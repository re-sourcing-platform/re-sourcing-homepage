---
template: members-page
title: Members
member:
  - name: Esther Laabs
    headshotImage: /files/esther_laabs.jpg
    institution: EIT RawMaterials GmbH
    subInstitution: ""
    position: Project Manager Innovation
    linkedIn: https://www.linkedin.com/in/esther-laabs-335514189/
  - headshotImage: /files/masuma_farooki.jpg
    institution: MineHutte
    name: Masuma Farooki
    position: Consulting Director
    linkedIn: https://www.linkedin.com/in/masumafarooki/
  - headshotImage: /files/andreas_endl.jpg
    name: Andreas Endl
    institution: Vienna University of Economics and Business
    subInstitution: Institute for Managing Sustainability
    position: Project Coordination & Research
    linkedIn: https://www.linkedin.com/in/andreas-endl-22877413a/
  - headshotImage: /files/alejandro_gonzalez.jpg
    name: Alejandro Gonzàlez
    institution: Centre for Research on Multinational Corporations
    position: Researcher SOMO / Coordinator GoodElectronics Network
    subInstitution: ""
    linkedIn: https://www.linkedin.com/in/alejandro-gonzalez-a4034761/
  - name: Alexander Graf
    headshotImage: /files/alexander_graf.jpg
    institution: Vienna University of Economics and Business
    subInstitution: Institute for Managing Sustainability
    position: Project Coordination & Research
    linkedIn: https://www.linkedin.com/in/alexander-graf-56149b160/
  - name: Andrew van Zyl
    headshotImage: /files/andrew_van_zyl.jpg
    institution: SRK Consulting (South Africa)
    position: Director and Principal Consultant
    linkedIn: https://www.linkedin.com/in/andrew-van-zyl-0559505/
  - name: Bjanka Korb
    headshotImage: /files/bjanka-korb-1-.jpg
    institution: SRK Consulting (South Africa)
    position: Senior Environmental Engineer
    linkedIn: https://www.linkedin.com/in/bjanka-korb/
  - headshotImage: /files/iris_wunderlich.jpg
    name: Iris Wunderlich
    institution: AHK Chile (German-Chilean Chamber of Commerce and Industry)
    position: Senior Project Manager Energy, Mining & Sustainability
    linkedIn: https://www.linkedin.com/in/iris-wunderlich-a507aba1/
  - linkedIn: https://www.linkedin.com/in/christoph-meyer-278899136/
    institution: "AHK Chile "
    name: Christoph Meyer
    position: Senior Project Manager Energy, Mining & Sustainability
    headshotImage: /files/christoph-meyer-camchal.jpg
  - headshotImage: /files/jan_rosenkranz.jpg
    name: Jan Rosenkranz
    institution: Luleå University of Technology
    position: Professor in Mineral Processing
    linkedIn: https://www.linkedin.com/in/jan-rosenkranz-4bb6bb1b/
  - headshotImage: /files/karin_robam.jpg
    name: Karin Robam
    institution: Tallinn University of Technology
    subInstitution: Department of Geology
    position: Division of Mining, Specialist
    linkedIn: https://www.linkedin.com/in/karin-robam-8559b417/
  - headshotImage: /files/mathias_schluep.jpg
    name: Mathias Schluep
    institution: World Resources Forum
    position: Managing Director
    linkedIn: https://www.linkedin.com/in/schluep/
  - headshotImage: /files/lisl_pullinger.jpg
    name: Lisl Pullinger
    institution: SRK Consulting (South Africa)
    position: Principal Sustainability Consultant
    linkedIn: https://www.linkedin.com/in/lislpullingersustainabilityconsultant/
  - headshotImage: /files/marie-theres_kugerl.jpg
    name: Marie-Theres Kügerl
    institution: Montanuniversität Leoben
    position: Chair of Mining Engineering and Mineral Economics, Junior Researcher
    linkedIn: https://www.linkedin.com/in/marie-theres-k%C3%BCgerl-59b000a2/
  - name: Emanuele Di Francesco
    headshotImage: /files/emanuele_di_francesco.jpg
    institution: World Resources Forum
    position: Communications and Events Manager
    linkedIn: https://www.linkedin.com/in/emanueledifrancesco/
  - headshotImage: /files/michael_tost.jpg
    name: Michael Tost
    institution: Montanuniversität Leoben
    position: Chair of Mining Engineering and Mineral Economics, Senior Researcher,
      Project Manager
    linkedIn: https://www.linkedin.com/in/michael-tost-4a8a2910/
  - headshotImage: /files/noe_barriere.jpg
    name: Noé Barriere
    institution: Vienna University of Economics of Business
    subInstitution: Institute for Managing Sustainability
    position: Peer learning on innovative business cases
    linkedIn: https://www.linkedin.com/in/no%C3%A9-barriere-202765106/
  - headshotImage: /files/patrick_nadoll.jpeg
    name: Patrick Nadoll
    institution: EIT RawMaterials GmbH
    position: Senior Adviser/Exploration and Resource Assessment
    linkedIn: https://www.linkedin.com/in/patrick-nadoll-b7023538/
  - name: Irene Schipper
    headshotImage: /files/irene-schipper.jpg
    institution: Centre for Research on Multinational Corporations
    position: Senior Researcher
    linkedIn: https://www.linkedin.com/in/irene-schipper-698624129/
  - headshotImage: /files/roger_dixon.jpg
    name: Roger Dixon
    institution: SRK Consulting (South Africa)
    position: Corporate Consultant
    linkedIn: https://www.linkedin.com/in/roger-dixon-1a023a14/
  - headshotImage: /files/shahrzad_manoocheri.jpg
    name: Shahrzad Manoochehri
    institution: World Resources Forum
    subInstitution: ""
    position: Project Manager, Communication and Stakeholder Engagement
    linkedIn: https://www.linkedin.com/in/shahrzad-manoochehri-263991104/
  - headshotImage: /files/stefanie-degreif.jpg
    name: Stefanie Degreif
    institution: Öko-Institut e.V.
    position: Deputy Head of Division Resources & Transport
    linkedIn: ""
  - headshotImage: /files/tobias_kind.jpg
    name: Tobias Kind-Rieper
    institution: World Wide Fund For Nature (WWF) Germany
    position: Global Lead Mining & Metals
    linkedIn: https://www.linkedin.com/in/tobias-kind-rieper-765356ba/
  - headshotImage: /files/veiko_karu_taltech.jpg
    name: Veiko Karu
    institution: Tallinn University of Technology, Department of Geology
    position: Division of Mining, Senior Project Manager
    linkedIn: https://www.linkedin.com/in/veikokaru/
  - institution: Öko-Institut e.V.
    position: Researcher, Division Resources & Transport
    name: Johannes Betz
    headshotImage: /files/johannes-betz.jpg
    linkedIn: https://www.linkedin.com/in/johannes-betz-1a6959137/
  - name: Sven Kreigenfeld
    headshotImage: /files/sven-kreigenfeld.jpg
    institution: EIT RawMaterials GmbH
    position: Compliance Officer
    linkedIn: https://www.linkedin.com/in/sven-kreigenfeld-152195101/
partner:
  - type: ADVISORY BOARD
    name: Johanna Sydow
    institution: Germanwatch
    position: Senior Advisor Resource Policy
    headshotImage: /files/germanwatch.png
  - type: ADVISORY BOARD
    institution: European Commission
    subInstitution: Directorate-General for Internal Market, Industry,
      Entrepreneurship and SMEs
    name: Maria Nyberg
    headshotImage: /files/3.Eueopean_Commission_Lucia_Mancini.jpg
    position: Policy officer
  - type: ADVISORY BOARD
    institution: Levin Sources
    name: Leon Riedel
    headshotImage: /files/levin-sources.png
    position: Business Area Manager for Mineral Sector Governance
  - type: ADVISORY BOARD
    institution: Initiative for Responsible Mining Assurance
    name: Rebecca Burton
    headshotImage: /files/3.IRMA_Lock up_Portrait_Blk_RGB.png
    position: Director of Communications and Corporate Engagement
  - type: PLATFORM STEERING COMMITTEE
    institution: Global Battery Alliance
    name: Guy Ethier
    headshotImage: /files/3.GBA.jpg
    position: Chair, Global Battery Alliance Executive Board
  - type: PLATFORM STEERING COMMITTEE
    institution: Global Battery Alliance
    name: Mathy V. Stanislaus, Esq.
    headshotImage: /files/3.GBA.jpg
    position: Interim Director
  - type: PLATFORM STEERING COMMITTEE
    institution: Global Battery Alliance
    name: Jonathan Eckart
    headshotImage: /files/3.GBA.jpg
    position: Project Lead
  - type: PLATFORM STEERING COMMITTEE
    institution: CSR Europe
    name: Stefan Crets
    headshotImage: /files/3.CSR_Europe.jpg
    position: Executive Director
  - type: PLATFORM STEERING COMMITTEE
    name: Catalina Pislaru
    headshotImage: /files/3.CSR_Europe.jpg
    institution: CSR Europe
    position: Manager
  - type: PLATFORM STEERING COMMITTEE
    institution: The Swedish Agency for Growth Policy
    name: Tobias Persson
    headshotImage: /files/3.The_Swedish_Agency_for_Growth_Analysis.jpg
    position: Analyst
  - type: PLATFORM STEERING COMMITTEE
    institution: Solarpower Europe
    name: Raffaele Rossi
    headshotImage: /files/3.SolarPowerEurope.jpg
    position: Policy Analyst
  - type: PLATFORM STEERING COMMITTEE
    institution: Transport & Environment
    name: Julia Poliscanova
    headshotImage: /files/3.T&E_newlogo.jpg
    position: Senior Director, Vehicles & Emobility
  - type: PLATFORM STEERING COMMITTEE
    institution: Business & Human Rights Resource Centre
    name: Jessie Cato
    headshotImage: /files/3.Business_Human_Rights_resource_cenrte.jpg
    position: Natural Resources & Human Rights Programme Manager
  - type: ADVISORY BOARD
    name: OECD
    headshotImage: /files/3.OECD-logo.jpg
    institution: " Organisation for Economic Co-operation and Development"
    position: " "
  - type: ADVISORY BOARD
    name: Antonio Pedro
    position: Director of UNECA sub-regional office for Central Africa
    institution: UNECA
    headshotImage: /files/bg_white.jpg
---
