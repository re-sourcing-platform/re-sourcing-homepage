---
template: event-post
title: The Roadmap Workshop on the Mobility Sector
type: Roadmap Workshop
virtualEvent: true
date_start: 2021-10-28T11:00:48.619Z
date_end: 2021-10-28T15:00:00.000Z
month_only: false
countdown: true
registerLink: ""
assets:
  - name: Participant Information Sheet
    file: mobility_roadmap_workshop_participant_information_sheet.pdf
  - name: Workshop Agenda
    file: ../../../static/files/mobility-roadmap-workshop/Re-Sourcing Roadmap
      Workshop Mobility Sector Agenda 28 Oct 2021.pdf
  - name: Introduction to RE-SOURCING
    file: ../../../static/files/mobility-roadmap-workshop/RE-SOURCING quick
      intro_v02_MS Roadmap WS.pdf
  - name: Summary of work to date
    file: ../../../static/files/mobility-roadmap-workshop/2021_10_28_Presentation_Introduction_work_JB_oP.pdf
  - name: Recycling session
    file: ../../../static/files/mobility-roadmap-workshop/RE-SOURCING_Roadmap_Workshop_Mobility_Session_Recycling_v2.pdf
  - name: Mining session
    file: ../../../static/files/mobility-roadmap-workshop/2021_10_28_Presentation_mining_session_final_oP.pdf
  - name: Cell production session
    file: ../../../static/files/mobility-roadmap-workshop/2021_10_28_Workshop_Mobility_Sector_Cell_Production_oP.pdf
---
The RE-SOURCING project organized a digital roadmap workshop on the mobility sector on **28 October 2021**. Participants from different stakeholder groups had a first brainstorming on relevant issues for a roadmap 2050 for responsible sourcing in the mobility sector. Thanks to all participants for making this event successful! The presentations are available at the left side.