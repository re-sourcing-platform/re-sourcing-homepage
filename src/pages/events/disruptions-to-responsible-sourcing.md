---
template: event-post
title: RE-SOURCING Virtual Event
subtitle1: Disruptions to Responsible Sourcing;
subtitle2: The Good, The Bad and The Ugly
featuredImage: virtual-event-oct.-2021-participants.png
type: Virtual Event
virtualEvent: true
date_start: 2020-10-09T10:00:06.788Z
date_end: 2020-10-09T13:30:00.000Z
month_only: false
countdown: true
assets:
  - file: disruptions_to_rs_agenda.pdf
    name: Event agenda
  - file: disruptions_to_responsible_sourcing_briefing.pdf
    name: Post event briefing
  - file: re-sourcing-virtual-event-presentation-slides.pdf
    name: Presentation slides
---

The RE-SOURCING project team would like to thank you for taking part in our first virtual event: **Disruptions to Responsible Sourcing; The Good, The Bad & The Ugly.**

The event was a great success for us, with plenty of interesting presentations, discussions, questions and comments that we will take on as input for our project!

The event outlined the responsible sourcing challenges for businesses transitioning their practices, particularly in the face of emerging challenges from Covid-19 and the expected impetus for materials demand resulting from the Green Deal. Together, we explored the positives and negatives for responsible sourcing in the short, medium and long term.

Here is a quick recap teaser for those of you who participated, and a small taste for those who were not able to do so:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3K2OQiqMXUI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In case you were not able to attend the event, you can view the recording at:

<https://www.facebook.com/GoodElectronics-Network-181391811880541/>

Here you can **find the event’s two panel discussions** about the impact of COVID-19 and the Green Deal on Responsible Sourcing:

COVID-19: <https://youtu.be/0HI6yeAA3cM>

EU Green Deal: <https://youtu.be/ioqfUXJ9QGY>

You can also read our **[short briefing](https://re-sourcing.eu/reports/disruptions-to-responsible-sourcing-briefing)** on the event’s most important outcomes.
