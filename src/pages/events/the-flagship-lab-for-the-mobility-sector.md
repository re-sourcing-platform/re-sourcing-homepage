---
template: event-post
title: The Flagship Lab for the Mobility Sector
subtitle1: ""
subtitle2: ""
featuredImage: pexels-rathaphon-nanthapreecha-3846205-1-1-.jpg
type: Flagship Lab
virtualEvent: true
date_start: 2022-03-01T13:00:11.340Z
date_end: 2022-03-01T15:00:00.000Z
month_only: false
countdown: false
password: ""
protectedContent: ""
---
The RE-SOURCING project organised a digital peer learning workshop (Flagship Lab) on good practice examples (Flagship Cases) in the mobility sector on March 1st 2022.

Participants from different stakeholder groups discussed  the good practice examples in working groups, challenges and relevant circumstances needed to improve responsible sourcing in their organisations.

The selected and presented Flagship Cases are:

**[Responsible procurement of minerals by using a strong standard](https://youtu.be/ATouXpKorAg)**: “Why IRMA is a strong standard and how purchasing companies can join and change the impacts by adjusting the supplier contracts” (IRMA and BMW)

**[Overarching regulation for a circular economy that covers the entire product value chain and focuses on sustainability](https://youtu.be/jIPq2bhcJ1c)**: “Content of the EU Commission’s Proposal for a new Battery Regulation” (European Commission)

[**How to implement a circular economy for batteries**:](https://youtu.be/sifPYh-XxMA) “Develop an effective system to collect, reuse, repurpose and recycle its own batteries” (Kyburz) 

**[Chinese standards: What can they achieve and where do they fail?](https://youtu.be/Q8nMgIuRPTM)** “What do Chinese standards for mining and recycling look like and how do they align with other guidelines from around the world” (MineHutte) [](https://youtu.be/Q8nMgIuRPTM)

**[Introduction to the Flagship Lab on Mobility](https://youtu.be/S5As3Z8AZt4)** (Oeko-Institut as Flagship Lab organiser) 

The pre-recorded presentations are now available via the project youtube channel.

If you have any questions on the Flagship Lab please contact Stefanie Degreif ([s.degreif@oeko.de](mailto:s.degreif@oeko.de)) or Johannes Betz ([j.betz@oeko.de](mailto:j.betz@oeko.de)).