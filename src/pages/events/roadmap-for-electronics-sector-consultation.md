---
template: event-post
title: Roadmap for Electronics Sector | Consultation
subtitle1: ""
subtitle2: RE-SOURCING State-of-Play and Roadmap Consultation Meeting
featuredImage: eee2.jpg
type: Virtual Event
virtualEvent: true
date_start: 2021-10-06T08:00:00.000Z
date_end: 2021-10-06T10:00:02.323Z
month_only: false
countdown: true
registerLink: https://forms.office.com/Pages/ResponsePage.aspx?id=DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAEbR0wyAJFUQzhMSzhKWFlOWllOQ0hKS1pNS0ZPQUNJOC4u
---
# Date & time

6 October 2021, 10-12 am CET

# Why are we doing this consultation process?

As part of the RE-SOURCING Project, SOMO (Centre for Research on Multinational Corporations) is currently working on a State-of-Play and Roadmap process for the electronics sector which will identify and analyze existing responsible sourcing initiatives in the EU and globally, key challenges of implementation and key participating actors. Such process will define a frame for a sectoral roadmap towards truly responsible sourcing in the ICT sector. 

The material scope of the project is focused on 3TG (Tin, Tungsten, Tantalum, Gold) and Mica. 

**We invite you to participate in a two-hour consultation to provide feedback for the development of such State-of-Play Report.** 

**For questions and comments please contact:** 
Alejandro González (A.Gonzalez@somo.nl) and Irene Schipper (I.Schipper@somo.nl).