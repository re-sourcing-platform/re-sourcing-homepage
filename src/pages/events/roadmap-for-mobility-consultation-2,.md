---
template: event-post
title: Roadmap for Mobility | Consultation 2
subtitle2: RE-SOURCING Roadmap Consultation Meeting
type: Virtual Event
virtualEvent: true
date_start: 2022-06-07T08:00:28.253Z
date_end: 2022-06-07T15:00:17.604Z
month_only: false
countdown: true
registerLink: https://forms.office.com/Pages/ResponsePage.aspx?id=VqXgvpHzIUWOMAeXFFyZbbVDTFgsrMNBtI3CZFK_1CFUQTBONEtaS1ZRTzhJNDVSQlkzMUVPTDBUQS4u
assets:
  - name: Participant Information
    file: mobility_roadmap_consultation_participant-information-sheet.pdf
---
## Date & time

The consultation process is divided into **two stages** and we would appreciate it if you participate in both sessions. To facilitate participation from all regions with different time zones, we offer two time slots for each consultation meeting:

**1st Meeting:**  24 May 2022, 10:00-12:00 am CEST or 2:30-4:30 pm CEST

**2nd Meeting:** 7 June 2022, 10:00-12:00 am CEST or 3:00-5:00 pm CEST

## Why are we doing this consultation process?

After the [State-of-Play Report](https://re-sourcing.eu/static/5b2eb582e0bd432e53da92bccc290a75/sop_mobility_sector.pdf), the [Roadmap Workshop](https://re-sourcing.eu/events/the-roadmap-workshop-on-the-mobility-sector/) and the [Flagship Lab](https://re-sourcing.eu/events/the-flagship-lab-for-the-mobility-sector/), the project team has developed a first draft of the **Roadmap** leading from the current state of the **mobility sector to responsible sourcing in 2050**.

The roadmap covers the **supply chains of lithium**, **cobalt**, **nickel** and **graphite** for **lithium-ion batteries**, focusing on the stages of mining, cell manufacturing and recycling. For each stage we will develop recommendations for **policy makers** (EU focus), **industry** and **civil society** (both global focus). We would be pleased to hear your opinion on what these areas should look like!

**Please note:** The deadline for your registration is **13 May 2022**. **Please register [HERE](https://forms.office.com/Pages/ResponsePage.aspx?id=VqXgvpHzIUWOMAeXFFyZbbVDTFgsrMNBtI3CZFK_1CFUQTBONEtaS1ZRTzhJNDVSQlkzMUVPTDBUQS4u).**

Due to the limited number of participants, we might not be able to confirm all registrations. We will send you a confirmation letter after assessing your application.

For each meeting you will receive the draft material and key questions for the discussion beforehand. During the meeting we alternate between **short presentations** of each section of the roadmap and **subsequent discussion**.\
\
If you cannot participate at both dates, but you still want to be part of the process, please get in touch with us and we will try to find a solution.\
\
For questions and comments, please contact **Stefanie Degreif** ([s.degreif@oeko.de) ](<mailto:s.degreif@oeko.de)%20>)or **Johannes Betz** ([j.betz@oeko.de](mailto:j.betz@oeko.de)).

## Who should attend these meetings?

These events will benefit companies along the lithium-ion battery supply chain, policy makers and those who advocate responsible business practices in this sector. You should attend this event to initiate your engagement with the RE-SOURCING network, to participate in peer learning and knowledge co-creation opportunities and the drafting of roadmaps and best practice cases for the wider uptake of responsible sourcing within EU businesses.