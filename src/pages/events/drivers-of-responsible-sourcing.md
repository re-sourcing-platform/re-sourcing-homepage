---
template: event-post
title: Drivers of Responsible Sourcing
subtitle2: Common Grounds - Collective Actions - Lasting Change
featuredImage: photo-1505373877841-8d25f7d46678.jpg
type: Virtual Conference
virtualEvent: true
date_start: 2021-01-18T13:30:00.000Z
date_end: 2021-01-19T18:00:53.411Z
month_only: false
countdown: false
registerLink: ""
assets:
  - file: re-sourcing_opening_conference-agenda.pdf
    name: Event Agenda
  - file: oc_conference_report_final.pdf
    name: Conference Report
---

The RE-SOURCING project team would like to thank you for taking part in our first virtual Conference: Drivers of Responsible Sourcing, Common Grounds - Collective Actions - Lasting Change.

The event was a great success for us, with more than 240 participants from around the globe, plenty of interesting presentations, discussions, questions and comments that we will take on as input for our project!

The event outlined the main mechanisms that drive this transition towards more responsible sourcing practices along the topics of:

- Awareness Building & Advocacy: Our global pledge to leave no one behind
- Industry Frontrunners & Business Alliances: Improving Supply Risks, Competitiveness & Reputation
- Regulations & Standards: The Interplay between policies & standards
- Investment & Stock and Commodity Markets: How the financial sector can push for more Responsible Sourcing

Here is a quick recap teaser for those of you who participated, and a small taste for those who were not able to do so:

<iframe width="560" height="315" style="margin-bottom: 1rem" src="https://www.youtube-nocookie.com/embed/lQy6zoTMiXc" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here you can find the event’s individual sessions, recordings and presentation slides:

# DAY 1: Monday 18th of January 2021

## Session 1. Introduction

- Conference Welcome by **André Martinuzzi**, Institute for Managing Sustainability, Vienna University of Economics and Business
- Opening Key-note by **Maija Laurila**, EC DG JUST
- Opening Key-note by **Bruno Oberle**, IUCN
- Opening Key-note by **Masuma Farooki**, Minehutte
- Opening Key-note by **Andreas Endl**, Institute for Managing Sustainability, Vienna University of Economics and Business
- **[Session recording (Youtube)](https://www.youtube.com/watch?v=N39jG5diuBs&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=1)**

## Session 2. Awareness Building & Advocacy

Our global pledge to leave no one behind - How to make frontline communities heard and respected in the process towards RS?

- Key-note presentation by **Mark Dummett**, Amnesty international
- Practice example by **Emmanuel Umpula**, AFREWATCH - [PDF](https://adoring-kalam-d298f0.netlify.app/files/Umpula_Practice-Example.pdf)
- **[Session recording (Youtube)](https://www.youtube.com/watch?v=AQFXqEmVBN4&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=2)**

## Session 3. Industry Frontrunners & Business Alliances

Exploring the case for building Alliances: Challenges & opportunities in strengthening business competitiveness and bench-marking responsible sourcing

- Key-note presentation by **Alexander Nick**, BMX - [PDF](https://adoring-kalam-d298f0.netlify.app/files/Nick_keynote.pdf)
- Practice example by **Badrinath Veluri**, Rare Earth Industry Association - [PDF](https://adoring-kalam-d298f0.netlify.app/files/Veluri_Practice-Example.pdf)
- **[Session recording (Youtube)](https://www.youtube.com/watch?v=1zOtUpofZME&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=3)**

<div style="padding-top: 2rem"></div>

# DAY 2: Tuesday 19th of January 2021

## Session 4. Regulations & Standards

The Interplay between policies & standards: A mutually supportive or conflicting relationship?

- Key-note presentation by **Tyler Gillard**, OECD
- Practice example by **Fiona Solomon**, Aluminium Stewardship Initiative - [PDF](https://adoring-kalam-d298f0.netlify.app/files/Solomon_Practice-Example.pdf)
- **[Session recording (Youtube)](https://www.youtube.com/watch?v=wIY_EpsBCPo&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=4)**

## Session 5. Investment & Stock and Commodity Markets

How can the financial sector push for more Responsible Sourcing – the role of ESG ratings, impact investment and what else?

- Key-note presentation by **Andreas Hoepner**, University of Dublin
- Practice example by **John Howchin**, Swedish Pension Fund - [PDF](https://adoring-kalam-d298f0.netlify.app/files/Howchin_Practice-Example.pdf)
- **[Session recording (Youtube)](https://www.youtube.com/watch?v=ysBiWSd5m5g&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=5)**

## Session 6. Wrap up of the Opening Conference

Change is in the air – What is it and are we on the right track?

- **[Session recording (Youtube)](https://www.youtube.com/watch?v=A44-fjLTFMw&list=PLzaLfv1cJzdbzUisqk2b-6V4atOYgpdb6&index=6)**
