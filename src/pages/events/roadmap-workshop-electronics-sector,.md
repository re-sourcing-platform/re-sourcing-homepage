---
template: event-post
title: Roadmap Workshop - Electronics Sector
featuredImage: re-sourcing_transparent.webp
type: Roadmap Workshop
virtualEvent: true
date_start: 2022-09-28T12:00:25.337Z
date_end: 2022-09-28T15:00:22.630Z
month_only: false
countdown: true
assets:
  - file: re-sourcing_roadmap-eees-final-agenda_28sept2022.pdf
    name: Agenda
  - file: re-sourcing-electronics-roadmap-workshop-participant-information-sheet.pdf
    name: Participant Information
  - name: RE-SOURCING Introduction
    file: re-sourcing-intro_eee-roadmap-ws_v02.pdf
---
After two successful workshops for [Renewable Energy](https://re-sourcing.eu/sectors/renewable-energy/overview/) and [Mobility](https://re-sourcing.eu/sectors/mobility/overview/#nav), the RE-SOURCING project organizes its **3rd digital roadmap workshop** in September 2022, this time on the **Electronics Sector**. 

The [RE-SOURCING Project](https://re-sourcing.eu/) is developing a roadmap for the electronics sector to achieve a sustainable and responsible value chain. The work on the [Electronics sector](https://re-sourcing.eu/sectors/electronics-and-electronic-equipment/overview/) is led by the Centre for Research on Multinational Corporations ([SOMO](https://www.somo.nl/)).

To develop the roadmap, SOMO is hosting the virtual workshop “***Responsible Sourcing in the Electronics Sector***” on 28th September, 2022, 14:00-17:00 (CET).

**Event overview**

Electronics is one of the largest and fastest growing industries of the world, employing millions of workers. Electronics are key for ambitious global goals such as digitalization and the energy transition. The global electronics industry is, however, associated with serious social and environmental risks and challenges for responsible sourcing along its supply chain.

The discussions will focus on key targets for policymakers and the electronics sector such as ***respect of human rights and level playing field; circular economy and decreased resource consumption; and responsible production and social responsibility.*** 

The Roadmap has a focus on **mining companies, manufacturers** (both contract and component manufacturers), and **brands**. The material focus is on **3TG minerals (tin, tungsten, tantalum and gold) and mica**.

The roadmap workshop is an interactive event, your active input and contribution would be greatly appreciated. **The event will be held under Chatham House Rule.** 

**Be pro-active and provide your input on the challenges to address, the targets to achieve and the urgency to tackle them!**

For questions and invitations contact: Alejandro González (a.gonzalez@somo.nl) or Irene Schipper (i.schipper@somo.nl)