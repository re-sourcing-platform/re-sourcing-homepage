---
template: event-post
title: Community Consultation & Engagements
subtitle1:
subtitle2: Tools & Challenges in Assessing Corporate Performance in the Mining Sector
type: Virtual Event
virtualEvent: true
date_start: 2021-04-27T13:00:32.840Z
date_end: 2021-04-27T14:30:59.862Z
month_only: false
countdown: true
registerLink: ""
assets:
  - name: Participant Information Sheet
    file: 2021-04-07_re-sourcing-expert-panel-participant-information-sheet.pdf
---

Community consultations & engagements are accepted as a central pillar for corporate performance on responsible sourcing & a mining company’s ESG credentials. This virtual event invites four experienced professionals in community engagement assessments, to discuss how corporate clients, investors & shareholders can evaluate a mining company’s performance on this subject. Our conversation will also consider some of the challenges faced by assessors in performing this function. This virtual event aims to increase your understanding of the tools commonly applied to community engagement assessments & audits and the challenges that lie within conducting such assessments.

##Panellists##

- [Lisl Pullinger](https://www.linkedin.com/in/lislpullingersustainabilityconsultant/), Principle ESH Consultant - SRK Consulting South Africa
- [Pamela Lesser](https://www.linkedin.com/in/pamela-lesser-970260b/), Researcher, Arctic Centre - University of Lapland
- [Pierre De Pasquale](https://www.linkedin.com/in/pierredepasquale/), Head of Stakeholder Engagement - Responsible Mining Foundation
- [Rebecca Burton](https://responsiblemining.net/about/team/), Director of Communications & Corporate Engagement - Initiative for Responsible Mining Assurance IRMA

##Webinar Recodings##

**Full Webinar:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-0kesgTOIOs" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In discussion with four experts, this webinar focuses on the tools & challenges in assessing corporate performance on community engagement in the mining sector, discussing tools, trust issues & community fatigue.

**Audits & Regulations:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RZDPQgGrghc" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The role of private governance mechanisms in providing assurance to stakeholders, the role of public governance and company messaging in assessing company performance on community engagement.

**Promoting Trust in Assessment:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ZwRmZD-V8c4" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The issue of the data and information generated in an assessment, as well as the quality of the engagement itself, impacts trust in tools commonly used for assessing corporate performance of community engagement.

**Community Fatigue:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/MZUS5Vfug8k" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The ‘community’ is not an unlimited resources and assessments need to be considerate and careful in how they engage with communities in their assessment approaches.

**Final Thoughts:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/NRB9GD8qZ6w" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Four experts outline one major priority for future work on assessing corporate performance on community engagement.

**Questions from the Floor:**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/0WG1sBB2-J0" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The panellists respond to questions from the audience on their experiences with assessing corporate performance on community engagement.
