---
template: event-post
title: On the Road to Responsible Sourcing
subtitle1: How to achieve meaningful impact
subtitle2: ""
featuredImage: ../../../static/files/photo-by-tom-fisk.jpg
type: Virtual Conference
virtualEvent: true
location: ""
date_start: 2021-11-08T12:45:00.000Z
date_end: 2021-11-10T16:15:00.000Z
month_only: false
countdown: true
assets:
  - name: "Conference Report"
    file: re-sourcing-virtual-conference-report.pdf
  - file: re-sourcing_virtual_conference-agenda.pdf
    name: Conference Agenda
---

#### **The RE-SOURCING Virtual Conference 2021 presented 3 days of live program and a resource library with session recordings and additional materials!**

#### **We would like to thank more than 400 registered participants and over 30 speakers from around the world for your great support!**

During three half-days we dived deeper into three key issues of responsible sourcing:

1. **Supply Chain Due Diligence** - How to move towards meaningful and holistic impact?
2. **Responsible Sourcing for the Green Transition** - A Roadmap to 2050
3. **Closing the Loop of Responsible Sourcing** - How to make global raw material flows more sustainable through circularity?

Below you can find the Session Recordings of the three days:

# **DAY 1: Supply Chain Due Diligence**

Supply Chain Due Diligence (DD) has gained traction over the last decade as a major means to increase transparency and accountability in companies’ supply chains. Led by foundational efforts such as the [UN Guiding Principles on Business and Human Rights](https://www.ohchr.org/Documents/Publications/GuidingPrinciplesBusinessHR_EN.pdf) and the [OECD Due Diligence Guidance](https://www.oecd.org/investment/due-diligence-guidance-for-responsible-business-conduct.htm), multiple actors continue to develop DD strategies and practices in order to work towards global sustainability agendas. However, the increased adoption also leads to a heterogeneous understanding and implementation of DD with differing outcomes and impacts. In this session, a diverse group of experts from business, policy and civil society discussed different issues surrounding a major question in this regard: How does DD lead to change that is both effective and holistic in terms of the society and the environment?

<iframe width="560" height="315" class="embed-video" src="https://www.youtube.com/embed/jj3a0qQmJkc" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**and with Spanish Interpretation:**

<iframe width="560" height="315" class="embed-video" src="https://www.youtube.com/embed/bl8hhJCyJi0" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Keynote speech: **Fabiana di Lorenzo**, Responsible Business Alliance

Panellists:

- **Angela Jorns**, Levin Sources
- **Telye Yurish**, Terram
- **James Nicholson**, Trafigura
- **Guy Muswil**, Kamoa Copper
- **Jan Kosmol**, German Environment Agency
- **Jane Joughin**, SRK Consulting
- **Jorge Sanhueza**, Codelco
- **Johanna Sydow**, Germanwatch

# **DAY 2: Responsible Sourcing for the Green Green Transition**

The global transition of the energy and mobility sectors that is necessary to combat climate change requires vast amounts of raw materials. To be truly sustainable, ’green’ energy and transportation need to ensure that the sourcing of raw materials is conducted in a socially and environmentally responsible way. In this session, international experts took a deep dive into the concrete targets and measures needed to realize this ambition, informed by the RE-SOURCING Project’s Roadmap for the Renewable Energy Sector and a preview of results from our Mobility Sector Roadmap.

<iframe width="560" height="315" class="embed-video" src="https://www.youtube.com/embed/dbIoeh1bRZk" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Keynote Speech: **Marie-Theres Kügerl**, Montanuniversität Leoben

Keynote Speech: **Johannes Betz**, Oeko-Institut e.V.

Panellists:

- **Jeff Geipel**, Engineers without Borders
- **Amir Shafaie**, Natural Resource Governance Institute
- **Jonas Astrup**, International Labour Organisation
- **Marie-Theres Kügerl**, Montanunversität Leoben

# **DAY 3: Closing the Loop of Responsible Sourcing - Circular Economy**

The Circular Economy (CE) has become a major vehicle to deliver the ambitions enshrined in global and local sustainability agendas. Closing or at least narrowing the loop of product life cycles for decoupling economic activity from primary resource consumption is a promising proposition for business and sustainability alike. Naturally, CE has significant implications for the sourcing of raw materials both in primary and secondary raw material flows. However, a profound discourse on the precise interlinkages between Responsible Sourcing and Circular Economy is only emerging. This session will started by exploring Responsible Sourcing’s role for CE and vice versa, followed by a diverse selection of high-level experts that will zoom in on two concrete issues:  How to improve Responsible Sourcing in global secondary raw material streams and what is the responsibility of manufacturing companies to improve sustainable circularity in raw material flows?

<iframe width="560" height="315" class="embed-video" src="https://www.youtube.com/embed/zer2WuMOJG8" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Keynote speech: **Matthias Buchert**, Oeko-Institut e.V.

Panellists:

- **Sonia Valdivia**, World Resources Forum
- **Tatiana Terekhova**, United Nations Environment Programme
- **Susanne Karcher**, African Circular Economy Network
- **Luca Marmo**, European Commission, DG Environment
- **Martin Eriksson**, Boliden
- **Pascal Leroy**, WEEE Forum
- **Olivier Groux,** Kyburz
- **Thea Kleinmagd**, Fairphone

On our [YouTube Channel](https://www.youtube.com/channel/UCFrqVkfz4yHeDvP3hrgLWbA), you will also find the paper presentations from the Resource Library

A Conference Report will be published in the following days.
