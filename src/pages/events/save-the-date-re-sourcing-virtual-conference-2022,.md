---
template: event-post
title: "Virtual Conference 2022: Reality check on Responsible Sourcing"
subtitle1: Trends, obstacles and opportunities
subtitle2: ""
featuredImage: mari-lezhava-q65bne9fw-w-unsplash.jpg
type: Virtual Conference
virtualEvent: true
date_start: 2022-11-07T08:00:00.000Z
date_end: 2022-11-13T17:00:38.095Z
month_only: false
countdown: true
assets:
  - name: Participant Information Sheet
    file: virtual-conference-2022-participant-information-sheet_final.pdf
---
# **The RE-SOURCING Virtual Conference 2022 is closed!**

#### **Thank you for your interest and all the great feedback we received from speakers and the audience!**

#### **Follow the recordings in case you have missed our exciting live programme:**

**Day 1 (November 8):** <https://www.youtube.com/watch?v=K0Ql2UHpt_Y>

**Day 2 (November 9):**[](https://www.youtube.com/watch?v=K0Ql2UHpt_Y) [https://www.youtube.com/watch?v=3E5giz1r5m](https://www.youtube.com/watch?v=3E5giz1r5mE)

#### Stay tuned - Our conference report and the publication of speaker slides will follow soon on our website!