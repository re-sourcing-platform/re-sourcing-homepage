---
template: event-post
title: "OECD Partner Session: Responsible Sourcing of Raw Materials"
subtitle1: "What makes the difference for a successful implementation? "
subtitle2: Insights from industry good practice cases
featuredImage: pexels-kindel-media-9800033.jpg
type: Virtual Event
virtualEvent: true
location: ""
date_start: 2022-05-05T07:00:41.000Z
date_end: 2022-05-05T08:30:00.996Z
month_only: false
countdown: true
registerLink: ""
assets:
  - name: Agenda
    file: oecd-forum_agenda_20220503.pdf
  - name: Participant Information
    file: participant-information.pdf
  - name: Renewable Energy Sector_Presentation
    file: res_oecd-session.pdf
  - name: Mobility Sector_Presentation
    file: ms_oecd-session.pdf
---
Did you miss the session? Check out the recording on our [YouTube Channel](https://www.youtube.com/channel/UCFrqVkfz4yHeDvP3hrgLWbA/featured)

The global transition of the energy and mobility sectors that is necessary to combat climate change requires vast amounts of raw materials. To be truly sustainable, ’green’ energy and transportation need to be powered by a socially and environmentally responsible sourcing of raw materials. In this session, international experts will take a deep dive into the concrete targets and measures the EU needs to consider to realize this ambition, informed by the RE-SOURCING Project’s [Roadmap for the Renewable Energy Sector](https://re-sourcing.eu/static/7d58b046a89ea05c923f24fa3bbbf0bf/final_res_roadmap_2021.pdf) and a preview of results from our Mobility Sector Roadmap.

**Topics to be discussed include:** 

1. The main challenge to responsible sourcing has been the failure to implement

* Do we see an improvement and acceleration of RS implementation?
* Why/Why not? What are the main drivers to implement RS?

1. Good practice cases supporting and showcasing the implementation of targets from the projects’ roadmaps:

* Responsible Procurement of minerals by using a strong standard
* Developing an effective collection & treatment system for end-of-life PV modules and batteries

No registration required! Please consult the[ Participant Information](https://re-sourcing.eu/static/f5fc2a9a5e3e9efe4de2704edd70a65b/participant-information.pdf).

This event is organized as a Partner Session of the [15th OECD Forum on Responsible Mineral Supply Chains](https://www.oecd.org/corporate/mne/forum-responsible-mineral-supply-chains.htm).