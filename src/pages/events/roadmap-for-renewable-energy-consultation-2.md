---
template: event-post
title: Roadmap for Renewable Energy | Consultation 2
subtitle1: ""
subtitle2: RE-SOURCING Roadmap Consultation Meeting
type: Virtual Event
virtualEvent: true
date_start: 2021-06-30T07:00:59.024Z
date_end: 2021-06-30T15:00:10.329Z
month_only: false
countdown: true
---
## Date & time

30 June 2021, 9-11 am CEST or 3-5 pm CEST

## Why are we doing this consultation process?

The RE-SOURCING meeting brings together industry leaders, research organisations, policy makers, NGOs and every day users of technology in the area of responsible sourcing. The goal of the consultation process is to develop a roadmap for the renewable energy sector to achieve responsible sourcing by 2050.

**This event is by invitation only.**

If you are interested in participating in the consultation meetings, please contact Marie-Theres Kügerl, [marie-theres.kuegerl@unileoben.ac.at](mailto:marie-theres.kuegerl@unileoben.ac.at) for further information and registration.