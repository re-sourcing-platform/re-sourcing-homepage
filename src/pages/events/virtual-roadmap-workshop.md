---
template: event-post
title: Virtual Roadmap Workshop
subtitle1: "Responsible Sourcing in the Renewable Energy Supply Chain:"
subtitle2: A reality or still a long way to go?
type: Roadmap Workshop
virtualEvent: true
date_start: 2020-11-09T21:00:21.070Z
date_end: null
month_only: true
countdown: false
assets:
  - file: re-sourcing_roadmap_ws_agenda_final.pdf
    name: Event agenda
  - name: Workshop minutes
    file: minutes-roadmap-workshop.pdf
---

## Thank you !

On 29 October 2020 Montanuniversität Leoben hosted the first Roadmap Workshop of the RE-SOURCING project **“Responsible Sourcing in the Renewable Energy Supply Chain: A reality or still a long way to go?”** as a virtual event.

We would like to thank you for your participation and for joining us in the discussion about the current situation of the renewable energy sector and necessary developments to ensure a sustainable and responsible supply.

The discussion basis was formed by the previously finished report “State of play and roadmap concepts for the renewable energy sector” (D4.1). Talks revolved around the **mining** of copper, rare earth elements, and silicon required for the **manufacturing** of wind turbines and solar PV panels and finally the **recycling** of the equipment.

D4.1 is now in the validation phase and we welcome any comments or feedback that will help to enhance the report. We would also like to ask all participants to continue participating in the roadmap development process and to support us with their expertise.

Furthermore, the presentations and a summary of the event are available for download [here](https://www.re-sourcing.eu/files/RE_Virtual_Roadmap_Workshop_Presentations.zip) (.zip, 7.8 MB).

We encourage you to contact us if you were not able to attend the workshop but would like to be involved in the development of the roadmap for the renewable energy sector.

Marie-Theres Kügerl, Montanuniversität Leoben,\
[marie-theres.kuegerl[at]unileoben.ac.at](mailto:marie-theres.kuegerl@unileoben.ac.at)
