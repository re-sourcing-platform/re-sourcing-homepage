---
template: event-post
title: Flagship Lab for the Renewable Energy Sector
subtitle1: Day 2
type: Flagship Lab
virtualEvent: true
date_start: 2021-04-22T12:00:00.585Z
date_end: 2021-04-22T15:00:00.585Z
countdown: true
month_only: false
---

The Flagship Lab is a workshop to discuss and learn from good practice cases in the renewable energy sector.
We will tackle issues such as an holistic approach to sustainability policies both from a company and a policy perspective in mining,
responsible sourcing of silicon metal in the solar PV industry, as well as good practice in the recycling of solar panels from a PV manufacturer's perspective.
The Flagship Lab's aim is to enable peers to learn and benefit from already exisiting activites to promote sustainability in their own operations.

**This event is by invitation only.**

If you are interested in participating in the Flagship Lab, please contact Marie-Theres Kügerl, [marie-theres.kuegerl[at]unileoben.ac.at](mailto:marie-theres.kuegerl@unileoben.ac.at) for further information and registration.
