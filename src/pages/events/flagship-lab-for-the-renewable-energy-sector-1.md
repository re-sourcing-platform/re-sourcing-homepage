---
template: event-post
title: Flagship Lab for the Renewable Energy Sector
subtitle1: Day 1
type: Flagship Lab
virtualEvent: true
date_start: 2021-04-15T12:00:00.585Z
date_end: 2021-04-15T15:00:00.585Z
countdown: true
month_only: false
---

# Info

The Flagship Lab is a workshop to discuss and learn from good practice cases in the renewable energy sector.
We will tackle issues such as an holistic approach to sustainability policies both from a company and a policy perspective in mining,
responsible sourcing of silicon metal in the solar PV industry, as well as good practice in the recycling of solar panels from a PV manufacturer's perspective.
The Flagship Lab's aim is to enable peers to learn and benefit from already exisiting activites to promote sustainability in their own operations.

# Case Studies

## Chile’s Mining Policy Case Study

<iframe width="560" height="315" style="margin-bottom: 1rem" src="https://www.youtube-nocookie.com/embed/oUmFvl1F2lk" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Juan Carlos Jobet Eluchans, Bi-Minister of Energy & Mining, Chile, shares his country’s experiences and approach with the Re-SOURCING Project in designing a consultation process for Chile’s National Mining Policy 2050.

- [Presentation slides](https://re-sourcing.eu/files/Consultations_for_a_National_Mining_Policy.pdf)

## First Solar Case Study

<iframe width="560" height="315" style="margin-bottom: 1rem" src="https://www.youtube-nocookie.com/embed/LXEpF84wLyI" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Andreas Wade, the Global Sustainability Director at First Solar, discusses the Circularity Pre-Requisites for Terawatt Scale Photovoltaics at a peer learning event held by the Re-SOURCING Project.

- [Presentation slides](https://re-sourcing.eu/files/First_Solar_Recycling_System.pdf)

## TfS Case Study

<iframe width="560" height="315" style="margin-bottom: 1rem" src="https://www.youtube-nocookie.com/embed/qZrMJC6soAU" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Together for Sustainability Initiative’s Jakob Smets shares insights in designing a supplier sustainability audit for the chemical industry, with lessons for other industrial clusters in this presentation, hosted by the Re-SOURCING Project.

- [Presentation slides](https://re-sourcing.eu/files/Auditing_for_RS_TfS_Case_Study.pdf)
