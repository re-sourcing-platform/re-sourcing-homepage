---
template: event-post
title: Global Advocacy Forum – Latin America
subtitle1: Responsible Sourcing in the Latin American region
featuredImage: copper-mine-close-up_bingham-kennecott.jpg
type: Roadmap Workshop
virtualEvent: false
location: Santiago, Chile
date_start: 2022-06-15T07:37:08.410Z
date_end: ""
month_only: true
countdown: false
assets:
  - name: Expert Interview with Peña Carpio and Koepke (Spanish)
    file: interview-pena-carpio-koepke-spanish-version.pdf
  - name: Agenda
    file: gaf-latam_agenda_june2022.pdf
  - name: Expert Interview with Pia Marchegiani
    file: interview-pia-marchegiani-final-version.pdf
---
**The Global Advocacy Forum Latin America will take place in June 2022, consisting of two events: a 1-day interactive workshop in Santiago, Chile (June 1st) and an online high level business-policy dialogue (June 30th).** 

As the first of a three-events series of Regional Fora to be held in Latin America, Sub-Saharan Africa and Asia, this event provides a platform for Latin American stakeholders to exchange on most pressing regional issues and discuss the present and future of responsible sourcing in Latin America. Regional opinion leaders from policy, industry, civil society and science are invited for interactive sessions to interface with RE-SOURCING activities and outputs, and to discuss regional needs and challenges for the implementation of responsible sourcing. The outcomes of the event will enable the identification and integration of region-specific inputs into the processes for a globally shared definition of responsible sourcing and related agenda setting.

Topics to be discussed during the event include:

* How is the responsible sourcing concept understood and implemented in Latin America?
* What are the challenges and solutions for implementing responsible sourcing practices in the Latin American region?
* What targets and priorities for responsible sourcing are pertinent to the region, encompassing economic, social, environmental and governance aspects?
* What is the degree of practicality and transferability of EU sectoral roadmaps in the Latin American context for the creation of a global playing field?

**Global Advocacy Forum Latin America** is composed of two complementary events taking place in distinct dates in June 2022: **a 1-day in-person workshop in Santiago, Chile** and an **online high-level regional policy dialogue**.

## 1-day Workshop

* *Date*: June 1st, 9.00am – 16.00pm(CLT)
* *Location*: Santiago, Chile
* *Venue:* [Casa Piedra](https://www.casapiedra.cl/), Avda. San Josemaría Escrivà de Balaguer 5600
* *Registration*: mandatory - register via email to [cmeyer@ahkchile.cl](mailto:cmeyer@ahkchile.cl "mailto\:cmeyer@ahkchile.cl") 

This interactive event is divided in two parts. In the first part, a **plenary session** will host presentations by the RE-SOURCING team and sprint keynotes by diverse regional stakeholders. These presentations, which will provide insights about the activities of RE-SOURCING and the regional context, will be followed by a Q&A and open discussion among all participants.

The second part of the event is dedicated to a **World Café session**, where participants will be divided into smaller working groups and will have the opportunity to deep-dive into specific issues related to responsible sourcing, encompassing environmental, social, governance and economic aspects. At the end of this session, participants will come together in a plenary session to share results from the different working groups, highlighting common conclusions and takeaways from the discussion.

A detailed agenda for the day will be available shortly.

## High-level regional policy dialogue

* *Date*: June 30th, 16.00 CEST
* *Location*: Online
* *Format*: closed-doors panel discussion with invited speakers