import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/Layout"
import Container from "../components/layout/Container"
import DetailsCard from "../components/layout/cards/DetailsCard"
import PageHeader from "../components/layout/headers/PageHeader"

const News = ({ data, location }) => {
  return (
    <Layout
      pageName="News"
      location={location}
      metaDescription="Read the latest news about RE-Sourcing! Gathering all newsletters and project related updates."
    >
      <PageHeader title="NEWS" />
      {/* CONTENT */}
      <Container>
        <div className="grid grid-cols-1 gap-4 lg:grid-cols-2 md:gap-8 px-4 md:px-0">
          {data.allMarkdownRemark.nodes.map((node, i) => {
            const {
              title,
              featuredImage,
              outsideLink,
              date,
              excerpt,
              subtitle = {},
            } = node.frontmatter

            return (
              <div key={i} className="w-full h-full col-span-1">
                <div className="mx-auto h-full">
                  <DetailsCard
                    title={title}
                    imageData={
                      featuredImage?.childImageSharp?.gatsbyImageData
                        ? featuredImage.childImageSharp.gatsbyImageData
                        : data.defaultImage.defaultImageData.gatsbyImageData
                    }
                    subtitle={subtitle}
                    date={date}
                    excerpt={excerpt}
                    outsideLink={outsideLink}
                    slug={node.fields.slug}
                    className="shadow-component"
                  />
                </div>
              </div>
            )
          })}
        </div>
      </Container>
    </Layout>
  )
}

export default News

export const query = graphql`
  query {
    allMarkdownRemark(
      filter: { frontmatter: { type: { eq: "news" } } }
      sort: { order: DESC, fields: frontmatter___date }
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          author
          date(formatString: "DD MMMM, YYYY")
          outsideLink
          title
          subtitle
          featuredImage {
            childImageSharp {
              gatsbyImageData(width: 250, placeholder: BLURRED)
            }
          }
          excerpt
        }
      }
    }
    defaultImageData: file(
      relativePath: { eq: "re-sourcing_transparent.png" }
    ) {
      childImageSharp {
        gatsbyImageData(width: 215, placeholder: BLURRED)
      }
    }
  }
`
