---
title: Contact
template: contact-page
subtitle: "Get in touch with us!"
addressHeading: "Coordinator contact"
email: info@re-sourcing.eu
phone: +43-1-31336-5452
positionLat: 48.2134275
positionLng: 16.4062779
mapLabel: "VIENNA UNIVERSITY OF ECONOMICS AND BUSINESS"
---

VIENNA UNIVERSITY OF ECONOMICS AND BUSINESS

Institute for Managing Sustainability

Welthandelsplatz 1A, 1020 Vienna, Austria
