---
template: news-post
type: news
title: Virtual Conference 2022 - save the date!
date: 2022-07-18T09:07:33.657Z
featuredImage: /files/virtual-conference-2022-banner-.png
excerpt: "Save the date for our upcoming Virtual Conference 2022 on November
  7-13, under the theme 'Reality Check on Responsible Sourcing: Trends,
  Obstacles and Opportunities'."
---
We're extremely happy to announce the upcoming **RE-SOURCING Virtual Conference 2022** on **November 7-13**!

Last year's Virtual Conference '*[On the Road to Responsible Sourcing - how to achieve meaningful impact](https://re-sourcing.eu/events/2nd-re-sourcing-virtual-conference/)*' was held in November 2021 and brought together more than 400 participants and over 30 speakers from around the world. This year we are pleased to invite you on November 7-13, 2022 for a full week of engagement at our Virtual Conference '**Reality Check on Responsible Sourcing: Trends, Obstacles and Opportunities**'

#### [SAVE THE DATE!](https://re-sourcing.eu/events/save-the-date-re-sourcing-virtual-conference-2022,/)



## What to expect at the RE-SOURCING Virtual Conference 2022

This year we continue to offer you a deep dive into some of the most urgent developments and challenges in the field of Responsible Sourcing for mineral raw materials:

* **Stress Test for Responsible Sourcing?** In-between our 2050 ambitions and economic resilience in view of rising geopolitical tensions
* **Local priorities for global Responsible Sourcing**: what are the differences and synergies between Latin America and the EU?
* **Technological solutions for tracking raw material flows**: A reality check of what to expect as a company from blockchain & co.
* **Believing in Unicorns**: Are we fooling ourselves on sustainable supply?

This edition of our Virtual Conference will be open from November 7-13 with a **3-day live program** (**November 8-10**) and an **on-demand library** of pre-recorded sessions and additional resources throughout the entire week.

Whether you are a business manager, CSO representative or policy-maker, this virtual conference will provide you with a rich experience including interviews, interactive sessions, panel discussions, networking with other projects and additional resources. 

**Registrations will open soon!**

**[Save the date in your calendar](https://re-sourcing.eu/events/save-the-date-re-sourcing-virtual-conference-2022,/).**

**Follow the [event page](https://re-sourcing.eu/events/save-the-date-re-sourcing-virtual-conference-2022,/) for updates.**