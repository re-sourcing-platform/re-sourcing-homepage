---
template: news-post
type: news
title: Read our latest Briefing Document on challenges and required actions for
  responsible sourcing in the e-mobility sector
date: 2022-06-23T07:38:31.460Z
featuredImage: /files/bd-12.png
outsideLink: https://re-sourcing.eu/reports/bd-12-e-mobility-final
excerpt: Rethinking and reducing transport has a key role to play to achieve the
  targets set under the Paris Agreement. How and to what extent the emobility
  sector can contribute to that is a very debated topic these days. In our
  latest briefing document, Stefanie Degreif and Johannes Betz systematically
  analyse the current challenges and gaps for responsible sourcing in the
  e-mobility sector across the mining, manufacturing and recycling stages.
  Explore their vision to ensure sustainability across the lifecycle of
  lithium-ion batteries with required actions in the short, medium and
  long-term.
---
Rethinking and reducing transport has a key role to play to achieve the targets set under the Paris Agreement. How and to what extent the emobility sector can contribute to that is a very debated topic these days. In our latest briefing document, Stefanie Degreif and Johannes Betz systematically analyse the current challenges and gaps for responsible sourcing in the e-mobility sector across the mining, manufacturing and recycling stages. Explore their vision to ensure sustainability across the lifecycle of lithium-ion batteries with required actions in the short, medium and long-term.