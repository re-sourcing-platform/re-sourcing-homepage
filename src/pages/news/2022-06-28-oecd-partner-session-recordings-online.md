---
template: news-post
type: news
title: OECD Partner Session - Recordings online!
subtitle: Responsible Sourcing of Raw Materials What makes the difference for a
  successful implementation?
date: 2022-06-28T09:41:36.609Z
featuredImage: /files/pexels-kindel-media-9800033_small.jpg
excerpt: >-
  Did you miss our Partner Session at the OECD Forum on Responsible Mineral
  Supply Chains?

  Check out the recordings of the session on our YouTube Channel.
---
Did you miss our Partner Session at the OECD Forum on Responsible Mineral Supply Chains?
Check out the recordings of the session on our[ YouTube Channel](https://www.youtube.com/channel/UCFrqVkfz4yHeDvP3hrgLWbA/featured).

Masuma Farooki (MineHutte) discusses questions such as '[What does responsibility mean in the country-context?](https://youtu.be/uWtCpgmAJos)' or '[Do we see an acceleration of the implementation of responsible sourcing?](https://youtu.be/lMBCMHUzsL4)' with our experts:

Jessie Cato (Business and Human Rights Resource Centre)\
Rebecca Burton (Initiative for Responsible Mining Assurance IRMA)\
Olivier Groux (Kyburz Switzerland AG)\
Mathias Schluep (World Resources Forum)