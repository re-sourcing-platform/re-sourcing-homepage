---
template: news-post
type: news
title: RE-SOURCING April Newsletter 2021
subtitle: Newsletter
date: 2021-04-08T07:47:22.477Z
featuredImage: /files/fafb4e3b-1743-4d4a-9fe0-1c16aee8321f.png
outsideLink: https://mailchi.mp/8be88b34b672/re-sourcing-newsletter-september-4752682?e=91c7a4a06d
author: Project Team
excerpt: >-
  The second edition of the RE-SOURCING newsletter has been published on April
  1st, 2021! Read the latest on:


  - The RE-SOURCING Opening Conference in January;


  - The upcoming Flagship lab (April 2021) and Roadmap workshop (October 2021);


  - Important developments in Responsible Sourcing for Q1 2021;


  - The RE-SOURCING partners’ newest publications and updates;


  Enjoy reading and let us know what you think!
---

The second edition of the RE-SOURCING newsletter has been published on April 1st, 2021! Read the latest on:

\- The RE-SOURCING Opening Conference in January

\- The upcoming Flagship lab (April 2021) and Roadmap workshop (October 2021)

\- Important developments in Responsible Sourcing for Q1 2021

\- The RE-SOURCING partners’ newest publications and updates

Enjoy reading and let us know what you think!
