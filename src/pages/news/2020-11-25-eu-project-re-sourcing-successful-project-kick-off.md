---
template: news-post
type: news
title: "EU project RE-SOURCING:"
subtitle: Successful project Kick-off meeting at the WU Vienna
date: 2020-01-10T08:11:34.105Z
featuredImage: /files/news.jpg
author: Project Team
excerpt: The first meeting of the EU project ‘RE-SOURCING – A Global Stakeholder
  Platform for Responsible Sourcing’ took place at the Vienna University of
  Economics and Business from the 16th-18th of December 2019. Interactive
  working groups, video interviews with experts, presentations of project tasks
  and an entertaining evening programme made for a solid basis to foster
  successful collaboration between the 25 participants over the next 4 years.
---

The Team from the Institute for Managing Sustainability will coordinate this project, consisting of 12 partners from 9 countries (including international collaboration with partners from Rwanda, China and Chile). Throughout the next 4 years, the project’s goal is to create a global stakeholder platform on responsible sourcing in global mineral value chains.

The project has been funded by the European Commission’s Horizon2020 Programme and focuses on sustainability in global mineral supply chains. The project has a strong focus on the Renewable Energy, Mobility and Electronics sectors, for which roadmaps on responsible sourcing, addressing sector-specific needs and challenges, will be developed. Furthermore, 14 different workshops and international conferences will be organised during the next four years to foster networking, collaboration and awareness raising for responsible sourcing among all stakeholders. The project aims at supporting international stakeholders, primarily industrial actors, to ensure responsible sourcing of minerals along every step of the supply chain.

![Consortium](/files/news.jpg "Consortium")
