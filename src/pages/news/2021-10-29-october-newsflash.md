---
template: news-post
type: news
title: RE-SOURCING Newsflash - October 2021
subtitle: New key documents published
date: 2021-10-29T10:50:43.119Z
featuredImage: /files/re-sector-alex-eckermann.jpg
outsideLink: ""
author: Project Team
excerpt: "The RE-SOURCING Team is delighted to announce that two key documents
  of the project, the Roadmap 2050 and Good Practice Guidance for the Renewable
  Energy Sector are completed and accessible for all interested stakeholders! "
---
Together, the Roadmap 2050 and Good Practice Guidance for the Renewable Energy Sector, provide concrete recommendations and practices for **policy makers**, **businesses** and **civil society** to implement and achieve Responsible Sourcing.

![Infographic](/files/re-sector-vision.jpg "vision for the renewable energy sector")

The **Roadmap** is based on a *[State of Play report](https://re-sourcing.eu/reports/08022021-re-sourcing-wp4-d41-v2-res)*, which pinpoints various impacts and challenges in both resource extraction of **copper**, **rare earth elements** and **silicon**, and the value chain of **wind turbines** and **solar PV modules**.\
\
The **five main areas** identified and addressed in the *[Roadmap](https://re-sourcing.eu/reports/final-res-roadmap-2021)* are: 

* Circular economy & decreased resource consumption, 
* Paris agreement & environmental sustainability, 
* Social sustainability & responsible production, 
* Responsible procurement, and
* The level playing field.

The **Roadmap** and **Guidance** address issues such as human rights violations and significant environmental impacts, lack of commitment to fair wages and gender equality, as well as conflicts with local populations. \
\
The reports are the result of a co-creative and consultative multi-stakeholder process. Results have been co-designed at the *[Renewable Energy Roadmap](https://re-sourcing.eu/events/virtual-roadmap-workshop/)****[ ](https://re-sourcing.eu/events/virtual-roadmap-workshop/)****[Workshop](https://re-sourcing.eu/events/virtual-roadmap-workshop/)* and *[Flagship Lab](https://re-sourcing.eu/events/flagship-lab-for-the-renewable-energy-sector-1/)* respectively and discussed with the project’s Platform Steering Committee and Advisory Board.\
\
In addition, the Roadmap development was based on consultations in the form of webinars and interviews with experts from all stakeholder groups and relevant regions, which led to the identification of necessary points of action and the establishment of recommendations.\
\
The *[Flagship Lab](https://re-sourcing.eu/events/flagship-lab-for-the-renewable-energy-sector-1/)* and the *[Good Practice Guidance Document](https://re-sourcing.eu/reports/d52-res-guidance-document-final)* provided input for good practices in policy making and industry.\
\
The Roadmap and its key recommendations will be presented at the *[RE-SOURCING Virtual Conference](https://re-sourcing.eu/hub/2nd-virtual-conference/about/)*, 8-10 November 2021. If you have not done yet, don't forget to register for this event.   

#### [Register here for the RE-SOURCING Virtual Conference](https://www.eventbrite.at/e/re-sourcing-virtual-conference-registration-163484098339)