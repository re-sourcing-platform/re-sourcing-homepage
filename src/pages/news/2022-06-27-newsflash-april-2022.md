---
template: news-post
type: news
title: Newsflash - April 2022
subtitle: Two upcoming RE-SOURCING events
date: 2022-04-13T09:14:08.219Z
featuredImage: /files/re-sourcing_transparent.png
outsideLink: https://mailchi.mp/5427e5d40746/re-sourcing-newsletter-september-5417729
excerpt: "Two upcoming RE-SOURCING events: A webinar at the 15th OECD Forum on
  Responsible Mineral Supply Chains  (May 5) and our Global Advocacy Forum Latin
  America (June 1)."
---
Two upcoming RE-SOURCING events: A webinar at the 15th OECD Forum on Responsible Mineral Supply Chains  (May 5) and our Global Advocacy Forum Latin America (June 1). [Read more](https://mailchi.mp/5427e5d40746/re-sourcing-newsletter-september-5417729)