---
template: news-post
type: news
title: Newsletter - July 2022
date: 2022-07-21T13:02:50.148Z
featuredImage: /files/fafb4e3b-1743-4d4a-9fe0-1c16aee8321f.png
outsideLink: https://mailchi.mp/fca8a8e9678f/re-sourcing-newsletter-september-5609157?e=[UNIQID]
---
In the fourth edition of our newsletter, we provide an overview of the projects' main publications and events in the first half of 2022 and look forward to what lies ahead for the rest of the year. Get your updates [here](https://mailchi.mp/fca8a8e9678f/re-sourcing-newsletter-september-5609157?e=[UNIQID]).