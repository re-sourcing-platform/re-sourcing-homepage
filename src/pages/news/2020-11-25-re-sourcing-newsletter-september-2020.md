---
excerpt: 1st edition of our Newsletter, which brings to you the most recent developments as well as actions and events of the RE-SOURCING project. We bring together businesses, policy makers and civil society in their endeavor to establish Responsible Sourcing. In this Newsletter you will find important and first-hand information about the RE-SOURCING Project at a glance, latest news, upcoming events and recent publications, which we hope are useful for your work.
template: news-post
type: news
title: RE-SOURCING September Newsletter 2020
subtitle: Newsletter
author: "Project Team"
date: 2020-11-10T10:50:57.254Z
featuredImage: /files/og-image.png
outsideLink: https://mailchi.mp/6330fb173aa0/re-sourcing-newsletter-september-4448302?e=[UNIQID]
---

1st edition of our Newsletter, which brings to you the most recent developments as well as actions and events of the RE-SOURCING project. We bring together businesses, policy makers and civil society in their endeavor to establish Responsible Sourcing. In this Newsletter you will find important and first-hand information about the following topics which we hope are useful for your work.

- RE-SOURCING Project at a Glance
- Latest News
- Upcoming Events
- Recent Publications
