---
template: news-post
type: news
title: "'Responsible Sourcing' in Argentina: challenges, discourses and pathways
  for international collaboration"
date: 2022-06-01T08:08:35.137Z
featuredImage: /files/argentina.jpg
outsideLink: https://re-sourcing.eu/reports/interview-pia-marchegiani-final-version
excerpt: Are you interested to know more about the discourse and facts about
  responsible sourcing in Argentina? Hear from Pía Marchegiani, Director of
  Environmental Policy at the Fundación Ambiente y Recursos Naturales (FARN), in
  this interview by Emanuele Di Francesco in the context of our Global Advocacy
  Forum Latin America.
---
Are you interested to know more about the discourse and facts about responsible sourcing in Argentina? Hear from Pía Marchegiani, Director of Environmental Policy at the Fundación Ambiente y Recursos Naturales (FARN), in this interview by Emanuele Di Francesco in the context of our Global Advocacy Forum Latin America.