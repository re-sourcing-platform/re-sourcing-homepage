---
template: news-post
type: news
title: The RE-SOURCING Virtual Conference 2021 was a big success!
date: 2021-11-22T08:14:26.000Z
featuredImage: /files/ph0b7d_1.jpg
author: Project Team
excerpt: "After 3 days of lively and insightful expert presentations and panel
  discussions—with the participation of over 30 speakers and more than 350
  attendees from all over the world—the RE-SOURCING Virtual Conference 2021 “On
  the Road to Responsible Sourcing” successfully came to an end on the 10th of
  November. "
---
The conference proved to be an excellent opportunity for representatives from civil society, policy, industry, research and international organizations, to come together and discuss three issues at the core of responsible sourcing: **supply chain due diligence, responsible sourcing for the green transition, and circular economy in the context of responsible sourcing.**

![Virtual Conference banner](/files/vc-banner_04-1-.jpg "RE-SOURCING Virtual Conference - On the Road to Responsible Sourcing")

Following up on the [RE-SOURCING Opening Conference](https://re-sourcing.eu/events/drivers-of-responsible-sourcing/) in January 2021, which focused on the drivers and needs for responsible sourcing,  and after the publication of the [RE-SOURCING Roadmap 2050 for Responsible Sourcing in the Renewable Energy Sector](https://re-sourcing.eu/reports/final-res-roadmap-2021) in October 2021, this Virtual Conference was the opportunity to initiate a dialogue with experts around **three key issues of responsible sourcing:**   

* **Supply Chain Due Diligence** – How can due diligence activities along the supply chain lead to change that is both effective and holistic in terms of society and environment?  
* **Responsible Sourcing for the Green Transition** – How to ensure that the sourcing of raw materials needed for “green” energy and transportation systems is socially and environmentally responsible?   
* **Closing the Loop of Responsible Sourcing** - How to make global raw material flows more sustainable through circularity? 

A common, overarching message coming out of the conference was—once again—the urgent need to move towards a responsible sourcing of raw materials in order to enable the global green transition and the achievement of climate targets in a socially and environmentally responsible way. Despite increased efforts over the past years by many actors, organizations, initiatives and alliances in this field, experts argued that much progress is still needed in order to harmonize and operationalize standards and processes for responsible sourcing.  

In the area of **supply chain due diligence and related risk assessment approaches**, experts highlighted the key role played by an inclusive and collective dialogue among supply chain actors during the implementation phase. The prevalent view was that only by establishing a close dialogue among policy, businesses and communities, real and long-lasting change can be achieved. 

The thematic focus on **responsible sourcing for the green transition** was the opportunity for the RE-SOURCING project to present some of the major recommendations included in the recently published Roadmap 2050 for the Renewable Energy Sector. The Roadmap provides a set of targets and milestones with a strong view of their urgency in order to assure the sustainable sourcing of raw materials needed for the green energy transition. In this area, experts called for more attention to the implementation of existing solutions rather than the development of new ones. This requires systemic changes within organizations and throughout entire supply chains, accompanied by an effective enforcement mechanism placed in a larger legal and political framework.  

Finally, experts discussed the interlinkages between **Responsible Sourcing and Circular Economy**, for closing, slowing and narrowing material loops. In this respect, experts stressed the need to foster responsible sourcing practices in secondary markets, new trade agreements and legal frameworks. Furthermore, they called for a stronger presence of social considerations and sustainable consumption in the concept of circular economy, highlighting the importance of a collective and inclusive sense of “responsibility” at all levels of the value chain.  

In order to facilitate the dissemination of the conference outputs, we will soon publish a report including the event’s key takeaways and learnings. The session recordings will also become publicly available for all interested stakeholders via our [YouTube channel](https://www.youtube.com/channel/UCFrqVkfz4yHeDvP3hrgLWbA). If you have not done yet, [subscribe to our newsletter](https://re-sourcing.us9.list-manage.com/subscribe?u=4131de53447fbeab61124f6d0&id=2a1f831795) to make sure you are up to date with the latest news and developments from Re-Sourcing. 

In the meantime, you can learn more about Responsible Sourcing by visiting our [knowledge hub](https://re-sourcing.eu/wiki/) and reading our [project outputs](https://re-sourcing.eu/project-outputs/).