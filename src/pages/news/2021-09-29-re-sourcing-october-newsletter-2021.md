---
template: news-post
type: news
title: RE-SOURCING October Newsletter 2021
subtitle: Newsletter
date: 2021-09-29T08:22:50.626Z
featuredImage: /files/fafb4e3b-1743-4d4a-9fe0-1c16aee8321f.png
outsideLink: https://mailchi.mp/246832d04a14/re-sourcing-newsletter-september-4817985?e=e44a335fca
author: Project Team
excerpt: >-
  The third edition of the RE-SOURCING newsletter has been published on
  September 29th, 2021! 

  Read the latest news on: 


  - Upcoming events: RE-SOURCING Virtual Conference (November 2021), RE-SOURCING Workshop at WRF2021 Conference (October 2021), Consultation Meeting on the Electronics Sector (October 2021), and Roadmap Workshop on the Mobility Sector (October 2021)


  - Recent publications: several new Briefing Documents


  - Relevant international news on Responsible Sourcing


  Enjoy reading and let us know what you think!
---
The third edition of the RE-SOURCING newsletter has been published on September 29th, 2021! 
Read the latest news on: 

\- Upcoming RE-SOURCING Virtual Conference (November 2021), RE-SOURCING Workshop at WRF2021 Conference (October 2021), Consultation Meeting on the Electronics Sector (October 2021), and Roadmap Workshop on the Mobility Sector (October 2021)

\- Recent publications: several new Briefing Documents

\- Relevant international news on Responsible Sourcing

Enjoy reading and let us know what you think!