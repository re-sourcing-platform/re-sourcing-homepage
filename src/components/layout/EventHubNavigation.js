import React from "react";
import {Link} from "gatsby";

export default function EventHubNavigation({ linkPrefix, eventIsClosed, showEventPage, showResourcesPage, location }) {
    if ( eventIsClosed ) {
        return null;
    }

    const links = [];

    if (showEventPage) {
        links.push({
            title: 'Event',
            link: linkPrefix
        });
    }

    links.push({
        title: 'About',
        link: `${linkPrefix}about/`
    });

    links.push({
        title: 'Agenda',
        link: `${linkPrefix}agenda-and-sessions/`
    });

    if (showResourcesPage) {
        links.push({
            title: 'Resources',
            link: `${linkPrefix}resource-library/`
        });
    }

    return (
        <div className="bg-white shadow-md rounded-md mx-2 md:mx-0 p-4 text-xs sm:text-sm w-full max-w-prose md:max-w-3xl">
            <div className="flex justify-center items-center">
                {links.map((nav, i) => (
                    <Link
                        key={i}
                        to={nav.link}
                        activeClassName="isActive"
                        className={`flex-1 px-2 text-center text-xs font-bold hover:text-primary3 md:text-base ${
                            location.pathname === nav.link
                                ? "text-primary3 cursor-default"
                                : "text-primary1"
                        }`}
                    >
                        {nav.title}
                    </Link>
                ))}
            </div>
        </div>
    );
}