import React from "react"
import useForm from "../../utility/hooks/useForm"
import GradientButton from "../../utility/GradientButton"

const PasswordCheck = ({
  password,
  storageKey = "re-sourcing-password",
  title = "Enter password",
  placeholder = "Enter password",
  label = "library-password",
  onChange,
}) => {
  const onSubmit = () => {
    sessionStorage.setItem(storageKey, true)
    return onChange(true)
  }

  const { handleSubmit, handleChange, data, errors } = useForm({
    validations: {
      password: {
        required: { value: true, message: "Password is required" },
        custom: {
          isValid: value => value === password,
          message: "Password is incorrect",
        },
      },
    },
    onSubmit: () => onSubmit(),
  })

  return (
    <form onSubmit={handleSubmit} className="mt-6 mb-12 mx-auto max-w-prose ">
      <div className="flex flex-col items-center">
        <label
          htmlFor={label}
          className="block text-xs sm:text-sm font-bold mb-1"
        >
          {title}
        </label>
        <div className="flex">
          <input
            type="text"
            name={label}
            id={label}
            placeholder={placeholder}
            value={data.password || ""}
            onChange={handleChange("password")}
            required
            autoComplete="current-password"
            className="focus:ring-primary3 focus:ring-1 focus:border-primary3 focus: outline-none flex-1 block w-full rounded-l-sm sm:text-sm border border-primary4 px-2 py-1"
          />

          <GradientButton type="submit" className="rounded-r-sm">
            Submit
          </GradientButton>
        </div>
      </div>
      <div className="relative text-center w-full">
        {errors.password && (
          <p className="absolute w-full top-0 text-xs -mt-1 text-accent1">
            {errors.password}
          </p>
        )}
      </div>
    </form>
  )
}

export default PasswordCheck
