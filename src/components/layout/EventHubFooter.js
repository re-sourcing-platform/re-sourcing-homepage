import React from "react"
import { Link } from "gatsby"

import { StaticImage } from "gatsby-plugin-image"

const EventHubFooter = () => {
  const quickLinks = [
    { name: "Contact us", link: "/contact" },
    { name: "Partners", link: "/partners" },
    { name: "News", link: "/news" },
    { name: "Legal notice & Privacy Policy", link: "/privacy" },
  ]

  return (
    <footer className="relative bg-primary1 mt-12">
      <div
        className="absolute right-0"
        style={{ transform: "translateY(-99%)" }}
      >
        <div className="curved-corner-bottomright"></div>
      </div>
      <div style={{ minHeight: "3rem" }}>
        {/* <p className="text-xs text-white pl-4 p-3">
          *Please read the{" "}
          <a
            className="underline font-bold"
            href="https://re-sourcing.eu/files/virtual-conference-participant-information-sheet_v02.pdf"
            target="_blank"
            rel="noreferrer"
          >
            Participant Information Sheet
          </a>{" "}
          to learn more about the event’s data protection & privacy
        </p> */}
      </div>

      <div className="bg-primary2 sm:px-8 py-12">
        <div
          className="flex flex-col-reverse md:flex-row mx-auto"
          style={{ maxWidth: "1120px" }}
        >
          <div className="w-full text-center md:text-left lg:w-1/2 md:flex">
            <div className="px-8 pb-3 md:pl-0">
              <StaticImage
                src="../../../static/files/1200px-Flag_of_Europe.svg.png"
                alt="EU flag"
                width={110}
                layout="fixed"
                className="mx-auto"
                placeholder="dominantColor"
              />
            </div>
            <div className="px-8 md:px-0 md:w-1/3">
              <p className="text-xs md:text-sm text-white">
                This project has received funding from the European Union's
                Horizon 2020 research and innovation program under Grant
                Agreement No. 869276
              </p>
            </div>
          </div>
          <div className="w-full text-white text-center md:text-left lg:w-1/2 md:flex">
            <div className="md:w-1/2 md:-ml-6 md:mr-6">
              <h2 className="text-sm md:text-md font-bold pb-1">
                Quick links:
              </h2>
              <ul className="list-disc pl-5">
                {quickLinks.map((link, i) => {
                  return (
                    <li className="text-xs md:text-sm list-none" key={i}>
                      <span className="text-base -ml-5 pr-2 ">•</span>
                      <Link to={link.link}>{`${link.name}`}</Link>
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className="py-10 md:w-1/2 md:py-0">
              <h2 className="text-sm md:text-md font-bold pb-1">
                Coordinated by:
              </h2>
              <ul className="text-xs md:text-sm">
                <li>Vienna University of Economics and Business,</li>
                <li>Institute for Managing Sustainability</li>
                <li>Welthandelsplatz 1A</li>
                <li>1020 Vienna</li>
                <li>email: info@re-sourcing.eu</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default EventHubFooter
