import React from "react"

const Container = ({ children, className }) => {
  return (
    <div
      className={`md:container mx-auto relative z-20 -mt-8 flex flex-col lg:px-24 ${className}`}
    >
      {children}
    </div>
  )
}

export default Container
