import React, { useState } from "react"
import { Link } from "gatsby"
import Searchbar from "./Searchbar"
import { StaticImage } from "gatsby-plugin-image"

const NavBar = () => {
  const [dropdownOpen, setDropdownOpen] = useState(null)
  const [menuOpen, setMenuOpen] = useState(false)

  const upperlinks = [
    { name: "home", link: "/" },
    { name: "about", link: "/about" },
    { name: "partners", link: "/partners" },
    { name: "contact", link: "/contact" },
  ]

  const knowledgeHub = [
    { name: "wiki", link: "/wiki" },
    {
      name: "existing approaches",
      link: "/existing-approaches",
    },
    {
      name: "external links",
      link: "/external-links",
    },
    { name: "consultations", link: "/consultations" },
  ]

  const sectors = [
    {
      name: "Overview",
      link: "/sectors/",
    },
    {
      name: "renewable energy",
      link: "/sectors/renewable-energy/overview/",
    },
    { name: "mobility", link: "/sectors/mobility/overview/" },
    {
      name: "EEE",
      link: "/sectors/electronics-and-electronic-equipment/overview/",
    },
  ]

  const bottomlinks = [
    {
      name: "Conference",
      link: "/hub/3rd-virtual-conference/about/"
    },
    {
      name: "Sectors",
      dropdown: sectors,
    },
    { name: "news", link: "/news" },
    { name: "events", link: "/events" },
    {
      name: "knowledge hub",
      link: "/",
      dropdown: knowledgeHub,
    },
    {
      name: "project outputs",
      link: "/project-outputs",
    },
  ]

  const menuClosedStyles = "absolute h-100px"
  const menuOpenStyles = "h-screen fixed"

  const handleDropdown = i => {
    if (dropdownOpen === null) setDropdownOpen(i)
    if (dropdownOpen !== null) setDropdownOpen(null)
  }

  let menuClosedNav = (
    <>
      {/* Upper nav START*/}
      <div>
        <ul className="hidden md:flex pb-4 text-primary2 items-center">
          <li className="mr-6">
            <Searchbar />
          </li>
          {upperlinks.map((link, i) => (
            <li key={i} className="mx-6 hover:text-primary1">
              <Link to={link.link}>{link.name}</Link>
            </li>
          ))}
        </ul>
      </div>
      {/* Upper nav END*/}
      {/* Bottom nav START*/}
      <div
        className="absolute shadow-lg bg-white px-4 hidden md:flex z-50 rounded"
        style={{ right: "0", top: "75px" }}
      >
        <div className="flex">
          {bottomlinks.map((link, i) => {
            if (link.dropdown)
              return (
                <div className="relative dropbtn text-center flex" key={i}>
                  <button
                    onClick={() => handleDropdown(i)}
                    className="dropbtn px-6 py-3 hover:text-primary1 text-primary2 uppercase font-bold focus:shadow-outline-accent1"
                  >
                    <span className="dropbtn">{link.name}</span>

                    <span className="absolute dropbtn">
                      <svg
                        className="w-3 h-3 dropbtn"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        {dropdownOpen === i ? (
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M5 15l7-7 7 7"
                          ></path>
                        ) : (
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M19 9l-7 7-7-7"
                          ></path>
                        )}
                      </svg>
                    </span>
                  </button>
                  {/* Nav Dropdown */}
                  <div
                    className={`absolute block shadow-lg bg-white ${
                      dropdownOpen !== i ? "hidden" : ""
                    }`}
                    style={{ right: "0px", top: "60px" }}
                  >
                    {link.dropdown.map((link, i) => {
                      return (
                        <div
                          key={i}
                          className="w-full text-left text-primary2 uppercase font-bold"
                        >
                          <Link to={link.link}>
                            <div className="py-3 px-4 flex items-center">
                              {link.name}
                            </div>
                          </Link>
                        </div>
                      )
                    })}
                  </div>
                  {/* Nav Dropdown END*/}
                </div>
              )
            return (
              <div
                key={i}
                className="hover:text-primary1 text-primary2 uppercase font-bold text-center"
              >
                <Link to={link.link} className="">
                  <div className="h-full flex items-center px-6 flex-shrink">
                    {link.name}
                  </div>
                </Link>
              </div>
            )
          })}
        </div>
      </div>
      {/* Bottom nav END */}
    </>
  )

  let linkStyle = "py-1 mx-6 hover:text-primary1"
  let menuOpenNav = (
    <>
      {/* Upper nav START*/}
      <div className="mt-8 mx-auto text-center text-lg uppercase font-bold">
        <ul className="text-primary2">
          {upperlinks.map((link, i) => (
            <li key={i} className={linkStyle}>
              <Link
                to={link.link}
                activeClassName="bg-primary1 text-white px-3"
              >
                {link.name}
              </Link>
            </li>
          ))}
          {sectors.map((link, i) => (
            <li key={i} className={linkStyle}>
              <Link
                to={link.link}
                activeClassName="bg-primary1 text-white px-3"
              >
                {`${
                  link.name === "Overview"
                    ? "Sectors overview"
                    : `${link.name} sector`
                }`}
              </Link>
            </li>
          ))}
          {knowledgeHub.map((link, i) => (
            <li key={i} className={linkStyle}>
              <Link
                to={link.link}
                activeClassName="bg-primary1 text-white px-3"
              >
                {link.name}
              </Link>
            </li>
          ))}
          {bottomlinks
            .filter(link => !link.dropdown)
            .map((link, i) => (
              <li key={i} className={linkStyle}>
                <Link
                  to={link.link}
                  activeClassName="bg-primary1 text-white px-3"
                >
                  {link.name}
                </Link>
              </li>
            ))}
        </ul>
      </div>
      {/* Upper nav END*/}
    </>
  )

  if (typeof window !== `undefined`) {
    window.onclick = function (event) {
      if (dropdownOpen !== null) {
        if (
          !event.target.matches(".dropbtn") &&
          !event.target.matches(".dropdown-content")
        ) {
          setDropdownOpen(null)
        }
      }
    }
  }

  return (
    <div
      className={`bg-primary5 w-full top-0 z-50 font-body ${
        menuOpen ? menuOpenStyles : menuClosedStyles
      }`}
    >
      <div className="container relative mx-auto flex justify-between pt-4 md:pt-6">
        {/* Logo START*/}
        <div className="mt-2 ml-2 md:ml-0 md:-mt-2 lg:-mt-3">
          <Link to="/">
            <StaticImage
              src="../../../static/files/re-sourcing_transparent.png"
              className="w-32 md:w-46 lg:w-52"
              alt="Re-sourcing logo"
              placeholder="tracedSVG"
            />
          </Link>
        </div>
        {/* Logo END */}
        {/* Hamburger */}
        <button
          onClick={() => setMenuOpen(!menuOpen)}
          className="absolute mr-4 -mt-2 p-2 md:hidden"
          style={{ right: "0px", top: "35px" }}
        >
          <div className={`relative ${menuOpen && "py-2"}`}>
            <div
              className={`w-6 h-1 rounded bg-primary2 duration-500 ease-in-out ${
                menuOpen ? "absolute transform rotate-45" : ""
              }`}
            ></div>
            <div
              className={`w-6 h-1 rounded bg-primary2 duration-500 ease-in-out ${
                menuOpen ? "absolute transform -rotate-45" : "mt-1"
              }`}
            ></div>
            <div
              className={`w-6 h-1 rounded bg-primary2 duration-500 ease-in-out ${
                menuOpen ? "transform opacity-0" : "mt-1"
              }`}
            ></div>
          </div>
        </button>
        {/* Hamburger*/}
        {!menuOpen ? menuClosedNav : ""}
      </div>
      {menuOpen ? menuOpenNav : ""}
    </div>
  )
}

export default NavBar
