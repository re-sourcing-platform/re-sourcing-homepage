import React, { useState } from "react"

const ScrollBar = ({ onChange = () => console.log("change") }) => {
  const [currentScrollPos, setCurrentScrollPos] = useState(1)

  const handleClick = e => {
    setCurrentScrollPos(e)
    onChange(e)
  }

  return (
    <div className="w-full py-2 rounded-lg flex justify-center items-center bg-white text-2xl text-primary2 shadow-lg">
      <svg
        className="w-6 h-6 mx-8"
        fill="none"
        stroke="currentColor"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M10 19l-7-7m0 0l7-7m-7 7h18"
        ></path>
      </svg>
      <p>1/6</p>
      <svg
        className="w-6 h-6 mx-8"
        fill="none"
        stroke="currentColor"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M14 5l7 7m0 0l-7 7m7-7H3"
        ></path>
      </svg>
    </div>
  )
}

export default ScrollBar
