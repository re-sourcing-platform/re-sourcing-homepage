import React from "react"
import bestPracticesIcon from "./bestpractices-icon.svg"
import definitionIcon from "./definition-icon.svg"
import communityIcon from "./community-icon.svg"
import frameworksIcon from "./frameworks-icon.svg"
import arrowLeft from "./arrow-left.svg"
import arrowRight from "./arrow-right.svg"
import pointLine from "./point-line.svg"
import pointLineTop from "./point-line-top.svg"
import pointLineMiddle from "./point-line-middle.svg"
import eeeIcon from "./EEE-icon.svg"
import MobilityIcon from "./Mobility-icon.svg"
import businessesIcon from "./businesses-icon.svg"
import civilSocietyIcon from "./civil-society-icon.svg"
import policyIcon from "./policy-icon.svg"
import ReIcon from "./RE-icon.svg"
import { StaticImage } from "gatsby-plugin-image"

const Infographic = () => {
  let bottomIcons = [
    { icon: frameworksIcon, text: "Supportive EU policy frameworks" },
    {
      icon: communityIcon,
      text: "Globally connected responsible sourcing community",
    },
    { icon: bestPracticesIcon, text: "Scaling up of best practices" },
    { icon: definitionIcon, text: "A common definition" },
  ]

  let textBoxLeft = {
    title: "Our contribution to Responsible Sourcing:",
    listItems: [
      "Create a global network",
      "Designing sectoral roadmaps",
      "Providing best practice cases",
      "Forming a common understanding",
    ],
  }

  let textBoxRight = {
    title: "Your involvement:",
    listItems: [
      "9 workshops sharing best practices & global exchanges",
      "4 conferences promoting peer learning & networking",
      "Online platform for knowledge sharing & best practice cases",
    ],
  }

  return (
    <>
      {/*  TOP */}

      <section id="top">
        <div className="py-8 md:pt-16 md:pb-8 w-full bg-primary1 lg:py-12">
          <div className="container m-auto max-w-screen-md">
            <h2 className="text-xl font-bold text-white w-4/5 text-center m-auto xl:text-2xl xl:w-full">
              A Global Stakeholder Platform for Responsible Sourcing in Mineral
              Value Chains
            </h2>
          </div>
        </div>
        <BgRounded backBg="bg-primary1" roundBg="bg-body" />
        <div className="-mt-20 md:-mt-8">
          <Triangle variant="stroke" text="Challenge" />
        </div>
        <div className="container text-center m-auto pb-6 pt-12 px-6 xl:max-w-prose">
          <p className="text-lg text-primary2 font-bold xl:text-xl ">
            Create the necessary framework conditions for responsible sourcing
            in the EU and globally
          </p>
          <StaticImage
            src="../../../../static/files/re-sourcing_transparent.png"
            alt="Re-sourcing logo"
            className="mt-6 m-auto"
            placeholder="tracedSVG"
            width={150}
          />
        </div>
      </section>

      {/* MIDDLE */}

      <section id="middle">
        <Triangle text="Solutions for" top="34%" />
        <BgRounded backBg="bg-body" roundBg="bg-primary3" />

        <div className="pb-8 md:pt-12 w-full bg-primary3">
          <div className="container m-auto lg:flex lg:items-start">
            <TextBox
              title={textBoxLeft.title}
              listItems={textBoxLeft.listItems}
              className="hidden lg:block w-1/4 z-40 lg:-mt-8 xl:mt-0"
            />
            <div>
              <div className="md:max-w-screen-sm m-auto lg:px-16">
                <div className="relative m-auto box-border w-full">
                  <IGTop />
                </div>
              </div>

              <div className="py-2 text-center xl:pt-0 xl:pb-2 ">
                <h2 className="text-primary1 font-bold text-xl md:text-3xl xl:text-4xl">
                  Focus
                </h2>
                <h3 className="text-white font-bold text-sm md:text-lg xl:text-xl">
                  3 key EU industry sectors
                </h3>
              </div>

              <div className="md:max-w-screen-sm m-auto relative lg:px-16">
                <img
                  src={arrowLeft}
                  alt="loop arrow"
                  className="absolute z-20 hidden lg:block"
                  style={{
                    top: "-330px",
                    left: "-130px",
                    width: "410px",
                  }}
                />

                <IGBottom />
                <img
                  src={arrowRight}
                  alt="loop arrow"
                  className="absolute z-20 hidden lg:block"
                  style={{
                    top: "-345px",
                    right: "-260px",
                    width: "410px",
                  }}
                />
              </div>
            </div>
            <TextBox
              title={textBoxRight.title}
              listItems={textBoxRight.listItems}
              className="hidden lg:block w-1/4 z-40 lg:-mt-8 xl:mt-0"
            />
          </div>
        </div>
        <div className=" -mt-4 relative z-40">
          <Triangle text="Objectives" />
        </div>
      </section>

      {/* BOTTOM */}

      <section id="bottom">
        <BgRounded backBg="bg-primary3" roundBg="bg-body" />

        <div className="md:mt-6 w-full bg-body">
          <div className="container m-auto flex justify-around max-w-screen-sm xl:gap-16 ">
            {bottomIcons.map((icon, i) => {
              return <BottomIcon text={icon.text} icon={icon.icon} key={i} />
            })}
          </div>
        </div>
      </section>
    </>
  )
}

export default Infographic

const Triangle = ({ text, variant = "fill", top = "37%" }) => {
  return (
    <div
      className="relative w-full flex justify-center h-10 -mb-8 z-100"
      id="triangle"
    >
      <h2
        className={`text-lg font-bold absolute z-40 ${
          variant === "fill" ? "text-white" : "text-primary1"
        }`}
        style={{
          top: top,
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        {text}
      </h2>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 120 20"
        className={`absolute h-full ${
          variant === "fill" && "fill-current text-primary1"
        }`}
      >
        {variant === "fill" ? (
          <polygon points="0,0 60,20 120,0 	" />
        ) : (
          <>
            <g>
              <polygon style={{ fill: "#FFFFFF" }} points="0,0 60,20 120,0 	" />
            </g>
            <g>
              <path
                style={{ fill: "#1C5469" }}
                d="M113.8,1L60,18.9L6.2,1H113.8 M120,0H0l60,20L120,0L120,0z"
              />
            </g>
          </>
        )}
      </svg>
    </div>
  )
}

const BottomIcon = ({ icon, text, ...props }) => {
  return (
    <div
      className="flex flex-col justify-start items-center xl:flex-row"
      {...props}
    >
      <div className="p-3 box-border w-16 xl:w-24">
        <img
          src={icon}
          alt="re-sourcing infographic"
          className="m-auto rounded-md z-30 relative w-full"
        />
      </div>

      <div className="px-2 font-bold text-xs text-primary2 z-40 w-16 sm:w-24 xl:w-32">
        <p className="text-center">{text}</p>
      </div>
    </div>
  )
}

const TextBox = ({ title, listItems, className }) => {
  return (
    <div
      className={`bg-white border-2 border-primary1 rounded-md py-2 px-4 ${className}`}
    >
      <h3 className="text-lg font-bold mb-3" id="title">
        {title}
      </h3>
      <ul className="list-disc pl-5">
        {listItems.map((text, i) => {
          return <li key={i}>{text}</li>
        })}
      </ul>
    </div>
  )
}

const BgRounded = ({ roundBg, backBg }) => {
  return (
    <div className={`overflow-hidden h-16 md:h-4 ${backBg}`}>
      <div
        className={roundBg}
        style={{
          width: "150%",
          height: "350px",
          marginLeft: "-25%",
          borderRadius: "75%",
          marginBottom: "-150px",
        }}
      />
    </div>
  )
}

const IGTop = () => {
  return (
    <>
      <div className="grid grid-cols-7 w-full gap-1 px-6">
        <StakeholderIcon />
        <div className="col-span-2 px-2 sm:px-6 flex items-end">
          <img src={pointLineTop} className="" alt="" />
        </div>

        <StakeholderIcon topLine icon={civilSocietyIcon} text="Civil society" />
        <div className="col-span-2 px-2 sm:px-6 flex items-end">
          <img src={pointLineTop} className="reflect-x" alt="" />
        </div>

        <StakeholderIcon icon={businessesIcon} text="Businesses" />
      </div>
    </>
  )
}

const IGBottom = () => {
  return (
    <>
      <div className="grid grid-cols-7 w-full gap-1 px-6">
        <TargetGroupIcon />
        <div className="col-span-2 px-2 sm:px-6">
          <div className="h-auto flex">
            <img src={pointLine} className="h-full" alt="" />
          </div>
        </div>

        <TargetGroupIcon topLine icon={MobilityIcon} text="Mobility" />
        <div className="col-span-2 px-2 sm:px-6">
          <div className="h-auto">
            <img src={pointLine} className="h-full reflect-x " alt="" />
          </div>
        </div>

        <TargetGroupIcon
          icon={eeeIcon}
          text="Electrical and electronic equipment"
        />
      </div>
    </>
  )
}

const TargetGroupIcon = ({
  icon = ReIcon,
  text = "Renewable energy",
  topLine = false,
  className,
}) => (
  <div className="text-center col-span-1">
    {topLine && (
      <img
        src={pointLineMiddle}
        alt=""
        className="h-5 mb-1 sm:h-10 sm:mb-6 m-auto lg:h-5 lg:mb-1"
      />
    )}
    <div
      className={`flex justify-center items-center m-auto ${
        !topLine && "mt-6 sm:mt-16 lg:mt-6"
      } ${className}`}
      style={{
        width: "clamp(2.5rem, 100%, 4rem)",
      }}
    >
      <img src={icon} alt="" />
    </div>
    <p className="text-xs text-white font-bold">{text}</p>
  </div>
)

const StakeholderIcon = ({
  icon = policyIcon,
  text = "Policy Makers",
  topLine = false,
  className,
}) => (
  <div className="text-center col-span-1">
    <div
      className={`flex justify-center items-center m-auto ${className}`}
      style={{
        width: "clamp(2.5rem, 100%, 4rem)",
      }}
    >
      <img src={icon} alt="" />
    </div>
    <p className="text-xs text-white font-bold">{text}</p>
    {topLine && (
      <img
        src={pointLineMiddle}
        alt=""
        className="h-5 mt-1 sm:h-10 sm:mt-6 m-auto lg:h-5 lg:mt-1"
      />
    )}
  </div>
)
