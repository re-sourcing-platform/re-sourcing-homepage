import React, { useRef, useEffect } from "react"
import SectorIcon from "./SectorIcon"
import "../../../styles/roadmap.css"

import { Link } from "gatsby"

const Roadmap = ({ className, size = 1600 }) => {
  const scrollEl = useRef(null)
  useEffect(() => {
    scrollEl.current.scrollLeft = size / 4
    return
  }, [size])

  if (typeof size != "number") size = 1600
  let height = size * 0.2
  let topIcons = [
    {
      sector: "renewable",
      borderColor: "border-accent1",
      link: "/sectors/renewable-energy/overview/",
    },
    {
      sector: "mobility",
      borderColor: "border-accent2",
      link: "/sectors/mobility/overview/",
    },
    {
      sector: "EEE",
      borderColor: "border-accent3",
      link: "/sectors/electronics-and-electronic-equipment/overview/",
    },
  ]

  const SectorIcons = ({ sectors = [], className }) => {
    return (
      <div className={`flex gap-4 ${className}`}>
        {sectors.map((sector, i) => (
          <div
            key={i}
            className={`border-b-8 ${sector?.borderColor} w-24 md:w-32`}
          >
            <SectorIcon
              to={sector?.link}
              sector={sector?.sector}
              className="w-1/2 mx-auto rounded-full p-2 md:p-4 mb-2 hover:opacity-75 transform hover:-translate-y-1 transition-all duration-200"
            />
          </div>
        ))}
      </div>
    )
  }
  return (
    <div className="w-full flex flex-col items-center -mt-16 p-4">
      <SectorIcons
        sectors={topIcons}
        className="relative z-20 transform translate-y-full"
      />
      <div className={`w-full ${className}`} style={{ height: `${height}px` }}>
        {/* tausta jooned */}
        <div className="w-full h-full relative overflow-x-hidden">
          <svg
            version="1.1"
            id="Roadmap"
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            viewBox="0 0 1680 320"
            className="absolute"
            style={{
              enableBackground: "new 0 0 1680 320",
              background: "transparent",
              width: `${size}px`,
              right: "50%",
              transform: "translateX(50%)",
            }}
          >
            <g id="bg-lines">
              <rect className="st0" width="1680" height="320" />
              <polygon
                className="st1"
                points="485.8,68.3 514.8,68.3 83,214.7 15.4,214.7 	"
              />
              <polygon
                className="st1"
                points="547.2,68.3 576.2,68.3 226.3,214.7 158.7,214.7 	"
              />
              <polygon
                className="st1"
                points="608.7,68.3 637.7,68.3 369.7,214.7 302,214.7 	"
              />
              <polygon
                className="st1"
                points="670.2,68.3 699.2,68.3 513,214.7 445.4,214.7 	"
              />
              <polygon
                className="st1"
                points="731.6,68.3 760.6,68.3 656.3,214.7 588.7,214.7 	"
              />
              <polygon
                className="st1"
                points="793.1,68.3 822.1,68.3 799.6,214.7 732,214.7 	"
              />
              <polygon
                className="st1"
                points="854.5,68.3 883.5,68.3 942.9,214.7 875.3,214.7 	"
              />
              <polygon
                className="st1"
                points="916,68.3 945,68.3 1086.2,214.7 1018.6,214.7 	"
              />
              <polygon
                className="st1"
                points="977.5,68.3 1006.5,68.3 1229.5,214.7 1161.9,214.7 	"
              />
              <polygon
                className="st1"
                points="1038.9,68.3 1067.9,68.3 1372.8,214.7 1305.2,214.7 	"
              />
              <polygon
                className="st1"
                points="1100.4,68.3 1129.4,68.3 1516.1,214.7 1448.5,214.7 	"
              />
              <polygon
                className="st1"
                points="1161.8,68.3 1190.8,68.3 1659.4,214.7 1591.8,214.7 	"
              />
            </g>
          </svg>
        </div>
      </div>
      <div
        className={`w-full scroll relative overflow-x-auto uverflow-y-hidden ${className}`}
        style={{ height: `${height}px`, marginTop: `-${height}px` }}
        ref={scrollEl}
      >
        <svg
          version="1.1"
          id="Roadmap"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 1680 320"
          className="absolute"
          style={{
            enableBackground: "new 0 0 1680 320",
            width: `${size}px`,
          }}
        >
          <g id="Ebene_3">
            <rect
              id="timeline"
              x="16.2"
              y="217"
              className="st1"
              width="1646.5"
              height="29.1"
            />
            <text
              transform="matrix(1 0 0 1 396.198 234.823)"
              className="st4 st5 st6"
            >
              Mar
            </text>
            <text
              transform="matrix(1 0 0 1 465.6653 234.823)"
              className="st4 st5 st6"
            >
              May
            </text>
            <text
              transform="matrix(1 0 0 1 542.4582 234.823)"
              className="st4 st5 st6"
            >
              Jul
            </text>
            <text
              transform="matrix(1 0 0 1 611.9606 234.823)"
              className="st4 st5 st6"
            >
              Sep
            </text>
            <text
              transform="matrix(1 0 0 1 683.1965 234.823)"
              className="st4 st5 st6"
            >
              Nov
            </text>
            <text
              transform="matrix(1 0 0 1 825.9348 234.823)"
              className="st4 st5 st6"
            >
              Mar
            </text>
            <text
              transform="matrix(1 0 0 1 895.4016 234.823)"
              className="st4 st5 st6"
            >
              May
            </text>
            <text
              transform="matrix(1 0 0 1 972.1935 234.823)"
              className="st4 st5 st6"
            >
              Jul
            </text>
            <text
              transform="matrix(1 0 0 1 1041.6975 234.823)"
              className="st4 st5 st6"
            >
              Sep
            </text>
            <text
              transform="matrix(1 0 0 1 1112.9319 234.823)"
              className="st4 st5 st6"
            >
              Nov
            </text>
            <text
              transform="matrix(1 0 0 1 1255.8948 234.823)"
              className="st4 st5 st6"
            >
              Mar
            </text>
            <text
              transform="matrix(1 0 0 1 1325.3625 234.823)"
              className="st4 st5 st6"
            >
              May
            </text>
            <text
              transform="matrix(1 0 0 1 1402.1545 234.823)"
              className="st4 st5 st6"
            >
              Jul
            </text>
            <text
              transform="matrix(1 0 0 1 1471.6584 234.823)"
              className="st4 st5 st6"
            >
              Sep
            </text>
            <text
              transform="matrix(1 0 0 1 1542.8938 234.823)"
              className="st4 st5 st6"
            >
              Nov
            </text>
            <text
              transform="matrix(1 0 0 1 181.9997 234.823)"
              className="st4 st5 st6"
            >
              Sep
            </text>

            <text
              transform="matrix(1 0 0 1 112.7375 234.823)"
              className="st4 st5 st6"
            >
              Jul
            </text>
            <text
              transform="matrix(1 0 0 1 36.0467 234.823)"
              className="st4 st5 st6"
            >
              May
            </text>
            <text
              transform="matrix(1 0 0 1 252.2406 234.823)"
              className="st4 st5 st6"
            >
              Nov
            </text>

            {/* EVENTS */}
            <Link
              to="/events/drivers-of-responsible-sourcing/"
              id="opening-conference"
              className="hover:opacity-75 transform hover:translate-y-1 transition-all duration-200"
            >
              <g>
                <path
                  className="st2 "
                  d="M388.8,260.6H281.2c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1h107.6c1.7,0,3.1-1.4,3.1-3.1v-36.9
			C391.9,262,390.5,260.6,388.8,260.6z"
                />
                <polygon
                  className="st2"
                  points="335,249.7 327.6,261.8 342.4,261.8 		"
                />
                <text
                  transform="matrix(1 0 0 1 312.2989 279.265)"
                  className="st0 st5 st6"
                >
                  Opening
                </text>
                <text
                  transform="matrix(1 0 0 1 304.1989 294.265)"
                  className="st0 st5 st6"
                >
                  Conference
                </text>
              </g>
            </Link>
            <Link
              to={`events/2nd-re-sourcing-virtual-conference/`}
              id="1st-virtual-conference"
              className="hover:opacity-75 transform hover:translate-y-1 transition-all duration-200"
            >
              <g>
                <path
                  className="st2"
                  d="M749,260.6H641.4c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1H749c1.7,0,3.1-1.4,3.1-3.1v-36.9
			C752.1,262,750.7,260.6,749,260.6z"
                />
                <polygon
                  className="st2"
                  points="695.2,249.7 687.8,261.8 702.6,261.8"
                />
                <text
                  transform="matrix(1 0 0 1 667.7253 279.265)"
                  className="st0 st5 st6"
                >
                  1st Virtual
                </text>
                <text
                  transform="matrix(1 0 0 1 664.4253 294.265)"
                  className="st0 st5 st6"
                >
                  Conference
                </text>
              </g>
            </Link>

            <g>
              <path
                className="st7"
                d="M1179,260.6h-107.6c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1H1179c1.7,0,3.1-1.4,3.1-3.1v-36.9
			C1182.1,262,1180.7,260.6,1179,260.6z"
              />
              <polygon
                className="st7"
                points="1125.2,249.7 1117.8,261.8 1132.7,261.8 		"
              />
              <text
                transform="matrix(1 0 0 1 1095.6324 279.265)"
                className="st0 st5 st6"
              >
                2nd Virtual
              </text>
              <text
                transform="matrix(1 0 0 1 1094.4325 294.265)"
                className="st0 st5 st6"
              >
                Conference
              </text>
            </g>
            <Link
              to={`events/global-advocacy-forum-latin-america/`}
              className="hover:opacity-75 transform hover:translate-y-1 transition-all duration-200"
            >
              <g>
                <path
                  className="st2"
                  d="M996.7,260.6H889.1c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1h107.6c1.7,0,3.1-1.4,3.1-3.1v-36.9
			C999.8,262,998.4,260.6,996.7,260.6z"
                />
                <polygon
                  className="st2"
                  points="942.9,249.7 935.5,261.8 950.3,261.8 		"
                />
              </g>
              <text
                transform="matrix(1 0 0 1 898.9286 279.265)"
                className="st0 st5 st6"
              >
                Global Advocacy
              </text>
              <text
                transform="matrix(1 0 0 1 907.8286 294.265)"
                className="st0 st5 st6"
              >
                Forum LatAm
              </text>
            </Link>
            <path
              className="st7"
              d="M1290,260.6h-107.6c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1H1290c1.7,0,3.1-1.4,3.1-3.1v-36.9
		C1293.1,262,1291.7,260.6,1290,260.6z"
            />
            <polygon
              className="st7"
              points="1236.2,249.7 1228.8,261.8 1243.6,261.8 	"
            />
            <text
              transform="matrix(1 0 0 1 1192.2389 279.265)"
              className="st0 st5 st6"
            >
              Global Advocacy
            </text>
            <text
              transform="matrix(1 0 0 1 1209.1389 294.265)"
              className="st0 st5 st6"
            >
              Forum Asia
            </text>
            <g>
              <path
                className="st7"
                d="M1393,260.6h-107.6c-1.7,0-3.1,1.4-3.1,3.1v36.9c0,1.7,1.4,3.1,3.1,3.1H1393c1.7,0,3.1-1.4,3.1-3.1v-36.9
			C1396.1,262,1394.7,260.6,1393,260.6z"
              />
              <polygon
                className="st7"
                points="1339.2,249.7 1331.8,261.8 1346.7,261.8 		"
              />
            </g>
            <text
              transform="matrix(1 0 0 1 1296.2845 279.265)"
              className="st0 st5 st6"
            >
              Global Advocacy
            </text>
            <text
              transform="matrix(1 0 0 1 1307.8845 294.265)"
              className="st0 st5 st6"
            >
              Forum Africa
            </text>
            <path
              className="st7"
              d="M1500.1,260.6h-104c-1.7,0-3,1.4-3,3v37c0,1.7,1.4,3,3,3h104c1.7,0,3-1.4,3-3v-37
		C1503.2,261.9,1501.8,260.6,1500.1,260.6z"
            />
            <polygon
              className="st7"
              points="1446.3,249.7 1438.8,262.5 1453.7,262.5 	"
            />
            <text
              transform="matrix(1 0 0 1 1427.0032 279.265)"
              className="st0 st5 st6"
            >
              Closing
            </text>
            <text
              transform="matrix(1 0 0 1 1415.5032 294.265)"
              className="st0 st5 st6"
            >
              Conference
            </text>
            <text
              transform="matrix(1 0 0 1 1604.6008 237.7411)"
              className="st7 st8 st9"
            >
              2024
            </text>
            <text
              transform="matrix(1 0 0 1 1174.7522 237.7411)"
              className="st7 st8 st9"
            >
              2023
            </text>
            <text
              transform="matrix(1 0 0 1 745.0158 237.7411)"
              className="st7 st8 st9"
            >
              2022
            </text>
            <text
              transform="matrix(1 0 0 1 315.1672 237.7411)"
              className="st7 st8 st9"
            >
              2021
            </text>
            <g id="year-lines">
              <line
                className="st10"
                x1="1625.2"
                y1="241.2"
                x2="1625.2"
                y2="245.4"
              />
              <line
                className="st10"
                x1="1195.3"
                y1="241.2"
                x2="1195.3"
                y2="245.4"
              />
              <line
                className="st10"
                x1="765.6"
                y1="241.2"
                x2="765.6"
                y2="245.4"
              />
              <line
                className="st10"
                x1="335.2"
                y1="241.2"
                x2="335.2"
                y2="245.4"
              />
            </g>
            <rect
              x="265"
              y="195.5"
              className="st11"
              width="287.6"
              height="19.2"
            />
            <rect
              x="657"
              y="195.5"
              className="st12"
              width="285.4"
              height="19.2"
            />
            <rect
              x="1018.6"
              y="195.5"
              className="st13"
              width="247.6"
              height="19.2"
            />
            <g id="box-separators">
              <line
                className="st14"
                x1="1179.8"
                y1="256.2"
                x2="1179.8"
                y2="319.2"
              />
              <line
                className="st14"
                x1="1287.7"
                y1="256.2"
                x2="1287.7"
                y2="319.2"
              />
              <line
                className="st14"
                x1="1393.8"
                y1="256.2"
                x2="1393.8"
                y2="319.2"
              />
            </g>
            <g id="res-lines">
              <path
                className="st15"
                d="M266,171.6v-70c0-7.4,6.1-13.5,13.5-13.5h258c7.4,0,13.5,6.1,13.5,13.5V169"
              />
              <polyline
                className="st16"
                points="446.3,172.5 446.3,167.4 361.1,167.4 361.1,140.4 	"
              />
              <polyline
                className="st16"
                points="516.3,170.2 516.3,167.3 459.8,167.3 459.8,141 	"
              />
            </g>

            <g>
              <polygon
                className="st11"
                points="320.5,78.8 329.2,87.5 320.5,96.1 		"
              />
              <path
                className="st0"
                d="M321,80l7.5,7.4l-7.5,7.4V80 M320,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4L320,77.6
			L320,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st11"
                points="415.6,78.8 424.3,87.5 415.6,96.1 		"
              />
              <path
                className="st0"
                d="M416.1,80l7.5,7.4l-7.5,7.4V80 M415.1,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L415.1,77.6L415.1,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st11"
                points="510.7,78.8 519.4,87.5 510.7,96.1 		"
              />
              <path
                className="st0"
                d="M511.2,80l7.5,7.4l-7.5,7.4V80 M510.2,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L510.2,77.6L510.2,77.6z"
              />
            </g>
            <line
              className="st16"
              x1="380.6"
              y1="129.4"
              x2="407.4"
              y2="129.4"
            />
            <line
              className="st16"
              x1="484.5"
              y1="129.4"
              x2="511.4"
              y2="129.4"
            />
            <g>
              <path
                className="st21"
                d="M658,171.6v-71c0-7.4,6.1-13.5,13.5-13.5l258,0.9c7.4,0,13.5,6.1,13.5,13.6V169"
              />
            </g>
            <polyline
              className="st22"
              points="765.4,172.6 765.4,167.4 753.4,167.4 753.4,138.5 	"
            />
            <polyline
              className="st22"
              points="837.7,170.2 837.7,167.4 851.8,167.4 851.8,139 	"
            />
            <g>
              <polygon
                className="st12"
                points="712.5,78.8 721.2,87.5 712.5,96.1 		"
              />
              <path
                className="st0"
                d="M713,80l7.5,7.4l-7.5,7.4V80 M712,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4L712,77.6
			L712,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st12"
                points="807.6,78.8 816.3,87.5 807.6,96.1 		"
              />
              <path
                className="st0"
                d="M808.1,80l7.5,7.4l-7.5,7.4V80 M807.1,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L807.1,77.6L807.1,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st12"
                points="902.7,78.8 911.4,87.5 902.7,96.1 		"
              />
              <path
                className="st0"
                d="M903.2,80l7.5,7.4l-7.5,7.4V80 M902.2,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L902.2,77.6L902.2,77.6z"
              />
            </g>
            <line
              className="st22"
              x1="772.6"
              y1="129.4"
              x2="799.4"
              y2="129.4"
            />
            <line
              className="st22"
              x1="876.5"
              y1="129.4"
              x2="903.4"
              y2="129.4"
            />
            <g>
              <path
                className="st23"
                d="M1019.1,171.6v-71c0-6.9,5.6-12.6,12.6-12.6h221.1c6.9,0,12.6,5.6,12.6,12.6V169"
              />
            </g>
            <polyline
              className="st24"
              points="1159.6,172.5 1159.6,167.5 1099.2,167.5 1099.2,140.4 	"
            />
            <polyline
              className="st24"
              points="1236.2,170.1 1236.2,167.3 1192.7,167.3 1192.7,138.9 	"
            />
            <g>
              <polygon
                className="st13"
                points="1068.6,78.8 1077.3,87.5 1068.6,96.1 		"
              />
              <path
                className="st0"
                d="M1069.1,80l7.5,7.4l-7.5,7.4V80 M1068.1,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L1068.1,77.6L1068.1,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st13"
                points="1147.8,78.8 1156.5,87.5 1147.8,96.1 		"
              />
              <path
                className="st0"
                d="M1148.3,80l7.5,7.4l-7.5,7.4V80 M1147.3,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L1147.3,77.6L1147.3,77.6z"
              />
            </g>
            <g>
              <polygon
                className="st13"
                points="1227.1,78.8 1235.8,87.5 1227.1,96.1 		"
              />
              <path
                className="st0"
                d="M1227.6,80l7.5,7.4l-7.5,7.4V80 M1226.6,77.6V80v14.8v2.4l1.7-1.7l7.5-7.4l0.7-0.7l-0.7-0.7l-7.5-7.4
			L1226.6,77.6L1226.6,77.6z"
              />
            </g>

            <text
              transform="matrix(1 0 0 1 324.3198 209.2834)"
              className="st0 st8 st6"
            >
              Renewable Energy Sector (RES)
            </text>
            <text
              transform="matrix(1 0 0 1 743.8286 209.2834)"
              className="st0 st8 st6"
            >
              Mobility Sector (MS)
            </text>
            <text
              transform="matrix(0.98 0 0 1 1024.7734 209.2832)"
              className="st0 st8 st6"
            >
              Electrical &amp; Electronic Equipment Sector (EEES)
            </text>

            {/* OUTPUTS  */}

            <Link
              to={`events/virtual-roadmap-workshop/`}
              rel="noreferrer noopener"
              className="transform hover:-translate-y-1 transition-all duration-200"
            >
              <circle
                className="re-sector-linked"
                cx="274.8"
                cy="130.4"
                r="32.5"
              />
              <text
                transform="matrix(1 0 0 1 249.601 126.4947)"
                className="st5 st6 st0"
              >
                Roadmap
              </text>
              <text
                transform="matrix(1 0 0 1 247.601 141.4947)"
                className="st5 st6 st0"
              >
                Workshop
              </text>
            </Link>
            <Link
              to={`events/flagship-lab-for-the-renewable-energy-sector-1/`}
              rel="noreferrer noopener"
              className="transform hover:translate-x-1 transition-all duration-200"
            >
              <circle
                className="re-sector-linked"
                cx="361.1"
                cy="130.3"
                r="32.5"
              />
              <text
                transform="matrix(1 0 0 1 348.6352 119.0226)"
                className="st5 st6 st0"
              >
                Peer
              </text>
              <text
                transform="matrix(1 0 0 1 338.0352 134.0226)"
                className="st5 st6 st0 "
              >
                Learning
              </text>
              <text
                transform="matrix(1 0 0 1 351.4352 149.0226)"
                className="st5 st6 st0"
              >
                Lab
              </text>
            </Link>

            <Link
              to={`/files/d5.2_res_guidance-document_final.pdf`}
              rel="noreferrer noopener"
              className="transform hover:translate-x-1 transition-all duration-200"
            >
              <rect
                x="416.4"
                y="110.8"
                className="re-sector-linked"
                width="83.8"
                height="38"
              />
              <text
                transform="matrix(1 0 0 1 421.5767 126.4026)"
                className="st5 st6 st0"
              >
                Good Practice
              </text>

              <text
                transform="matrix(1 0 0 1 442.8767 141.4026)"
                className="st5 st6 st0"
              >
                Guide
              </text>
            </Link>
            <Link
              to={`/files/final_res_roadmap_2021.pdf`}
              rel="noreferrer noopener"
              className="transform hover:-translate-y-1 transition-all duration-200"
            >
              <rect
                x="522.4"
                y="110.8"
                className="re-sector-linked"
                width="68.9"
                height="38"
              />
              <text
                transform="matrix(1 0 0 1 546.6611 126.4024)"
                className="st5 st6 st0"
              >
                RES
              </text>
              <text
                transform="matrix(1 0 0 1 531.1611 141.4024)"
                className="st5 st6 st0"
              >
                Roadmap
              </text>
            </Link>

            <g>
              <polygon
                className="st11"
                points="409.6,129.2 402.1,121.8 402.1,136.6"
              />
            </g>
            <g>
              <polygon
                className="st11"
                points="516.2,129.2 508.7,121.8 508.7,136.6"
              />
            </g>
            <Link
              to={`events/the-roadmap-workshop-on-the-mobility-sector/`}
              rel="noreferrer noopener"
              className="transform hover:-translate-y-1 transition-all duration-200"
            >
              <circle
                className="mob-sector-linked"
                cx="666.8"
                cy="130.4"
                r="32.5"
              />
              <text
                transform="matrix(1 0 0 1 641.6001 126.4947)"
                className="st5 st6 st0"
              >
                Roadmap
              </text>
              <text
                transform="matrix(1 0 0 1 639.6001 141.4947)"
                className="st5 st6 st0"
              >
                Workshop
              </text>
            </Link>
            <Link
              to={`events/the-flagship-lab-for-the-mobility-sector/`}
              id="mobility-flagship"
              rel="noreferrer noopener"
              className="transform hover:translate-x-1 transition-all duration-200"
            >
              <circle
                className="mob-sector-linked"
                cx="753.1"
                cy="130.3"
                r="32.5"
              />
              <text
                transform="matrix(1 0 0 1 740.6343 119.0226)"
                className="st5 st6 st0"
              >
                Peer
              </text>
              <text
                transform="matrix(1 0 0 1 730.0343 134.0226)"
                className="st5 st6 st0"
              >
                Learning
              </text>
              <text
                transform="matrix(1 0 0 1 743.4343 149.0226)"
                className="st5 st6 st0"
              >
                Lab
              </text>
            </Link>

            <rect
              x="914.4"
              y="110.8"
              className="st29"
              width="67.9"
              height="38"
            />
            <rect
              x="808.4"
              y="110.8"
              className="st29"
              width="83.8"
              height="38"
            />
            <text
              transform="matrix(1 0 0 1 939.9068 126.4024)"
              className="st5 st6"
            >
              MS
            </text>
            <text
              transform="matrix(1 0 0 1 923.2068 141.4024)"
              className="st5 st6"
            >
              Roadmap
            </text>
            <text
              transform="matrix(1 0 0 1 813.5758 126.4026)"
              className="st5 st6"
            >
              Good Practice
            </text>
            <text
              transform="matrix(1 0 0 1 834.8758 141.4026)"
              className="st5 st6"
            >
              Guide
            </text>
            <polygon
              className="st12"
              points="801.6,129.2 794.1,121.8 794.1,136.6 	"
            />
            <polygon
              className="st12"
              points="908.2,129.2 900.7,121.8 900.7,136.6 	"
            />
            <circle className="st31" cx="1027.9" cy="130.6" r="32.5" />
            <text
              transform="matrix(1 0 0 1 1002.7003 126.4947)"
              className="st5 st6"
            >
              Roadmap
            </text>
            <text
              transform="matrix(1 0 0 1 1000.7003 141.4947)"
              className="st5 st6"
            >
              Workshop
            </text>
            <rect
              x="1250.8"
              y="110.8"
              className="st32"
              width="74.1"
              height="38"
            />
            <text
              transform="matrix(1 0 0 1 1278.2683 126.4024)"
              className="st5 st6"
            >
              EEE
            </text>
            <text
              transform="matrix(1 0 0 1 1262.5684 141.4024)"
              className="st5 st6"
            >
              Roadmap
            </text>
            <line
              className="st24"
              x1="1115.7"
              y1="129.4"
              x2="1142.5"
              y2="129.4"
            />
            <circle className="st33" cx="1099.2" cy="130.3" r="32.5" />
            <text
              transform="matrix(1 0 0 1 1086.7345 119.0226)"
              className="st5 st6"
            >
              Peer
            </text>
            <text
              transform="matrix(1 0 0 1 1076.1345 134.0226)"
              className="st5 st6"
            >
              Learning
            </text>
            <text
              transform="matrix(1 0 0 1 1089.5345 149.0226)"
              className="st5 st6"
            >
              Lab
            </text>
            <polygon
              className="st13"
              points="1144.7,129.2 1137.2,121.8 1137.2,136.6 	"
            />
            <line
              className="st24"
              x1="1217.6"
              y1="129.4"
              x2="1244.5"
              y2="129.4"
            />
            <rect
              x="1149.5"
              y="110.8"
              className="st32"
              width="83.8"
              height="38"
            />
            <text
              transform="matrix(1 0 0 1 1154.676 126.4026)"
              className="st5 st6"
            >
              Good Practice
            </text>
            <text
              transform="matrix(1 0 0 1 1175.9761 141.4026)"
              className="st5 st6"
            >
              Guide
            </text>
            <polygon
              className="st13"
              points="1246.3,129.2 1238.8,121.8 1238.8,136.6 	"
            />
            <polyline
              className="st34"
              points="1517.7,189.9 1517.7,179.3 1443.2,179.3 1443.2,165.1 	"
            />
            <rect
              x="1353.1"
              y="111.1"
              className="st35"
              width="180.1"
              height="53.8"
            />
            <text
              transform="matrix(1 0 0 1 1408.3868 126.5987)"
              className="st8 st6"
            >
              Final Report:
            </text>
            <text
              transform="matrix(1 0 0 1 1357.7869 141.5987)"
              className="st5 st6"
            >
              Moving Towards a unified vision
            </text>
            <text
              transform="matrix(1 0 0 1 1377.7869 156.5987)"
              className="st5 st6"
            >
              for Responsible Sourcing
            </text>
          </g>
          <g id="symbols">
            <rect
              x="1150.6"
              y="178.9"
              className="st0"
              width="17.4"
              height="12.3"
            />
            <polygon
              className="st0"
              points="1232.2,173.6 1230.3,173.7 1229.3,175.2 1229.3,177.8 1229.3,190 1229.9,191.5 1231.8,192.2 
		1241.8,192.2 1243.3,191.4 1243.8,189.7 1243.8,178.9 1238.3,173.6 	"
            />
            <polygon
              className="st0"
              points="1261.5,173.6 1259.7,173.7 1258.7,175.2 1258.7,177.8 1258.7,190 1259.2,191.5 1261.1,192.2 
		1271.2,192.2 1272.6,191.4 1273.1,189.7 1273.1,178.9 1267.6,173.6 	"
            />
            <polygon
              className="st0"
              points="547.3,173.5 545.4,173.6 544.4,175.1 544.4,177.6 544.4,189.9 545,191.4 546.9,192 556.9,192 
		558.4,191.2 558.9,189.5 558.9,178.7 553.4,173.4 	"
            />
            <polygon
              className="st0"
              points="512.1,173.5 510.3,173.6 509.3,175.1 509.3,177.6 509.3,189.9 509.8,191.4 511.7,192 521.8,192 
		523.2,191.2 523.7,189.5 523.7,178.7 518.2,173.4 	"
            />
            <rect
              x="1010.3"
              y="178.8"
              className="st0"
              width="17.7"
              height="12.7"
            />
            <rect
              x="256.9"
              y="178.6"
              className="st0"
              width="17.7"
              height="12.7"
            />
            <rect
              x="649"
              y="178.9"
              className="st0"
              width="17.8"
              height="12.6"
            />
            <rect
              x="756.5"
              y="178.9"
              className="st0"
              width="17.8"
              height="12.6"
            />
            <g>
              <g>
                <path
                  className="st11"
                  d="M258.3,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4h-1.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7h-5.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H258.3z"
                />
                <path
                  className="st0"
                  d="M273.3,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6h16.5v10.6C274.1,190.5,273.7,190.8,273.3,190.8z"
                />
              </g>
              <path
                className="st11"
                d="M261.3,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C260.4,176.5,260.8,176.9,261.3,176.9z"
              />
              <path
                className="st11"
                d="M270.3,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C269.4,176.5,269.8,176.9,270.3,176.9z"
              />
              <polygon
                className="st11"
                points="265.8,181.3 267,183.8 269.8,184.2 267.8,186.1 268.3,188.9 265.8,187.6 263.3,188.9 263.8,186.1 
			261.8,184.2 264.6,183.8 		"
              />
            </g>
            <g>
              <g>
                <path
                  className="st11"
                  d="M438.9,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4h-1.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7h-5.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H438.9z"
                />
                <path
                  className="st0"
                  d="M453.9,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6h16.5v10.6C454.6,190.5,454.3,190.8,453.9,190.8z"
                />
              </g>
              <path
                className="st11"
                d="M441.9,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C441,176.5,441.4,176.9,441.9,176.9z"
              />
              <path
                className="st11"
                d="M450.9,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C450,176.5,450.4,176.9,450.9,176.9z"
              />
              <polygon
                className="st11"
                points="446.4,181.3 447.6,183.8 450.4,184.2 448.4,186.1 448.9,188.9 446.4,187.6 443.9,188.9 444.4,186.1 
			442.4,184.2 445.2,183.8 		"
              />
            </g>
            <g>
              <path
                className="st11"
                d="M518.3,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C518.7,172.9,518.5,172.8,518.3,172.8z M521.5,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C522.9,190.6,522.3,191.2,521.5,191.2z M522,178h-3.1v-3.1L522,178
			z"
              />
              <g>
                <path
                  className="st11"
                  d="M512.1,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H512.1z"
                />
              </g>
              <g>
                <path
                  className="st11"
                  d="M512.1,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H512.1z"
                />
              </g>
              <g>
                <path
                  className="st11"
                  d="M512.1,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H512.1z"
                />
              </g>
            </g>
            <g>
              <path
                className="st11"
                d="M553.5,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C554,172.9,553.8,172.8,553.5,172.8z M556.8,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C558.2,190.6,557.6,191.2,556.8,191.2z M557.3,178h-3.1v-3.1
			L557.3,178z"
              />
              <g>
                <path
                  className="st11"
                  d="M547.3,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H547.3z"
                />
              </g>
              <g>
                <path
                  className="st11"
                  d="M547.3,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H547.3z"
                />
              </g>
              <g>
                <path
                  className="st11"
                  d="M547.3,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H547.3z"
                />
              </g>
            </g>
            <g>
              <g>
                <path
                  className="st12"
                  d="M650.4,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4h-1.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7h-5.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H650.4z"
                />
                <path
                  className="st0"
                  d="M665.4,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6h16.5v10.6C666.2,190.5,665.8,190.8,665.4,190.8z"
                />
              </g>
              <path
                className="st12"
                d="M653.4,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C652.5,176.5,652.9,176.9,653.4,176.9z"
              />
              <path
                className="st12"
                d="M662.4,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C661.5,176.5,661.9,176.9,662.4,176.9z"
              />
              <polygon
                className="st12"
                points="657.9,181.3 659.1,183.8 661.9,184.2 659.9,186.1 660.4,188.9 657.9,187.6 655.4,188.9 655.9,186.1 
			653.9,184.2 656.7,183.8 		"
              />
            </g>
            <g>
              <g>
                <path
                  className="st12"
                  d="M758.1,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4H772v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7H763v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H758.1z"
                />
                <path
                  className="st0"
                  d="M773.2,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6H774v10.6C773.9,190.5,773.6,190.8,773.2,190.8z"
                />
              </g>
              <path
                className="st12"
                d="M761.2,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C760.3,176.5,760.7,176.9,761.2,176.9z"
              />
              <path
                className="st12"
                d="M770.1,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C769.3,176.5,769.7,176.9,770.1,176.9z"
              />
              <polygon
                className="st12"
                points="765.7,181.3 766.9,183.8 769.7,184.2 767.7,186.1 768.1,188.9 765.7,187.6 763.2,188.9 763.7,186.1 
			761.7,184.2 764.4,183.8 		"
              />
            </g>
            <g>
              <path
                className="st12"
                d="M839.7,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6H843c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C840.2,172.9,840,172.8,839.7,172.8z M843,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C844.4,190.6,843.8,191.2,843,191.2z M843.5,178h-3.1v-3.1
			L843.5,178z"
              />
              <g>
                <path
                  className="st12"
                  d="M833.5,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H833.5z"
                />
              </g>
              <g>
                <path
                  className="st12"
                  d="M833.5,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H833.5z"
                />
              </g>
              <g>
                <path
                  className="st12"
                  d="M833.5,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H833.5z"
                />
              </g>
            </g>
            <g>
              <path
                className="st12"
                d="M944.7,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C945.1,172.9,944.9,172.8,944.7,172.8z M947.9,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C949.3,190.6,948.7,191.2,947.9,191.2z M948.4,178h-3.1v-3.1
			L948.4,178z"
              />
              <g>
                <path
                  className="st12"
                  d="M938.4,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H938.4z"
                />
              </g>
              <g>
                <path
                  className="st12"
                  d="M938.4,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H938.4z"
                />
              </g>
              <g>
                <path
                  className="st12"
                  d="M938.4,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H938.4z"
                />
              </g>
            </g>
            <g>
              <g>
                <path
                  className="st13"
                  d="M1011.7,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4h-1.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7h-5.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H1011.7z"
                />
                <path
                  className="st0"
                  d="M1026.8,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6h16.5v10.6C1027.5,190.5,1027.2,190.8,1026.8,190.8z"
                />
              </g>
              <path
                className="st13"
                d="M1014.8,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C1013.9,176.5,1014.3,176.9,1014.8,176.9z"
              />
              <path
                className="st13"
                d="M1023.7,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C1022.9,176.5,1023.2,176.9,1023.7,176.9z"
              />
              <polygon
                className="st13"
                points="1019.2,181.3 1020.5,183.8 1023.3,184.2 1021.3,186.1 1021.7,188.9 1019.2,187.6 1016.8,188.9 
			1017.2,186.1 1015.2,184.2 1018,183.8 		"
              />
            </g>
            <g>
              <g>
                <path
                  className="st13"
                  d="M1151.9,175.4c-1.3,0-2.4,1.1-2.4,2.4v12.3c0,1.3,1.1,2.4,2.4,2.4h15c1.3,0,2.4-1.1,2.4-2.4v-12.3
				c0-1.3-1.1-2.4-2.4-2.4h-1.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7h-5.2v0.7c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9v-0.7
				H1151.9z"
                />
                <path
                  className="st0"
                  d="M1167,190.8h-15c-0.4,0-0.7-0.3-0.7-0.7v-10.6h16.5v10.6C1167.7,190.5,1167.4,190.8,1167,190.8z"
                />
              </g>
              <path
                className="st13"
                d="M1154.9,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C1154.1,176.5,1154.4,176.9,1154.9,176.9z"
              />
              <path
                className="st13"
                d="M1163.9,176.9c0.5,0,0.9-0.4,0.9-0.9v-1.4c0-0.5-0.4-0.9-0.9-0.9s-0.9,0.4-0.9,0.9v1.4
			C1163,176.5,1163.4,176.9,1163.9,176.9z"
              />
              <polygon
                className="st13"
                points="1159.4,181.3 1160.7,183.8 1163.4,184.2 1161.4,186.1 1161.9,188.9 1159.4,187.6 1156.9,188.9 
			1157.4,186.1 1155.4,184.2 1158.2,183.8 		"
              />
            </g>
            <g>
              <path
                className="st13"
                d="M1238.5,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C1238.9,172.9,1238.7,172.8,1238.5,172.8z M1241.7,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C1243.1,190.6,1242.5,191.2,1241.7,191.2z M1242.2,178h-3.1v-3.1
			L1242.2,178z"
              />
              <g>
                <path
                  className="st13"
                  d="M1232.3,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1232.3z
				"
                />
              </g>
              <g>
                <path
                  className="st13"
                  d="M1232.3,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1232.3z
				"
                />
              </g>
              <g>
                <path
                  className="st13"
                  d="M1232.3,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1232.3z
				"
                />
              </g>
            </g>
            <g>
              <path
                className="st13"
                d="M1267.8,172.8h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6v-11.2
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C1268.2,172.9,1268,172.8,1267.8,172.8z M1271,191.2h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C1272.4,190.6,1271.8,191.2,1271,191.2z M1271.6,178h-3.1v-3.1
			L1271.6,178z"
              />
              <g>
                <path
                  className="st13"
                  d="M1261.6,187.4c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1261.6z
				"
                />
              </g>
              <g>
                <path
                  className="st13"
                  d="M1261.6,184.5c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1261.6z
				"
                />
              </g>
              <g>
                <path
                  className="st13"
                  d="M1261.6,181.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1261.6z
				"
                />
              </g>
            </g>
            <g>
              <path
                className="st7"
                d="M1519.9,193.1h-7.2c-1.4,0-2.6,1.2-2.6,2.6v14.4c0,1.4,1.2,2.6,2.6,2.6h10.4c1.4,0,2.6-1.2,2.6-2.6V199
			c0-0.2-0.1-0.4-0.3-0.5l0,0l-5-5C1520.3,193.2,1520.1,193.1,1519.9,193.1z M1523.1,211.5h-10.4c-0.8,0-1.4-0.6-1.4-1.4v-14.4
			c0-0.8,0.6-1.4,1.4-1.4h6.6v4.6c0,0.3,0.3,0.6,0.6,0.6h4.6v10.6C1524.5,210.9,1523.9,211.5,1523.1,211.5z M1523.6,198.3h-3.1v-3.1
			L1523.6,198.3z"
              />
              <g>
                <path
                  className="st7"
                  d="M1513.7,207.7c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1513.7z"
                />
              </g>
              <g>
                <path
                  className="st7"
                  d="M1513.7,204.9c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1513.7z"
                />
              </g>
              <g>
                <path
                  className="st7"
                  d="M1513.7,202c-0.3,0-0.6,0.3-0.6,0.6s0.3,0.6,0.6,0.6h8.5c0.3,0,0.6-0.3,0.6-0.6s-0.3-0.6-0.6-0.6H1513.7z"
                />
              </g>
            </g>
          </g>
        </svg>
      </div>
    </div>
  )
}

export default Roadmap
