import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"

const ContentBox = ({ html, imageData, className, logo, containImg }) => {
  /* let imgClassName =
    "absolute w-full h-80 object-cover rounded-t-lg lg:rounded-r-lg lg:rounded-t-none lg:h-full"

  if (logo) imgClassName = "w-32 m-16 lg:w-48"
  if (containImg)
    imgClassName =
      "rounded-lg w-full max-w-lg m-2 md:m-8 lg:m-4 xl:m-0 xl:rounded-none" */
  return (
    <div
      className={`shadow-component w-full rounded-lg flex flex-col-reverse lg:flex-row bg-white ${className}`}
    >
      <div className="px-4 py-2 rounded-b-lg lg:rounded-l-lg md:p-8 w-full lg:w-3/5">
        <h4
          className="text-primary2 body-content md:text-lg"
          dangerouslySetInnerHTML={{ __html: html }}
        ></h4>
      </div>

      {imageData ? (
        logo ? (
          <div className="lg:w-2/5 m-auto px-4 lg:px-0 pt-4 md:pt-8 lg:pt-0 lg:pl-4 lg:pr-12 flex items-center justify-center">
            <GatsbyImage
              image={imageData}
              className="object-contain"
              alt="logo
              "
            />
          </div>
        ) : containImg ? (
          <div className="lg:w-2/5 m-auto px-4 lg:px-0 pt-4 md:pt-8 lg:pt-4 lg:pb-4 lg:pr-4 flex items-center justify-center max-w-md">
            <GatsbyImage
              image={imageData}
              className="object-contain rounded-lg"
              alt=""
            />
          </div>
        ) : (
          <GatsbyImage
            image={imageData}
            imgStyle={!logo && { minHeight: "20rem" }}
            className="w-full bg-white max-h-80 lg:max-h-full lg:w-2/5 flex-1 relative overflow-hidden rounded-t-lg lg:rounded-t-none lg:rounded-r-lg"
            alt=""
          />
        )
      ) : (
        ""
      )}
    </div>
  )
}

export default ContentBox
