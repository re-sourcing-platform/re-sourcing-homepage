import React, { useState, useEffect } from "react"

const ScrollToTop = ({ showBelow }) => {
  const [show, setShow] = useState(showBelow ? false : true)

  const handleScroll = () => {
    if (typeof window !== `undefined`) {
      if (window.pageYOffset > showBelow) {
        if (!show) setShow(true)
      } else {
        if (show) setShow(false)
      }
    } else return
  }

  const handleClick = () => {
    if (typeof window !== `undefined`) {
      window[`scrollTo`]({ top: 0, behavior: `smooth` })
    } else return
  }

  useEffect(() => {
    if (showBelow) {
      if (typeof window !== `undefined`) {
        window.addEventListener(`scroll`, handleScroll)
        return () => window.removeEventListener(`scroll`, handleScroll)
      } else return
    }
  })

  return (
    <>
      {show && (
        <button
          onClick={handleClick}
          className="w-12 h-12 md:w-16 md:h-16 bg-primary1 rounded-full fixed bottom-16 md:bottom-5 right-5 z-30 hover:-translate-y-3 transform duration-200 shadow-md"
          aria-label="to top"
          component="span"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6 md:w-8 md:h-8 mx-auto fill-current text-white"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M7 11l5-5m0 0l5 5m-5-5v12"
            />
          </svg>
        </button>
      )}
    </>
  )
}
export default ScrollToTop
