import React, { useState, useEffect } from "react"

const Accordion = ({
  text,
  children,
  className,
  refresh,
  titleColor = "text-primary1",
  titleBg = "bg-transparent",
  clean,
}) => {
  const [open, setOpen] = useState(false)
  useEffect(() => setOpen(false), [refresh])

  return (
    <div className={`card-bg w-full shadow-component ${className}`}>
      <button
        className={`flex justify-between items-center w-full box-border px-4 py-2 md:py-5 md:px-5 ${titleBg} ${titleColor} `}
        style={{ minHeight: "60px" }}
        onClick={() => setOpen(!open)}
      >
        <div className="w-full">
          <p className={`text-left text-sm md:text-base font-bold`}>{text}</p>
        </div>

        <div className="w-4 md:w-6 text-center flex justify-center">
          <svg
            className="w-full"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            {open ? (
              <path
                fillRule="evenodd"
                d="M14.707 12.707a1 1 0 01-1.414 0L10 9.414l-3.293 3.293a1 1 0 01-1.414-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 010 1.414z"
                clipRule="evenodd"
              ></path>
            ) : (
              <path
                fillRule="evenodd"
                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            )}
          </svg>
        </div>
      </button>
      <div
        className={
          clean
            ? ""
            : `${
                open ? "" : "hidden"
              } body-content px-4 pb-4 md:text-base md:pt-2 md:pb-5 md:px-5 text-primary2`
        }
      >
        {children}
      </div>
    </div>
  )
}

export default Accordion
