import React, { useState } from "react"
import { navigate } from "gatsby"

const SearchForm = () => {
  const [formData, setFormData] = useState("")
  const handleInput = event => {
    setFormData(event.target.value)
  }

  const handleSubmit = e => {
    e.preventDefault()
    if (formData.length > 0) {
      if (window.location.pathname === "/search" && window.location.search) {
        navigate(`?q=${formData}`)
        window.location.search = `?q=${formData}`
        //window.location.reload()
      } else {
        navigate(`/search?q=${formData}`)
      }
    } else return
  }

  return (
    <form
      className="w-full flex items-stretch justify-center rounded-md overflow-hidden"
      style={{ maxWidth: "40rem" }}
    >
      <input
        type="text"
        name="google_search_site"
        className="appearance-none h-full w-full text-sm pl-2 py-1 border-2 border-white focus:outline-none focus:shadow-outline focus:border-buttonDark"
        placeholder="Search..."
        onChange={e => handleInput(e)}
      />
      <button
        className="flex items-center px-2 justify-center text-white bg-button-gradient transition duration-300"
        type="submit"
        onClick={e => handleSubmit(e)}
      >
        <svg
          className="w-4 h-4 text-white"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
        >
          <path d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
        </svg>
      </button>
    </form>
  )
}

export default SearchForm
