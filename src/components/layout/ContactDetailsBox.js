import React from "react"

const ContactDetailsBox = props => {
  let link
  if (props.type === "email") {
    link = (
      <a href="mailto:info@re-sourcing.eu" aria-label="mail us" title="Mail us">
        {props.info}
      </a>
    )
  } else if (props.type === "phone") {
    link = (
      <a href={`tel:${props.info}`} aria-label="call us" title="Call us">
        {props.info}
      </a>
    )
  } else if (props.type === "homepage") {
    link = (
      <a
        href={`https://${props.info}`}
        aria-label="homepage"
        title="homepage"
        target="blank"
      >
        {props.info}
      </a>
    )
  }

  return (
    <div className="w-full card-bg shadow-component text-center py-6">
      <svg
        className={`w-1/4 text-primary3 mx-auto mb-4`}
        fill="currentColor"
        viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg"
      >
        {props.children}
      </svg>
      <div className="uppercase text-primary1 text-3xl font-bold">
        {props.title}
      </div>
      <div className="text-primary3 text-xl mt-3 mb-5">{link}</div>
    </div>
  )
}

export default ContactDetailsBox
