import React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

const Footer = () => {
  const quickLinks = [
    { name: "Contact us", link: "/contact" },
    { name: "Partners", link: "/partners" },
    { name: "News", link: "/news" },
    { name: "Legal notice & Privacy Policy", link: "/privacy" },
  ]
  return (
    <div className="overflow-hidden -mt-24 md:-mt-12 relative z-10">
      <div
        className="bg-primary3 "
        style={{
          width: "150%",
          height: "350px",
          marginLeft: "-25%",
          borderRadius: "75%",
          marginBottom: "-150px",
        }}
      ></div>
      <footer className="bg-primary2 px-8 py-12">
        <div
          className="flex flex-col-reverse md:flex-row mx-auto"
          style={{ maxWidth: "1120px" }}
        >
          <div className="w-full text-center md:text-left lg:w-1/2 md:flex">
            <div className="px-8 pb-3 md:pl-0">
              <StaticImage
                src="../../../static/files/1200px-Flag_of_Europe.svg.png"
                alt="EU flag"
                width={110}
                layout="fixed"
                className="mx-auto"
                placeholder="dominantColor"
              />
            </div>
            <div className="px-8 md:px-0 md:w-1/3">
              <p className="text-sm text-white">
                This project has received funding from the European Union's
                Horizon 2020 research and innovation program under Grant
                Agreement No. 869276
              </p>
            </div>
          </div>
          <div className="w-full text-white text-center md:text-left lg:w-1/2 md:flex">
            <div className="md:w-1/2 md:-ml-6 md:mr-6">
              <h2 className="text-md font-bold pb-1">Quick links:</h2>
              <ul className="list-disc pl-5">
                {quickLinks.map((link, i) => {
                  return (
                    <li className="text-sm list-none" key={i}>
                      <span className="text-base -ml-5 pr-2 ">•</span>
                      <Link to={link.link}>{`${link.name}`}</Link>
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className="py-10 md:w-1/2 md:py-0">
              <h2 className="text-md font-bold pb-1">Coordinated by:</h2>
              <ul className="text-sm">
                <li>Vienna University of Economics and Business,</li>
                <li>Institute for Managing Sustainability</li>
                <li>Welthandelsplatz 1A</li>
                <li>1020 Vienna</li>
                <li>email: info@re-sourcing.eu</li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  )
}

export default Footer
