import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import ArrowButton from "../buttons/ArrowButton"
import { GatsbyImage } from "gatsby-plugin-image"

const Card = props => {
  const data = useStaticQuery(graphql`
    query cardLogoQuery {
      file(relativePath: { eq: "re-sourcing_transparent.png" }) {
        childImageSharp {
          gatsbyImageData(width: 80, placeholder: BLURRED)
        }
      }
    }
  `)

  let footer
  if (props.footer) {
    footer = (
      <div className="mt-1 flex justify-between">
        <div className="px-3 py-2 w-3/12">
          <GatsbyImage
            image={data?.file?.childImageSharp?.gatsbyImageData}
            alt="Re-sourcing logo"
          />
        </div>
        <div className="rounded-br bg-button-gradient w-2/12 flex items-center">
          <Link to={props.link}>
            <ArrowButton />
          </Link>
        </div>
      </div>
    )
  }
  return (
    <div
      className={`w-full h-full min-h-56 bg-white shadow-component rounded flex flex-col justify-between `}
    >
      <div className="w-full text-center text-primary2 font-bold pt-3 pb-1">
        {props.title || "Upcoming"}
      </div>
      <div className={`flex flex-col justify-self-end ${props.className}`}>
        {props.children}
      </div>
      {footer}
    </div>
  )
}

export default Card
