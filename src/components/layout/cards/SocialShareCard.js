import React from "react"

import {
  LinkedinShare,
  FacebookShare,
  TwitterShare,
  EmailShare,
} from "../buttons/ShareButtons"

const SocialShareCard = ({
  title,
  type = "",
  socialShareLink,
  className,

}) => {
  

  let footer = (
    <div
      className={`flex h-10 ${
        socialShareLink ? "justify-between" : "justify-end"
      }`}
    >
      {socialShareLink && (
        <div className="flex overflow-hidden">
          <LinkedinShare url={socialShareLink} />
          <FacebookShare url={socialShareLink} />
          <TwitterShare url={socialShareLink} />
          <EmailShare
            url={socialShareLink}
            subject={`Re-sourcing report ${type && ` - ${type}`}`}
            body={`Link to Re-sourcing report "${title}" ${
              type === "Video" ? "" : "[pdf]"
            }`}
            separator=" - "
          />
        </div>
      )}
    </div>
  )

  return (
    <div className={`w-full card-bg h-full ${className}`}>
      {footer}
    </div>
  )
}

export default SocialShareCard
