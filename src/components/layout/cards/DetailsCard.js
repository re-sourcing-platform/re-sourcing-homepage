import React from "react"
import ArrowButton from "../buttons/ArrowButton"
import {
  LinkedinShare,
  FacebookShare,
  TwitterShare,
  EmailShare,
} from "../buttons/ShareButtons"
import { GatsbyImage, StaticImage } from "gatsby-plugin-image"
import LinkProvider from "../../utility/LinkProvider"

const DetailsCard = ({
  title,
  subtitle,
  date,
  excerpt,
  type = "",
  socialShareLink,
  slug, // directs to a page in the system using Gatsby Link
  outsideLink, // directs to a outside page with anchor tag
  className,
  min,
  trackEventName,
  imageData,
}) => {
  const handlePlausible = () => {
    typeof window !== "undefined" &&
      window.plausible(`${trackEventName}`, {
        props: { file: `${title}` },
      })
  }

  let footer = (
    <div
      className={`flex h-10 ${
        socialShareLink ? "justify-between" : "justify-end"
      }`}
    >
      {socialShareLink && (
        <div className="flex overflow-hidden rounded-bl-md">
          <LinkedinShare url={socialShareLink} />
          <FacebookShare url={socialShareLink} />
          <TwitterShare url={socialShareLink} />
          <EmailShare
            url={socialShareLink}
            subject={`Re-sourcing Project Output ${type && ` - ${type}`}`}
            body={`Link to Re-sourcing Project Output "${title}" ${
              type === "Video" ? "" : "[pdf]"
            }`}
            separator=" - "
          />
        </div>
      )}
      <div className="rounded-br bg-button-gradient w-2/12 flex items-center">
        {outsideLink ? (
          <a href={outsideLink} target="blank" rel="noopener noreferrer">
            {trackEventName ? (
              <ArrowButton onClick={() => handlePlausible()} />
            ) : (
              <ArrowButton />
            )}
          </a>
        ) : (
          <LinkProvider to={slug}>
            <ArrowButton />
          </LinkProvider>
        )}
      </div>
    </div>
  )

  return (
    <div className={`w-full card-bg h-full ${className}`}>
      <div
        className={`w-full h-full flex flex-col justify-between pb-10 px-2 pt-2 sm:px-5 md:pt-5 -mb-10`}
      >
        {/* TOP */}
        <div className="flex">
          {/* IMAGE */}
          <div className="w-2/5 sm:pb-2 sm:pr-2 relative">
            {imageData ? (
              <div className="w-full h-full rounded-md relative">
                <LinkProvider href={outsideLink} to={slug}>
                  <GatsbyImage
                    image={imageData}
                    alt=""
                    className="object-contain rounded-md"
                  />
                </LinkProvider>
              </div>
            ) : (
              <div className="w-full h-full rounded-md relative">
                <LinkProvider href={outsideLink} to={slug}>
                  <StaticImage
                    src="../../../../static/files/re-sourcing_transparent.png"
                    alt=""
                    className="object-contain rounded-md"
                    placeholder="blurred"
                  />
                </LinkProvider>
              </div>
            )}
          </div>
          {/* TOP RIGHT */}
          <div className="w-3/5 min-h-full pl-2 pb-1 -mt-1">
            <h1 className="sm:text-xl font-bold text-primary1">
              <LinkProvider href={outsideLink} to={slug}>
                {title}
              </LinkProvider>
            </h1>
            <h2 className="text-sm sm:text-base font-bold text-primary3 pb-1">
              {subtitle}
            </h2>
            <p className="text-xs sm:text-sm text-black">{date}</p>
          </div>
        </div>
        {/* MIDDLE */}
        <div
          className={`h-full text-xs pt-2 pb-1 sm:text-sm sm:pt-6 sm:pb-3 ${
            min && "hidden md:block"
          }`}
        >
          {excerpt}
        </div>
        {/* BOTTOM */}
      </div>
      {footer}
    </div>
  )
}

export default DetailsCard
