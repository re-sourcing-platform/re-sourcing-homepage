import React from "react"
import { Link } from "gatsby"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const SectorCard = props => {
  let textColor = props.textColor
  let bgColor = "bg-white"
  if (props.active) {
    textColor = "text-white"
    bgColor = props.activeBgColor
  }
  return (
    <div className="w-full">
      <Link to={props.to}>
        <div
          className={`${bgColor}  flex flex-col items-center sm:px-4 sm:pt-2  md:py-4 h-full ${
            props.sm ? "py-4 lg:py-8" : "py-16"
          } sm:pb-0 ${props.className}`}
        >
          {/* Icon */}
          <div className="icon flex-shrink flex flex-row justify-center items-center sm:py-8 md:py-4">
            <FontAwesomeIcon
              icon={["fas", `${props.icon}`]}
              className={`${
                props.sm
                  ? "text-base sm:text-xl md:text-2xl lg:text-4xl"
                  : "text-4xl"
              } ${textColor}`}
            />
          </div>
          {/* Title */}
          <div
            className={`${textColor} text-center font-bold flex-shrink-0 pb-4 lg:text-xl hidden md:block`}
          >
            <h2>{props.title}</h2>
          </div>
        </div>
      </Link>
    </div>
  )
}

export default SectorCard
