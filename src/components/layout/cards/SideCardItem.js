import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"

const SideCardItem = ({
  className,
  title,
  author,
  date,
  outsideLink,
  slug,
  imageData,
}) => {
  const data = useStaticQuery(graphql`
    query SideCardItem {
      file(relativePath: { eq: "fafb4e3b-1743-4d4a-9fe0-1c16aee8321f.png" }) {
        childImageSharp {
          gatsbyImageData(width: 125, placeholder: BLURRED)
        }
      }
    }
  `)

  return (
    <div className={`flex ${className}`}>
      <div className="w-2/5 overflow-hidden px-2">
        <div className="pb-1/2 relative">
          <div className="h-full w-full absolute py-1">
            <GatsbyImage
              image={
                imageData
                  ? imageData
                  : data?.file?.childImageSharp?.gatsbyImageData
              }
              alt=""
              className="w-full h-full object-cover rounded-md"
            />
          </div>
        </div>
      </div>
      <div className="w-3/5 flex flex-col text-primary3">
        <div className="h-full flex flex-col justify-between">
          <div className="font-bold underline flex items-center">
            {outsideLink ? (
              <a href={outsideLink} target="blank" rel="noopener noreferrer">
                {title ? title : "Title"}
              </a>
            ) : (
              <Link to={slug}>{title ? title : "Title"}</Link>
            )}
          </div>
          <div className="text-xs flex justify-between pr-2">
            <div>{author ? author : "Project Team"}</div>
            <div>{date ? date : "Date"}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SideCardItem
