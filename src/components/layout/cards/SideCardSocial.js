import React, { useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const SideCardSocial = ({ social, round }) => {
  let icon
  let textColor
  let bgColor
  let text
  let rounded
  let link

  if (social === "twitter") {
    icon = ["fab", "twitter-square"]
    textColor = "text-twitter"
    bgColor = "bg-twitter"
    text = "Follow us on Twitter"
    link = "https://twitter.com/re_sourcing"
  } else if (social === "linkedin") {
    icon = ["fab", "linkedin"]
    bgColor = "bg-linkedin"
    textColor = "text-linkedin"
    text = "Follow us on LinkedIn"
    link = "https://www.linkedin.com/company/re-sourcing-stakeholder-platform/"
  } else if (social === "youtube") {
    icon = ["fab", "youtube-square"]
    bgColor = "bg-youtube"
    textColor = "text-youtube"
    text = "Follow us on YouTube"
    link = "https://www.youtube.com/channel/UCFrqVkfz4yHeDvP3hrgLWbA"
  }

  const [hover, setHover] = useState(false)

  if (round) rounded = "rounded-br-md"
  return (
    <>
      <div
        className={`transition duration-200 ease-in-out flex w-full h-16 ${
          hover ? bgColor : "bg-white"
        } ${rounded}`}
      >
        <div className="w-1/5 flex items-center justify-center">
          <FontAwesomeIcon
            icon={icon}
            className={`text-4xl transition duration-200 ease-in-out ${
              hover ? "text-white" : textColor
            }`}
          />
        </div>
        <div
          className={`transition duration-200 ease-in-out ${
            hover ? "text-white" : "text-primary2"
          } w-3/5 flex items-center justify-start pl-3 text-xl md:text-base xl:text-xl`}
        >
          {text}
        </div>
        <button
          className="w-1/5"
          onMouseEnter={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          aria-label={`Go to ${social}`}
        >
          <a href={link} target="blank" rel="noreferrer noopener">
            <div
              className={`transition duration-200 ease-in-out w-full h-full flex items-center justify-center ${bgColor} ${rounded}`}
            >
              <svg
                className="w-1/2 text-white"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M14 5l7 7m0 0l-7 7m7-7H3"
                ></path>
              </svg>
            </div>
          </a>
        </button>
      </div>
    </>
  )
}

export default SideCardSocial
