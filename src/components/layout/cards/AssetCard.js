import React from "react"
import ArrowButton from "../buttons/ArrowButton"
import { GatsbyImage } from "gatsby-plugin-image"

const AssetCard = ({
  title,
  imageData,
  subtitle,
  className,
  excerpt,
  outsideLink,
}) => {
  let footer = (
    <div className="flex justify-end h-10">
      <div className="rounded-br bg-button-gradient w-2/12 flex items-center">
        <a href={outsideLink} target="blank" rel="noopener noreferrer">
          <ArrowButton />
        </a>
      </div>
    </div>
  )
  return (
    <div className={`w-full card-bg shadow-component h-full ${className}`}>
      <div
        className={`w-full h-full flex flex-col justify-between pb-12 px-5 -mb-10`}
      >
        <a href={outsideLink} target="blank" rel="noopener noreferrer">
          <h1 className="text-xl font-bold text-primary1 py-4">{title}</h1>
        </a>
        <div className="h-full">
          {/* IMAGE */}
          <div className="w-2/5 pl-2 pb-2 pr-2 relative float-left">
            {imageData ? (
              <div className="pr-2">
                <a href={outsideLink} target="blank" rel="noopener noreferrer">
                  <GatsbyImage
                    className=" rounded-md"
                    image={imageData}
                    alt=""
                  ></GatsbyImage>
                </a>
              </div>
            ) : (
              <div className="pr-2 h-16 bg-white"></div>
            )}
          </div>
          {/* TOP RIGHT */}
          <div className=" min-h-full pl-2 pb-1 -mt-1">
            <h2 className="text-sm font-bold text-primary3 mb-2">{subtitle}</h2>
            <p className="text-sm text-black pb-2">{excerpt}</p>
          </div>
        </div>
      </div>
      {footer}
    </div>
  )
}

export default AssetCard
