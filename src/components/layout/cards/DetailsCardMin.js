import React from "react"
import LinkProvider from "../../utility/LinkProvider"
import ArrowButton from "../buttons/ArrowButton"
import { GatsbyImage } from "gatsby-plugin-image"

const DetailsCardMin = ({
  title,
  slug,
  href,
  imageData,
  subtitle,
  subtitle2,
  bodyText,
  className,
}) => {
  return (
    <div
      className={`w-full card-bg h-full relative shadow-component ${className}`}
    >
      <div className={`w-full h-full flex flex-col justify-between`}>
        <div className="h-full flex p-2 items-center">
          {/* IMAGE */}
          <div className="w-1/3 pb-2 pr-2 relative">
            {imageData ? (
              <GatsbyImage
                image={imageData}
                className="object-contain rounded-md"
                alt=""
              />
            ) : (
              /* TODO: add default image */
              <>
                <div className="w-full h-full bg-purple-500 rounded-md absolute"></div>
              </>
            )}
          </div>

          {/* RIGHT */}
          <div className="min-h-full pl-5 mb-5 pt-3 w-2/3">
            <h1 className="text-lg sm:text-xl font-bold text-primary1">
              {title}
            </h1>
            {subtitle && (
              <h2 className="md:text-lg font-bold text-primary3">
                {`${subtitle},`}
              </h2>
            )}
            <h2 className="md:text-lg font-bold text-primary3">{subtitle2}</h2>
            <p className="md:text-lg text-black md:mt-3 pb-2">{bodyText}</p>
          </div>
        </div>
        {/* Button */}
      </div>
      <div className="rounded-br-lg bg-button-gradient w-16 flex items-center absolute bottom-0 right-0">
        {slug ? (
          <LinkProvider to={slug} className="py-1">
            <ArrowButton />
          </LinkProvider>
        ) : (
          <LinkProvider href={href} target="blank" className="py-1">
            <ArrowButton />
          </LinkProvider>
        )}
      </div>
    </div>
  )
}
export default DetailsCardMin
