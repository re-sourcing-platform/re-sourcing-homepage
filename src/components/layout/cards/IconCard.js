import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const IconCard = props => {
  return (
    <div className={`${props.className}`}>
      <div
        className={`bg-white shadow-component flex sm:flex-col items-center rounded-lg h-full`}
      >
        {/* Icon */}
        <div className="icon flex-shrink flex flex-row justify-center w-1/4 sm:pt-8 md:pt-8 lg:pb-2 sm:w-full">
          <FontAwesomeIcon
            icon={["fas", `${props.icon}`]}
            className="text-4xl text-primary1"
          />
        </div>
        <div
          className="w-3/4 sm:w-auto pr-2 pt-4 pb-4 sm:px-4 sm:pb-6 sm:pt-6 md:px-3 lg:px-6"
          style={{ minWidth: "50px" }}
        >
          {/* Title */}
          <div className="text-primary1 font-bold flex-shrink-0 pb-4 lg:pb-6 text-lg sm:text-xl text-center">
            <h2>{props.title}</h2>
          </div>
          {/* Body */}
          <div className="text-primary3 text-center text-xs sm:text-sm md:text-base lg:text-lg">
            <p>{props.body}</p>
            {props.children}
          </div>
        </div>
      </div>
    </div>
  )
}

export default IconCard
