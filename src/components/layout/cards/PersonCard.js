import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const PersonCard = props => {
  const name = props.name || "Name"
  let image

  if (props.imageData) {
    image = (
      <GatsbyImage
        image={props.imageData}
        alt="Headshot"
        className="w-full h-full object-cover rounded-t-lg"
      />
    )
  } else {
    image = (
      <img
        src={`./files/${props.src}`}
        alt="Headshot"
        className="w-full h-full object-cover rounded-t-lg"
      />
    )
  }

  return (
    <>
      <div className={`${props.className} card-bg shadow-xl relative`}>
        <div className="overflow-hidden">
          {/* Image */}
          <div className="pb-6/7 relative">
            <div className="h-full w-full absolute">{image}</div>
          </div>
        </div>
        {/* Title */}
        {props.linkedIn ? (
          <a href={props.linkedIn} rel="noopener noreferrer" target="blank">
            <div className="text-white bg-primary2 w-full text-center ">
              <h2 className="font-bold py-1 text-xl">{name}</h2>
            </div>
          </a>
        ) : (
          <div className="text-white bg-primary2 w-full text-center">
            <h2 className="font-bold py-1 text-xl">{name}</h2>
          </div>
        )}
        {/* Body */}
        <div className="px-8 py-8 h-full">{props.children}</div>
        {props.linkedIn && (
          <a href={props.linkedIn} rel="noopener noreferrer" target="blank">
            <FontAwesomeIcon
              icon={["fab", "linkedin"]}
              className={`text-2xl ease-in-out absolute right-2 bottom-1 text-linkedin`}
            />
          </a>
        )}
      </div>
    </>
  )
}

export default PersonCard
