import React from "react"
import { Link } from "gatsby"
import Tag from "../Tag"
import { GatsbyImage } from "gatsby-plugin-image"

const EventsCard = ({
  title,
  slug,
  imageData,
  date_start,
  date_end,
  registerLink,
  subtitle1,
  subtitle2,
  className,
  location,
}) => {
  let footer = (
    <div className="flex justify-between h-10 rounded-b-lg">
      {registerLink && (
        <div className="w-full rounded-bl-lg bg-button-gradient">
          <a href={registerLink} target="blank" rel="noopener noreferrer">
            <div className="w-full h-full flex justify-center items-center text-white">
              REGISTER
            </div>
          </a>
        </div>
      )}
      <div
        className={`w-full border-2 border-buttonLight ${
          registerLink ? "rounded-br-lg" : "rounded-b-md"
        }`}
      >
        <Link to={slug}>
          <div className="w-full h-full flex justify-center items-center text-buttonLight font-bold">
            DETAILS
          </div>
        </Link>
      </div>
    </div>
  )

  return (
    <div className={`w-full card-bg shadow-component h-full ${className}`}>
      <div
        className={`w-full h-full flex flex-col justify-between pb-12 px-5 -mb-10`}
      >
        <h1 className="text-xl font-bold text-primary1 py-4">{title}</h1>
        <div className="h-full flex overflow-hidden">
          {/* IMAGE */}
          <div className="w-2/5 pb-2 pr-2 overflow-hidden relative h-40">
            {imageData ? (
              <GatsbyImage
                className="rounded-md"
                image={imageData}
                alt=""
              ></GatsbyImage>
            ) : (
              <>
                <div className="w-full h-full bg-purple-500 rounded-md absolute"></div>
              </>
            )}
          </div>
          {/* TOP RIGHT */}
          <div className="w-3/5 min-h-full pl-2 pb-1 -mt-1">
            <h2 className="text-sm font-bold text-primary3 mb-2">
              {subtitle1}
            </h2>
            <h2 className="text-sm font-bold text-primary3 mb-2">
              {subtitle2}
            </h2>
            <div className="flex flex-wrap text-sm text-black font-bold pb-2">
              <p className="">{date_start}</p>

              {date_end && date_end !== date_start && (
                <>
                  <p className="px-1"> - </p>
                  <p className="">{date_end}</p>
                </>
              )}
            </div>

            <Tag icon="map" text={location} />
          </div>
        </div>
      </div>
      {footer}
    </div>
  )
}

export default EventsCard
