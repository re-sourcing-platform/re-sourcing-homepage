import React from "react"

const Tag = ({ icon, text }) => {
  const iconPath = chooseIcon(icon)

  return (
    <div className="inline-block">
      <div
        className="flex justify-start items-center pl-2 pr-3 rounded-full bg-primary3 text-white text-xs"
        style={{ paddingTop: "2px", paddingBottom: "2px" }}
      >
        <svg
          className="w-4 h-4 mr-2 "
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          {iconPath}
        </svg>

        <p className="font-bold">{text}</p>
      </div>
    </div>
  )
}

export default Tag

const chooseIcon = icon => {
  let iconPath
  if (icon === "map") {
    iconPath = (
      <path
        fillRule="evenodd"
        d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
        clipRule="evenodd"
      ></path>
    )
  } else if (icon === "time") {
    iconPath = (
      <path
        fillRule="evenodd"
        d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
        clipRule="evenodd"
      ></path>
    )
  }
  return iconPath
}
