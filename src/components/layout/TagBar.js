import React, { useState } from "react"

const TagBar = ({
  navOptions = ["1", " 2", " 3"],
  onChange = () => console.log(`change`),
  defaultActive = 0,
  className,
}) => {
  const [navOptionActive, setNavOptionActive] = useState(defaultActive)

  const handleClick = e => {
    setNavOptionActive(e)
    onChange(e)
  }

  return (
    <div className={className}>
      <div
        className={`h-px w-1/3 m-auto bg-gradient-to-r from-white via-primary2 to-white opacity-50`}
      />
      <div className={`flex flex-wrap justify-center items-center my-2 `}>
        {navOptions.map((option, i) => (
          <button
            key={i}
            data-text={option}
            className={`flex flex-col justify-between items-center text-xs px-2 m-1 border-primary2 rounded-lg shadow pseudo-bold transition duration-200 ease-in-out md:py-2 md:px-3 md:text-sm border hover:text-white hover:bg-primary2 hover:font-bold hover:shadow-md ${
              navOptionActive === i
                ? "text-white bg-primary2 font-bold"
                : "text-primary2 white"
            }`}
            onClick={() => handleClick(i)}
          >
            {option}
          </button>
        ))}
      </div>
      <div
        className={`h-px w-1/3 mx-auto bg-gradient-to-r from-white via-primary2 to-white opacity-50`}
      />
    </div>
  )
}

export default TagBar
