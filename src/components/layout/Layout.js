import React from "react"
import Head from "../utility/Head"
import NavBar from "./NavBar"
import Footer from "./Footer"
import { library } from "@fortawesome/fontawesome-svg-core"
import { fas } from "@fortawesome/free-solid-svg-icons"
import {
  faTwitterSquare,
  faLinkedin,
  faYoutubeSquare,
} from "@fortawesome/free-brands-svg-icons"
import useSiteMetadata from "../SiteMetadata"

library.add(fas, faTwitterSquare, faLinkedin, faYoutubeSquare)

const Layout = ({ children, pageName, location, metaDescription }) => {
  const seo = useSiteMetadata()
  const { title, siteUrl } = seo
  const description = metaDescription || seo.description
  return (
    <>
      <Head
        pageName={pageName}
        link={location?.href}
        {...{ title, description, siteUrl }}
      />
      <div className="flex flex-col justify-between min-h-screen bg-body">
        <div className="font-body">
          <NavBar />
          <div className="mx-auto mt-100px">{children}</div>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default Layout
