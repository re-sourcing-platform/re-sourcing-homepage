import React from "react"
import GradientButton from "../../utility/GradientButton"
import TitleBox from "./TitleBox"
import { GatsbyImage } from "gatsby-plugin-image"

const PageHeaderNews = ({
  title,
  subtitle,
  imageData,
  link,
  author,
  date,
  buttonLink,
}) => {
  const day = date.split(" ")[0]
  const month = date.split(" ")[1]
  const year = date.split(" ")[2]

  const HeaderDateFormatted = ({ day, month, year }) => (
    <div className="text-right">
      <h1 className="text-4xl text-white font-bold -mb-3">{day}</h1>
      <h2 className="text-2xl text-white lowercase -mb-2">{month}</h2>
      <h3 className="text-md text-white tracking-wider">{year}</h3>
    </div>
  )

  return (
    <div>
      <div
        className="w-full flex items-end justify-center hero-bg-gradient relative overflow-hidden"
        style={{ height: "24rem" }}
      >
        <div className="w-full h-full z-30 flex flex-col md:flex-row md:h-64 lg:container lg:px-32 lg:mx-auto ">
          {/* TITLE BOX */}
          <TitleBox link={link} title={title} subtitle={subtitle} />
          {/* DETAILS BOX */}
          <div className="w-full flex flex-col md:w-1/4">
            <div
              className="h-full flex md:flex-col xl:flex-row"
              style={{ flex: 5 }}
            >
              <div
                className="justify-end pr-4 pb-3 w-full md:pr-8 md:py-4 xl:w-2/3 xl:pt-8 flex "
                style={{ background: "#CEDEE4" }}
              >
                <HeaderDateFormatted day={day} month={month} year={year} />
              </div>
              <div
                className={`
                text-right flex w-full h-full justify-end pt-3 pr-4
                md:items-center md:justify-end md:px-8 md:pt-0
                xl:block xl:w-1/3 xl:pr-4 xl:pl-0 xl:pt-12 
                `}
                style={{ background: "#90B4C2" }}
              >
                <h3 className="text-sm text-white pr-1 xl:pr-0">By</h3>
                <h3 className="text-sm text-white font-bold">{author}</h3>
              </div>
            </div>
            <div className="h-full" style={{ flex: 2 }}>
              <GradientButton full className="rounded-0" to={buttonLink}>
                BACK TO NEWS
              </GradientButton>
            </div>
          </div>
        </div>
        {/* BOX LEFT BG */}
        <div className="bg-white absolute bottom-0 left-0 w-1/2 h-64 z-20"></div>
        {/* HEADER BG */}

        {imageData ? (
          <>
            <div className="absolute w-full h-full opacity-25">
              <GatsbyImage
                image={imageData}
                className="object-cover"
                style={{ marginTop: "-25%" }}
                alt="heroImage"
              ></GatsbyImage>
            </div>
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  )
}

export default PageHeaderNews
