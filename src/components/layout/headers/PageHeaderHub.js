import React, { useState, useEffect } from "react"
import {
  LinkedinShare,
  FacebookShare,
  TwitterShare,
  EmailShare,
} from "../buttons/ShareButtons"

import { Link } from "gatsby"
import { StaticImage, GatsbyImage } from "gatsby-plugin-image"
import { isFutureEvent, getTimeRemaining } from "../../utility/timeMethods"
import GradientButton from "../../utility/GradientButton";
import useSiteMetadata from "../../SiteMetadata";

const PageHeaderHub = ({
  className,
  title,
  subtitle,
  link,
  registerLink,
  date,
  imageData,
  eventStart = "",
}) => {
  let navStyle = "z-30 absolute top-3 right-3 md:top-5 md:right-10"
  const _isFutureEvent = eventStart !== null && isFutureEvent(new Date(eventStart));
  const { siteUrl } = useSiteMetadata();

  return (
    <div className={`relative w-full text-center ${className}`}>
      <Link
        to="/"
        className="z-30 absolute top-3 left-3 md:top-4 md:left-10 w-24 md:w-32"
      >
        <StaticImage
          src="../../../../static/files/re-sourcing_transparent.png"
          alt="Re-sourcing logo"
          placeholder="tracedSVG"
          imgClassName="filter contrast-200 brightness-0 invert"
        />
      </Link>

      <ShareBox
        className={navStyle}
        iconFillColor="white"
        bgStyle={{ fill: "none" }}
        size={30}
        {...{ title, link, subtitle }}
      />

      <BgWave imageData={imageData ? imageData : null} />

      <div className="container mx-auto pt-14 sm:pt-16 md:pt-20 lg:pt-12 z-20 relative lg:max-w-4xl px-2 mb-8">
        <h1
          className={`text-white text-sm sm:text-base md:text-xl lg:text-2xl ${
            !subtitle && "pt-2"
          }`}
        >
          {title}
        </h1>
        {subtitle && (
          <h2 className="text-white text-xl sm:text-2xl mt-2 md:text-3xl inline-block lg:text-5xl font-bold lg:leading-tight">
            {subtitle}
          </h2>
        )}
        {date && (
          <h2 className=" text-white text-base mt-2 md:text-xl font-bold">
            {date}
          </h2>
        )}
        {_isFutureEvent && (
          <CountdownTimer
            eventStart={eventStart}
            className="text-white mt-2 mx-auto"
          />
        )}

        {/* BUTTONS */}
        {_isFutureEvent && (
            <div
                className="w-full mx-auto flex justify-center gap-4 mt-3 mb-2 md:mt-4 md:mb-4"
                style={{ maxWidth: "435px" }}
            >
              <div className="w-full">
                <GradientButton
                    href={registerLink ? registerLink : null}
                    className="rounded-sm text-xs"
                    full
                >
                  Register
                </GradientButton>
              </div>
              <div className="w-full">
                <GradientButton
                    href={`${siteUrl}/files/RE-SOURCING_Virtual_Conference_2022_-_Reality_Check_on_Responsible_Sourcing.ics`}
                    className="rounded-sm text-xs"
                    full
                >
                  Save the date
                </GradientButton>
              </div>
            </div>
        )}
      </div>
    </div>
  )
}

export default PageHeaderHub

export const ShareBox = ({
  className,
  link,
  iconFillColor,
  bgStyle,
  size,
  title,
  subtitle,
}) => {
  const [isMobile, setIsMobile] = useState(
    typeof window !== `undefined` && window.innerWidth < 768 ? true : false
  )
  const [isOpen, setIsOpen] = useState(false)

  //choose the screen size
  const handleResize = () => {
    if (typeof window !== `undefined`) {
      if (window.innerWidth < 768) {
        setIsMobile(true)
      } else {
        setIsMobile(false)
      }
    }
  }

  // create an event listener

  useEffect(() => {
    if (typeof window !== `undefined`)
      window.addEventListener("resize", handleResize)
  })

  const ShareIcon = () => (
    <div
      style={{ width: "30px", height: "30px" }}
      className="flex items-center justify-center"
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="h-4 w-4 m-1 text-white"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"
        />
      </svg>
    </div>
  )

  return (
    <div
      className={`border-2 border-primary4 rounded-md flex items-center ${className}`}
    >
      {isMobile ? (
        !isOpen ? (
          <button onClick={() => setIsOpen(!isOpen)}>
            <ShareIcon />
          </button>
        ) : (
          <>
            <LinkedinShare
              url={link}
              {...{ iconFillColor, bgStyle, size }}
            ></LinkedinShare>
            <FacebookShare
              url={link}
              {...{ iconFillColor, bgStyle, size }}
            ></FacebookShare>
            <TwitterShare
              url={link}
              {...{ iconFillColor, bgStyle, size }}
            ></TwitterShare>
            <EmailShare
              url={link}
              {...{ iconFillColor, bgStyle, size }}
            ></EmailShare>
            <button onClick={() => setIsOpen(!isOpen)}>
              <ShareIcon />
            </button>
          </>
        )
      ) : (
        <>
          <p className="text-xs text-white pl-2 pr-4">SHARE:</p>
          <LinkedinShare
            url={link}
            {...{ iconFillColor, bgStyle, size }}
          ></LinkedinShare>
          <FacebookShare
            url={link}
            {...{ iconFillColor, bgStyle, size }}
          ></FacebookShare>
          <TwitterShare
            url={link}
            {...{ iconFillColor, bgStyle, size }}
          ></TwitterShare>
          <EmailShare
            url={link}
            subject={`Re-sourcing Project Event | ${title} - ${subtitle}`}
            body={`Link to Re-sourcing Project Event - "${title} - ${subtitle}" `}
            separator=" - "
            {...{ iconFillColor, bgStyle, size }}
          ></EmailShare>
        </>
      )}
    </div>
  )
}

export const BgWave = ({ imageData }) => (
  <>
    <div className="absolute w-full bg-cover">
      <div className="relative">
        {imageData ? (
          <GatsbyImage
            image={imageData}
            alt="hero image"
            className="absolute top-0 left-0 h-80 md:h-128 xl:w-full z-10 bg-cover"
          />
        ) : (
          ""
        )}

        <svg
          viewBox="0 0 1000 300"
          preserveAspectRatio="none"
          className="absolute top-0 left-0 h-80 md:h-128 xl:w-full z-10 bg-cover"
        >
          <defs>
            <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop
                offset="0%"
                style={{ stopColor: "#397992", stopOpacity: "1" }}
              />
              <stop
                offset="100%"
                style={{ stopColor: "#7CB0BD", stopOpacity: "1" }}
              />
            </linearGradient>
          </defs>
          <path
            d="M0,237.74C433.66,439,608.6,70,1000,271.26V0H0Z"
            style={{
              stroke: "none",
              fill: "url(#grad1)",
              opacity: "70%",
            }}
          />
          <g>
            <path
              d="M0,237.74C99.23,283.8,184.92,300,262.75,300H0Z"
              style={{ stroke: "#FDFDFD", strokeWidth: "2", fill: "#FDFDFD" }}
            />
            <path
              d="M1000,271.26V300H262.75C525.06,300,698.16,116,1000,271.26Z"
              style={{ stroke: "#FDFDFD", strokeWidth: "2", fill: "#FDFDFD" }}
            />
          </g>
        </svg>
      </div>
    </div>
  </>
)

export const BackToEvent = ({ className }) => {
  let size = 5
  return (
    <Link to="/hub/2nd-virtual-conference/" className={className}>
      <div className="flex m-2">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className={`h-${size} w-${size} text-white`}
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M7 16l-4-4m0 0l4-4m-4 4h18"
          />
        </svg>
        <p className="text-xs text-white pl-2 pr-4">BACK</p>
      </div>
    </Link>
  )
}

export const CountdownTimer = ({ eventStart, className }) => {
  let eventStartDateObject = new Date(eventStart)
  const showCountdown = () => {
    return isFutureEvent(eventStartDateObject)
  }
  const [timeRemaining, setTimeRemaining] = useState();

  useEffect(() => {
    if (!showCountdown()) return
    const timerId = setInterval(
      () => setTimeRemaining(getTimeRemaining(eventStartDateObject)),
      1000
    )
    return () => clearInterval(timerId)
  })

  return (
    <>
      {timeRemaining !== undefined && timeRemaining.time > 0 ? (
        <div
          className={`w-full flex justify-around gap-4 ${className}`}
          style={{ maxWidth: "435px" }}
        >
          {[
            {
              amount: timeRemaining.days,
              unit: "Days",
            },
            {
              amount: timeRemaining.hours,
              unit: "Hours",
            },
            {
              amount: timeRemaining.mins,
              unit: "Minutes",
            },
            {
              amount: timeRemaining.seconds,
              unit: "Seconds",
            },
          ].map((value, i) => (
            <div className="text-center" key={i}>
              <h2 className="text-xl md:text-3xl">
                {value.amount < 10 ? `0${value.amount}` : value.amount}
              </h2>
              <h3 className="text-xs md:text-sm md:font-bold">{value.unit}</h3>
            </div>
          ))}
        </div>
      ) : (
        ""
      )}
    </>
  )
}
