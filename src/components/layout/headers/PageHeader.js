import React from "react"

const PageHeader = ({ title, subtitle }) => {
  return (
    <div>
      <div
        className={`w-full bg-primary4 hero-bg-gradient text-center pt-12 h-48 md:pt-24 md:h-80`}
      >
        <div>
          <h1
            className={`uppercase text-white text-3xl md:text-6xl font-bold ${
              !subtitle && "pt-4"
            }`}
          >
            {title}
          </h1>
        </div>
        {subtitle ? (
          <div>
            <h2 className=" text-white text-lg md:text-2xl md:font-bold">
              {subtitle}
            </h2>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  )
}

export default PageHeader
