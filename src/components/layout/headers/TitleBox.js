import React from "react"
import Tag from "../Tag"
import {
  LinkedinShare,
  FacebookShare,
  TwitterShare,
} from "../buttons/ShareButtons"

const TitleBox = ({ title, subtitle, link, location }) => {
  return (
    <div className="w-full md:w-3/4 h-full bg-white flex flex-col justify-between pl-8 lg:pl-0">
      <div className="pr-4 flex-grow flex items-center">
        <div>
          <h1 className="text-primary1 font-bold text-3xl md:text-4xl xl:text-5xl">
            {title}
          </h1>
          <h2 className="text-primary3 font-bold text-xl xl:text-2xl">
            {subtitle}
          </h2>
        </div>
      </div>
      <div>
        {location ? <Tag icon="map" text={location} /> : ""}
        <div className="flex mt-4">
          <LinkedinShare url={link} />
          <FacebookShare url={link} />
          <TwitterShare url={link} />
        </div>
      </div>
    </div>
  )
}

export default TitleBox
