import React from "react"
import GradientButton from "../../utility/GradientButton"
import TitleBox from "./TitleBox"
import { GatsbyImage } from "gatsby-plugin-image"

const PageHeaderEvent = ({
  title,
  subtitle,
  imageData,
  link,
  date_start,
  date_end,
  month_only,
  buttonLink,
  time_start,
  time_end,
  location,
}) => {
  const dayStart = date_start.split(" ")[0]
  const monthStart = date_start.split(" ")[1]
  const yearStart = date_start.split(" ")[2]

  const dayEnd = date_end?.split(" ")[0]
  const monthEnd = date_end?.split(" ")[1]
  const yearEnd = date_end?.split(" ")[2]

  const HeaderDateFormatted = ({ day, month, year }) => {
    return (
      <div className="text-right">
        {day && <h1 className="text-4xl text-white font-bold -mb-3">{day}</h1>}
        <h2 className="text-2xl text-white lowercase -mb-2">{month}</h2>
        <h3 className="text-md text-white tracking-wider">{year}</h3>
      </div>
    )
  }

  const HeaderDateSection = () => {
    if (!month_only && date_end && date_start !== date_end) {
      return (
        <>
          <HeaderDateFormatted
            day={month_only ? null : dayStart}
            month={monthStart}
            year={yearStart}
          />
          <div className="px-2 pt-5">
            <svg
              className="w-4 h-4"
              fill="none"
              stroke="white"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="3"
                d="M17 8l4 4m0 0l-4 4m4-4H3"
              ></path>
            </svg>
          </div>
          <HeaderDateFormatted day={dayEnd} month={monthEnd} year={yearEnd} />
        </>
      )
    } else {
      return (
        <HeaderDateFormatted
          day={month_only ? null : dayStart}
          month={monthStart}
          year={yearStart}
        />
      )
    }
  }

  return (
    <div>
      <div
        className="w-full flex items-end justify-center hero-bg-gradient relative overflow-hidden"
        style={{ height: "24rem" }}
      >
        <div className="w-full h-full z-30 flex flex-col md:flex-row md:h-64 lg:container lg:px-32 lg:mx-auto">
          <TitleBox
            link={link}
            title={title}
            subtitle={subtitle}
            location={location}
          />
          {/* DETAILS BOX */}
          <div className="w-full flex flex-col md:w-1/4">
            <div
              className="h-full flex md:flex-col xl:flex-row"
              style={{ flex: 5 }}
            >
              <div
                className="justify-end pr-4 pb-3 w-full md:pr-8 md:py-4 xl:w-2/3 xl:pt-8 flex "
                style={{ background: "#CEDEE4" }}
              >
                <HeaderDateSection />
              </div>
              <div
                className={`
                text-right justify-around pt-3 pl-4 pr-2
                flex w-full h-full md:items-center md:justify-between md:px-8 
                xl:block xl:w-1/3 xl:pr-4 xl:pl-0 xl:pt-12 
                `}
                style={{ background: "#90B4C2" }}
              >
                {!month_only && time_end && (
                  <>
                    <div>
                      <h3 className="text-xs text-white xl:-mt-1">FROM</h3>
                      <h3 className="text-sm text-white font-bold">
                        {time_start}{" "}
                        <span className="text-sm text-white">CET</span>
                      </h3>
                    </div>
                    <div>
                      <h3 className="text-xs text-white">TO</h3>
                      <h3 className="text-sm text-white font-bold">
                        {time_end}{" "}
                        <span className="text-sm text-white ">CET</span>
                      </h3>
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className="h-full" style={{ flex: 2 }}>
              {buttonLink ? (
                <GradientButton full className="rounded-0" href={buttonLink}>
                  REGISTER
                </GradientButton>
              ) : (
                <GradientButton full className="rounded-0" to="/events">
                  BACK TO EVENTS
                </GradientButton>
              )}
            </div>
          </div>
        </div>
        {/* BOX LEFT BG */}
        <div className="bg-white absolute bottom-0 left-0 w-1/2 h-64 z-20"></div>
        {/* HEADER BG */}

        {imageData ? (
          <>
            <div className="absolute w-full h-full opacity-25">
              <GatsbyImage
                image={imageData}
                className="object-cover"
                alt="heroImage"
              ></GatsbyImage>
            </div>
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  )
}

export default PageHeaderEvent
