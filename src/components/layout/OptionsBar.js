import React, { useState } from "react"

const OptionsBar = ({
  navOptions = ["1", " 2", " 3"],
  onChange = () => console.log(""),
  defaultActive = 0,
  className,
}) => {
  const [navOptionActive, setNavOptionActive] = useState(defaultActive)
  const [navOptionFocused, setNavOptionFocused] = useState(null)

  const handleClick = e => {
    setNavOptionActive(e)
    onChange(e)
  }

  return (
    <div
      className={`bg-white md:rounded-md shadow-lg flex mb-8 h-16 ${className}`}
    >
      {navOptions.map((option, i) => (
        <button
          key={i}
          className="w-full h-full flex items-center justify-center"
          onClick={() => handleClick(i)}
          onMouseEnter={() => setNavOptionFocused(i)}
          onMouseLeave={() => setNavOptionFocused(null)}
        >
          <div
            className={`relative h-full ${
              navOptionFocused === i ? "text-primary4" : "text-primary3"
            }`}
          >
            <div
              className={`font-bold uppercase text-sm md:text-base h-full flex items-center
			
			${navOptionActive === i ? "text-primary3" : ""}
			`}
            >
              {option}
            </div>
            {navOptionActive === i ? (
              <div className="absolute h-1 -mt-1 w-full bg-primary3"></div>
            ) : (
              ""
            )}
          </div>
        </button>
      ))}
    </div>
  )
}

export default OptionsBar
