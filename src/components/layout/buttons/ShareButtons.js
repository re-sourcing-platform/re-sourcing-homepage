import React from "react"
import {
  FacebookShareButton,
  FacebookIcon,
  EmailShareButton,
  EmailIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon,
} from "react-share"

export const EmailShare = ({
  url,
  subject,
  body,
  separator,
  round = false,
  size = 40,
  iconFillColor,
  bgStyle,
}) => {
  return (
    <EmailShareButton {...{ url, subject, body, separator }}>
      <EmailIcon {...{ size, round, iconFillColor, bgStyle }} />
    </EmailShareButton>
  )
}

export const FacebookShare = ({
  url,
  hashtag,
  size = 40,
  round = false,
  iconFillColor,
  bgStyle,
}) => {
  return (
    <FacebookShareButton {...{ url, hashtag }}>
      <FacebookIcon {...{ size, round, iconFillColor, bgStyle }} />
    </FacebookShareButton>
  )
}

export const TwitterShare = ({
  url,
  title,
  hashtags,
  size = 40,
  round = false,
  iconFillColor,
  bgStyle,
}) => {
  return (
    <TwitterShareButton {...{ url, title, hashtags }}>
      <TwitterIcon {...{ size, round, iconFillColor, bgStyle }} />
    </TwitterShareButton>
  )
}

export const LinkedinShare = ({
  url,
  size = 40,
  round = false,
  iconFillColor,
  bgStyle,
}) => {
  return (
    <LinkedinShareButton {...{ url }}>
      <LinkedinIcon {...{ size, round, iconFillColor, bgStyle }} />
    </LinkedinShareButton>
  )
}
