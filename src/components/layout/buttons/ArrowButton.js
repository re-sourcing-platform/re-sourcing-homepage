import React from "react"

const ArrowButton = ({ icon = "right", onClick }) => {
  let path

  if (icon === "right") path = "M14 5l7 7m0 0l-7 7m7-7H3"
  if (icon === "down") path = "M19 14l-7 7m0 0l-7-7m7 7V3"

  return (
    <button className="flex justify-center items-center" onClick={onClick}>
      <svg
        className="w-2/5 text-white"
        fill="none"
        stroke="currentColor"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d={path}
        ></path>
      </svg>
    </button>
  )
}

export default ArrowButton
