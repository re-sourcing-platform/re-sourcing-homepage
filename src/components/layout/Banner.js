import React from "react"
import GradientButton from "../utility/GradientButton"

const Banner = ({
  className,
  textTop,
  textBottom,
  primaryButtonText,
  primaryButtonLink,
  secondaryButtonText,
  secondaryButtonLink,
  bgColor,
}) => {
  return (
    <div className={`w-full relative ${bgColor}`}>
      <div className={`container mx-auto ${className} z-20 relative`}>
        <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
          <h2 className="text-2xl font-bold tracking-tight text-white sm:text-3xl md:text-4xl">
            <span className="lg:block" dangerouslySetInnerHTML={{__html: textTop }}></span>
            <span className="lg:block" dangerouslySetInnerHTML={{ __html: textBottom }}></span>
          </h2>
          <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
            <div className="hidden lg:inline-flex gap-6">
              <a href={primaryButtonLink}>
                <GradientButton className="rounded-md shadow-md" lg>
                  {primaryButtonText}
                </GradientButton>
              </a>
              {secondaryButtonText && secondaryButtonLink && (
                <a href={secondaryButtonLink}>
                  <GradientButton className="rounded-md shadow-md" lg>
                    {secondaryButtonText}
                  </GradientButton>
                </a>
              )}
            </div>
            <div className="lg:hidden flex gap-6">
              <a href={primaryButtonLink}>
                <GradientButton className="rounded-md shadow-md">
                  {primaryButtonText}
                </GradientButton>
              </a>
              {secondaryButtonText && secondaryButtonLink && (
                <a href={secondaryButtonLink}>
                  <GradientButton className="rounded-md shadow-md">
                    {secondaryButtonText}
                  </GradientButton>
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="absolute w-full h-full opacity-75 top-0 left-0">
        <svg
          id="patternId"
          width="100%"
          height="100%"
          xmlns="http://www.w3.org/2000/svg"
        >
          <defs>
            <pattern
              id="a"
              patternUnits="userSpaceOnUse"
              width="32"
              height="32"
              patternTransform="scale(2) rotate(0)"
            >
              <path
                d="M40 16h-6m-4 0h-6m8 8v-6m0-4V8M8 16H2m-4 0h-6m8 8v-6m0-4V8"
                strokeLinecap="square"
                strokeWidth="1"
                stroke="hsla(339, 63%, 41%, 1)"
                fill="none"
              />
              <path
                d="M16-8v6m0 4v6m8-8h-6m-4 0H8m8 24v6m0 4v6m8-8h-6m-4 0H8"
                strokeLinecap="square"
                strokeWidth="1"
                stroke="hsla(4, 70%, 33%, 1)"
                fill="none"
              />
            </pattern>
          </defs>
          <rect
            width="800%"
            height="800%"
            transform="translate(0,0)"
            fill="url(#a)"
          />
        </svg>
      </div>
    </div>
  )
}

export default Banner
