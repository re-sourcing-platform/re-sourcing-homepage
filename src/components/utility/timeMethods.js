export const isFutureEvent = eventStartDateObject => {
  let now = Date.now()
  return eventStartDateObject > now
}

export const getTimeRemaining = eventStartDateObject => {
  let now = Date.now()
  let time = eventStartDateObject.getTime() - now
  let second = 1000
  let minute = 1000 * 60
  let hour = minute * 60
  let day = hour * 24
  let days = Math.floor(time / day)
  let hours = Math.floor((time % day) / hour)
  let mins = Math.floor((time % hour) / minute)
  let seconds = Math.floor((time % minute) / second)
  return { days, hours, mins, seconds, time }
}
