export const getAssetTagList = assets => {
  let tagList = ["All"]

  // get all used tags
  if (assets) {
    assets.map((asset, i) => {
      const { tags } = asset
      if (tags) {
        return tags.map((tag, i) => {
          let tagTitle = tag.title.trim()
          if (!tagList.includes(tagTitle)) {
            return tagList.push(tagTitle)
          } else return ""
        })
      } else return ""
    })
  }

  return tagList
}

export const filterAssets = (assetsArray, filtersArray, filterNum) => {
  if (filterNum === 0) return assetsArray

  const result = assetsArray.filter(asset => {
    const { tags } = asset
    let assetTagList = []
    if (tags)
      tags.forEach(tag => assetTagList.push((tag.title = tag.title.trim())))
    return assetTagList.includes(filtersArray[filterNum])
  })
  return result
}
