import React from "react"
import { navigate } from "gatsby"

const GradientButton = ({
  href,
  to,
  children,
  className,
  full,
  lg,
  disabled,
}) => {
  let variant = ""

  if (full) {
    variant += " w-full h-full"
  }

  let size = "py-2 px-6"
  if (lg) variant = "text-lg py-4 px-16"

  let activeClassNames = ""
  let disabledClassNames = "opacity-50 cursor-default focus:outline-none"

  let classNames = `bg-button-gradient transition duration-300 ease-in-out text-white uppercase ${className} ${variant} ${size} ${
    !disabled ? activeClassNames : disabledClassNames
  }`

  let button = (
    <button
      className={classNames}
      onClick={e => {
        if (to) navigate(`${to}`)
      }}
    >
      {children}
    </button>
  )

  if (href) {
    button = (
      <a href={href} target="blank" rel="noopener noreferrer">
        <button className={classNames}>{children}</button>
      </a>
    )
  }

  return button
}

export default GradientButton
