import { useLayoutEffect, useState } from "react"

const useHasMounted = () => {
  const [hasMounted, setHasMounted] = useState(false)
  useLayoutEffect(() => {
    setHasMounted(true)
  }, [])
  return hasMounted
}

export default useHasMounted
