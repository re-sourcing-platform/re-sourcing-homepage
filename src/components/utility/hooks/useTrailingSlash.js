export const useWithTrailingSlash = string => {
  if (typeof string !== "string") return
  if (string.endsWith("/")) return string

  return `${string}/`
}

export const useWithoutTrailingSlash = string => {
  if (typeof string !== "string") return
  if (string.endsWith("/")) return string.slice(0, -1)

  return string
}
