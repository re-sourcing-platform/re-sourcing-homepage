import React from "react"
import { Helmet } from "react-helmet"
import { withPrefix } from "gatsby"
import { useWithTrailingSlash } from "./hooks/useTrailingSlash"

const Head = ({ pageName, title, description, link, siteUrl }) => {
  return (
    <Helmet>
      <html lang="en" />
      <title>{pageName ? `${pageName} | ${title}` : `${title}`}</title>
      <meta name="description" content={description} />

      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href={`${withPrefix("/")}files/apple-touch-icon.png`}
      />
      <link
        rel="icon"
        type="image/png"
        href={`${withPrefix("/")}files/favicon-32x32.png`}
        sizes="32x32"
      />
      <link
        rel="icon"
        type="image/png"
        href={`${withPrefix("/")}files/favicon-16x16.png`}
        sizes="16x16"
      />

      <link
        rel="mask-icon"
        href={`${withPrefix("/")}files/safari-pinned-tab.svg`}
        color="#ff4400"
      />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      />
      <meta name="theme-color" content="#fff" />
      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content={pageName ? `${pageName} | ${title}` : `${title}`}
      />
      <meta name="og:description" content={description} />
      <meta property="og:url" content={link} />
      <meta
        property="og:image"
        content={`${useWithTrailingSlash(siteUrl)}files/og-image.jpg`}
      />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@re_sourcing" />
      {process.env.NODE_ENV === "production" && (
        <>
          <script
            onload="this.setAttribute('data-domain',window.location.host)"
            src={`${withPrefix("/")}js/script.js`}
            async
            defer
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
          window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }
          `,
            }}
          ></script>
        </>
      )}
    </Helmet>
  )
}

export default Head
