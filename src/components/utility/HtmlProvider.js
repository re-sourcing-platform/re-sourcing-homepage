import React from "react"
import showdown from "showdown"

const HtmlProvider = ({ className, markdown }) => {
  const converter = new showdown.Converter({ openLinksInNewWindow: true })
  return (
    <div
      className={className}
      dangerouslySetInnerHTML={{ __html: converter.makeHtml(markdown) }}
    />
  )
}

export default HtmlProvider
