import React from "react"
import { Link } from "gatsby"

const LinkProvider = ({ to, href, children, className, target, rel }) => {
  return (
    <>
      {to ? (
        <Link to={to} className={className}>
          {children}
        </Link>
      ) : (
        <a href={href} target={target} rel={rel} className={className}>
          {children}
        </a>
      )}
    </>
  )
}

export default LinkProvider
