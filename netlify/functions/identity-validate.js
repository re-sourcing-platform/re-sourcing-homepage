const allowedDomains = [
  "taltech.ee",
  "wu.ac.at",
  "minehutte.com",
  "oeko.de",
  "wrforum.org",
  "re-sourcing.eu",
  "unileoben.ac.at",
  "chillius.com",
]

const validateEmail = email => {
  var parts = email.split("@")
  if (parts.length === 2) {
    if (allowedDomains.includes(parts[1])) {
      console.log(`${email} is validated`)
      return true
    }
  }
  console.log(`${email} is not validated`)
  return false
}

exports.handler = async event => {
  console.log("Validate event triggered")
  const email = JSON.parse(event.body).user.email
  let isValidEmail = validateEmail(email)
  return { statusCode: isValidEmail ? 200 : 403 }
}
