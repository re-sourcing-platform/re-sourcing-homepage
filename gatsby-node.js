const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
// const remark = require('remark');
// const remarkHTML = require('remark-html');

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }

  // if (node.frontmatter.passwordCheckIntroText1) {
  //   const value1 = remark()
  //       .use(remarkHTML)
  //       .processSync(node.frontmatter.passwordCheckIntroText1)
  //       .toString();
  //
  //   createNodeField({
  //     name: 'passwordCheckIntroText1_html',
  //     node,
  //     value1
  //   });
  // }
  //
  // if (node.frontmatter.passwordCheckIntroText2) {
  //   const value2 = remark()
  //       .use(remarkHTML)
  //       .processSync(node.frontmatter.passwordCheckIntroText2)
  //       .toString();
  //
  //   createNodeField({
  //     name: 'passwordCheckIntroText2_html',
  //     node,
  //     value2
  //   });
  // }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  createPage({
    path: `/404/`,
    component: require.resolve(`./src/pages/404.js`),
  })

  const result = await graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              template
              title
              eventIsClosed
              showEventPage
              showResourcesPage
              assets {
                active
                file {
                  name
                  absolutePath
                  publicURL
                  extension
                }
                subtitle
                title
              }
            }
            html
          }
        }
      }
    }
  `)

  const pdfpages = await graphql(`
    {
      allFile(filter: { extension: { eq: "pdf" } }) {
        edges {
          node {
            name
            publicURL
          }
        }
      }
    }
  `)

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    if (node.frontmatter.template === "hub-event") {
      if (!node.frontmatter.eventIsClosed && node.frontmatter.showEventPage === true) {
        createPage({
          path: `${node.fields.slug}`,
          component: path.resolve(`./src/templates/hub/stream.js`),
          context: {
            // Data passed to context is available
            // in page queries as GraphQL variables.
            slug: node.fields.slug,
          }
        })
      }

      createPage({
        path: `${node.fields.slug}about`,
        component: path.resolve(`./src/templates/hub/hub-about.js`),
        context: {
          // Data passed to context is available
          // in page queries as GraphQL variables.
          slug: node.fields.slug,
        }
      })

      if (!node.frontmatter.eventIsClosed) {
        createPage({
          path: `${node.fields.slug}agenda-and-sessions`,
          component: path.resolve(`./src/templates/hub/agenda-and-sessions.js`),
          context: {
            slug: node.fields.slug,
          }
        })

        if (node.frontmatter.showResourcesPage === true) {
          createPage({
            path: `${node.fields.slug}resource-library`,
            component: path.resolve(`./src/templates/hub/resource-library.js`),
            context: {
              slug: node.fields.slug,
            }
          })
        }
      }
    } else if (node.frontmatter.template !== null) {
      if (node.frontmatter.title === "Existing Approaches") {
        createPage({
          path: `${node.fields.slug.slice(0, -1)}-view/`,
          component: path.resolve(`./src/templates/assets-page-clean.js`),
          context: {
            // Data passed to context is available
            // in page queries as GraphQL variables.
            slug: node.fields.slug,
          },
        })
        createPage({
          path: node.fields.slug,
          component: path.resolve(
            `./src/templates/${String(node.frontmatter.template)}.js`
          ),
          context: {
            // Data passed to context is available
            // in page queries as GraphQL variables.
            slug: node.fields.slug,
            currentDate: getCurrentDate(),
          },
        })
      } else {
        createPage({
          path: node.fields.slug,
          component: path.resolve(
            `./src/templates/${String(node.frontmatter.template)}.js`
          ),
          context: {
            // Data passed to context is available
            // in page queries as GraphQL variables.
            slug: node.fields.slug,
            currentDate: getCurrentDate(),
          },
        })
      }
    }

   
  })
  
  pdfpages.data.allFile.edges.forEach(({ node }) => {
    createPage({
      path: "reports/" + string_to_slug(node.name),
      component: path.resolve(`./src/templates/report-page.js`),
      context: {
        location: node.publicURL,
      },
    })
  })

}



const getCurrentDate = () => {
  const d = new Date()
  let month = (d.getMonth() + 1).toString()
  if (month.length < 2) {
    month = `0${month}`
  }
  let day = d.getDate().toString()
  if (day.length < 2) {
    day = `0${day}`
  }
  return `${d.getFullYear()}-${month}-${day}`
}

// Create schema type for optional fields.
exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type MarkdownRemarkFrontmatterDays implements Node {
      resourceFile: [Resource]
      agendaItem: [Agenda]
      body: String
      description: String
    }

    type Resource {
      title: String
      icon: String
    }

    type Agenda {
      title: String
      description: String
      hour: Int
      minute: Int
      endTime: String
      body: String
    }
  `
  createTypes(typeDefs)
}

function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, "") // trim
  str = str.toLowerCase()

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
  var to = "aaaaeeeeiiiioooouuuunc------"
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i))
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes

  return str
}
